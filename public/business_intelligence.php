<?php
/*
 * wx_access_token.php
 * 
 * get the weixin access token 
 * */

header("Content-type:text/html;charset=utf-8"); 

class BusinessIntelligence{
	
    const SERVICE_IP='35.238.224.170';
    const USERNAME='root';
    const USERPWD='compass2010';

	private static function connectDB() {
		$connect=new mysqli(self::SERVICE_IP,self::USERNAME,self::USERPWD, "business_intelligence");
		$connect->set_charset('utf8');
		if($connect->connect_error){
			die("连接失败: " . self::$connect->connect_error);
            $connect = null;
		}else{
// 			echo "连接成功";
		}
		return $connect;
	}
	
	private static function closeDB($connect){
		mysqli_close($connect);
	}

	/*
	* 向数据库写入从微信公众号后台抓取的数据
	* 返回 200 写入成功 300 已经存在同样的数据 400 写入失败
	*/
	public static function insertWeChatData($data){
		$feedback = 400;
		$currentDate = "";

        $connect = self::connectDB();
        if($connect==null){
            return "连接失败";
        }

        $nickname = $data->nickname;//公众号的昵称
        $username = $data->username;//公众号的用户名
        $totalArticleDatas = $data->totalArticleData;
        foreach ($totalArticleDatas as $row) {
            //获得图文的标题
            $articleTitle = $row -> title;
            $articleTitle = str_replace("\"","\\\"", $articleTitle);
            //获得图文的发布时间
            $publishDate = $row -> publish_date;
            $publishTimestamp = ($row -> publish_timestamp)/1000;
            //获得图文阅读人数
            $userReadPageCount = $row -> int_page_read_user;
            $mediaDataList = $row -> media_data_list;
            $shareUser =  $row -> share_user;
            $targetUser =  $row -> target_user;
            $shareCount =  $row -> share_count;
            $result = $connect->query('select 1 from 490_wechat_article_data where publish_date = "'.$publishDate.'" and title = "'.$articleTitle.'" and nickname="'.$nickname.'"');
            $isExist = $result->fetch_all();
            if(!$isExist)
            {
                $sql = " insert into `490_wechat_article_data` (`publish_date`,`publish_timestamp`,`username`,`nickname`,`title`,`int_page_read_user`,`share_user`,`target_user`,`share_count`) 
			    VALUES (\"".$publishDate."\",".$publishTimestamp.",\"".$username."\", \"".$nickname."\",  \"".$articleTitle."\",".$userReadPageCount.",\"".$shareUser."\",".$targetUser.",".$shareCount.")";
                mysqli_query($connect, $sql);

                foreach($mediaDataList as $mediaData) {
                    $name = $mediaData -> name;
                    $name = str_replace("\"","\\\"",$name);
                    $pv = $mediaData -> pv;
                    $uv = $mediaData -> uv;
                    $media_id = $mediaData -> id;
                    $sql = "INSERT INTO `490_wechat_article_media_list` (`publish_date`,`username`,`name`,`pv`,`uv`,`media_id`)
								VALUES (\"".$publishDate."\",'".$username."',\"".$name."\", ".$pv.", ".$uv.", '".$media_id."')";
//                    var_dump($sql);
                    if (mysqli_query($connect, $sql)) {
//             					echo "New record created successfully";
                    } else {
//             					echo "Error: " . $sql . "" . mysqli_error(self::$connect);
                    }
                }
            }
            
        }

        // 流量主 曝光量
        $costList = $data->costList;

        foreach ($costList as $row) {
            //获得图文的标题

            $date = $row -> date;
            $click_count = $row -> click_count;
            $exposure_count = $row -> exposure_count;
            $order_count = $row -> order_count;
            $total_fee = $row -> total_fee;
            $total_commission = $row -> total_commission;

            $result = $connect->query('select 1 from 490_wechat_gospel_data where publish_date = "'.$date.'" and user_name="'.$username.'"');

            $isExist = $result->fetch_all();
            if(!$isExist)
            {
                $sql = " insert into `490_wechat_gospel_data` (`publish_date`,`user_name`,`click_count`,`exposure_count`,`order_count`,`total_fee`,`total_commission`) 
			    VALUES (\"".$date."\",\"".$username."\", ".$click_count.",".$exposure_count.", ".$order_count.",".$total_fee.",".$total_commission.")";
                mysqli_query($connect, $sql);
            }
        }
        mysqli_close($connect);
        $feedback = 200;
		return $feedback;
	}

    /*
    * 向数据库写入企鹅号的信息
    * 返回 200 写入成功 300 已经存在同样的数据 400 写入失败
    */
    public static function insertPenguinData($data){
        $feedback = 400;
        $currentDate = "";

        $connect = self::connectDB();
        if($connect==null){
            return "连接失败";
        }

        $dataList = $data->data;
        $sql = "";
        foreach ($dataList as $row) {
            //获得图文的标题
            $date = $row->date;
            $accountName = $row->accountName;
            $vid = $row->vid;
            if($sql ==""){
                $sql  = ' (publish_date = "'.$date.'" and accountName = "'.$accountName.'" and vid="'.$vid.'") ';
            }
            else{
                $sql  = $sql.' or (publish_date = "'.$date.'" and accountName = "'.$accountName.'" and vid="'.$vid.'") ';
            }
        }

        $result = $connect->query(' select publish_date,accountName,vid from 490_penguin_video_gospel_data where  '.$sql);
        $existList = $result->fetch_all();
//        var_dump($isExistList);
        $sql = "";
        foreach ($dataList as $row) {
            //获得图文的标题
            $date = $row->date;
            $accountName = $row->accountName;
            $name = $row->name;
            $vid = $row->vid;
            $new_daily_play_pv = $row->new_daily_play_pv;
            $name = str_replace("\"","\\\"", $name);

            $isExist = false;
            foreach ($existList as $item){
                if($item[0] == $date && $item[1] == $accountName && $item[2] == $vid){
                    $isExist = true;
                    break;
                }
            }
            if($isExist == false)
            {
                $tempSql = " insert into `490_penguin_video_gospel_data` (`accountName`,`name`,`publish_date`,`vid`,`new_daily_play_pv`) 
			      VALUES (\"".$accountName."\",\"".$name."\",\"".$date."\", \"".$vid."\",".$new_daily_play_pv.")";
                if($sql == ""){
                    $sql = $tempSql;
                }
                else{
                    $sql = $sql." ; ".$tempSql;
                }
            }
        }
        if($sql!=""){
            $connect->multi_query($sql);
        };
        mysqli_close($connect);
        $feedback = 200;
        return $feedback;
    }

    /*
    * 向数据库写入企鹅号的信息
    * 返回 200 写入成功 300 已经存在同样的数据 400 写入失败
    */
    public static function insertPenguinJFilmData($data){
        $feedback = 400;
        $currentDate = "";
        $connect = self::connectDB();
        if($connect==null){
            return "连接失败";
        }

        $dataList = $data->data->list;
        $sql = "";
        foreach ($dataList as $row) {
            //获得图文的标题
            $date = $row->date;
            if($sql ==""){
                $sql  = ' publish_date = "'.$date.'" ';
            }
            else{
                $sql  = $sql.' or publish_date = "'.$date.'" ';
            }
        }

        $result = $connect->query(' select publish_date from 490_penguin_jfilm_date where  '.$sql);
        $existList = $result->fetch_all();

        $sql = "";
        foreach ($dataList as $row) {
            //获得图文的标题
            $date = $row->date;
            $new_daily_play_pv =  $row->new_daily_play_pv;

            $isExist = false;
            foreach ($existList as $item){
                if($item[0] == $date){
                    $isExist = true;
                    break;
                }
            }
            if($isExist == false)
            {
                $tempSql = " insert into `490_penguin_jfilm_date` (`publish_date`,`new_daily_play_pv`) 
			      VALUES (\"".$date."\",".$new_daily_play_pv.")";
                if($sql == ""){
                    $sql = $tempSql;
                }
                else{
                    $sql = $sql." ; ".$tempSql;
                }
            }
        }
       
        if($sql!=""){
            $connect->multi_query($sql);
        };
        mysqli_close($connect);
        $feedback = 200;
        return $feedback;
    }

}
 
// echo '$_POST接收:<br/>'; 
// print_r($_POST); 
// echo json_decode($_POST);
// json_encode($array, JSON_UNESCAPED_UNICODE);
// echo '<hr/>'; 
// echo $_POST;
// $postData = json_decode($_POST["user"]);
// echo json_decode($_POST);

$postData  = json_decode($_POST['user']);
// $postData  = str_replace("&","",$postData);
//$postData  = json_decode('{"type":"jfilm","data":{"list":[{"vid":"","mediaid":"8312634","new_daily_play_pv":584,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"80","qzone_play_pv":"83","o_other_play_pv":421,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-31"},{"vid":"","mediaid":"8312634","new_daily_play_pv":835,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"89","qzone_play_pv":"111","o_other_play_pv":635,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-30"},{"vid":"","mediaid":"8312634","new_daily_play_pv":385,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"92","qzone_play_pv":"18","o_other_play_pv":275,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-29"},{"vid":"","mediaid":"8312634","new_daily_play_pv":476,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"85","qzone_play_pv":"12","o_other_play_pv":379,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-28"},{"vid":"","mediaid":"8312634","new_daily_play_pv":878,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"125","qzone_play_pv":"18","o_other_play_pv":735,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-27"},{"vid":"","mediaid":"8312634","new_daily_play_pv":823,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"79","qzone_play_pv":"9","o_other_play_pv":735,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-26"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1386,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"1","live_play_pv":"78","qzone_play_pv":"18","o_other_play_pv":1289,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-25"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1255,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"90","qzone_play_pv":"11","o_other_play_pv":1154,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-24"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1397,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"78","qzone_play_pv":"10","o_other_play_pv":1309,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-23"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1785,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"79","qzone_play_pv":"15","o_other_play_pv":1690,"kandian_play_pv":"1","qb_play_pv":"0","date":"2018-07-22"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1199,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"3","inews_play_pv":"1","live_play_pv":"66","qzone_play_pv":"24","o_other_play_pv":1105,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-21"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1056,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"2","live_play_pv":"57","qzone_play_pv":"28","o_other_play_pv":968,"kandian_play_pv":"1","qb_play_pv":"0","date":"2018-07-20"},{"vid":"","mediaid":"8312634","new_daily_play_pv":824,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"1","inews_play_pv":"0","live_play_pv":"70","qzone_play_pv":"8","o_other_play_pv":745,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-19"},{"vid":"","mediaid":"8312634","new_daily_play_pv":825,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"0","live_play_pv":"73","qzone_play_pv":"6","o_other_play_pv":746,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-18"},{"vid":"","mediaid":"8312634","new_daily_play_pv":966,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"0","inews_play_pv":"1","live_play_pv":"72","qzone_play_pv":"13","o_other_play_pv":880,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-17"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1283,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"1","inews_play_pv":"1","live_play_pv":"93","qzone_play_pv":"41","o_other_play_pv":1147,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-16"},{"vid":"","mediaid":"8312634","new_daily_play_pv":5054,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"1","inews_play_pv":"1","live_play_pv":"89","qzone_play_pv":"23","o_other_play_pv":4940,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-15"},{"vid":"","mediaid":"8312634","new_daily_play_pv":545,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"5","inews_play_pv":"0","live_play_pv":"98","qzone_play_pv":"8","o_other_play_pv":433,"kandian_play_pv":"1","qb_play_pv":"0","date":"2018-07-14"},{"vid":"","mediaid":"8312634","new_daily_play_pv":604,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"8","inews_play_pv":"0","live_play_pv":"76","qzone_play_pv":"14","o_other_play_pv":499,"kandian_play_pv":"7","qb_play_pv":"0","date":"2018-07-13"},{"vid":"","mediaid":"8312634","new_daily_play_pv":739,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"19","inews_play_pv":"0","live_play_pv":"115","qzone_play_pv":"14","o_other_play_pv":591,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-12"},{"vid":"","mediaid":"8312634","new_daily_play_pv":644,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"14","inews_play_pv":"0","live_play_pv":"104","qzone_play_pv":"30","o_other_play_pv":496,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-11"},{"vid":"","mediaid":"8312634","new_daily_play_pv":789,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"5","inews_play_pv":"2","live_play_pv":"86","qzone_play_pv":"13","o_other_play_pv":683,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-10"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1268,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"1","inews_play_pv":"0","live_play_pv":"97","qzone_play_pv":"23","o_other_play_pv":1147,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-09"},{"vid":"","mediaid":"8312634","new_daily_play_pv":2489,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"2","inews_play_pv":"0","live_play_pv":"109","qzone_play_pv":"19","o_other_play_pv":2359,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-08"},{"vid":"","mediaid":"8312634","new_daily_play_pv":752,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"2","inews_play_pv":"0","live_play_pv":"101","qzone_play_pv":"33","o_other_play_pv":611,"kandian_play_pv":"5","qb_play_pv":"0","date":"2018-07-07"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1416,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"1","inews_play_pv":"0","live_play_pv":"79","qzone_play_pv":"18","o_other_play_pv":1318,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-06"},{"vid":"","mediaid":"8312634","new_daily_play_pv":2441,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"4","inews_play_pv":"0","live_play_pv":"113","qzone_play_pv":"23","o_other_play_pv":2301,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-05"},{"vid":"","mediaid":"8312634","new_daily_play_pv":749,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"2","inews_play_pv":"0","live_play_pv":"68","qzone_play_pv":"60","o_other_play_pv":604,"kandian_play_pv":"15","qb_play_pv":"0","date":"2018-07-04"},{"vid":"","mediaid":"8312634","new_daily_play_pv":795,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"5","inews_play_pv":"0","live_play_pv":"78","qzone_play_pv":"28","o_other_play_pv":684,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-03"},{"vid":"","mediaid":"8312634","new_daily_play_pv":1165,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"5","inews_play_pv":"0","live_play_pv":"99","qzone_play_pv":"8","o_other_play_pv":1053,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-02"},{"vid":"","mediaid":"8312634","new_daily_play_pv":2075,"inews_kuaibao_play_pv":0,"kuaibao_play_pv":"3","inews_play_pv":"1","live_play_pv":"92","qzone_play_pv":"23","o_other_play_pv":1956,"kandian_play_pv":"0","qb_play_pv":"0","date":"2018-07-01"}],"total":31}}');

if($postData -> type == "weixin" || $postData -> type == "penguin" || $postData -> type == "jfilm") {
// 	BusinessIntelligence::insertData();
    $code = 400;
    if($postData -> type == "weixin") {
        $code = BusinessIntelligence::insertWeChatData($postData);
    }
    else if($postData -> type == "penguin"){
        $code = BusinessIntelligence::insertPenguinData($postData);
    }
    else if($postData -> type == "jfilm"){
        $code = BusinessIntelligence::insertPenguinJFilmData($postData);
    }

	if($code == 400) {
		$message = "数据提交失败";
	}else if($code == 300) {
		$message = "数据已经存在，请勿重复操作。";
	}else{
		$code = 200;
		$message = "数据提交成功";
	}

	$result = array(
    	'code' => $code,
    	'message' => $message
	);

	echo json_encode($result);
}

?>