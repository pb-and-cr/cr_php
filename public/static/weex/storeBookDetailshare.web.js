// { "framework": "Vue" }

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 202);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  scopeId,
  cssModules
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  // inject cssModules
  if (cssModules) {
    var computed = options.computed || (options.computed = {})
    Object.keys(cssModules).forEach(function (key) {
      var module = cssModules[key]
      computed[key] = function () { return module }
    })
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(127)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.paramsToString = paramsToString;
exports.jump = jump;
exports.back = back;
exports.getWithParameter = getWithParameter;
exports.jumpSubPage = jumpSubPage;

var _util = __webpack_require__(36);

var _util2 = _interopRequireDefault(_util);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getNavigator() {
    var nav = weex.requireModule('navigator');
    return nav;
};

function paramsToString(obj) {
    var param = "";
    for (var name in obj) {
        param += "&" + name + "=" + encodeURI(obj[name]);
    }
    return param.substring(1);
};

function getUrl(url, jsFile) {
    var bundleUrl = url;
    var host = '';
    var path = '';
    var nativeBase = '';
    var isWebAssets = bundleUrl.indexOf('http://') >= 0;
    var isAndroidAssets = bundleUrl.indexOf('file://assets/') >= 0;
    var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('CReader.app') > 0;
    if (isAndroidAssets) {
        nativeBase = 'file://assets/';
    } else if (isiOSAssets) {
        // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
        // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
        nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
    } else {
        var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
        var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
        if (matches && matches.length >= 2) {
            host = matches[1];
        }
        if (matchFirstPath && matchFirstPath.length >= 2) {
            path = matchFirstPath[1];
        }
        nativeBase = 'http://' + host + '/';
    }
    var h5Base = './index.html?page=';
    // in Native
    var base = nativeBase;
    if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
        // check if in weexpack project
        if (path === 'web' || path === 'dist') {
            base = h5Base + '/dist/';
        } else {
            base = h5Base + '';
        }
    } else {
        base = nativeBase + (!!path ? path + '/' : '');
    }

    var newUrl = base + jsFile;
    return newUrl;
}
// 跳转到新的url地址
function jump(self, url) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html";
    } else {

        var path = self.$getConfig().bundleUrl;
        path = _util2.default.setWXBundleUrl(path, url + ".js");
        getNavigator().push({
            url: url,
            animated: "true"
        });
    }
}

function back(self) {
    if (WXEnvironment.platform == 'Web') {
        window.history.go(-1);
    } else {
        getNavigator().pop({
            animated: "true"
        });
    }
}

function getWithParameter(self, url, parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html?phantom_limb=true&" + paramsToString(parameter);
    } else {
        var path = self.$getConfig().bundleUrl;
        path = getUrl(path, url + ".js?" + paramsToString(parameter));
        var event = weex.requireModule('event');
        event.openURL(path);
    }
}

function jumpSubPage(self, url, parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html?phantom_limb=true&" + paramsToString(parameter);
    } else {
        var path = self.$getConfig().bundleUrl;
        path = getUrl(path, url + ".js?isSubPage&" + paramsToString(parameter));
        var event = weex.requireModule('event');
        console.log(paramsToString(parameter));
        event.openURL(path);
    }
}

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var prompt = {
    getModel: function getModel() {
        return weex.requireModule('modal');
    },
    toast: function toast(info) {
        this.getModel().toast({ message: info, duration: 0.3 });
    },
    alert: function alert(info) {
        this.getModel().alert({ message: info, duration: 0.3 });
    }
};

exports.default = prompt;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getImgPath = getImgPath;
exports.globalEvent = globalEvent;
exports.checkNetStatusOnOrOff = checkNetStatusOnOrOff;

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// 获取图片在三端上不同的路径
// e.g. 图片文件名是 test.jpg, 转换得到的图片地址为
// - H5      : http: //localhost:1337/src/images/test.jpg
// - Android : local:///test
// - iOS     : ../images/test.jpg
function getImgPath(img_name) {
    var bundleUrl = weex.config.bundleUrl;
    var platform = weex.config.env.platform.toLowerCase();
    var img_path = '';

    if (platform == 'android') {
        // android 不需要后缀
        img_name = img_name.substr(0, img_name.lastIndexOf('.'));
        img_path = 'local:///' + img_name;
    } else if (platform == 'ios') {
        // img_path =bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1)+`images/${img_name}`
        img_path = 'local:///bundlejs/images/' + img_name;
        console.log(img_path);
    } else {
        img_path = '/assets/images/' + img_name;
    }

    return img_path;
};

function globalEvent() {
    var globalEvent = weex.requireModule('globalEvent');
    globalEvent.addEventListener("geolocation", function (e) {
        console.log("get geolocation");
    });
}
function checkNetStatusOnOrOff() {
    var event = weex.requireModule('event');
    var OnNetWork = 0; //0表示没网，1表示有网
    try {
        if (typeof event != "undefined" && typeof event.isNetworkAvailable == "function") {
            OnNetWork = event.isNetworkAvailable();
        }
    } catch (e) {
        console.log(e);
        _prompt2.default.toast(e);
    }
    return OnNetWork;
}

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var environment = {
  language: "zh" //改变这里的值。来切换中英文
};

var langFiles = {
  en: __webpack_require__(61).lang,
  zh: __webpack_require__(63).lang,
  zh_tw: __webpack_require__(62).lang
};

var already = false;

var global = {

  init: function init() {
    if (already == true) {
      return;
    }
    console.log(" global init ");
    var platform = weex.config.env.platform;
    var lang = "zh";
    try {
      var deviceInfo = weex.requireModule('deviceInfo');
      if (typeof deviceInfo != "undefined" && typeof deviceInfo.GetLanguage == "function") {
        lang = deviceInfo.GetLanguage().toLocaleLowerCase();
      }
    } catch (e) {
      console.log(" lang " + e);
    }

    if (platform.toLocaleLowerCase() == "ios") {
      // # ios #
      // 中文简体：zh-Hans
      // 英文：en
      // 中文繁体：zh-Hant
      // 印地语：hi
      // 西班牙语：es
      // 墨西哥西班牙语：es-MX
      // 拉丁美洲西班牙语：es-419
      if (lang.indexOf("zh") >= 0) {
        if (lang.indexOf("hans") >= 0) {
          environment.language = "zh";
        } else {
          environment.language = "zh_tw";
        }
      } else if (lang.indexOf("en") >= 0) {
        environment.language = "en";
      } else if (lang.indexOf("es") >= 0) {
        environment.language = "en";
      } else {
        environment.language = "zh";
      }
    } else {
      if (lang.indexOf("zh_cn") >= 0 || lang.indexOf("zh_cn") >= 0) {
        environment.language = "zh";
      } else if (lang.indexOf("zh_") >= 0) {
        environment.language = "zh_tw";
      } else if (lang.indexOf("cn") >= 0 || lang.indexOf("cn") >= 0) {
        environment.language = "zh";
      } else if (lang.indexOf("en") >= 0) {
        environment.language = "en";
      } else {
        environment.language = "zh";
      }
    }
  },
  lang: function lang(key) {
    return "";
  },
  display: function display(key) {
    if (already == false) {
      this.init();
    }
    console.log(" already " + already);
    if (environment.language == "zh") {
      return langFiles.zh[key] || "not find";
    } else if (environment.language == "en") {
      return langFiles.en[key] || "not find";
    } else if (environment.language == "zh-tw") {
      return langFiles.zh_tw[key] || "not find";
    }
    return key;
  },
  getLanguage: function getLanguage() {
    if (already == false) {
      this.init();
    }
    return environment.language;
  }
};

exports.default = global;
//
// setTimeout(function(){
//     exports.lang = function(key) {
//         if (environment.language == "zh") {
//             return langFiles.zh[key] || "not find";
//         }
//         else if (environment.language == "en") {
//             return langFiles.en[key] || "not find";
//         }
//         else if (environment.language == "zh-tw") {
//             return langFiles.zh_tw[key] || "not find";
//         }
//         return key;
//     }
// },100);

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by vincent on 2017/8/27.
 * 使用杜威的十进制来进行数据分析和处理
 */

var googleTrack = {

    USER_BEHAVIOR: 'User Behavior',

    USER_BEHAVIOR_HOME: '100 - home', // 主页
    USER_BEHAVIOR_ALBUM: '110 - album',
    USER_BEHAVIOR_ARTICLE: '120 - article', // 文章点击

    USER_BEHAVIOR_LIBRARY: '200 - library', // 图书馆
    USER_BEHAVIOR_LIBRARY_BOOKSHELF: '210 - bookshelf', // 图书馆-书架
    USER_BEHAVIOR_LIBRARY_CATEGORY: '220 - book category', // 图书馆-分类
    USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION: '221 - book introduction', // 图书馆-书籍介绍
    USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD: '222 - book add to bookshelf', // 图书馆-分类
    USER_BEHAVIOR_LIBRARY_BOOKLIST: '230 - book list', // 图书馆-书单
    USER_BEHAVIOR_LIBRARY_BOOKSHOP: '240 - shop', // 图书馆-书店
    USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE: '241 - shop more', // 图书馆-书店
    USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL: '242 - shop book show', // 图书馆-书店-书的详情介绍
    USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY: '243 - shop buy', // 图书馆-书店-去购买
    USER_BEHAVIOR_LIBRARY_SEARCH: '250 - search', // 图书馆-搜索

    USER_BEHAVIOR_FOLLOW: '300 - follow', // 订阅
    USER_BEHAVIOR_CHAT: '400 - chat', // 答疑
    USER_BEHAVIOR_ME: '500 - me', // 我
    USER_BEHAVIOR_USER: '510 - login or reg', // 用户注册和登录
    USER_BEHAVIOR_USER_REGBYMAIL: 'reg by mail', // 用户邮件注册
    USER_BEHAVIOR_USER_REGBYPHONE: 'reg by phone', // 用户手机注册

    USER_BEHAVIOR_USER_OTHER: '520 - other', // 其他信息
    USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION: 'subscription', // 订阅
    USER_BEHAVIOR_USER_OTHER_FEEDBACK: 'feedback', // 其他信息
    USER_BEHAVIOR_USER_OTHER_ARTICLE: 'artilce', // 收藏
    USER_BEHAVIOR_USER_OTHER_FRIDEND: 'share to friend', // 分享给朋友

    //用户点击事件跟踪
    userAction: function userAction(action, label, id) {
        var event = weex.requireModule('event');
        if (typeof event.googleTrack != 'undefined') {
            event.googleTrack(this.USER_BEHAVIOR, action, label, id);
        }
        console.log(action + " -- " + label + " -- " + id);
    },
    userHome: function userHome() {
        this.userAction(this.USER_BEHAVIOR_HOME, "", 0);
    },
    userLibrary: function userLibrary() {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF, "", 0);
    },

    //订阅
    userFollow: function userFollow() {
        this.userAction(this.USER_BEHAVIOR_FOLLOW, "", 0);
    },

    //用户点击专栏
    userOpenAlbum: function userOpenAlbum(albumName, albumId) {
        this.userAction(this.USER_BEHAVIOR_ALBUM, albumName, albumId);
    },

    //用户点击文章次数
    userOpenArticle: function userOpenArticle(title, id) {
        this.userAction(this.USER_BEHAVIOR_ARTICLE, title, id);
    },

    //点击阅读书
    userLibraryOpenBook: function userLibraryOpenBook(title) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF, title, 0);
    },

    //点击书籍分类
    userLibraryOpenCategory: function userLibraryOpenCategory(categoryName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_CATEGORY, categoryName, 0);
    },

    //点击书籍分类
    userOpenBookIntroduction: function userOpenBookIntroduction(bookName, bookId) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION, bookName, bookId);
    },

    //点击书籍分类
    userOpenBookIntroductionAdd: function userOpenBookIntroductionAdd(bookName, bookId) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD, bookName, bookId);
    },

    //点击书单
    userLibraryOpenBookList: function userLibraryOpenBookList() {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST, '', 0);
    },

    //点击书单
    userLibraryOpenBookListByName: function userLibraryOpenBookListByName(name) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST, name, 0);
    },

    //点击书店导航
    userLibraryOpenBookshop: function userLibraryOpenBookshop() {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP, "", 0);
    },

    //点击书店中的更多
    userLibraryOpenBookshopMore: function userLibraryOpenBookshopMore(categoryName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE, categoryName, 0);
    },

    //点击书店中某本书
    userLibraryOpenBookshopDetails: function userLibraryOpenBookshopDetails(bookName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL, bookName, 0);
    },

    //点击书店中去购买
    userLibraryOpenBookshopBuy: function userLibraryOpenBookshopBuy(bookName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY, bookName, 0);
    },

    //点击搜索
    userSearch: function userSearch(keywords) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_SEARCH, keywords, 0);
    },

    //点击答疑
    userChat: function userChat() {
        this.userAction(this.USER_BEHAVIOR_CHAT, "", 0);
    },

    //点击Me
    userMe: function userMe() {
        this.userAction(this.USER_BEHAVIOR_ME, "", 0);
    },

    //用户点击登录或注册按钮
    userLoginOrReg: function userLoginOrReg() {
        this.userAction(this.USER_BEHAVIOR_USER, "", 0);
    },

    //用户用邮件注册
    userRegByMail: function userRegByMail() {
        this.userAction(this.USER_BEHAVIOR_USER, this.USER_BEHAVIOR_USER_REGBYMAIL, 0);
    },

    //用户用手机注册
    userRegByPhone: function userRegByPhone() {
        this.userAction(this.USER_BEHAVIOR_USER, this.USER_BEHAVIOR_USER_REGBYPHONE, 0);
    },

    //订阅
    userOtherSubscription: function userOtherSubscription() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION, 0);
    },

    //收藏
    userOtherArticle: function userOtherArticle() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_ARTICLE, 0);
    },

    //其他
    userOtherFeedback: function userOtherFeedback() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_FEEDBACK, 0);
    },
    userOtherShareToFriend: function userOtherShareToFriend() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_FRIDEND, 0);
    }
};

exports.default = googleTrack;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getBookShelf = getBookShelf;
exports.saveBookShelf = saveBookShelf;
exports.openBook = openBook;
exports.getBookListByService = getBookListByService;
exports.getArticleListInfo = getArticleListInfo;
exports.getSubscribeInfo = getSubscribeInfo;
exports.getAlbumInfoById = getAlbumInfoById;
exports.getBooks = getBooks;
exports.getBookProfile = getBookProfile;
exports.userAddBookToShelf = userAddBookToShelf;
exports.userRemoveBookToShelf = userRemoveBookToShelf;
exports.serchBookName = serchBookName;
exports.userAddAticleToLove = userAddAticleToLove;
exports.userRemoveAticleFromLove = userRemoveAticleFromLove;
exports.isArticleInLove = isArticleInLove;
exports.userGetLovedArticle = userGetLovedArticle;
exports.getSuggestBookLists = getSuggestBookLists;
exports.getBookListsInfo = getBookListsInfo;
exports.getPersonalData = getPersonalData;
exports.getFollowColumnList = getFollowColumnList;
exports.getNotsubscribedColumnInfo = getNotsubscribedColumnInfo;
exports.userFollow = userFollow;
exports.userRemoveColumn = userRemoveColumn;
exports.getColumnInfo = getColumnInfo;
exports.getBookList = getBookList;
exports.getBookListByTag = getBookListByTag;

var _fetch = __webpack_require__(12);

var _user = __webpack_require__(11);

var _storage = __webpack_require__(14);

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(13);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _googleTrack = __webpack_require__(7);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BOOK_SHELF = 'BookShelf';

//用户获取书架信息
function getBookShelf() {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    return (0, _storage.storageGetItem)(BOOK_SHELF).then(function (res) {
        // console.log(res.data);
        if (res.result == "failed") {
            return [];
        } else {
            var data = JSON.parse(res.data);
            var len = data.length;
            for (var i = 0; i < len; i++) {
                data[i]['progress'] = { "visible": false, "value": 0 };
            }
            console.log(data);
            return data;
        }
    });
}

//用户获取书架信息
function saveBookShelf(bookList) {
    //登陆成功后返回token，和user_id
    return (0, _storage.storageSetItem)(BOOK_SHELF, JSON.stringify(bookList));
}

function openBook(bookInfo) {}

//从服务器端同步数据到本地
function getBookListByService(bookList, pageIndex) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/getBooksOnShelf', { 'user_id': userId, "page_index": pageIndex });
    });
}

function getArticleListInfo(pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/index', { 'openId': openId, 'pageIndex': pageIndex });
    });
}
function getSubscribeInfo() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/index', { 'openId': openId });
    });
    // return get('/api/follow/v1/index', { 'openId': openId}, fun);
}
function getAlbumInfoById(id, pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/album', { 'openId': openId, 'pageIndex': pageIndex, 'id': id });
    });
    // return get('/api/Article/v1/album', { 'openId': openId, 'id': id,'pageIndex':pageIndex}, fun);
}
//获取图书信息
function getBooks(id, pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBooksTags', { 'openId': openId, 'id': id, 'pageIndex': pageIndex });
    });
    // return get('/api/Books/getBooksTags',{'openId': openId,'id': id,'pageIndex':pageIndex},fun);
}
//获取书籍信息
function getBookProfile(id) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBookProfile', { 'openId': openId, 'id': id });
    });
    // return get('/api/Books/getBookProfile',{'openId': openId,'id': id,},fun);
}
//用户添加书籍到书架
function userAddBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_id = 7245;
    //type = 1 表示书籍
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/addBookToShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 });
    });
    // return get('/api/Books/addBookToShelf', { 'book_id': bookId, 'user_id': user_id}, fun);
}
//用户从书架移出书籍
function userRemoveBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_token = 7245;
    // return get('/api/Books/removeBookFromShelf', { 'book_id': bookId, 'user_token': user_token}, fun);
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/removeBookFromShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 });
    });
}
//用户搜索图书
function serchBookName(name, pageIndex) {
    return (0, _fetch.get)('/api/Books/searchBook', { 'title': name, 'pageIndex': pageIndex });
}
//用户添加文章到我的收藏
function userAddAticleToLove(articleId) {
    //type = 2 表示文章
    return (0, _user.getUserId)().then(function (userId) {
        if (userId == -1) {
            return -1;
        } else {
            return (0, _fetch.get)('/api/Article/addArticleToLove', { 'article_book_id': articleId, 'user_id': userId, 'type': 2 });
        }
    });
}
//用户移出文章到我的收藏
function userRemoveAticleFromLove(articleId) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/removeArticleFromLove', { 'article_book_id': articleId, 'user_id': userId });
    });
}
function isArticleInLove(articleId) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/isArticleInLove', { 'article_book_id': articleId, 'user_id': userId });
    });
}
//用户获取收藏的文章信息
function userGetLovedArticle(pageIndex) {
    //type = 2 表示文章
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/articleInLove', { 'user_id': userId, 'pageIndex': pageIndex, 'type': 2 });
    });
}

//获取书单
function getSuggestBookLists() {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getSuggestBookLists', { 'openId': openId });
    });
    // return get('/api/Books/getSuggestBookLists',{'openId': openId},fun);
}
//获取书单下的详细书本信息
function getBookListsInfo(bookList_id) {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBookListsInfo', { 'openId': openId, 'bookList_id': bookList_id });
    });
    // return get('/api/Books/getBookListsInfo',{'openId': openId,'bookList_id':bookList_id},fun);
}
//用户获取个人信息
function getPersonalData() {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/ChristianReaderUser/getPersonalData', { 'openId': openId }).then(function (res) {
            if (res.data.status == 1) {
                (0, _storage.storageSetItem)(res.data.data);
            } else {
                (0, _storage.storageSetItem)(res.data.data);
            }
            return res;
        });
    });
    // return getPromise('/api/ChristianReaderUser/getPersonalData',{'openId': openId});
    // return get('/api/ChristianReaderUser/getPersonalData',{'openId': openId},fun);
}

//获得用户已经订阅的专栏信息：
function getFollowColumnList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/followColumnInfo', { 'openId': openId });
    });
    // return get('/api/follow/v1/followColumnInfo', { 'openId': openId}, fun);
}
//获得用户未订阅的专栏信息：
function getNotsubscribedColumnInfo() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/notsubscribedList', { 'openId': openId });
    });
    // return get('/api/follow/v1/notsubscribedList', { 'openId': openId}, fun);
}
//用户订阅某个专栏
function userFollow(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId });
    });
    // syncAblumInfo.follow(columnId);
    // return get('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId}, fun);
}
//用户取消某个专栏的订阅：
function userRemoveColumn(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId });
    });
    // syncAblumInfo.cancel(columnId);
    // return get('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId}, fun);
}
//根据专栏id获得专栏详细信息
function getColumnInfo(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId });
    });
    // return get('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
function getBookList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/books/getBooksInShop', { 'openId': openId });
    });
    // return get('/api/books/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
function getBookListByTag(tagid) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/books/getBooksInShopByTag', { 'openId': openId, 'tag_id': tagid });
    });
    // return get('/api/books/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(5);

exports.default = {
    tabItems: [{
        index: 0,
        title: "主页",
        titleColor: '#666666',
        selectedColor: '#e72c27',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        image: (0, _common.getImgPath)("home.png"),
        selectedImage: (0, _common.getImgPath)('homeselected.png')
    }, {
        index: 1,
        title: "图书馆",
        titleColor: '#666666',
        selectedColor: '#e72c27',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        image: (0, _common.getImgPath)("library.png"),
        selectedImage: (0, _common.getImgPath)("libraryselected.png")
    }, {
        index: 2,
        title: "订阅",
        titleColor: '#666666',
        selectedColor: '#e72c27',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        image: (0, _common.getImgPath)("subscribe.png"),
        selectedImage: (0, _common.getImgPath)("subscribeselected.png")
    }, {
        index: 3,
        title: "答疑",
        titleColor: '#666666',
        selectedColor: '#e72c27',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        image: (0, _common.getImgPath)("chat.png"),
        selectedImage: (0, _common.getImgPath)("chatselected.png")
    }, {
        index: 4,
        title: "我",
        titleColor: '#666666',
        selectedColor: '#e72c27',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        image: (0, _common.getImgPath)("me1.png"),
        selectedImage: (0, _common.getImgPath)("meselected.png")
    }],
    bookstoremessage: { message: "来自合作伙伴的精选书籍推荐给您" },
    bottommessage: { message: "没有更多了!" }

    // 正常模式的tab title配置
    // tabTitles: [
    //   {
    //     title: '主页',
    //     icon: getImgPath("home.png"),
    //     activeIcon: getImgPath('homeselected.png'),
    //   },
    //   {
    //     title: '图书馆',
    //     icon: getImgPath("library.png"),
    //     activeIcon: getImgPath("libraryselected.png")
    //   },
    //   {
    //     title: '订阅',
    //     icon: getImgPath("subscribe.png"),
    //     activeIcon: getImgPath("subscribeselected.png")
    //   },
    //   {
    //     title: '答疑',
    //     icon: getImgPath("chat.png"),
    //     activeIcon: getImgPath("chatselected.png")
    //   },
    //   {
    //     title: '我',
    //     icon: getImgPath("me1.png"),
    //     activeIcon: getImgPath("meselected.png")
    //   }
    // ],
    // tabStyles: {
    //   bgColor: '#FFFFFF',
    //   titleColor: '#666666',
    //   activeTitleColor: '#e72c27',
    //   activeBgColor: '#ffffff',
    //   isActiveTitleBold: true,
    //   iconWidth: 60,
    //   iconHeight: 60,
    //   width: 160,
    //   height: 120,
    //   BottomColor: '#FFC900',
    //   BottomWidth: 0,
    //   BottomHeight: 0,
    //   activeBottomColor: 'green',
    //   activeBottomWidth: 160,
    //   activeBottomHeight: 6,
    //   fontSize: 24,
    //   textPaddingLeft: 10,
    //   textPaddingRight: 10
    // },
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//import UrlParser from 'url-parse';

var Utils = {
  //UrlParser: UrlParser,
  _typeof: function _typeof(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
  },
  isPlainObject: function isPlainObject(obj) {
    return Utils._typeof(obj) === 'object';
  },
  isString: function isString(obj) {
    return typeof obj === 'string';
  },
  isNonEmptyArray: function isNonEmptyArray() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    return obj && obj.length > 0 && Array.isArray(obj) && typeof obj !== 'undefined';
  },
  isObject: function isObject(item) {
    return item && (typeof item === 'undefined' ? 'undefined' : _typeof2(item)) === 'object' && !Array.isArray(item);
  },
  isEmptyObject: function isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  },
  decodeIconFont: function decodeIconFont(text) {
    // 正则匹配 图标和文字混排 eg: 我去上学校&#xe600;,天天不&#xe600;迟到
    var regExp = /&#x[a-z]\d{3,4};?/;
    if (regExp.test(text)) {
      return text.replace(new RegExp(regExp, 'g'), function (iconText) {
        var replace = iconText.replace(/&#x/, '0x').replace(/;$/, '');
        return String.fromCharCode(replace);
      });
    } else {
      return text;
    }
  },
  mergeDeep: function mergeDeep(target) {
    for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      sources[_key - 1] = arguments[_key];
    }

    if (!sources.length) return target;
    var source = sources.shift();
    if (Utils.isObject(target) && Utils.isObject(source)) {
      for (var key in source) {
        if (Utils.isObject(source[key])) {
          if (!target[key]) {
            Object.assign(target, _defineProperty({}, key, {}));
          }
          Utils.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, _defineProperty({}, key, source[key]));
        }
      }
    }
    return Utils.mergeDeep.apply(Utils, [target].concat(sources));
  },
  appendProtocol: function appendProtocol(url) {
    if (/^\/\//.test(url)) {
      var bundleUrl = weex.config.bundleUrl;

      return 'http' + (/^https:/.test(bundleUrl) ? 's' : '') + ':' + url;
    }
    return url;
  },
  encodeURLParams: function encodeURLParams(url) {
    var parsedUrl = new UrlParser(url, true);
    return parsedUrl.toString();
  },
  goToH5Page: function goToH5Page(jumpUrl) {
    var animated = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    var Navigator = weex.requireModule('navigator');
    var jumpUrlObj = new Utils.UrlParser(jumpUrl, true);
    var url = Utils.appendProtocol(jumpUrlObj.toString());
    Navigator.push({
      url: Utils.encodeURLParams(url),
      animated: animated.toString()
    }, callback);
  },

  env: {
    isTaobao: function isTaobao() {
      var appName = weex.config.env.appName;

      return (/(tb|taobao|淘宝)/i.test(appName)
      );
    },
    isTrip: function isTrip() {
      var appName = weex.config.env.appName;

      return appName === 'LX';
    },
    isBoat: function isBoat() {
      var appName = weex.config.env.appName;

      return appName === 'Boat' || appName === 'BoatPlayground';
    },
    isWeb: function isWeb() {
      var platform = weex.config.env.platform;

      return (typeof window === 'undefined' ? 'undefined' : _typeof2(window)) === 'object' && platform.toLowerCase() === 'web';
    },
    isIOS: function isIOS() {
      var platform = weex.config.env.platform;

      return platform.toLowerCase() === 'ios';
    },

    /**
     * 是否为 iPhone X
     * @returns {boolean}
     */
    isIPhoneX: function isIPhoneX() {
      var deviceHeight = weex.config.env.deviceHeight;

      if (Utils.env.isWeb()) {
        return (typeof window === 'undefined' ? 'undefined' : _typeof2(window)) !== undefined && window.screen && window.screen.width && window.screen.height && parseInt(window.screen.width, 10) === 375 && parseInt(window.screen.height, 10) === 812;
      }
      return Utils.env.isIOS() && deviceHeight === 2436;
    },
    isAndroid: function isAndroid() {
      var platform = weex.config.env.platform;

      return platform.toLowerCase() === 'android';
    },
    isAlipay: function isAlipay() {
      var appName = weex.config.env.appName;

      return appName === 'AP';
    },
    isTmall: function isTmall() {
      var appName = weex.config.env.appName;

      return (/(tm|tmall|天猫)/i.test(appName)
      );
    },
    isAliWeex: function isAliWeex() {
      return Utils.env.isTmall() || Utils.env.isTrip() || Utils.env.isTaobao();
    },
    supportsEB: function supportsEB() {
      var weexVersion = weex.config.env.weexVersion || '0';
      var isHighWeex = Utils.compareVersion(weexVersion, '0.10.1.4') && (Utils.env.isIOS() || Utils.env.isAndroid());
      var expressionBinding = weex.requireModule('expressionBinding');
      return expressionBinding && expressionBinding.enableBinding && isHighWeex;
    },


    /**
     * 判断Android容器是否支持是否支持expressionBinding(处理方式很不一致)
     * @returns {boolean}
     */
    supportsEBForAndroid: function supportsEBForAndroid() {
      return Utils.env.isAndroid() && Utils.env.supportsEB();
    },


    /**
     * 判断IOS容器是否支持是否支持expressionBinding
     * @returns {boolean}
     */
    supportsEBForIos: function supportsEBForIos() {
      return Utils.env.isIOS() && Utils.env.supportsEB();
    },


    /**
     * 获取weex屏幕真实的设置高度，需要减去导航栏高度
     * @returns {Number}
     */
    getPageHeight: function getPageHeight() {
      var env = weex.config.env;

      var navHeight = Utils.env.isWeb() ? 0 : Utils.env.isIPhoneX() ? 176 : 132;
      return env.deviceHeight / env.deviceWidth * 750 - navHeight;
    }
  },

  /**
   * 版本号比较
   * @memberOf Utils
   * @param currVer {string}
   * @param promoteVer {string}
   * @returns {boolean}
   * @example
   *
   * const { Utils } = require('@ali/wx-bridge');
   * const { compareVersion } = Utils;
   * console.log(compareVersion('0.1.100', '0.1.11')); // 'true'
   */
  compareVersion: function compareVersion() {
    var currVer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '0.0.0';
    var promoteVer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0.0.0';

    if (currVer === promoteVer) return true;
    var currVerArr = currVer.split('.');
    var promoteVerArr = promoteVer.split('.');
    var len = Math.max(currVerArr.length, promoteVerArr.length);
    for (var i = 0; i < len; i++) {
      var proVal = ~~promoteVerArr[i];
      var curVal = ~~currVerArr[i];
      if (proVal < curVal) {
        return true;
      } else if (proVal > curVal) {
        return false;
      }
    }
    return false;
  },

  /**
   * 分割数组
   * @param arr 被分割数组
   * @param size 分割数组的长度
   * @returns {Array}
   */
  arrayChunk: function arrayChunk() {
    var arr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 4;

    var groups = [];
    if (arr && arr.length > 0) {
      groups = arr.map(function (e, i) {
        return i % size === 0 ? arr.slice(i, i + size) : null;
      }).filter(function (e) {
        return e;
      });
    }
    return groups;
  },
  truncateString: function truncateString(str, len) {
    var hasDot = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

    var newLength = 0;
    var newStr = '';
    var singleChar = '';
    var chineseRegex = /[^\x00-\xff]/g;
    var strLength = str.replace(chineseRegex, '**').length;
    for (var i = 0; i < strLength; i++) {
      singleChar = str.charAt(i).toString();
      if (singleChar.match(chineseRegex) !== null) {
        newLength += 2;
      } else {
        newLength++;
      }
      if (newLength > len) {
        break;
      }
      newStr += singleChar;
    }

    if (hasDot && strLength > len) {
      newStr += '...';
    }
    return newStr;
  }
};

exports.default = Utils;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getOpenId = getOpenId;
exports.getUserId = getUserId;
exports.getUserInfo = getUserInfo;
exports.exit = exit;
exports.setUserInfo = setUserInfo;
exports.login = login;
exports.registerByEmail = registerByEmail;
exports.registerByPhone = registerByPhone;
exports.getPswBackByEmail = getPswBackByEmail;
exports.getPswBackByPhone = getPswBackByPhone;
exports.sendPhoneCode = sendPhoneCode;
exports.updatePsw = updatePsw;
exports.updateProfile = updateProfile;
exports.userFeedback = userFeedback;

var _fetch = __webpack_require__(12);

var _storage = __webpack_require__(14);

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(13);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var syncUser = new BroadcastChannel('christianReaderUser');
var USER_KEY = 'userKey'; //用户存储的KeyValue

var syncUserInfo = {
    receive: function receive(fun) {
        syncUser.onmessage = function (event) {
            // console.log(event.data) // Assemble!
            fun();
        };
    },
    post: function post() {
        var message = {
            status: 'update'
        };
        syncUser.postMessage(message);
    },
    close: function close(fun) {
        syncUser.close();
    }
};

exports.default = syncUserInfo;
function getOpenId() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {
        if (res.result == "failed") {
            return "";
        } else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).openId;
        }
        // return "8A471FD74AF017EE0680381611D442CD";
    });
}

function getUserId() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {
        if (res.result == "failed") {
            return -1;
        } else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).id;
        }
    });
}

function getUserInfo() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {

        if (res.result == "failed") {
            return _defineProperty({
                id: -1,
                openId: '',
                head_icon: (0, _fetch.getHost)() + '/static/default/2.jpg',
                nickName: '主的好孩子,没有任何信息',
                profile: '点击这里登录或注册'
            }, 'openId', '');
        } else {
            return JSON.parse(res.data);
        }
    });
}

//更新资料
function exit() {
    (0, _storage.storageRemoveItem)(USER_KEY);
}

function setUserInfo(value) {
    return (0, _storage.storageSetItem)(USER_KEY, JSON.stringify(value));
}

//登陆
function login(userName, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password }).then(function (res) {
        console.log(" -- login -- ");
        console.log(res);
        if (res.data.status == 1) {
            setUserInfo(res.data.data);
        }
        return res;
    });
    // return post('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password}, fun);
}
//用户邮箱注册
function registerByEmail(nickName, email, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName, 'email': email, 'password': password });
    // return post('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName ,'email': email,'password': password}, fun);
}
//用户手机注册
function registerByPhone(nickName, mobile, verifyCode, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName, 'mobile': mobile, 'verifyCode': verifyCode, 'password': password });
    // return post('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName ,'mobile': mobile,'verifyCode': verifyCode,'password': password}, fun);
}
//找回密码
function getPswBackByEmail(email) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getPasswordBack', { 'email': email });
    // return post('/api/ChristianReaderUser/getPasswordBack', { 'email': email}, fun);
}
function getPswBackByPhone(mobile, verifyCode) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode });
    // return post('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode,}, fun);
}
//发送手机验证码
function sendPhoneCode(mobile) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getSms', { 'mobile': mobile });
}
//修改密码
function updatePsw(mobile, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/updatePswByPhone', { 'mobile': mobile, 'password': password });
}
//更新资料
function updateProfile(name, nickName, des) {
    return getOpenId().then(function (openId) {
        return (0, _fetch.post)('/api/ChristianReaderUser/updatePersonalProfile', { 'name': name, 'nickName': nickName, 'brief_personal_des': des, 'user_token': openId }).then(function (res) {
            if (res.data.status == 1) {
                getUserInfo().then(function (res) {
                    res.nickName = nickName;
                    res.profile = des;
                    setUserInfo(res);
                });
            }
            return res;
        });
    });
}
//用户反馈
function userFeedback(contactWays, content) {
    return (0, _fetch.post)('/api/ChristianReaderUser/feedback', { 'ContactInformation': contactWays, 'Content': content, 'AppInfo': '基督徒阅读' });
}

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getHost = getHost;
exports.fetch = fetch;
exports.get = get;
exports.post = post;
//const host = "http://file.biblemooc.net/";
//const host = "http://192.168.2.56:1070";
//const host = "http://192.168.31.247:1070";
// const host = "http://192.168.2.100:12000";
var host = "http://christian-reading.daddygarden.com";

function getHost() {
    return host;
}

function getStream() {
    var stream = weex.requireModule('stream');
    return stream;
}

function paramsToString(obj) {
    var param = "";
    for (var name in obj) {
        param += "&" + name + "=" + obj[name];
    }
    return param.substring(1);
}

function fetch(path, method, param, successFun) {
    path = host + path;
    getStream().fetch({
        method: method,
        url: path,
        type: 'json',
        body: paramsToString(param)
    }, successFun);
    console.log(path);
}

// export function  get(path,param,fun) {
//     console.log(host+path+"?"+paramsToString(param));
//     return getStream().fetch({
//         method: 'GET',
//         type: 'json',
//         url: host+path+"?"+paramsToString(param),
//     }, fun)
// }

function get(path, param) {
    console.log(host + path + "?" + paramsToString(param));
    return new Promise(function (resolve, reject) {
        getStream().fetch({
            method: 'GET',
            url: host + path + "?" + paramsToString(param),
            type: 'json'
        }, function (ret) {
            resolve(ret);
        });
    });
}
//
// export function post(path,param,successFun)
// {
//     path = host+path;
//     console.log(path);
//     console.log(paramsToString(param));
//     getStream().fetch({
//             method: 'POST',
//             url: path,
//             type: 'json',
//             headers:{'Content-Type':'application/x-www-form-urlencoded'},
//             body: paramsToString(param)
//         }
//         ,successFun);
// }

function post(path, param) {
    path = host + path;
    console.log(path);
    console.log(paramsToString(param));
    return new Promise(function (resolve, reject) {
        getStream().fetch({
            method: 'POST',
            url: path,
            type: 'json',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: paramsToString(param)
        }, function (ret) {
            resolve(ret);
        });
    });
}

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sync = new BroadcastChannel('christian reader');

var syncAblumInfo = {
  receive: function receive(fun) {
    sync.onmessage = fun;
  },
  follow: function follow(ablumnId) {
    var message = {
      status: 'add',
      id: ablumnId
    };
    sync.postMessage(message);
  },
  cancel: function cancel(ablumnId) {
    var message = {
      status: 'cancel',
      id: ablumnId
    };
    sync.postMessage(message);
  },
  getData: function getData() {
    var message = {
      status: 'getData'
    };
    sync.postMessage(message);
  },
  close: function close(ablumnId, fun) {
    sync.close();
  }
};

exports.default = syncAblumInfo;

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.storageSetItem = storageSetItem;
exports.storageGetItem = storageGetItem;
exports.storageRemoveItem = storageRemoveItem;

var storage = weex.requireModule('storage');

function storageSetItem(key, value) {
    // console.log(' -- storageSetItem -- ');
    // console.log(value);
    return new Promise(function (resolve, reject) {
        storage.setItem(key, value, function (event) {
            // console.log(event);
            resolve(event);
        });
    });
};

function storageGetItem(key) {
    return new Promise(function (resolve, reject) {
        storage.getItem(key, function (event) {
            // console.log(" storage event ");
            // console.log(event);
            resolve(event);
        });
    });
}

function storageRemoveItem(key) {
    return new Promise(function (resolve, reject) {
        storage.removeItem(key, function (event) {
            // console.log(event);
            resolve(event);
        });
    });
}

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(114)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(40),
  /* template */
  __webpack_require__(93),
  /* scopeId */
  "data-v-50b33d5c",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\Index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-50b33d5c", Component.options)
  } else {
    hotAPI.reload("data-v-50b33d5c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(126)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(41),
  /* template */
  __webpack_require__(105),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\chat.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] chat.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e85e8c30", Component.options)
  } else {
    hotAPI.reload("data-v-e85e8c30", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(110)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(42),
  /* template */
  __webpack_require__(89),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\commonFun.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] commonFun.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32957998", Component.options)
  } else {
    hotAPI.reload("data-v-32957998", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(111)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(43),
  /* template */
  __webpack_require__(90),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\defaultImage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] defaultImage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-38bd612a", Component.options)
  } else {
    hotAPI.reload("data-v-38bd612a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(122)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(44),
  /* template */
  __webpack_require__(101),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\defaultLoading.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] defaultLoading.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a33a226a", Component.options)
  } else {
    hotAPI.reload("data-v-a33a226a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(116)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(45),
  /* template */
  __webpack_require__(95),
  /* scopeId */
  "data-v-584cd5ee",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\library.bookShelf.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] library.bookShelf.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-584cd5ee", Component.options)
  } else {
    hotAPI.reload("data-v-584cd5ee", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(119)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(46),
  /* template */
  __webpack_require__(98),
  /* scopeId */
  "data-v-62ffd2f8",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\libraryNav.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] libraryNav.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-62ffd2f8", Component.options)
  } else {
    hotAPI.reload("data-v-62ffd2f8", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(113)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(47),
  /* template */
  __webpack_require__(92),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\loadingData.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] loadingData.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-479bbfa6", Component.options)
  } else {
    hotAPI.reload("data-v-479bbfa6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(117)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(48),
  /* template */
  __webpack_require__(96),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\me.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] me.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5b02b228", Component.options)
  } else {
    hotAPI.reload("data-v-5b02b228", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(123)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(49),
  /* template */
  __webpack_require__(102),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\netError.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] netError.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-cd9f490a", Component.options)
  } else {
    hotAPI.reload("data-v-cd9f490a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(120)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(50),
  /* template */
  __webpack_require__(99),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\networkErrorDisplay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] networkErrorDisplay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-79900e88", Component.options)
  } else {
    hotAPI.reload("data-v-79900e88", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(121)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(51),
  /* template */
  __webpack_require__(100),
  /* scopeId */
  "data-v-7b08ba3d",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\progress.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] progress.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7b08ba3d", Component.options)
  } else {
    hotAPI.reload("data-v-7b08ba3d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(107)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(52),
  /* template */
  __webpack_require__(86),
  /* scopeId */
  "data-v-16a3652a",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\searchBar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-16a3652a", Component.options)
  } else {
    hotAPI.reload("data-v-16a3652a", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(112)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(53),
  /* template */
  __webpack_require__(91),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\submitting.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] submitting.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42cf8c96", Component.options)
  } else {
    hotAPI.reload("data-v-42cf8c96", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(108)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(54),
  /* template */
  __webpack_require__(87),
  /* scopeId */
  "data-v-17d335ea",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\subscribe.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] subscribe.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-17d335ea", Component.options)
  } else {
    hotAPI.reload("data-v-17d335ea", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(115)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(55),
  /* template */
  __webpack_require__(94),
  /* scopeId */
  "data-v-540c97a4",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\topbar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] topbar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-540c97a4", Component.options)
  } else {
    hotAPI.reload("data-v-540c97a4", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(109)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(56),
  /* template */
  __webpack_require__(88),
  /* scopeId */
  "data-v-1a32ac6f",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\videoPlay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] videoPlay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1a32ac6f", Component.options)
  } else {
    hotAPI.reload("data-v-1a32ac6f", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(124)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(57),
  /* template */
  __webpack_require__(103),
  /* scopeId */
  "data-v-d88579ee",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\weexDialogue.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weexDialogue.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d88579ee", Component.options)
  } else {
    hotAPI.reload("data-v-d88579ee", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(118)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(58),
  /* template */
  __webpack_require__(97),
  /* scopeId */
  "data-v-5dd495a2",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\weexOverlay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weexOverlay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5dd495a2", Component.options)
  } else {
    hotAPI.reload("data-v-5dd495a2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(125)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(59),
  /* template */
  __webpack_require__(104),
  /* scopeId */
  "data-v-e41dd710",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\weexWebView.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weexWebView.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e41dd710", Component.options)
  } else {
    hotAPI.reload("data-v-e41dd710", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(106)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(60),
  /* template */
  __webpack_require__(85),
  /* scopeId */
  "data-v-075dc4af",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\03.components\\weextabbar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weextabbar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-075dc4af", Component.options)
  } else {
    hotAPI.reload("data-v-075dc4af", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var utilFunc = {
    initIconFont: function initIconFont() {
        var domModule = weex.requireModule('dom');
        domModule.addRule('fontFace', {
            'fontFamily': "iconfont",
            'src': "url('http://at.alicdn.com/t/font_404010_jgmnakd1zizr529.ttf')"
        });
    },
    setWXBundleUrl: function setWXBundleUrl(url, jsFile) {
        var bundleUrl = url;
        var host = '';
        var path = '';
        var nativeBase = void 0;
        var isAndroidAssets = bundleUrl.indexOf('your_current_IP') >= 0 || bundleUrl.indexOf('file://assets/') >= 0;
        var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('WeexDemo.app') > 0;
        if (isAndroidAssets) {
            nativeBase = 'file://assets/dist';
        } else if (isiOSAssets) {
            // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
            // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
            nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
        } else {
            var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
            var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
            if (matches && matches.length >= 2) {
                host = matches[1];
            }
            if (matchFirstPath && matchFirstPath.length >= 2) {
                path = matchFirstPath[1];
            }
            nativeBase = 'http://' + host + '/';
        }
        var h5Base = './index.html?page=';
        // in Native
        var base = nativeBase;
        if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
            // check if in weexpack project
            if (path === 'web' || path === 'dist') {
                base = h5Base + '/dist/';
            } else {
                base = h5Base + '';
            }
        } else {
            base = nativeBase + (!!path ? path + '/' : '');
        }

        var newUrl = base + jsFile;
        return newUrl;
    },
    setWebBundleUrl: function setWebBundleUrl(url, jsFile) {
        var bundleUrl = url;
        var host = '';
        var path = '';
        var nativeBase = void 0;
        var isAndroidAssets = bundleUrl.indexOf('your_current_IP') >= 0 || bundleUrl.indexOf('file://assets/') >= 0;
        var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('WeexDemo.app') > 0;
        if (isAndroidAssets) {
            nativeBase = 'file://assets/dist';
        } else if (isiOSAssets) {
            // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
            // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
            nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
        } else {
            var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
            var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
            if (matches && matches.length >= 2) {
                host = matches[1];
            }
            if (matchFirstPath && matchFirstPath.length >= 2) {
                path = matchFirstPath[1];
            }
            nativeBase = 'http://' + host + '/';
        }
        var h5Base = './index.html?page=';
        // in Native
        var base = nativeBase;
        if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
            // check if in weexpack project
            if (path === 'web' || path === 'dist') {
                base = h5Base + '/web/';
            } else {
                base = h5Base + '';
            }
        } else {
            base = nativeBase + (!!path ? path + '/' : '');
        }

        var newUrl = base + jsFile;
        return newUrl;
    },
    getUrlSearch: function getUrlSearch(url, name) {
        // debugger;
        var address,
            val = "";
        if (WXEnvironment.platform == 'Web') {
            address = window.location.href;
        } else {
            address = url;
        }

        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = address.slice(address.indexOf('?') + 1).match(reg);

        if (r != null) {
            try {
                val = decodeURIComponent(r[2]);
            } catch (_e) {}
        }
        return val;
    }
};

exports.default = utilFunc;

/***/ }),
/* 37 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sync = new BroadcastChannel('book');

var syncBookShelf = {
  receive: function receive(fun) {
    sync.onmessage = fun;
  },
  addToShelf: function addToShelf(bookId) {
    var message = {
      status: 'add',
      bookId: bookId
    };
    sync.postMessage(message);
  },
  cancel: function cancel() {
    var message = {
      status: 'cancel'
    };
    sync.postMessage(message);
  },
  getData: function getData() {
    var message = {
      status: 'getData'
    };
    sync.postMessage(message);
  },
  close: function close(ablumnId, fun) {
    sync.close();
  }
};

exports.default = syncBookShelf;

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var EVENT_DOWNLOAD = 'downLoadBook';

var downloadEvent = {
    addDownLoadBookEventListener: function addDownLoadBookEventListener(fun) {
        var event = weex.requireModule('globalEvent');
        event.addEventListener(EVENT_DOWNLOAD, function (value) {
            console.log("get downLoadBook");
            fun(value);
        });
    },
    removeDownLoadBookEventListener: function removeDownLoadBookEventListener() {
        var event = weex.requireModule('globalEvent');
        console.log("remove downLoadBook");
        event.removeEventListener(EVENT_DOWNLOAD);
    }
};

exports.default = downloadEvent;

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(10);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(9);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(8);

var _common = __webpack_require__(5);

var _googleTrack = __webpack_require__(7);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            currentPage: 0,
            height: '',
            articles: [],
            clientUrl: "",
            global: _global2.default,
            msg: '首页',
            pageIndex: 1,
            loadinging: false,
            errorType: '',
            errorMsg: '',
            netStatus: 0 //0表示正在加载数据，1表示网络错误，2表示加载数据成功
        };
    },
    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 132;
        this.getData(this.pageIndex);
    },

    methods: {
        bannerJump: function bannerJump(event) {
            console.log("11");
        },
        jumpToBookProfile: function jumpToBookProfile() {
            (0, _navigator.jumpSubPage)(this, 'bookprofile', { closeThisPage: true });
        },
        test: function test() {
            _prompt2.default.toast("测试信息");
        },
        jumpToRead: function jumpToRead(id, title, des, img_url) {
            (0, _navigator.jumpSubPage)(this, 'read', { id: id, title: title, des: des, img_url: img_url });
        },
        loadData: function loadData() {
            this.getData(this.pageIndex);
        },

        anim: function anim(styles, timingFunction, duration, callback) {
            animation.transition(this.$refs.loadingAnimation, {
                styles: styles,
                timingFunction: timingFunction,
                duration: duration
            }, callback);
        },
        loadMore: function loadMore(event) {
            var self = this;
            self.loadinging = true;
            if (self.articles.length < 10 * self.pageIndex) {
                _prompt2.default.toast("数据已经加载完成");
                setTimeout(function () {
                    self.loadinging = false;
                }, 2000);
            } else {
                self.pageIndex = self.pageIndex + 1;
                self.getData(self.pageIndex);
            }
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData(self.pageIndex);
        },
        arrNoRepeat: function arrNoRepeat(arr) {
            return Array.from(new Set(arr));
        },
        browseInfo: function browseInfo(id) {
            //                jump(this,this.clientUrl+"?id="+id)
            console.log(id);
        },
        getData: function getData(pageIndex) {
            var self = this;
            (0, _creader.getArticleListInfo)(pageIndex).then(function (res) {
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined") {
                    self.loadinging = false;
                    self.netStatus = 2;
                    self.articles = self.articles.concat(res.data.data);
                    self.clientUrl = res.data.client_url;
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        },
        jumpToAuthorColumn: function jumpToAuthorColumn(albumName, albumId) {
            (0, _navigator.jumpSubPage)(this, 'authorColumn', { id: albumId });
        }
    }
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(5);

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(10);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(9);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(8);

var _user = __webpack_require__(11);

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {};
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 96;
    },

    methods: {
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        },
        goToPersonalProfile: function goToPersonalProfile() {
            if (this.personInfo.id > 0) {
                (0, _navigator.jumpSubPage)(this, 'personalProfile');
            } else {
                (0, _navigator.jumpSubPage)(this, 'login');
            }
        },
        goToMySubscribe: function goToMySubscribe() {
            (0, _navigator.jumpSubPage)(this, 'mySubscribe');
        },
        goToMyLists: function goToMyLists() {
            (0, _navigator.jumpSubPage)(this, 'myArticles');
        },
        goToFeedback: function goToFeedback() {
            (0, _navigator.jumpSubPage)(this, 'feedback');
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
        },
        getData: function getData() {
            var _this = this;

            var self = this;
            (0, _user.getUserInfo)().then(function (res) {
                console.log(" --- getUserInfo --- ");
                self.personInfo = res;
                //                        console.log(self.personInfo.nickName);
                self.netStatus = 2;
                (0, _creader.getPersonalData)().then(function (res) {
                    var self = _this;
                    console.log(res);
                    if (res.status != -1 && typeof res.data != "undefined") {
                        self.netStatus = 2;
                        self.personInfo = res.data.data;
                        console.log(self.personInfo);
                    } else {
                        self.netStatus = 1;
                        self.errorType = 'neterror';
                    }
                });
            });
        }
    }

};

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//

exports.default = {};

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        backgroup: { type: String },
        source: { type: String }

    },
    data: function data() {
        return {
            loadFinished: false
        };
    },
    created: function created() {
        console.log(' --- ');
    },

    methods: {
        onImageLoad: function onImageLoad(event) {
            if (event.success) {
                // Do something to hanlde success
                this.loadFinished = true;
            }
        }
    }
};

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    name: 'defaultLoading',
    data: function data() {
        return {
            msg: 'hello vue'
        };
    }
};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(5);

var _navigator = __webpack_require__(3);

var _fetch = __webpack_require__(12);

var _weextabbarConfig = __webpack_require__(9);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _creader = __webpack_require__(8);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

var _globalEvent = __webpack_require__(39);

var _globalEvent2 = _interopRequireDefault(_globalEvent);

var _syncBookShelf = __webpack_require__(38);

var _syncBookShelf2 = _interopRequireDefault(_syncBookShelf);

var _googleTrack = __webpack_require__(7);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            currentPage: 1,
            currentPosition: 0,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            errorType: '',
            errorMsg: '',
            global: global,
            netStatus: 0,
            isShelf: false,
            height: '',
            title: '!',
            content: '您确定要将本书移出书架吗？',
            confirmText: '确定',
            cancelText: '取消',
            show: false,
            single: false,
            showNoPrompt: false,
            isChecked: false,
            currentBookId: -1,
            currentIndex: -1,
            bookTotal: 0,
            bookIndex: 1,
            books: [],
            isSync: false,
            downLoadIndex: -1,
            downLoadStatus: 0,
            imgDownLoad: false
        };
    },
    created: function created() {
        var self = this;
        // self.getData();
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        self.height = 750 / deviceWidth * deviceHeight - 132;
        _syncBookShelf2.default.receive(function (res) {
            if (res.data.status == 'add') {
                (0, _creader.getBookShelf)().then(function (data) {
                    self.books = [];
                    self.books = data;
                    self.netStatus = 2;
                });
            }
            if (res.data.status == 'getData') {
                self.syncData();
            }
        });
        _globalEvent2.default.addDownLoadBookEventListener(function (res) {

            if (res.status == 1) {
                if (self.books[self.downLoadIndex]['progress'].visible == false) {
                    self.books[self.downLoadIndex]['progress'].visible = true;
                }
                self.books[self.downLoadIndex]['progress'].value = res.progress;
            } else if (res.status == 2) {
                self.books[self.downLoadIndex]['progress'] = { visible: false, value: 0 };
                self.downLoadStatus = 0;
                self.downLoadIndex = 0;
            } else if (res.status == 3) {
                _prompt2.default.toast("下载异常，请稍后再试！");
                self.books[self.downLoadIndex]['progress'] = { visible: false, value: 0 };
                self.downLoadStatus = 0;
                self.downLoadIndex = 0;
            } else if (res.status == 4) {
                _prompt2.default.toast("网络无法连接，请联网后再操作. ");
                self.books[self.downLoadIndex]['progress'] = { visible: false, value: 0 };
                self.downLoadStatus = 0;
                self.downLoadIndex = 0;
            }
            this.$set(self.books, self.downLoadIndex, self.books[self.downLoadIndex]);
            console.log(res);
        });
    },

    methods: {
        onchange: function onchange(event) {
            console.log('changed:', event.index);
        },
        bannerJump: function bannerJump(event) {
            console.log("11");
        },
        loadData: function loadData() {
            console.log("--------------loadData ----------------");
            var self = this;
            self.getData();
        },
        jumpToBookProfile: function jumpToBookProfile(id, title, address, imgUrl) {
            var _this = this;

            (0, _creader.getBookShelf)().then(function (data) {
                var h = -1;
                for (var _i = 0; _i < data.length; _i++) {
                    if (data[_i].id == id) {
                        // data = data.splice(i,1);
                        h = _i;
                    }
                }
                var arr = data.splice(h, 1);
                data.unshift(arr[0]);
                (0, _creader.saveBookShelf)(data);
                if (_this.downLoadStatus == 1) {
                    _prompt2.default.toast("一次只能下载一本书籍，请稍后再试.");
                    return;
                } else {
                    _this.downLoadStatus == 1;
                    var len = _this.books.length;
                    for (var i = 0; i < len; i++) {
                        if (_this.books[i].id == id) {
                            _this.downLoadIndex = i;
                            break;
                        }
                    }
                    _googleTrack2.default.userLibraryOpenBook(title, id);
                    var event = weex.requireModule('event');
                    event.openBook(id, title, address, imgUrl, (0, _fetch.getHost)() + "/display/index/displayBook?id=" + id);
                }
            });
        },
        openPopUp: function openPopUp(id, index) {
            var self = this;
            self.currentBookId = id;
            self.currentIndex = index;
            // prompt.toast(self.currentBookId+'-----'+self.currentIndex);
            self.show = true;
        },
        dialogCancelBtnClick: function dialogCancelBtnClick() {
            this.show = false;
            // prompt.toast("我点了取消");
        },
        dialogConfirmBtnClick: function dialogConfirmBtnClick() {
            var _this2 = this;

            var self = this;
            self.show = false;
            // prompt.toast(self.currentBookId);
            (0, _creader.userRemoveBookToShelf)(self.currentBookId).then(function (res) {
                console.log(res);
                if (res.data.status == 1) {
                    self.books.splice(self.currentIndex, 1);
                    (0, _creader.saveBookShelf)(_this2.books);
                } else {
                    _prompt2.default.toast('移除失败，请稍后再试');
                }
            });
        },
        refresh: function refresh() {
            var self = this;
            global.init();
            self.netStatus = 0;
            self.getData();
        },
        isExist: function isExist(item) {
            var len = this.books.length;
            for (var i = 0; i < len; i++) {
                if (this.books[i].id == item.id) {
                    return true;
                    break;
                }
            }
            return false;
        },
        syncData: function syncData() {
            var _this3 = this;

            //采用循环调用，把所有的数据同步完成
            (0, _creader.getBookListByService)(this.books, this.bookIndex).then(function (res) {
                var res = res.data;
                console.log(" -- this.bookIndex  --");
                console.log(_this3.bookIndex);
                console.log(res);
                if (res.status == 1) {
                    _this3.bookTotal = res.data.total;
                    var tlist = [];
                    res.data.list.forEach(function (item) {
                        //从服务器拿到数据后遍历，是否有新添加的书籍
                        if (_this3.isExist(item) == false) {
                            item['progress'] = { visible: false, value: 0 };
                            item['img_url'] = res.data.host + item['img_url'];
                            item['address'] = res.data.host + item['address'];
                            tlist.push(item);
                        }
                    });
                    if (tlist.length > 0) {
                        _this3.books = _this3.books.concat(tlist);
                    }
                    if (_this3.bookIndex * 50 >= _this3.bookTotal) {
                        //                                console.log(" ----- this.books ---- ");
                        //                                console.log(this.books);
                        (0, _creader.saveBookShelf)(_this3.books);
                        if (_this3.books.length == 0) {
                            _this3.netStatus = 1;
                            _this3.errorType = 'noResult';
                            _this3.errorMsg = "您的书架里还没有添加任何书籍..";
                        } else {
                            _this3.netStatus = 2;
                            _this3.imgDownLoad = true;
                        }
                    } else {
                        _this3.bookIndex = _this3.bookIndex + 1;
                        _this3.syncData();
                    }
                }
            });
        },
        getData: function getData() {
            var self = this;
            if (self.isSync == false) {
                self.isSync = true;
                (0, _creader.getBookShelf)().then(function (data) {
                    self.books = data;
                    if (self.books.length > 0) {
                        self.netStatus = 2;
                    }
                    if (data.length == 0) {
                        setTimeout(self.syncData, 400);
                        self.netStatus = 1;
                        self.errorType = 'noResult';
                        self.errorMsg = "您的书架里还没有添加任何书籍..";
                    } else {
                        setTimeout(self.syncData, 3000);
                    }
                });
            }
        },
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        }
    }

}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(37)))

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Utils = __webpack_require__(10);

var _Utils2 = _interopRequireDefault(_Utils);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(5);

var _googleTrack = __webpack_require__(7);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var dom = weex.requireModule('dom');
var animation = weex.requireModule('animation');
exports.default = {
  props: {
    currentPage: {
      type: Number
    },
    currentPosition: {
      type: Number
    },
    tabTitles: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    isShelf: {
      type: Boolean,
      default: true
    },
    tabStyles: {
      type: Object,
      default: function _default() {
        return {
          bgColor: '#FFFFFF',
          titleColor: '#515151',
          activeTitleColor: '#e72c27',
          activeBgColor: '#ffffff',
          isActiveTitleBold: true,
          iconWidth: 60,
          iconHeight: 60,
          width: 160,
          height: 120,
          borderBottomColor: '#FFC900',
          borderBottomWidth: 0,
          borderBottomHeight: 0,
          activeBorderBottomColor: '#e72c27',
          activeBorderBottomWidth: 6,
          activeBorderBottomHeight: 6,
          fontSize: 31,
          textPaddingLeft: 10,
          textPaddingRight: 10
        };
      }
    },
    titleType: {
      type: String,
      default: 'icon'
    },
    isTabView: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: String,
      default: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    }
  },
  data: function data() {
    return {
      topNav: ['书架', '分类', '书单', '书店'],
      getImgPath: _common.getImgPath

    };
  },
  created: function created() {
    var titleType = this.titleType,
        tabStyles = this.tabStyles;
  },

  methods: {
    setPage: function setPage(index) {
      var page = '';
      switch (index) {
        case 0:
          page = 'libraryBookShelf';
          break;
        case 1:
          page = 'libraryCategory';
          break;
        case 2:
          page = 'libraryBookLists';
          break;
        case 3:
          page = 'libraryBookStore';
          break;
      }
      if (page == 'libraryBookShelf') {
        (0, _navigator.jumpSubPage)(this, page, { backToRootView: true });
      } else {
        (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        // jumpSubPage(this,page);
        // jump(this,page);
      }
    },
    back: function back() {
      (0, _navigator.jumpSubPage)(this, 'libraryBookShelf', { backToRootView: true });
    },
    jumpToLibrarySearch: function jumpToLibrarySearch() {
      var page = 'librarySearch';
      (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
    }
  }

};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(5);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            loadinging: true,
            height: ''
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight;
        var deviceWidth = WXEnvironment.deviceWidth;
        this.height = (deviceHeight * 750 / deviceWidth - 108) / 2;
        _global2.default.init();
    }
};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(5);

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(10);

var _Utils2 = _interopRequireDefault(_Utils);

var _fetch = __webpack_require__(12);

var _weextabbarConfig = __webpack_require__(9);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(8);

var _user = __webpack_require__(11);

var _user2 = _interopRequireDefault(_user);

var _googleTrack = __webpack_require__(7);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            currentPage: 4,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            storagediv: [],
            networkStatus: 1,
            global: _global2.default,
            personInfo: [],
            errorType: '',
            errorMsg: '',
            netStatus: 0,
            height: ''
        };
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 132;
        // var self = this;
        // this.getData();
        // syncUserInfo.receive(this.getData);
    },

    methods: {
        loadData: function loadData() {
            this.getData();
            _user2.default.receive(this.getData);
        },
        goToPersonalProfile: function goToPersonalProfile() {
            if (this.personInfo.id > 0) {
                (0, _navigator.jumpSubPage)(this, 'personalProfile');
            } else {
                (0, _navigator.jumpSubPage)(this, 'login');
            }
        },
        goToMySubscribe: function goToMySubscribe() {
            (0, _navigator.jumpSubPage)(this, 'mySubscribe');
        },
        goToMyLists: function goToMyLists() {
            (0, _navigator.jumpSubPage)(this, 'myArticles');
        },
        goToFeedback: function goToFeedback() {
            (0, _navigator.jumpSubPage)(this, 'feedback');
        },
        goToShare: function goToShare() {
            _googleTrack2.default.userOtherShareToFriend();
            this.$refs.commonFun.ShareApp("推荐下载 基督徒阅读APP", (0, _fetch.getHost)() + "/display/index/introduce", (0, _fetch.getHost)() + "/static/default/icon-512.jpg", "各大应用市场中有超过1000+的好评，深受弟兄姊妹的喜爱 是基督徒的必备工具。");
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
        },
        getData: function getData() {
            var _this = this;

            var self = this;
            (0, _user.getUserInfo)().then(function (res) {
                console.log(" --- getUserInfo --- ");
                self.personInfo = res;
                //                        console.log(self.personInfo.nickName);
                self.netStatus = 2;
                (0, _creader.getPersonalData)().then(function (res) {
                    var self = _this;
                    console.log(res);
                    if (res.status != -1 && typeof res.data != "undefined") {
                        self.netStatus = 2;
                        self.personInfo = res.data.data;
                        console.log(self.personInfo);
                    } else {
                        self.netStatus = 1;
                        self.errorType = 'neterror';
                    }
                });
            });
        }
    }

};

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(5);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            noResult: 'noResult',
            neterror: 'neterror'
        };
    },

    props: {
        networkError: String,
        errorType: {
            type: String
        },
        errorMsg: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            this.$emit('toNetError');
        }
    },

    created: function created() {
        _global2.default.init();
    }
};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        _global2.default.init();
    }
};

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        barColor: {
            type: String,
            default: '#9B7B56'
        },
        barWidth: {
            type: Number,
            default: 600
        },
        barHeight: {
            type: Number,
            default: 8
        },
        value: {
            type: Number,
            default: 0
        }
    },
    computed: {
        runWayStyle: function runWayStyle() {
            var barWidth = this.barWidth,
                barHeight = this.barHeight;

            return {
                width: barWidth + 'px',
                height: barHeight + 'px'
            };
        },
        progressStyle: function progressStyle() {
            var value = this.value,
                barWidth = this.barWidth,
                barHeight = this.barHeight,
                barColor = this.barColor;

            var newValue = value < 0 ? 0 : value > 100 ? 100 : value;
            return {
                backgroundColor: barColor,
                height: barHeight + 'px',
                width: newValue / 100 * barWidth + 'px'
            };
        }
    }
};

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(5);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    alwaysShowCancel: {
      type: Boolean,
      default: false
    },
    inputType: {
      type: String,
      default: 'text'
    },
    mod: {
      type: String,
      default: 'default'
    },
    autofocus: {
      type: Boolean,
      default: false
    },
    theme: {
      type: String,
      default: 'gray'
    },
    defaultValue: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: '书名或作者'
    }
  },
  computed: {
    needShowCancel: function needShowCancel() {
      return this.alwaysShowCancel || this.showCancel;
    }
  },
  data: function data() {
    return {
      showCancel: false,
      showClose: false,
      value: '',
      getImgPath: _common.getImgPath
    };
  },
  created: function created() {
    this.defaultValue && (this.value = this.defaultValue);
    if (this.disabled) {
      this.showCancel = false;
      this.showClose = false;
    }
  },

  methods: {
    onBlur: function onBlur(e) {
      var self = this;
      setTimeout(function () {
        self.showCancel = false;
        self.detectShowClose();
        // self.value = e.value;
        self.$emit('wxcSearchbarInputOnBlur', { value: self.value });
      }, 10);
    },
    autoBlur: function autoBlur() {
      this.$refs['search-input'].blur();
    },
    onInput: function onInput(e) {
      this.value = '';
      this.value = e.value;
      this.showCancel = true;
      // this.detectShowClose();
      this.$emit('wxcSearchbarInputOnInput', { value: this.value });
    },
    onFocus: function onFocus() {
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputOnFocus', { value: this.value });
    },
    closeClicked: function closeClicked() {
      var self = this;
      self.value = '';
      this.showCancel && (self.showCancel = false);
      // this.showClose && (self.showClose = false);
      this.$emit('wxcSearchbarCloseClicked', { value: self.value });
      // this.$emit('wxcSearchbarInputOnInput', { value: self.value });
    },
    searchClicked: function searchClicked() {
      var self = this;
      self.autoBlur();
      self.$emit('searchBarClicked', { value: self.value });
    },
    detectShowClose: function detectShowClose() {
      this.showClose = this.value.length > 0 && this.showCancel;
    },
    depClicked: function depClicked() {
      this.$emit('wxcSearchbarDepChooseClicked', {});
    },
    setValue: function setValue(value) {
      this.value = value;
    }
  }
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(5);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            loadinging: true,
            height: ''
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight;
        var deviceWidth = WXEnvironment.deviceWidth;
        this.height = (deviceHeight * 750 / deviceWidth - 108) / 2;
        _global2.default.init();
    }
};

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(5);

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(10);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(9);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(4);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(8);

var _syncAblumInfo = __webpack_require__(13);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _user = __webpack_require__(11);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            currentPage: 2,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            netStatus: 0,
            followStatus: '',
            banners: [],
            albums: [],
            hotArticles: [],
            storagediv: [],
            global: _global2.default,
            clientUrl: "",
            errorType: '',
            errorMsg: '',
            height: ''
        };
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 132;
        var self = this;
        _syncAblumInfo2.default.receive(function (res) {
            if (res.data.status == 'getData') {
                self.getData();
            } else {
                for (var i = 0; i < self.albums.length; i++) {
                    if (res.data.id == self.albums[i].id && res.data.status == 'add') {
                        self.albums[i].status = 1;
                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                    }
                    if (res.data.id == self.albums[i].id && res.data.status == 'cancel') {
                        self.albums[i].status = 0;
                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                    }
                }
            }
        });
    },
    computed: {
        Id: function Id() {
            var self = this;
            return self.albums.length;
        }
    },
    methods: {
        jumpToRead: function jumpToRead(id, title, des, img_url) {
            (0, _navigator.jumpSubPage)(this, 'read', { url: this.clientUrl + "?id=" + id, id: id, title: title, des: des, img_url: img_url });
        },
        loadData: function loadData() {
            var self = this;
            self.getData();
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
            _syncAblumInfo2.default.receive(function (res) {
                for (var i = 0; i < self.albums.length; i++) {
                    if (res.data.id == self.albums[i].id && res.data.status == 'add') {
                        self.albums[i].status = 1;
                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                    }
                    if (res.data.id == self.albums[i].id && res.data.status == 'cancel') {
                        self.albums[i].status = 0;
                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                    }
                }
            });
        },
        getData: function getData() {
            var self = this;
            (0, _creader.getSubscribeInfo)().then(function (res) {
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined") {
                    var alreadyFollowList = res.data.data.alreadyFollowList;
                    console.log(alreadyFollowList);
                    self.albums = res.data.data.notSubscribedList;
                    self.hotArticles = res.data.data.hotArticleList;
                    self.clientUrl = res.data.data.client_url;
                    self.banners = res.data.data.banners;
                    for (var i = 0; i < self.albums.length; i++) {
                        for (var k = 0; k < alreadyFollowList.length; k++) {
                            if (self.albums[i].id == alreadyFollowList[k].id) {
                                self.albums[i].status = 1;
                            }
                        }
                    }
                    // for(let i = 0; i<self.hotArticles.length; i++){
                    //     self.hotArticles[i].des = "这是用来测试用的文字，虽然很短，但是很有用，😂，再来增加点长度，哈哈";
                    // }
                    self.loadinging = false;
                    self.netStatus = 2;
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        onchange: function onchange(event) {
            console.log('changed:', event.index);
        },
        bannerJump: function bannerJump(id) {
            (0, _navigator.jumpSubPage)(this, 'storeBookDetail', { 'id': id });
        },
        toggleSubscribe: function toggleSubscribe(albumId, index) {
            var _this = this;

            var self = this;
            (0, _user.getOpenId)().then(function (openId) {
                if (openId) {
                    var _loop = function _loop(i) {
                        if (i == index) {
                            if (self.albums[i].status == 0) {
                                (0, _creader.userFollow)(albumId).then(function (res) {
                                    _prompt2.default.toast("订阅成功");
                                    if (res.data.status == 1) {
                                        self.albums[i].status = 1;
                                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                                    }
                                });
                            } else {
                                (0, _creader.userRemoveColumn)(albumId).then(function (res) {
                                    _prompt2.default.toast("取消成功");
                                    if (res.data.status == 1) {
                                        self.albums[i].status = 0;
                                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                                    }
                                });
                            }
                        }
                    };

                    for (var i = 0; i < self.albums.length; i++) {
                        _loop(i);
                    }
                } else {
                    _prompt2.default.toast("请登录");
                    setTimeout(function () {
                        (0, _navigator.jumpSubPage)(_this, 'login');
                    }, 2000);
                }
            });
        },
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        },
        jumpToAuthorColumn: function jumpToAuthorColumn(albumId, status) {
            (0, _navigator.jumpSubPage)(this, 'authorColumn', { id: albumId, status: status });
        }
    }

};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _navigator = __webpack_require__(3);

var _global = __webpack_require__(6);

var _global2 = _interopRequireDefault(_global);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//

exports.default = {
  name: 'topbar',
  data: function data() {
    return {
      global: _global2.default
    };
  },

  props: {
    topbarname: String,
    textStyle: String,
    bgcolor: {
      type: String,
      default: '#f6f6f6'
    }
  },
  methods: {
    goBack: function goBack() {
      (0, _navigator.back)(this, "");
    }
  }
};

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        src: String,
        title: String,
        isLandscape: Boolean
    }
};

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  props: {
    show: {
      type: Boolean,
      default: false
    },
    single: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: ''
    },
    content: {
      type: String,
      default: ''
    },
    top: {
      type: Number,
      default: 400
    },
    cancelText: {
      type: String,
      default: '取消'
    },
    confirmText: {
      type: String,
      default: '确定'
    },
    mainBtnColor: {
      type: String,
      default: '#e72c27'
    },
    secondBtnColor: {
      type: String,
      default: '#666666'
    },
    showNoPrompt: {
      type: Boolean,
      default: false
    },
    noPromptText: {
      type: String,
      default: '不再提示'
    },
    isChecked: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      pageHeight: 1334
    };
  },
  created: function created() {
    var _weex$config$env = weex.config.env,
        deviceHeight = _weex$config$env.deviceHeight,
        deviceWidth = _weex$config$env.deviceWidth;

    this.pageHeight = deviceHeight / deviceWidth * 750;
  },

  methods: {
    secondaryClicked: function secondaryClicked() {
      this.$emit('wxcDialogCancelBtnClicked', {
        type: 'cancel'
      });
    },
    primaryClicked: function primaryClicked(e) {
      this.$emit('wxcDialogConfirmBtnClicked', {
        type: 'confirm'
      });
    }
  }
};

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');
exports.default = {
  props: {
    show: {
      type: Boolean,
      default: true
    },
    hasAnimation: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: Array,
      default: function _default() {
        return ['ease-in', 'ease-out'];
      }
    },
    opacity: {
      type: [Number, String],
      default: 0.6
    },
    canAutoClose: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    overlayStyle: function overlayStyle() {
      return {
        opacity: this.hasAnimation ? 0 : 1,
        backgroundColor: 'rgba(0, 0, 0,' + this.opacity + ')'
      };
    },
    shouldShow: function shouldShow() {
      var _this = this;

      var show = this.show,
          hasAnimation = this.hasAnimation;

      hasAnimation && setTimeout(function () {
        _this.appearOverlay(show);
      }, 50);
      return show;
    }
  },
  methods: {
    overlayClicked: function overlayClicked(e) {
      this.canAutoClose ? this.appearOverlay(false) : this.$emit('wxcOverlayBodyClicked', {});
    },
    appearOverlay: function appearOverlay(bool) {
      var _this2 = this;

      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.duration;
      var hasAnimation = this.hasAnimation,
          timingFunction = this.timingFunction,
          canAutoClose = this.canAutoClose;

      var needEmit = !bool && canAutoClose;
      needEmit && this.$emit('wxcOverlayBodyClicking', {});
      var overlayEl = this.$refs['wxc-overlay'];
      if (hasAnimation && overlayEl) {
        animation.transition(overlayEl, {
          styles: {
            opacity: bool ? 1 : 0
          },
          duration: duration,
          timingFunction: timingFunction[bool ? 0 : 1],
          delay: 0
        }, function () {
          needEmit && _this2.$emit('wxcOverlayBodyClicked', {});
        });
      } else {
        needEmit && this.$emit('wxcOverlayBodyClicked', {});
      }
    }
  }
};

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//


exports.default = {
    props: {
        src: String,
        web: String
    }
};

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _navigator = __webpack_require__(3);

exports.default = {
    // mixins: [mixins],
    props: {
        tabItems: {
            type: Array,
            default: function _default() {
                return [];
            },
            required: true
        },

        styles: {
            type: Object,
            default: function _default() {
                return {};
            },
            required: false
        },

        height: {
            type: String,
            default: '108px'
        },
        network: {
            default: 1
        }
    },

    data: function data() {
        return {
            selectedTab: 0,
            translateX: 'translateX(0px)',
            deviceWidth: 750,
            titleStyle: {}
        };
    },
    created: function created() {
        var self = this;
        self.totalWidth = self.deviceWidth * self.tabItems.length;
    },
    mounted: function mounted() {
        var self = this;
        if (self.network == 1) {
            self.selectedTab = 0;
        } else {
            self.selectedTab = 1;
            self.$emit('wxChange', self.selectedTab);
        }
        // self.totalWidth = self.deviceWidth * self.tabItems.length;
        self.setTranslateX();
    },


    methods: {
        changeTab: function changeTab(item, index) {
            if (3 == index) {
                this.$emit('wxChange', index);
                (0, _navigator.jumpSubPage)(this, 'chat', {});
            } else {
                this.selectedTab = index;
                this.setTranslateX();
                this.$emit('wxChange', index);
            }
        },
        setTranslateX: function setTranslateX() {
            var x = this.selectedTab * 750; //this.deviceWidth;
            this.translateX = 'translateX(-' + x + 'px)';
        },
        getStyles: function getStyles() {
            var baseStyle = {
                'bottom': 0,
                'height': this.height
            };
            return Object.assign({}, baseStyle, this.styles);
        },
        getIconStyle: function getIconStyle(item) {
            return {
                width: item.iconWdith || '48px',
                height: item.iconHeight || '48px'
            };
        },
        getTitleStyle: function getTitleStyle(item) {
            return {
                'font-size': item.fontSize || '28px',
                'color': this.selectedTab === item.index ? item.selectedColor : item.titleColor
            };
        }
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
    "loading": "Loading",
    "networkError": "Unable to get network data, please connect to network and try again",
    "refresh": "Refresh",
    "columnInfoItro": "Album Introduction",
    "suggestion": "Recommended Album",
    "alreadyFollow": "Aready Follow",
    "videoplay": "Video Playing",
    "follow": "follow",
    "videofrom": "VideoFrom",
    "videoList": "Recommended Videos",
    "recommendation": "Excellent Column Recommendation",
    "recommendedvideos": "Excellent Video Recommendation"

};
exports.lang = lang;

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
					"loading": "加載中",
					"networkError": "無法獲取網絡數據 請連接網絡後重試",
					"refresh": "刷新",
					"columnInfoItro": "專輯介紹",
					"suggestion": "專欄推薦",
					"alreadyFollow": "已訂閱專欄",
					"videoplay": "視頻播放",
					"follow": "訂閱",
					"videofrom": "視頻來源",
					"videoList": "本專欄視頻推薦",
					"recommendation": "優秀專欄推薦",
					"recommendedvideos": "優秀視頻推薦"
};
exports.lang = lang;

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
    "loading": "加载中",
    "networkError": "无法获取网络数据 请连接网络后重试",
    "refresh": "刷新",
    "columnInfoItro": "专辑介绍",
    "suggestion": "专栏推荐",
    "alreadyFollow": "已订阅专栏",
    "videoplay": "视频播放",
    "follow": "订阅",
    "videofrom": "视频来源",
    "videoList": "本专栏视频推荐",
    "recommendation": "优秀专栏推荐",
    "recommendedvideos": "优秀视频推荐"
};
exports.lang = lang;

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wx-tabbar[data-v-075dc4af] {\n    width:750px;\n}\n.tab-component[data-v-075dc4af] {\n    flex-direction: row;\n}\n.tabbar[data-v-075dc4af] {\n    width: 750px;\n    position: fixed;\n    left: 0;\n    right: 0;\n    bottom:0;\n    padding-bottom: 15px;\n    z-index: 1000;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    border-top-width: 1px;\n    border-top-style: solid;\n    border-top-color: #D8D8D8;\n    background-color: #fff;\n}\n.tabbar-item[data-v-075dc4af] {\n    flex: 1;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n}\n.icon[data-v-075dc4af] {\n    margin-top: 14px;\n    margin-bottom: 10px;\n    width: 48px;\n    height: 48px;\n}\n.wx-text[data-v-075dc4af] {\n    font-size: 25px;\n    padding-top: 2px;\n    text-align: center;\n    color: #646464;\n}\n", ""]);

// exports


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-search-bar[data-v-16a3652a] {\n  padding-left: 20px;\n  padding-right: 20px;\n  background-color: #ffffff;\n  width: 700px;\n  height: 84px;\n  flex-direction: row;\n}\n.wxc-search-bar-red[data-v-16a3652a] {\n  background-color: #ffc900;\n}\n.search-bar-input[data-v-16a3652a] {\n  position: absolute;\n  top: 10px;\n  padding-top: 0;\n  padding-bottom: 0;\n  padding-right: 40px;\n  padding-left: 60px;\n  font-size: 26px;\n  width: 624px;\n  height: 64px;\n  line-height: 64px;\n  background-color: #E4E5E7;\n  border-radius: 6px;\n  placeholder-color:#A4A5A4;\n}\n.search-bar-input-red[data-v-16a3652a] {\n  background-color: #E4E5E7;\n  placeholder-color: #A4A5A4;\n}\n.search-bar-ICON[data-v-16a3652a] {\n  position: absolute;\n  width: 30px;\n  height: 30px;\n  left: 34px;\n  top: 28px;\n}\n.search-bar-close[data-v-16a3652a] {\n  position: absolute;\n  width: 30px;\n  height: 30px;\n  right: 120px;\n  top: 28px;\n}\n.search-bar-button[data-v-16a3652a] {\n  width: 94px;\n  height: 36px;\n  font-size: 30px;\n  text-align: center;\n  background-color: #ffffff;\n  margin-top: 16px;\n  margin-right: 0;\n  color: #333333;\n  position: absolute;\n  right: 8px;\n  top: 9px;\n}\n.search-bar-button-red[data-v-16a3652a] {\n  background-color: #FFC900;\n}\n.input-has-dep[data-v-16a3652a] {\n  padding-left: 240px;\n  width: 710px;\n}\n.bar-dep[data-v-16a3652a] {\n  width: 170px;\n  padding-right: 12px;\n  padding-left: 12px;\n  height: 42px;\n  align-items: center;\n  flex-direction: row;\n  position: absolute;\n  left: 24px;\n  top: 22px;\n  border-right-style: solid;\n  border-right-width: 1px;\n  border-right-color: #C7C7C7;\n}\n.bar-dep-red[data-v-16a3652a] {\n  border-right-color: #C7C7C7;\n}\n.dep-text[data-v-16a3652a] {\n  flex: 1;\n  text-align: center;\n  font-size: 26px;\n  color: #666666;\n  margin-right: 6px;\n  lines: 1;\n  text-overflow: ellipsis;\n}\n.dep-arrow[data-v-16a3652a] {\n  width: 24px;\n  height: 24px;\n}\n.ICON-has-dep[data-v-16a3652a] {\n  left: 214px;\n}\n.disabled-input[data-v-16a3652a] {\n  width: 750px;\n  height: 64px;\n  position: absolute;\n  left: 0;\n  background-color: transparent;\n}\n.has-dep-disabled[data-v-16a3652a] {\n  width: 550px;\n  left: 200px;\n}\n", ""]);

// exports


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-17d335ea]{\n    /*position: absolute;\n    left:0;\n    top:0;\n    right:0;\n    bottom:0;*/\n    background-color: #f5f5f5;\n    /*background-color: green;*/\n    width:750px;\n}\n.scroller[data-v-17d335ea]{\n    position: absolute;\n    left:0;\n    top:0;\n    right:0;\n    bottom:120px;\n}\n.slider[data-v-17d335ea] {\n    height:320px;\n    width:720px;\n    margin-left:15px;\n    margin-top:20px;\n    /*border-radius: 10px;*/\n}\n.image[data-v-17d335ea] {\n    width: 720px;\n    height:320px;\n    border-radius: 10px;\n}\n.frame[data-v-17d335ea] {\n    width: 720px;\n    height:320px;\n    position:relative;\n}\n.indicator[data-v-17d335ea] {\n   width: 200px;\n   height: 30px;\n   item-color: white;\n   item-selected-color: #e72c27;\n   item-size: 20px;\n   position: absolute;\n   top: 290px;\n   left: 275px;\n}\n.suggestionBar[data-v-17d335ea]{\n     width:750px;\n     height:116px;\n     display: flex;\n     flex-direction: row;\n     align-items: center;\n     padding-left: 20px;\n     padding-right: 20px;\n     border-top-width:20px;\n     border-top-style: solid;\n     border-top-color: white;\n     justify-content: space-between;\n     background-color:#f6f6f6;\n}\n.suggestionBar1[data-v-17d335ea]{\n    width:750px;\n     height:96px;\n     display: flex;\n     flex-direction: row;\n     align-items: center;\n     padding-left: 20px;\n     padding-right: 20px;\n     justify-content: space-between;\n     background-color:#f6f6f6;\n}\n.text_suggestion[data-v-17d335ea]{\n    color: #959595;\n    font-size:31.83px;\n}\n.text_suggestion_icon[data-v-17d335ea]{\n    color: #959595;\n    font-size: 31.83px;\n}\n.suggestionList[data-v-17d335ea]{\n    height:160px;\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: center;\n   /* border-bottom-width:1px;\n    border-bottom-style: solid;\n    border-bottom-color: #e5e5e5;\n    border-top-width:1px;\n    border-top-style: solid;\n    border-top-color: #e5e5e5;*/\n    background-color: #ffffff;\n    /*background-color: yellow;*/\n}\n.album_image[data-v-17d335ea]{\n    width:100px;\n    height:100px;\n    border-radius: 50px;\n    margin-left: 15px;\n    margin-right:15px;\n}\n.albumInfo[data-v-17d335ea]{\n    width:600px;\n    height:160px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    /*margin-top: 15px;*/\n    margin-right: 35px;\n    margin-left:20px;\n    border-bottom-width:1px;\n    border-bottom-style: solid;\n    border-bottom-color: #e5e5e5;\n    /*background-color: lightblue;*/\n}\n.albumInfo_last[data-v-17d335ea]{\n    width:600px;\n    height:160px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    /*margin-top: 15px;*/\n    margin-right: 35px;\n    margin-left:20px;\n}\n.albumInfoDetail[data-v-17d335ea]{\n    height:100px;\n    width:470px;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: flex-start;\n    padding-bottom: 10px;\n    /*background-color: lightgreen;*/\n}\n.album_NS[data-v-17d335ea]{\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: center;\n    padding-top: 5px;\n}\n.album_subscribed[data-v-17d335ea]{\n    border:1px solid grey;\n    border-width:1px;\n    border-style: solid;\n    border-color: #D2D2D2;\n    border-radius: 3px;\n    padding: 5px;\n    margin-left: 30px;\n}\n.text_album_subscribed[data-v-17d335ea]{\n    font-size: 18px;\n    color:#888888;\n}\n.text_album_name[data-v-17d335ea]{\n    font-size: 40px;\n    font-weight: bold;\n    padding-bottom: 8px;\n}\n.album_des[data-v-17d335ea]{\n    color: #888888;\n    font-size: 25px;\n    padding-top: 8px;\n    lines:1;\n}\n.subscribeBtn[data-v-17d335ea]{\n    width:110px;\n    height:60px;\n    background-color: #e72c27;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-right: 30px;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 5px;\n}\n.subscribeBtn-subscribed[data-v-17d335ea]{\n    width:110px;\n    height:60px;\n    background-color: white;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    margin-right: 30px;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 5px;\n}\n.text_subscribeBtn[data-v-17d335ea]{\n    color:white;\n    font-size: 25px;\n}\n.img_subscribeBtn[data-v-17d335ea]{\n    width:35px;\n    height:35px;\n}\n.title[data-v-17d335ea]{\n    color:#2f2f2f;\n    font-size:40px;\n    font-family: \"Helvetica Neue\",sans-serif;\n    font-weight: bold;\n    line-height: 60px;\n    padding-top: 20px;\n    padding-bottom: 15px;\n    /*letter-spacing: 10;*/\n}\n.article-des[data-v-17d335ea]{\n    color:#8f8f8f;\n    font-size: 29px;\n    line-height: 40px;\n    text-overflow: ellipsis;\n    font-family: Segoe UI;\n    lines:2;\n}\n.article-mid[data-v-17d335ea]{\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: center;\n    padding-left: 33px;\n    padding-right: 33px;\n    background-color: #ffffff;\n    /*background-color: lightgreen;*/\n    margin-bottom: 19px;\n}\n.article-content[data-v-17d335ea]{\n    flex-direction: column;\n    flex-wrap: nowrap;\n    justify-content: center;\n    align-items: flex-start;\n    width:500px;\n    padding-top:30px;\n    padding-bottom: 30px;\n    padding-right: 34px;\n    font-family: Segoe UI;\n    /*background-color: lightblue;*/\n}\n\n", ""]);

// exports


/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.video[data-v-1a32ac6f] {\n    width: 750px;\n    height: 480px;\n}\n", ""]);

// exports


/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.loadingData{\n    position: absolute;\n    top: 0px;\n    left:0;\n    right:0;\n    bottom:0;\n    justify-content: center;\n    align-items: center;\n    background-color: rgba(0,0,0,0.2);\n}\n.loading {\n    width: 750;\n    display: -ms-flex;\n    display: -webkit-flex;\n    display: flex;\n    -ms-flex-align: center;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    align-items: center;\n}\n.indicator-text {\n    color: #e72c27;\n    font-size: 42px;\n    text-align: center;\n}\n.indicator {\n    margin-top: 16px;\n    height: 40px;\n    width: 40px;\n    color: #e72c27;\n}\n", ""]);

// exports


/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.loadingData{\n    position: absolute;\n    bottom: 0;\n    left:0;\n    right:0;\n    justify-content: flex-start;\n    align-items: center;\n    /*background-color: green;*/\n}\n.loading {\n    width: 750;\n    display: -ms-flex;\n    display: -webkit-flex;\n    display: flex;\n    -ms-flex-align: center;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    align-items: center;\n}\n.indicator-text {\n    color: #e72c27;\n    font-size: 42px;\n    text-align: center;\n}\n.indicator {\n    margin-top: 16px;\n    height: 40px;\n    width: 40px;\n    color: #e72c27;\n}\n", ""]);

// exports


/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-50b33d5c]{\n    background-color: #f5f5f5;\n    width:750px;\n}\n.list[data-v-50b33d5c]{\n    width:750px;\n    background-color: #f5f5f5;\n}\n.article[data-v-50b33d5c]{\n    /*margin-bottom: 19px;*/\n    border-bottom-width: 19px;\n    border-bottom-style: solid;\n    border-bottom-color: #f5f5f5;\n    background-color: #ffffff;\n}\n.article-top[data-v-50b33d5c]{\n    height:98px;\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: center;\n    border-bottom-width: 2px;\n    border-bottom-style: solid;\n    border-bottom-color: #ececec;\n    padding-left: 33px;\n    padding-right: 33px;\n    font-family: Segoe UI;\n    /*background-color: lightgreen;*/\n}\n.article-name[data-v-50b33d5c]{\n    color:#252525;\n    font-size:26px;\n    margin-left:33px;\n    font-weight: bold;\n    font-family: Segoe UI;\n    /*letter-spacing: 13px;*/\n    line-height: 45px;\n}\n.article-tag[data-v-50b33d5c]{\n    color:#888888;\n    font-size: 25px;\n    margin-left:20px;\n    font-family: Segoe UI;\n    /*background-color: yellow;*/\n}\n.article-time[data-v-50b33d5c]{\n    width:150px;\n    margin-left: 20px;\n    color:#888888;\n    font-size: 22px;\n    text-align: right;\n    font-family: Segoe UI;\n    /*background-color: red;*/\n}\n.title[data-v-50b33d5c]{\n    color:#2f2f2f;\n    font-size:40px;\n    font-family: \"Helvetica Neue\",sans-serif;\n    font-weight: bold;\n    line-height: 60px;\n    padding-top: 20px;\n    padding-bottom: 15px;\n    lines:2;\n    /*letter-spacing: 10;*/\n}\n.article-des[data-v-50b33d5c]{\n    color:#8f8f8f;\n    font-size: 29px;\n    line-height: 40px;\n    text-overflow: ellipsis;\n    font-family: Segoe UI;\n    lines:2;\n}\n.article-mid[data-v-50b33d5c]{\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: center;\n    padding-left: 33px;\n    padding-right: 33px;\n    /*background-color: lightgreen;*/\n}\n.article-content[data-v-50b33d5c]{\n    flex-direction: column;\n    flex-wrap: nowrap;\n    justify-content: center;\n    align-items: flex-start;\n    width:500px;\n    padding-top:30px;\n    padding-bottom: 30px;\n    padding-right: 34px;\n    font-family: Segoe UI;\n    /*background-color: lightblue;*/\n}\n.loading[data-v-50b33d5c] {\n    width: 750;\n    display: -ms-flex;\n    display: -webkit-flex;\n    display: flex;\n    -ms-flex-align: center;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    align-items: center;\n}\n.indicator-text[data-v-50b33d5c] {\n    color: #e72c27;\n    font-size: 42px;\n    text-align: center;\n}\n.indicator[data-v-50b33d5c] {\n    margin-top: 16px;\n    height: 40px;\n    width: 40px;\n    color: #e72c27;\n}\n.loadingData[data-v-50b33d5c]{\n    position: absolute;\n    top:0;\n    bottom: 0;\n    left:0;\n    right:0;\n    justify-content: center;\n    align-items: center;\n}\n\n", ""]);

// exports


/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.topbar[data-v-540c97a4]{\n  position: fixed;\n  top:0;\n  width: 750px;\n  height:100px;\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  /*background-color: #f6f6f6;*/\n}\n.backButton[data-v-540c97a4]{\n  width:100px;\n  height:100px;\n  text-align: center;\n  line-height:100px;\n  font-size: 40px;\n  position: absolute;\n  left:0px;\n}\n.appName[data-v-540c97a4]{\n  font-size: 45px;\n  font-weight: bold;\n  line-height: 100px;\n  font-family: \"SimSun\";\n}\n", ""]);

// exports


/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-584cd5ee]{\n     width:750px;\n}\n.top[data-v-584cd5ee]{\n     position: absolute;\n     top:0;\n     left: 0;\n     right: 0;\n     flex-direction: row;\n     flex-wrap: nowrap;\n     justify-content: space-around;\n     align-items: center;\n     width:750px;\n     background-color: #fdfdfd;\n     /*border-bottom-color: #e9e9e9;\n     border-bottom-width: 2px;\n     border-bottom-style: solid;*/\n     /* background-color: red; */\n}\n.type[data-v-584cd5ee]{\n     font-size: 38px;\n     color:#636363;\n     font-weight: bold;\n}\n.content[data-v-584cd5ee]{\n     position: relative;\n     top:102px;\n     right:0px;\n     left:0px;\n     /*flex-direction: row;\n     flex-wrap: wrap;\n     justify-content: flex-start;\n     align-items: flex-start;*/\n}\n.alreadyFollowList_album[data-v-584cd5ee]{\n     width:200px;\n     height: 320px;\n     margin-left:37px;\n     margin-top: 37px;\n     flex-direction: column;\n     justify-content: flex-start;\n     align-items: center;\n     /*background-color: red;*/\n}\n.alreadyFollowList_album_name[data-v-584cd5ee]{\n     font-size: 26px;\n     margin-top: 26px;\n     margin-bottom: 12px;\n     margin-left: 12px;\n     margin-right: 12px;\n     color:#505050;\n}\n", ""]);

// exports


/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body{\n        /*position: absolute;\n        left:0;\n        top:0;\n        right:0;\n        bottom:0;*/\n        background-color: #f8f8f8;\n        width:750px;\n}\n.suggestionBar{\n        width:750px;\n        height:96px;\n        display: flex;\n        flex-direction: row;\n        align-items: center;\n        padding-left: 20px;\n        padding-right: 20px;\n        justify-content: space-between;\n        background-color:#ffffff;\n}\n.suggestionBar1{\n        width:750px;\n        /*height:100px;*/\n        margin-top:20px;\n        flex-direction: row;\n        align-items: center;\n        padding-left: 20px;\n        padding-right: 20px;\n        padding-top: 30px;\n        padding-bottom: 30px;\n        justify-content: space-between;\n        background-color:#ffffff;\n}\n.top_img{\n         width:48px;\n         height:48px;\n}\n.top_img1{\n         width:36px;\n         height:36px;\n}\n.top_icon{\n        width:48px;\n        height:48px;\n        /*background-color: yellow;*/\n}\n.top_icon1{\n        width:36px;\n        height:36px;\n        /*background-color: yellow;*/\n}\n.suggestionList{\n        height:195px;\n        flex-direction: row;\n        justify-content: flex-start;\n        align-items: center;\n        padding:20px;\n        background-color: #ffffff;\n}\n.album_image{\n        width:130px;\n        height:130px;\n        border-radius: 50px;\n        margin-right:20px;\n        /*background-color: green;*/\n}\n.albumInfo{\n        width:580px;\n        height:120px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        padding: 20px;\n        align-items: center;\n        margin-top: 15px;\n        margin-bottom: 15px;\n        margin-right: 35px;\n        /*background-color: lightblue;*/\n}\n.albumInfoDetail{\n        height:120px;\n        width:470px;\n        display: flex;\n        flex-direction: column;\n        justify-content: space-between;\n        align-items: flex-start;\n        padding-bottom: 10px;\n        /*background-color: lightgreen;*/\n}\n.album_NS{\n        display: flex;\n        flex-direction: row;\n        justify-content: flex-start;\n        align-items: center;\n        padding-top: 5px;\n}\n.album_subscribed{\n        border:1px solid grey;\n        border-width:1px;\n        border-style: solid;\n        border-color: #D2D2D2;\n        border-radius: 3px;\n        padding: 5px;\n        margin-left: 30px;\n}\n.text_album_subscribed{\n        font-size: 18px;\n        color:#888888;\n}\n.text_album_name{\n        font-size: 40px;\n        font-weight: bold;\n        color:#202020;\n        padding-bottom: 10px;\n}\n.album_des{\n        color: #9e9e9e;\n        font-size: 29px;\n        padding-top: 8px;\n}\n.img_subscribeBtn{\n        width:36px;\n        height:36px;\n        /*background-color: yellow;*/\n}\n", ""]);

// exports


/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-overlay[data-v-5dd495a2] {\n  width: 750px;\n  position: fixed;\n  left: 0;\n  top: 0;\n  bottom: 0;\n  right: 0;\n}\n", ""]);

// exports


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-tab-page[data-v-62ffd2f8] {\n  height:102px;\n  width: 750px;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  border-bottom-style: solid;\n  border-bottom-width: 2px;\n  border-bottom-color: #f0f0f0;\n  /*box-shadow:  0 15px 30px rgba(0, 0, 0, 0.2);*/\n}\n.title-item[data-v-62ffd2f8] {\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-bottom-style:solid;\n  \n  border-bottom-width:3px;\n  /* border-bottom-color:black; */\n}\n.tab-text[data-v-62ffd2f8]{\n  height:102px;\n  font-weight: bold;\n  line-height: 102px;\n}\n\n", ""]);

// exports


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.netDivError{\n    width:750px;\n    display: flex;\n    flex-flow: column;\n    align-items:center;\n    justify-content: center;\n    height:300px;\n}\n.netTextError{\n    padding-top: 10px;\n    padding-bottom: 10px;\n    font-size:35px;\n}\n.button{\n    width: 280px;\n    height:80px;\n    border-width: 1px;\n    border-color: black;\n    border-style:solid;\n    text-align: center;\n    line-height:80px;\n    font-size: 35px;\n    border-radius: 10px;\n    transform: translateY(50px);\n}\n", ""]);

// exports


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-progress[data-v-7b08ba3d] {\n    background-color: #f2f3f4;\n}\n.progress[data-v-7b08ba3d] {\n    position: absolute;\n    background-color: #FFC900;\n}\n", ""]);

// exports


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n/*.test {*/\n\n    /*height: 500px;*/\n/*}*/\n", ""]);

// exports


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.netError{\n    position: absolute;\n    top:0;\n    bottom: 0;\n    left:0;\n    right:0;\n    justify-content: center;\n    align-items: center;\n}\n.netError1{\n    justify-content: center;\n    align-items: center;\n}\n.netError2{\n    justify-content: center;\n    align-items: center;\n}\n.errorHint{\n    font-size: 30;\n    color:#8f8f8f;\n    margin-top:30px;\n}\n.refresh{\n    margin-top:50px;\n    font-size: 35;\n    color:white;\n    width:550;\n    height:80;\n    background-color: #e72c27;\n    text-align: center;\n    line-height: 80;\n    border-radius: 30;\n    box-shadow: 0 15px 30px rgba(0, 0, 0, 0.2);\n}\n", ""]);

// exports


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.container[data-v-d88579ee] {\n  position: fixed;\n  width: 750px;\n  /*兼容H5异常*/\n  z-index: 99999;\n}\n.dialog-box[data-v-d88579ee] {\n  position: fixed;\n  left: 96px;\n  width: 558px;\n  background-color: #FFFFFF;\n}\n.dialog-content[data-v-d88579ee] {\n  padding-top: 36px;\n  padding-bottom: 36px;\n  padding-left: 36px;\n  padding-right: 36px;\n}\n.content-title[data-v-d88579ee] {\n  color: #333333;\n  font-size: 36px;\n  text-align: center;\n  margin-bottom: 24px;\n}\n.content-subtext[data-v-d88579ee] {\n  color: #666666;\n  font-size: 26px;\n  line-height: 36px;\n  text-align: center;\n}\n.dialog-footer[data-v-d88579ee] {\n  flex-direction: row;\n  align-items: center;\n  border-top-color: #F3F3F3;\n  border-top-width: 1px;\n}\n.footer-btn[data-v-d88579ee] {\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n  flex: 1;\n  height: 90px;\n}\n.cancel[data-v-d88579ee] {\n  border-right-color: #F3F3F3;\n  border-right-width: 1px;\n}\n.btn-text[data-v-d88579ee] {\n  font-size: 36px;\n  color: #666666;\n}\n.no-prompt[data-v-d88579ee] {\n  width: 486px;\n  align-items: center;\n  justify-content: center;\n  flex-direction: row;\n  margin-top: 24px;\n}\n.no-prompt-icon[data-v-d88579ee] {\n  width: 24px;\n  height: 24px;\n  margin-right: 12px;\n}\n.no-prompt-text[data-v-d88579ee] {\n  font-size: 24px;\n  color: #A5A5A5;\n}\n", ""]);

// exports


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n", ""]);

// exports


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body{\n        /*position: absolute;\n        left:0;\n        top:0;\n        right:0;\n        bottom:0;*/\n        width:750px;\n        /*background-color: purple;*/\n}\n", ""]);

// exports


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wx-tabbar"
  }, [_c('div', {
    staticClass: "tab-component",
    style: ({
      'transform': _vm.translateX,
      width: _vm.totalWidth + 'px'
    })
  }, [_vm._t("default")], 2), _vm._v(" "), _c('div', {
    staticClass: "tabbar",
    style: (_vm.getStyles())
  }, _vm._l((_vm.tabItems), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "tabbar-item",
      on: {
        "click": function($event) {
          _vm.changeTab(item, index)
        }
      }
    }, [_c('image', {
      staticClass: "icon",
      style: (_vm.getIconStyle(item)),
      attrs: {
        "src": _vm.selectedTab === item.index ? item.selectedImage : item.image
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "wx-text",
      style: (_vm.getTitleStyle(item))
    }, [_vm._v(_vm._s(item.title))])])
  }))])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-075dc4af", module.exports)
  }
}

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.mod === 'default') ? _c('div', {
    class: ['wxc-search-bar', 'wxc-search-bar-' + _vm.theme]
  }, [_c('input', {
    ref: "search-input",
    class: ['search-bar-input', 'search-bar-input-' + _vm.theme],
    style: ({
      width: _vm.needShowCancel ? '574px' : '660px'
    }),
    attrs: {
      "autofocus": _vm.autofocus,
      "disabled": _vm.disabled,
      "type": _vm.inputType,
      "placeholder": _vm.placeholder
    },
    domProps: {
      "value": _vm.value
    },
    on: {
      "blur": _vm.onBlur,
      "input": _vm.onInput,
      "focus": _vm.onFocus
    }
  }), _vm._v(" "), _c('image', {
    staticClass: "search-bar-ICON",
    attrs: {
      "aria-hidden": true,
      "src": _vm.getImgPath('search.png')
    }
  }), _vm._v(" "), (_vm.needShowCancel) ? _c('text', {
    class: ['search-bar-button', 'search-bar-button-' + _vm.theme],
    on: {
      "click": _vm.searchClicked
    }
  }, [_vm._v("搜索 ")]) : _vm._e()]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-16a3652a", module.exports)
  }
}

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    style: ({
      height: _vm.height
    })
  }, [(_vm.netStatus == 2) ? _c('scroller', {
    staticClass: "scroller",
    style: ({
      height: _vm.height
    })
  }, [_c('slider', {
    staticClass: "slider",
    attrs: {
      "interval": "5000",
      "infinite": "true",
      "auto-play": "true"
    },
    on: {
      "change": _vm.onchange
    }
  }, [_vm._l((_vm.banners), function(banner) {
    return _c('div', {
      staticClass: "frame"
    }, [_c('image', {
      staticClass: "image",
      attrs: {
        "resize": "contain",
        "src": banner.img_url
      },
      on: {
        "click": function($event) {
          _vm.bannerJump(banner.article_id)
        }
      }
    })])
  }), _vm._v(" "), _c('indicator', {
    staticClass: "indicator"
  })], 2), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar"
  }, [_c('text', {
    staticClass: "text_suggestion"
  }, [_vm._v("编辑推荐专栏")]), _vm._v(" "), _c('text', {
    staticClass: "text_suggestion_icon"
  }, [_vm._v(">")])]), _vm._v(" "), _vm._l((_vm.albums), function(item, index) {
    return _c('div', {
      staticClass: "suggestionList"
    }, [_c('image', {
      staticClass: "album_image",
      attrs: {
        "src": item.img_url
      },
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.id, item.status)
        }
      }
    }), _vm._v(" "), _c('div', {
      class: [_vm.Id === index + 1 ? 'albumInfo_last' : 'albumInfo']
    }, [_c('div', {
      staticClass: "albumInfoDetail",
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.id, item.status)
        }
      }
    }, [_c('div', {
      staticClass: "album_NS"
    }, [_c('div', {
      staticClass: "album_name"
    }, [_c('text', {
      staticClass: "text_album_name"
    }, [_vm._v(_vm._s(item.name))])]), _vm._v(" "), _c('div', {
      staticClass: "album_subscribed"
    }, [_c('text', {
      staticClass: "text_album_subscribed"
    }, [_vm._v(_vm._s(item.follow_count) + "人订阅")])])]), _vm._v(" "), _c('div', [_c('text', {
      staticClass: "album_des"
    }, [_vm._v(_vm._s(item.author))])])]), _vm._v(" "), (item.status == 1) ? _c('div', {
      staticClass: "subscribeBtn-subscribed",
      on: {
        "click": function($event) {
          _vm.toggleSubscribe(item.id, index)
        }
      }
    }, [_c('image', {
      staticClass: "img_subscribeBtn",
      attrs: {
        "src": _vm.getImgPath('followok.png')
      }
    })]) : _c('div', {
      staticClass: "subscribeBtn",
      on: {
        "click": function($event) {
          _vm.toggleSubscribe(item.id, index)
        }
      }
    }, [_c('text', {
      staticClass: "text_subscribeBtn"
    }, [_vm._v("订阅")])])])])
  }), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1"
  }, [_c('text', {
    staticClass: "text_suggestion"
  }, [_vm._v("优秀文章推荐")])]), _vm._v(" "), _vm._l((_vm.hotArticles), function(item) {
    return _c('div', {
      staticClass: "article-mid",
      on: {
        "click": function($event) {
          _vm.jumpToRead(item.id, item.title, item.des, item.img_url)
        }
      }
    }, [_c('div', {
      staticClass: "article-content"
    }, [_c('text', {
      staticClass: "title"
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('text', {
      staticClass: "article-des"
    }, [_vm._v(_vm._s(item.des))])]), _vm._v(" "), _c('image', {
      staticStyle: {
        "width": "184px",
        "height": "184px",
        "border-radius": "5px"
      },
      attrs: {
        "src": item.img_url,
        "resize": "cover"
      }
    })])
  })], 2) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingData') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-17d335ea", module.exports)
  }
}

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('video', {
    staticClass: "video",
    attrs: {
      "id": "videPlay",
      "src": _vm.src,
      "autoplay": "",
      "controls": ""
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1a32ac6f", module.exports)
  }
}

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: {
      "display": "none"
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-32957998", module.exports)
  }
}

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.loadFinished) ? _c('image', {
    staticStyle: {
      "width": "160px",
      "height": "220px",
      "border-radius": "5px"
    },
    attrs: {
      "src": _vm.source
    },
    on: {
      "load": _vm.onImageLoad
    }
  }) : _vm._e(), _vm._v(" "), (_vm.loadFinished == false) ? _c('image', {
    staticStyle: {
      "width": "160px",
      "height": "220px",
      "border-radius": "5px"
    },
    attrs: {
      "src": _vm.backgroup
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-38bd612a", module.exports)
  }
}

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loadingData"
  }, [_c('div', {
    staticStyle: {
      "backgroundColor": "rgba(255,255,255,0.3)",
      "height": "300px",
      "width": "300px",
      "justify-content": "center",
      "align-items": "center",
      "borderRadius": "10px"
    }
  }, [_c('image', {
    staticStyle: {
      "width": "80px",
      "height": "80px"
    },
    attrs: {
      "src": _vm.getImgPath('load.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "font-size": "30px",
      "color": "#8f8f8f",
      "margin-top": "20px"
    }
  }, [_vm._v("提交中...")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-42cf8c96", module.exports)
  }
}

/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loadingData",
    style: ({
      top: _vm.height
    })
  }, [_c('image', {
    staticStyle: {
      "width": "80px",
      "height": "80px"
    },
    attrs: {
      "src": _vm.getImgPath('load.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "font-size": "30px",
      "color": "#8f8f8f",
      "margin-top": "20px"
    }
  }, [_vm._v("正在为您努力加载数据中...")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-479bbfa6", module.exports)
  }
}

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    style: ({
      height: _vm.height
    })
  }, [(_vm.netStatus == 2) ? _c('list', {
    staticClass: "list",
    style: ({
      height: _vm.height
    })
  }, [_vm._l((_vm.articles), function(item) {
    return _c('cell', {
      staticClass: "article"
    }, [_c('div', {
      staticClass: "article-top"
    }, [_c('div', {
      staticStyle: {
        "flex-direction": "row",
        "justify-content": "space-between",
        "align-items": "center"
      },
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.album_name, item.album_id)
        }
      }
    }, [_c('image', {
      staticStyle: {
        "width": "51px",
        "height": "51px",
        "border-radius": "20px"
      },
      attrs: {
        "src": item.album_img_url
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "article-name"
    }, [_vm._v(_vm._s(item.album_name))]), _vm._v(" "), _c('text', {
      staticClass: "article-tag"
    }, [_vm._v("▸")])]), _vm._v(" "), _c('text', {
      staticClass: "article-time"
    }, [_vm._v(_vm._s(item.publish_dated))])]), _vm._v(" "), _c('div', {
      staticClass: "article-mid",
      on: {
        "click": function($event) {
          _vm.jumpToRead(item.id, item.title, item.des, item.img_url)
        }
      }
    }, [_c('div', {
      staticClass: "article-content"
    }, [_c('text', {
      staticClass: "title"
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('text', {
      staticClass: "article-des"
    }, [_vm._v(_vm._s(item.des))])]), _vm._v(" "), _c('image', {
      staticStyle: {
        "width": "184px",
        "height": "184px",
        "border-radius": "5px"
      },
      attrs: {
        "src": item.img_url,
        "resize": "stretch"
      }
    })])])
  }), _vm._v(" "), _c('loading', {
    staticClass: "loading",
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": _vm.loadMore
    }
  }, [_c('text', {
    staticClass: "indicator-text"
  }, [_vm._v("Loading ...")]), _vm._v(" "), _c('loading-indicator', {
    staticClass: "indicator"
  })], 1)], 2) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingData') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-50b33d5c", module.exports)
  }
}

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "topbar",
    style: ({
      backgroundColor: _vm.bgcolor
    })
  }, [_c('text', {
    staticClass: "backButton",
    style: ({
      color: _vm.textStyle
    }),
    on: {
      "click": _vm.goBack
    }
  }, [_vm._v("←")]), _vm._v(" "), _c('text', {
    staticClass: "appName",
    style: ({
      color: _vm.textStyle
    }),
    attrs: {
      "topbarname": _vm.topbarname
    }
  }, [_vm._v(_vm._s(_vm.topbarname))])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-540c97a4", module.exports)
  }
}

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    style: ({
      height: _vm.height
    })
  }, [_c('div', {
    staticClass: "top"
  }, [_c('libraryNav', {
    attrs: {
      "currentPosition": _vm.currentPosition,
      "isShelf": _vm.isShelf
    }
  })], 1), _vm._v(" "), (_vm.netStatus == 2) ? _c('waterfall', {
    ref: "waterfall",
    staticClass: "content",
    staticStyle: {
      "padding": "0"
    },
    style: ({
      height: _vm.height - 122
    }),
    attrs: {
      "loadmoreoffset": "3000",
      "column-width": _vm.auto,
      "column-count": 3,
      "column-gap": 12,
      "show-scrollbar": false,
      "scrollable": "true"
    },
    on: {
      "loadmore": function($event) {
        _vm.getMoreInfo()
      }
    }
  }, _vm._l((_vm.books), function(item, index) {
    return _c('cell', {
      staticClass: "book"
    }, [_c('div', {
      key: index,
      staticClass: "alreadyFollowList_album",
      on: {
        "click": function($event) {
          _vm.jumpToBookProfile(item.id, item.title, item.address, item.img_url)
        },
        "longpress": function($event) {
          _vm.openPopUp(item.id, index)
        }
      }
    }, [_c('image', {
      staticStyle: {
        "width": "160px",
        "height": "220px",
        "border-radius": "5px"
      },
      attrs: {
        "src": item.img_url,
        "placeholder": _vm.getImgPath('cover_default_new.png')
      }
    }), _vm._v(" "), (item.progress.visible == false) ? _c('text', {
      staticClass: "alreadyFollowList_album_name"
    }, [_vm._v(_vm._s(item.title))]) : _vm._e(), _vm._v(" "), (item.progress.visible) ? _c('progress', {
      staticStyle: {
        "margin-top": "20px"
      },
      attrs: {
        "bar-width": 200,
        "barHeight": 20
      },
      domProps: {
        "value": item.progress.value
      }
    }) : _vm._e()])])
  })) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingData') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e(), _vm._v(" "), (_vm.show) ? _c('weexOverlay', {
    attrs: {
      "show": true,
      "hasAnimation": false
    }
  }) : _vm._e(), _vm._v(" "), _c('weexDialogue', {
    attrs: {
      "title": _vm.title,
      "content": _vm.content,
      "confirm-text": _vm.confirmText,
      "cancel-text": _vm.cancelText,
      "show": _vm.show,
      "single": _vm.single
    },
    on: {
      "wxcDialogCancelBtnClicked": _vm.dialogCancelBtnClick,
      "wxcDialogConfirmBtnClicked": function($event) {
        _vm.dialogConfirmBtnClick()
      }
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-584cd5ee", module.exports)
  }
}

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    style: ({
      height: _vm.height
    })
  }, [(_vm.netStatus == 2) ? _c('div', [_c('div', {
    staticClass: "suggestionList"
  }, [_c('image', {
    staticClass: "album_image",
    attrs: {
      "src": _vm.personInfo.head_icon
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "albumInfo"
  }, [_c('div', {
    staticClass: "albumInfoDetail",
    on: {
      "click": _vm.goToPersonalProfile
    }
  }, [_c('div', {
    staticClass: "album_NS"
  }, [_c('div', {
    staticClass: "album_name"
  }, [_c('text', {
    staticClass: "text_album_name"
  }, [_vm._v(_vm._s(_vm.personInfo.nickName))])])]), _vm._v(" "), _c('div', [_c('text', {
    staticClass: "album_des"
  }, [_vm._v(_vm._s(_vm.personInfo.profile))])])]), _vm._v(" "), _c('image', {
    staticClass: "img_subscribeBtn",
    attrs: {
      "src": _vm.getImgPath('go.png')
    },
    on: {
      "click": _vm.goToPersonalProfile
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    on: {
      "click": _vm.goToMySubscribe
    }
  }, [_c('div', {
    staticStyle: {
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }
  }, [_c('image', {
    staticClass: "top_img",
    attrs: {
      "src": _vm.getImgPath('focus.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }
  }, [_vm._v("我的订阅")])]), _vm._v(" "), _c('image', {
    staticClass: "top_icon1",
    attrs: {
      "src": _vm.getImgPath('go.png')
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    on: {
      "click": _vm.goToMyLists
    }
  }, [_c('div', {
    staticStyle: {
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }
  }, [_c('image', {
    staticClass: "top_img",
    attrs: {
      "src": _vm.getImgPath('articles.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }
  }, [_vm._v("我的收藏")])]), _vm._v(" "), _c('image', {
    staticClass: "top_icon1",
    attrs: {
      "src": _vm.getImgPath('go.png')
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    on: {
      "click": _vm.goToFeedback
    }
  }, [_c('div', {
    staticStyle: {
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }
  }, [_c('image', {
    staticClass: "top_img",
    attrs: {
      "src": _vm.getImgPath('feedback.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }
  }, [_vm._v("反馈")])]), _vm._v(" "), _c('image', {
    staticClass: "top_icon1",
    attrs: {
      "src": _vm.getImgPath('go.png')
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    on: {
      "click": _vm.goToShare
    }
  }, [_c('div', {
    staticStyle: {
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }
  }, [_c('image', {
    staticClass: "top_img",
    attrs: {
      "src": _vm.getImgPath('setting_push.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }
  }, [_vm._v("分享给朋友")])])])]) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingData') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e(), _vm._v(" "), _c('commonFun', {
    ref: "commonFun"
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5b02b228", module.exports)
  }
}

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.show) ? _c('div', {
    ref: "wxc-overlay",
    staticClass: "wxc-overlay",
    style: (_vm.overlayStyle),
    attrs: {
      "hack": _vm.shouldShow
    },
    on: {
      "click": _vm.overlayClicked
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5dd495a2", module.exports)
  }
}

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wxc-tab-page"
  }, [(_vm.isShelf) ? _c('image', {
    staticStyle: {
      "width": "35px",
      "height": "35px",
      "border-radius": "5px"
    },
    attrs: {
      "src": _vm.getImgPath('backarrow.png')
    },
    on: {
      "click": _vm.back
    }
  }) : _vm._e(), _vm._v(" "), _vm._l((_vm.topNav), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "title-item",
      style: ({
        borderBottomColor: _vm.currentPosition == index ? _vm.tabStyles.activeBorderBottomColor : 'rgba(0, 0, 0, 0)'
      }),
      on: {
        "click": function($event) {
          _vm.setPage(index)
        }
      }
    }, [_c('text', {
      staticClass: "tab-text",
      style: ({
        fontSize: _vm.tabStyles.fontSize + 'px',
        fontWeight: (_vm.currentPosition == index && _vm.tabStyles.isActiveTitleBold) ? 'bold' : 'bold',
        color: _vm.currentPosition == index ? _vm.tabStyles.activeTitleColor : _vm.tabStyles.titleColor,
        paddingLeft: _vm.tabStyles.textPaddingLeft + 'px',
        paddingRight: _vm.tabStyles.textPaddingRight + 'px'
      })
    }, [_vm._v(_vm._s(item))])])
  }), _vm._v(" "), _c('image', {
    staticStyle: {
      "width": "35px",
      "height": "35px",
      "border-radius": "5px"
    },
    attrs: {
      "src": _vm.getImgPath('search.png')
    },
    on: {
      "click": _vm.jumpToLibrarySearch
    }
  })], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-62ffd2f8", module.exports)
  }
}

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "netDivError"
  }, [_c('text', {
    staticClass: "netTextError",
    attrs: {
      "lines": "2"
    }
  }, [_vm._v(_vm._s(_vm.global.display('networkError')))]), _vm._v(" "), _c('text', {
    staticClass: "button",
    on: {
      "click": _vm.Refresh
    }
  }, [_vm._v(_vm._s(_vm.global.display('refresh')))])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-79900e88", module.exports)
  }
}

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wxc-progress",
    style: (_vm.runWayStyle),
    attrs: {
      "accessible": true,
      "aria-label": ("进度为百分之" + _vm.value)
    }
  }, [_c('div', {
    staticClass: "progress",
    style: (_vm.progressStyle)
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7b08ba3d", module.exports)
  }
}

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('loading-indicator', {
    staticStyle: {
      "height": "80px",
      "width": "80px",
      "color": "#7ec9c2"
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-a33a226a", module.exports)
  }
}

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "netError"
  }, [(_vm.errorType == _vm.neterror) ? _c('div', {
    staticClass: "netError1"
  }, [_c('image', {
    staticStyle: {
      "width": "150px",
      "height": "150px"
    },
    attrs: {
      "src": _vm.getImgPath('neterror.png')
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "errorHint"
  }, [_vm._v("网络不给力，请稍后重试")]), _vm._v(" "), _c('text', {
    staticClass: "refresh",
    on: {
      "click": _vm.Refresh
    }
  }, [_vm._v("刷新")])]) : _vm._e(), _vm._v(" "), (_vm.errorType == _vm.noResult) ? _c('div', {
    staticClass: "netError2"
  }, [_c('image', {
    staticStyle: {
      "width": "150px",
      "height": "150px"
    },
    attrs: {
      "src": _vm.getImgPath('noresult.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: {
      "fontSize": "30px",
      "color": "#8f8f8f",
      "margin-top": "50px"
    }
  }, [_vm._v(_vm._s(_vm.errorMsg) + "...")])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-cd9f490a", module.exports)
  }
}

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [(_vm.show) ? _c('div', {
    staticClass: "dialog-box",
    style: ({
      top: _vm.top + 'px'
    })
  }, [_c('div', {
    staticClass: "dialog-content"
  }, [_vm._t("title", [_c('text', {
    staticClass: "content-title"
  }, [_vm._v(_vm._s(_vm.title))])]), _vm._v(" "), _vm._t("content", [_c('text', {
    staticClass: "content-subtext"
  }, [_vm._v(_vm._s(_vm.content))])])], 2), _vm._v(" "), _c('div', {
    staticClass: "dialog-footer"
  }, [(!_vm.single) ? _c('div', {
    staticClass: "footer-btn cancel",
    on: {
      "click": _vm.secondaryClicked
    }
  }, [_c('text', {
    staticClass: "btn-text",
    style: ({
      color: _vm.secondBtnColor
    })
  }, [_vm._v(_vm._s(_vm.cancelText))])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "footer-btn confirm",
    on: {
      "click": _vm.primaryClicked
    }
  }, [_c('text', {
    staticClass: "btn-text",
    style: ({
      color: _vm.mainBtnColor
    })
  }, [_vm._v(_vm._s(_vm.confirmText))])])])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-d88579ee", module.exports)
  }
}

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('web', {
    attrs: {
      "src": _vm.src
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-e41dd710", module.exports)
  }
}

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    style: ({
      height: _vm.height
    })
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-e85e8c30", module.exports)
  }
}

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(64);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("752d0a32", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-075dc4af&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weextabbar.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-075dc4af&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weextabbar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(65);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("40c72b48", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-16a3652a&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBar.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-16a3652a&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./searchBar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(66);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("7913686b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-17d335ea&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./subscribe.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-17d335ea&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./subscribe.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(67);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("3bbf0458", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1a32ac6f&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./videoPlay.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-1a32ac6f&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./videoPlay.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(68);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("8196f256", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-32957998!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./commonFun.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-32957998!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./commonFun.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(69);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("71d9d47c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-38bd612a!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./defaultImage.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-38bd612a!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./defaultImage.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(70);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("ea8f1310", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-42cf8c96!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./submitting.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-42cf8c96!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./submitting.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(71);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("5e317328", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-479bbfa6!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./loadingData.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-479bbfa6!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./loadingData.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(72);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("29f87f9b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-50b33d5c&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./Index.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-50b33d5c&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./Index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(73);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("66012cf1", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-540c97a4&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./topbar.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-540c97a4&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./topbar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(74);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("153e3d92", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-584cd5ee&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./library.bookShelf.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-584cd5ee&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./library.bookShelf.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(75);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("860c54ac", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5b02b228!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./me.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5b02b228!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./me.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(76);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("6194a502", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5dd495a2&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weexOverlay.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-5dd495a2&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weexOverlay.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(77);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("7f6cffe4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-62ffd2f8&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./libraryNav.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-62ffd2f8&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./libraryNav.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("619a2873", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-79900e88!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./networkErrorDisplay.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-79900e88!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./networkErrorDisplay.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(79);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("7b51657c", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7b08ba3d&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./progress.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-7b08ba3d&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./progress.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(80);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("8fb2923e", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a33a226a!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./defaultLoading.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-a33a226a!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./defaultLoading.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(81);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("a108fad0", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-cd9f490a!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./netError.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-cd9f490a!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./netError.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(82);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("1181e086", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d88579ee&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weexDialogue.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-d88579ee&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weexDialogue.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(83);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("081581c4", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-e41dd710&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weexWebView.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-e41dd710&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./weexWebView.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("0bf35344", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-e85e8c30!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./chat.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-e85e8c30!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./chat.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 127 */
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(264)

var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(170),
  /* template */
  __webpack_require__(239),
  /* scopeId */
  "data-v-434759c6",
  /* cssModules */
  null
)
Component.options.__file = "E:\\workspace-weex\\wx-creader\\src\\04.views\\storebookdetailshare.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] storebookdetailshare.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-434759c6", Component.options)
  } else {
    hotAPI.reload("data-v-434759c6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(5);

var _navigator = __webpack_require__(3);

var _util = __webpack_require__(36);

var _util2 = _interopRequireDefault(_util);

var _user = __webpack_require__(11);

var _creader = __webpack_require__(8);

var _googleTrack = __webpack_require__(7);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            getImgPath: _common.getImgPath,
            id: '',
            bookInfo: [],
            networkStatus: 1,
            global: global,
            netStatus: 0,
            errorType: '',
            errorMsg: '',
            url: "http://christian-reading.daddygarden.com/display/index/displayBook?id="
        };
    },

    created: function created() {
        var self = this;
        self.id = _util2.default.getUrlSearch(weex.config.bundleUrl, 'id');
        // prompt.toast(self.id);
        // self.id = 207;
        self.getData(self.id);
    },

    methods: {
        back: function back() {
            (0, _navigator.back)(this, "");
        },
        share: function share() {
            this.$refs.commonFun.ShareArticle(this.bookInfo.title, this.url + this.bookInfo.book_id, this.bookInfo.img_url, this.bookInfo.des);
            // prompt.alert(this.bookInfo.title);
        },
        gotoSell: function gotoSell(productid) {
            // googleTrack.userLibraryOpenBookshopBuy(this.bookInfo.title);
            //   var event = weex.requireModule('event');
            //             event.gosell(productid);
            // console.log("===========");
            window.location.href=download;
        },
        refresh: function refresh() {
            var self = this;
            self.netStatus = 0;
            self.getData(self.id);
        },
        getData: function getData(id) {
            var self = this;
            (0, _creader.getBookProfile)(id).then(function (res) {
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined" && res.data != null) {
                    self.netStatus = 2;
                    self.bookInfo = res.data.data.bookInfo;
                    console.log(self.bookInfo.book_address);
                    _googleTrack2.default.userLibraryOpenBookshopDetails(self.bookInfo.title);
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        }
    }

}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(37)))

/***/ }),
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */,
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var App = __webpack_require__(145);
var defaultLoading = __webpack_require__(19);
Vue.component('defaultLoading', defaultLoading);
var videoPlay = __webpack_require__(31);
Vue.component('videoPlay', videoPlay);
var webView = __webpack_require__(34);
Vue.component('webView', webView);
var networkError = __webpack_require__(25);
Vue.component('networkError', networkError);
var topbar = __webpack_require__(30);
Vue.component('topbar', topbar);
var commonFun = __webpack_require__(17);
Vue.component('commonFun', commonFun);
var weextabbar = __webpack_require__(35);
Vue.component('weextabbar', weextabbar);
var libraryNav = __webpack_require__(21);
Vue.component('libraryNav', libraryNav);
var searchBar = __webpack_require__(27);
Vue.component('searchBar', searchBar);
var netError = __webpack_require__(24);
Vue.component('netError', netError);
var loadingData = __webpack_require__(22);
Vue.component('loadingData', loadingData);
var progress = __webpack_require__(26);
Vue.component('progress', progress);
var weexDialogue = __webpack_require__(32);
Vue.component('weexDialogue', weexDialogue);
var weexOverlay = __webpack_require__(33);
Vue.component('weexOverlay', weexOverlay);
var defaultImage = __webpack_require__(18);
Vue.component('defaultImage', defaultImage);
var submitting = __webpack_require__(28);
Vue.component('submitting', submitting);
var Index = __webpack_require__(15);
Vue.component('Index', Index);
var me = __webpack_require__(23);
Vue.component('me', me);
var chat = __webpack_require__(16);
Vue.component('chat', chat);
var subscribe = __webpack_require__(29);
Vue.component('subscribe', subscribe);
var libraryBookShelf = __webpack_require__(20);
Vue.component('libraryBookShelf', libraryBookShelf);
App.el = '#root';
new Vue(App);

/***/ }),
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-434759c6]{\n     position: absolute;\n     left:0;\n     top:0;\n     right:0;\n     bottom:0;\n}\n.top[data-v-434759c6]{\n     width:750px;\n     height:96px;\n     flex-direction: row;\n     align-items: center;\n     padding-left: 40px;\n     padding-right: 40px;\n     justify-content: space-between;\n     background-color:#f6f6f6;\n}\n.top_img[data-v-434759c6]{\n      width:35px;\n      height:35px;\n}\n.top_icon[data-v-434759c6]{\n     width:45px;\n     height:45px;\n}\n.scroller[data-v-434759c6]{\n     width:750px;\n}\n.content[data-v-434759c6]{\n     background-color: #f6f6f6;\n     width:750px;\n     display: flex;\n     flex-direction: row;\n     justify-content: space-between;\n     align-items: center;\n     padding-left: 50px;\n     padding-right: 50px;\n     padding-top: 20px;\n     padding-bottom: 20px;\n}\n.book_img[data-v-434759c6]{\n     width:208px;\n     height:276px;\n     border-radius:5px;\n}\n.info[data-v-434759c6]{\n     margin-left: 50px;\n     height:230px;\n     flex-direction: column;\n     justify-content: space-between;\n     align-items: flex-start;\n}\n.title[data-v-434759c6]{\n     lines:2;\n     font-size:40px;\n     color:#1a1a1a;\n     width:350px;\n     height:120px;\n}\n.author[data-v-434759c6]{\n     font-size:35px;\n     width:350px;\n     color:#666666;\n     height:50px;\n}\n.mid[data-v-434759c6]{\n     width:400px;\n     flex-direction: row;\n     justify-content: flex-start;\n     align-items: center;\n}\n.tag[data-v-434759c6]{\n     color:#e72c27;\n     font-size:20px;\n     border-width:2px;\n     border-style: solid;\n     border-color:#e72c27;\n     border-radius: 20px;\n     padding-left: 10px;\n     padding-right: 10px;\n     padding-top: 5px;\n     padding-bottom: 5px;\n}\n.des[data-v-434759c6]{\n     padding:30px;\n     margin-bottom:80px;\n}\n.bookDes[data-v-434759c6]{\n     font-size:32px;\n     color:#7e7e7e;\n     line-height:54px;\n}\n.bottom[data-v-434759c6]{\nposition :fixed;\n     bottom: 0px;\n     width: 750px;\n     height: 80px;\n     background: #f6f6f6;\n     display: flex;\n     flex-direction: row;\n     justify-content: center;\n     align-items: center;\n}\n.selldiv[data-v-434759c6]{\n   flex-direction: row;\n     justify-content: center;\n     align-items: center;\n     width:550px;\n     height: 80px;\n     background-color: #e72c27;\n}\n.goselltext[data-v-434759c6]{\n     font-size: 36px;\n     color: #ffffff;\n}\n.gosellimg[data-v-434759c6]{\n     margin-left: 20px;\n     width:40px;\n     height: 40px;\n}\n", ""]);

// exports


/***/ }),
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */,
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body"
  }, [(_vm.netStatus == 2) ? _c('scroller', {
    staticClass: "scroller"
  }, [_c('div', {
    staticClass: "content"
  }, [_c('image', {
    staticClass: "book_img",
    attrs: {
      "src": _vm.bookInfo.img_url
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "info"
  }, [_c('text', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.bookInfo.title))]), _vm._v(" "), _c('div', {
    staticClass: "mid"
  }, [_c('text', {
    staticClass: "author"
  }, [_vm._v(_vm._s(_vm.bookInfo.author))])]), _vm._v(" "), _c('text', {
    staticClass: "tag"
  }, [_vm._v(_vm._s(_vm.bookInfo.name))])])]), _vm._v(" "), _c('div', {
    staticClass: "des"
  }, [_c('text', {
    staticClass: "bookDes"
  }, [_vm._v(_vm._s(_vm.bookInfo.content))])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_c('div', {
    staticClass: "selldiv",
    on: {
      "click": function($event) {
        _vm.gotoSell(_vm.bookInfo.book_address)
      }
    }
  }, [_c('text', {
    staticClass: "goselltext"
  }, [_vm._v("下载基督徒app去购买")]), _vm._v(" "), _c('img', {
    staticClass: "gosellimg",
    attrs: {
      "src": _vm.getImgPath('gosell.png')
    }
  })])]), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingData') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e(), _vm._v(" "), _c('commonFun', {
    ref: "commonFun"
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-434759c6", module.exports)
  }
}

/***/ }),
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(214);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("76036996", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-434759c6&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./storebookdetailshare.vue", function() {
     var newContent = require("!!../../node_modules/_css-loader@0.26.4@css-loader/index.js!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/style-rewriter.js?id=data-v-434759c6&scoped=true!../../node_modules/_vue-loader@10.3.0@vue-loader/lib/selector.js?type=styles&index=0!./storebookdetailshare.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })
/******/ ]);