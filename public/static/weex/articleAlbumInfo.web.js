// { "framework": "Vue" }

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 218);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),
/* 1 */
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// this module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate
    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(159)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.paramsToString = paramsToString;
exports.jump = jump;
exports.back = back;
exports.getWithParameter = getWithParameter;
exports.jumpSubPage = jumpSubPage;

var _util = __webpack_require__(40);

var _util2 = _interopRequireDefault(_util);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getNavigator() {
    var nav = weex.requireModule('navigator');
    return nav;
};

function paramsToString(obj) {
    var param = "";
    for (var name in obj) {
        param += "&" + name + "=" + encodeURI(obj[name]);
    }
    return param.substring(1);
};

function getUrl(url, jsFile) {
    var bundleUrl = url;
    var host = '';
    var path = '';
    var nativeBase = '';
    var isWebAssets = bundleUrl.indexOf('http://') >= 0;
    var isAndroidAssets = bundleUrl.indexOf('file://assets/') >= 0;
    var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('CReader.app') > 0;
    if (isAndroidAssets) {
        nativeBase = 'file://assets/';
    } else if (isiOSAssets) {
        // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
        // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
        nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
    } else {
        var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
        var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
        if (matches && matches.length >= 2) {
            host = matches[1];
        }
        if (matchFirstPath && matchFirstPath.length >= 2) {
            path = matchFirstPath[1];
        }
        nativeBase = 'http://' + host + '/';
    }
    var h5Base = './index.html?page=';
    // in Native
    var base = nativeBase;
    if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
        // check if in weexpack project
        if (path === 'web' || path === 'dist') {
            base = h5Base + '/dist/';
        } else {
            base = h5Base + '';
        }
    } else {
        base = nativeBase + (!!path ? path + '/' : '');
    }

    var newUrl = base + jsFile;
    return newUrl;
}
// 跳转到新的url地址
function jump(self, url) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html";
    } else {

        var path = self.$getConfig().bundleUrl;

        path = _util2.default.setWXBundleUrl(path, url + ".js");
        // prompt.alert(path);
        getNavigator().push({
            url: url,
            animated: "true"
        });
    }
}

function back(self) {
    if (WXEnvironment.platform == 'Web') {
        window.history.go(-1);
    } else {
        console.log("qingsong2");
        getNavigator().pop({
            animated: "true"
        });
    }
}

function getWithParameter(self, url, parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html?phantom_limb=true&" + paramsToString(parameter);
    } else {
        var path = self.$getConfig().bundleUrl;
        path = getUrl(path, url + ".js?" + paramsToString(parameter));
        var event = weex.requireModule('event');
        event.openURL(path);
    }
}

function jumpSubPage(self, url, parameter) {
    if (WXEnvironment.platform == 'Web') {
        window.location.href = "/web/" + url + ".html?phantom_limb=true&" + paramsToString(parameter);
    } else {
        var path = self.$getConfig().bundleUrl;
        path = getUrl(path, url + ".js?isSubPage&" + paramsToString(parameter));
        var event = weex.requireModule('event');
        // console.log(paramsToString(parameter)+"qingsongtest");
        event.openURL(path);
    }
}

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getImgPath = getImgPath;
exports.getIconFontPath = getIconFontPath;
exports.logVideoPlayEvent = logVideoPlayEvent;

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// 获取图片在三端上不同的路径
// e.g. 图片文件名是 test.jpg, 转换得到的图片地址为
// - H5      : http: //localhost:1337/src/images/test.jpg
// - Android : local:///test
// - iOS     : ../images/test.jpg
function getImgPath(img_name) {
    var bundleUrl = weex.config.bundleUrl;
    var platform = weex.config.env.platform;
    var img_path = '';

    if (platform == 'Web' || bundleUrl.indexOf("http") >= 0) {
        img_path = '/assets/images/' + img_name;
    } else if (platform == 'android') {
        // android 不需要后缀
        img_name = img_name.substr(0, img_name.lastIndexOf('.'));
        img_path = 'local:///' + img_name;
    } else {

        img_path = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1) + ('images/' + img_name);
    }
    return img_path;
};

function getIconFontPath() {
    var bundleUrl = weex.config.bundleUrl;
    var platform = weex.config.env.platform;
    var iconFont_path = '';

    if (platform == 'Web' || bundleUrl.indexOf("http") >= 0) {
        iconFont_path = '/assets/font/';
    } else if (platform == 'android') {
        // android 不需要后缀
        iconFont_path = iconFont_path.substr(0, iconFont_path.lastIndexOf('.'));
        iconFont_path = 'local:///' + iconFont_path;
    } else {
        //ios 
        // iconFont_path =bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1)+`font/`
        iconFont_path = 'local:///bundlejs/font/';
    }
    // console.log(iconFont_path+"qingsongdeiconfont路径");
    return iconFont_path;
}

// 发送信息到 google analytics
function logVideoPlayEvent(videoName, videoId, album) {
    if (WXEnvironment.platform != 'Web') {
        var event = weex.requireModule('event');
        if (typeof event.logVideoPlayEvent != "undefined") {
            event.logVideoPlayEvent(videoName, videoId, album);
        }
    }
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var environment = {
  language: "zh" //改变这里的值。来切换中英文
};

var langFiles = {
  en: __webpack_require__(73).lang,
  zh: __webpack_require__(75).lang,
  zh_tw: __webpack_require__(74).lang
};

var already = false;

var global = {

  init: function init() {
    if (already == true) {
      return;
    }
    console.log(" global init ");
    var platform = weex.config.env.platform;
    var lang = "zh";
    try {
      var deviceInfo = weex.requireModule('deviceInfo');
      if (typeof deviceInfo != "undefined" && typeof deviceInfo.GetLanguage == "function") {
        lang = deviceInfo.GetLanguage().toLocaleLowerCase();
      }
    } catch (e) {
      console.log(" lang " + e);
    }

    if (platform.toLocaleLowerCase() == "ios") {
      // # ios #
      // 中文简体：zh-Hans
      // 英文：en
      // 中文繁体：zh-Hant
      // 印地语：hi
      // 西班牙语：es
      // 墨西哥西班牙语：es-MX
      // 拉丁美洲西班牙语：es-419
      if (lang.indexOf("zh") >= 0) {
        if (lang.indexOf("hans") >= 0) {
          environment.language = "zh";
        } else {
          environment.language = "zh_tw";
        }
      } else if (lang.indexOf("en") >= 0) {
        environment.language = "en";
      } else if (lang.indexOf("es") >= 0) {
        environment.language = "en";
      } else {
        environment.language = "zh";
      }
    } else {
      if (lang.indexOf("zh_cn") >= 0 || lang.indexOf("zh_cn") >= 0) {
        environment.language = "zh";
      } else if (lang.indexOf("zh_") >= 0) {
        environment.language = "zh_tw";
      } else if (lang.indexOf("cn") >= 0 || lang.indexOf("cn") >= 0) {
        environment.language = "zh";
      } else if (lang.indexOf("en") >= 0) {
        environment.language = "en";
      } else {
        environment.language = "zh";
      }
    }
  },
  lang: function lang(key) {
    return "";
  },
  display: function display(key) {
    if (already == false) {
      this.init();
    }
    console.log(" already " + already);
    if (environment.language == "zh") {
      return langFiles.zh[key] || "not find";
    } else if (environment.language == "en") {
      return langFiles.en[key] || "not find";
    } else if (environment.language == "zh-tw") {
      return langFiles.zh_tw[key] || "not find";
    }
    return key;
  },
  getLanguage: function getLanguage() {
    if (already == false) {
      this.init();
    }
    return environment.language;
  }
};

exports.default = global;
//
// setTimeout(function(){
//     exports.lang = function(key) {
//         if (environment.language == "zh") {
//             return langFiles.zh[key] || "not find";
//         }
//         else if (environment.language == "en") {
//             return langFiles.en[key] || "not find";
//         }
//         else if (environment.language == "zh-tw") {
//             return langFiles.zh_tw[key] || "not find";
//         }
//         return key;
//     }
// },100);

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var prompt = {
    getModel: function getModel() {
        return weex.requireModule('modal');
    },
    toast: function toast(info) {
        this.getModel().toast({ message: info, duration: 0.3 });
    },
    alert: function alert(info) {
        this.getModel().alert({ message: info, duration: 0.3 });
    }
};

exports.default = prompt;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getBookShelf = getBookShelf;
exports.saveBookShelf = saveBookShelf;
exports.openBook = openBook;
exports.getBookListByService = getBookListByService;
exports.getArticleListInfo = getArticleListInfo;
exports.getSubscribeInfo = getSubscribeInfo;
exports.queryAllAlbumLists = queryAllAlbumLists;
exports.getAlbumInfoById = getAlbumInfoById;
exports.getBooks = getBooks;
exports.getBookProfile = getBookProfile;
exports.userAddBookToShelf = userAddBookToShelf;
exports.userRemoveBookToShelf = userRemoveBookToShelf;
exports.serchBookName = serchBookName;
exports.searchResult = searchResult;
exports.userAddAticleToLove = userAddAticleToLove;
exports.userRemoveAticleFromLove = userRemoveAticleFromLove;
exports.isArticleInLove = isArticleInLove;
exports.userGetLovedArticle = userGetLovedArticle;
exports.getArticleInfoById = getArticleInfoById;
exports.shareArticleToFriends = shareArticleToFriends;
exports.getSuggestBookLists = getSuggestBookLists;
exports.getBookListsInfo = getBookListsInfo;
exports.getPersonalData = getPersonalData;
exports.getFollowColumnList = getFollowColumnList;
exports.getNotsubscribedColumnInfo = getNotsubscribedColumnInfo;
exports.userFollow = userFollow;
exports.userRemoveColumn = userRemoveColumn;
exports.getColumnInfo = getColumnInfo;
exports.getBookList = getBookList;
exports.getBookListByTag = getBookListByTag;

var _fetch = __webpack_require__(13);

var _user = __webpack_require__(8);

var _storage = __webpack_require__(14);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(12);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _googleTrack = __webpack_require__(9);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BOOK_SHELF = 'BookShelf';

//用户获取书架信息
function getBookShelf() {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    return (0, _storage.storageGetItem)(BOOK_SHELF).then(function (res) {
        // console.log(res.data);
        if (res.result == "failed") {
            return [];
        } else {
            var data = JSON.parse(res.data);
            var len = data.length;
            for (var i = 0; i < len; i++) {
                data[i]['progress'] = { "visible": false, "value": 0 };
            }
            console.log(data);
            return data;
        }
    });
}

//用户获取书架信息
function saveBookShelf(bookList) {
    //登陆成功后返回token，和user_id
    return (0, _storage.storageSetItem)(BOOK_SHELF, JSON.stringify(bookList));
}

function openBook(bookInfo) {}

//从服务器端同步数据到本地
function getBookListByService(bookList, pageIndex) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/getBooksOnShelf', { 'user_id': userId, "page_index": pageIndex });
    });
}

function getArticleListInfo(pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/index', { 'openId': openId, 'pageIndex': pageIndex });
    });
}
function getSubscribeInfo() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/index', { 'openId': openId });
    });
    // return get('/api/follow/v1/index', { 'openId': openId}, fun);
}
function queryAllAlbumLists() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/queryAllAlbumLists', { 'openId': openId });
    });
}
function getAlbumInfoById(id, pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Article/v1/album', { 'openId': openId, 'pageIndex': pageIndex, 'id': id });
    });
    // return get('/api/Article/v1/album', { 'openId': openId, 'id': id,'pageIndex':pageIndex}, fun);
}
//获取图书信息
function getBooks(id, pageIndex) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBooksTags', { 'openId': openId, 'id': id, 'pageIndex': pageIndex });
    });
    // return get('/api/Books/getBooksTags',{'openId': openId,'id': id,'pageIndex':pageIndex},fun);
}
//获取书籍信息
function getBookProfile(id) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBookProfile', { 'openId': openId, 'id': id });
    });
}
//用户添加书籍到书架
function userAddBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_id = 7245;
    //type = 1 表示书籍
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/addBookToShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 });
    });
    // return get('/api/Books/addBookToShelf', { 'book_id': bookId, 'user_id': user_id}, fun);
}
//用户从书架移出书籍
function userRemoveBookToShelf(bookId) {
    //登陆成功后返回token，和user_id
    //let openId = getOpenId();
    //let user_id = getUserId();
    // let user_token = 7245;
    // return get('/api/Books/removeBookFromShelf', { 'book_id': bookId, 'user_token': user_token}, fun);
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Books/removeBookFromShelf', { 'article_book_id': bookId, 'user_id': userId, 'type': 1 });
    });
}
//用户搜索图书
function serchBookName(name, pageIndex) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.post)('/api/Books/searchBook', { 'title': name, 'pageIndex': pageIndex });
    });
}

//用户搜素文章，图书，专栏
function searchResult(value, type, pageIndex) {
    //type = 1,查询书;2表示查询文章;3，表示查询专辑
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.post)('/api/follow/searchResult', { 'title': value, 'pageIndex': pageIndex, 'type': type, 'user_id': userId });
    });
}
//用户添加文章到我的收藏
function userAddAticleToLove(articleId, loveCount) {
    //type = 2 表示文章
    return (0, _user.getUserId)().then(function (userId) {
        if (userId == -1) {
            return -1;
        } else {
            return (0, _fetch.get)('/api/Article/addArticleToLove', { 'article_book_id': articleId, 'love_count': loveCount, 'user_id': userId, 'type': 2 });
        }
    });
}
//用户移出文章到我的收藏
function userRemoveAticleFromLove(articleId, loveCount) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/removeArticleFromLove', { 'article_book_id': articleId, 'love_count': loveCount, 'user_id': userId });
    });
}
function isArticleInLove(articleId) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/isArticleInLove', { 'article_book_id': articleId, 'user_id': userId });
    });
}
//用户获取收藏的文章信息
function userGetLovedArticle(pageIndex) {
    //type = 2 表示文章
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/articleInLove', { 'user_id': userId, 'pageIndex': pageIndex, 'type': 2 });
    });
}
//用户阅读文章，获取文章的相关信息
function getArticleInfoById(id) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/articleInfoById', { 'id': id, 'user_id': userId });
    });
}
//用户分享文章
function shareArticleToFriends(id, shareCount) {
    return (0, _user.getUserId)().then(function (userId) {
        return (0, _fetch.get)('/api/Article/shareArticles', { 'id': id, 'user_id': userId, 'share_count': shareCount });
    });
}

//获取书单
function getSuggestBookLists() {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getSuggestBookLists', { 'openId': openId });
    });
    // return get('/api/Books/getSuggestBookLists',{'openId': openId},fun);
}
//获取书单下的详细书本信息
function getBookListsInfo(bookList_id) {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/Books/getBookListsInfo', { 'openId': openId, 'bookList_id': bookList_id });
    });
    // return get('/api/Books/getBookListsInfo',{'openId': openId,'bookList_id':bookList_id},fun);
}
//用户获取个人信息
function getPersonalData() {
    // let openId = getOpenId();
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/ChristianReaderUser/getPersonalData', { 'openId': openId }).then(function (res) {
            if (res.data.status == 1) {
                (0, _storage.storageSetItem)(res.data.data);
            } else {
                (0, _storage.storageSetItem)(res.data.data);
            }
            return res;
        });
    });
    // return getPromise('/api/ChristianReaderUser/getPersonalData',{'openId': openId});
    // return get('/api/ChristianReaderUser/getPersonalData',{'openId': openId},fun);
}

//获得用户已经订阅的专栏信息：
function getFollowColumnList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/followColumnInfo', { 'openId': openId });
    });
    // return get('/api/follow/v1/followColumnInfo', { 'openId': openId}, fun);
}
//获得用户未订阅的专栏信息：
function getNotsubscribedColumnInfo() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/notsubscribedList', { 'openId': openId });
    });
    // return get('/api/follow/v1/notsubscribedList', { 'openId': openId}, fun);
}
//用户订阅某个专栏
function userFollow(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId });
    });
    // syncAblumInfo.follow(columnId);
    // return get('/api/follow/v1/userFollow', { 'columnId': columnId, 'openId': openId}, fun);
}
//用户取消某个专栏的订阅：
function userRemoveColumn(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId });
    });
    // syncAblumInfo.cancel(columnId);
    // return get('/api/follow/v1/userRemoveColumn', { 'columnId': columnId, 'openId': openId}, fun);
}
//根据专栏id获得专栏详细信息
function getColumnInfo(columnId) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId });
    });
    // return get('/api/follow/v1/getColumnInfo', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
function getBookList() {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBooksInShop', { 'openId': openId });
    });
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}
function getBookListByTag(tagid) {
    return (0, _user.getOpenId)().then(function (openId) {
        return (0, _fetch.get)('/api/shop/getBooksInShopByTag', { 'openId': openId, 'tag_id': tagid });
    });
    // return get('/api/shop/getBooksInShop', { 'columnId': columnId, 'openId': openId , 'language':global.getLanguage()}, fun);
}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getOpenId = getOpenId;
exports.getUserId = getUserId;
exports.getUserInfo = getUserInfo;
exports.exit = exit;
exports.setUserInfo = setUserInfo;
exports.login = login;
exports.registerByEmail = registerByEmail;
exports.registerByPhone = registerByPhone;
exports.getPswBackByEmail = getPswBackByEmail;
exports.getPswBackByPhone = getPswBackByPhone;
exports.sendPhoneCode = sendPhoneCode;
exports.updatePsw = updatePsw;
exports.updateProfile = updateProfile;
exports.userFeedback = userFeedback;

var _fetch = __webpack_require__(13);

var _storage = __webpack_require__(14);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _syncAblumInfo = __webpack_require__(12);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var syncUser = new BroadcastChannel('christianReaderUser');
var USER_KEY = 'userKey'; //用户存储的KeyValue

var syncUserInfo = {
    receive: function receive(fun) {
        syncUser.onmessage = function (event) {
            // console.log(event.data) // Assemble!
            fun();
        };
    },
    post: function post() {
        var message = {
            status: 'update'
        };
        syncUser.postMessage(message);
    },
    close: function close(fun) {
        syncUser.close();
    }
};

exports.default = syncUserInfo;
function getOpenId() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {
        if (res.result == "failed") {
            return "";
        } else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).openId;
        }
        // return "8A471FD74AF017EE0680381611D442CD";
    });
}

function getUserId() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {
        if (res.result == "failed") {
            return -1;
        } else {
            console.log("openId: " + res.data);
            return JSON.parse(res.data).id;
        }
    });
}

function getUserInfo() {
    return (0, _storage.storageGetItem)(USER_KEY).then(function (res) {

        if (res.result == "failed") {
            return _defineProperty({
                id: -1,
                openId: '',
                head_icon: (0, _fetch.getHost)() + '/static/default/2.jpg',
                nickName: '主的好孩子,没有任何信息',
                profile: '点击这里登录或注册'
            }, 'openId', '');
        } else {
            return JSON.parse(res.data);
        }
    });
}

//更新资料
function exit() {
    (0, _storage.storageRemoveItem)(USER_KEY);
}

function setUserInfo(value) {
    return (0, _storage.storageSetItem)(USER_KEY, JSON.stringify(value));
}

//登陆
function login(userName, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password }).then(function (res) {
        console.log(" -- login -- ");
        console.log(res);
        if (res.data.status == 1) {
            setUserInfo(res.data.data);
        }
        return res;
    });
    // return post('/api/ChristianReaderUser/login', { 'userName': userName, 'password': password}, fun);
}
//用户邮箱注册
function registerByEmail(nickName, email, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName, 'email': email, 'password': password });
    // return post('/api/ChristianReaderUser/registerByEmail', { 'nickName': nickName ,'email': email,'password': password}, fun);
}
//用户手机注册
function registerByPhone(nickName, mobile, verifyCode, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName, 'mobile': mobile, 'verifyCode': verifyCode, 'password': password });
    // return post('/api/ChristianReaderUser/registerByPhone', { 'nickName': nickName ,'mobile': mobile,'verifyCode': verifyCode,'password': password}, fun);
}
//找回密码
function getPswBackByEmail(email) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getPasswordBack', { 'email': email });
    // return post('/api/ChristianReaderUser/getPasswordBack', { 'email': email}, fun);
}
function getPswBackByPhone(mobile, verifyCode) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode });
    // return post('/api/ChristianReaderUser/getPasswordBackByPhone', { 'mobile': mobile, 'verifyCode': verifyCode,}, fun);
}
//发送手机验证码
function sendPhoneCode(mobile) {
    return (0, _fetch.post)('/api/ChristianReaderUser/getSms', { 'mobile': mobile });
}
//修改密码
function updatePsw(mobile, password) {
    return (0, _fetch.post)('/api/ChristianReaderUser/updatePswByPhone', { 'mobile': mobile, 'password': password });
}
//更新资料
function updateProfile(name, nickName, des) {
    return getOpenId().then(function (openId) {
        return (0, _fetch.post)('/api/ChristianReaderUser/updatePersonalProfile', { 'name': name, 'nickName': nickName, 'brief_personal_des': des, 'user_token': openId }).then(function (res) {
            if (res.data.status == 1) {
                getUserInfo().then(function (res) {
                    res.nickName = nickName;
                    res.profile = des;
                    setUserInfo(res);
                });
            }
            return res;
        });
    });
}
//用户反馈
function userFeedback(contactWays, content) {
    return (0, _fetch.post)('/api/ChristianReaderUser/feedback', { 'ContactInformation': contactWays, 'Content': content, 'AppInfo': '基督徒阅读' });
}

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by vincent on 2017/8/27.
 * 使用杜威的十进制来进行数据分析和处理
 */

var googleTrack = {

    USER_BEHAVIOR: 'userBehavior',

    USER_BEHAVIOR_HOME: 'home', // 主页
    USER_BEHAVIOR_ALBUM: 'album',
    USER_BEHAVIOR_ARTICLE: 'albumArticleDetail', // 文章点击

    USER_BEHAVIOR_LIBRARY: 'library', // 图书馆
    USER_BEHAVIOR_LIBRARY_BOOKSHELF: 'libraryBookshelf', // 图书馆-书架
    USER_BEHAVIOR_LIBRARY_CATEGORY: 'libraryCategory', // 图书馆-分类
    USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION: 'libraryBookIntroduction', // 图书馆-书籍介绍
    USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD: 'libraryBookAddToBookshelf', // 图书馆-分类
    USER_BEHAVIOR_LIBRARY_BOOKLIST: 'libraryBookList', // 图书馆-书单
    USER_BEHAVIOR_LIBRARY_BOOKSHOP: 'store', // 图书馆-书店
    USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE: 'storeCategory', // 图书馆-书店
    USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL: 'storeBookDetail', // 图书馆-书店-书的详情介绍
    USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY: 'storeClickBuy', // 图书馆-书店-去购买
    USER_BEHAVIOR_LIBRARY_SEARCH: 'librarySearch', // 图书馆-搜索

    USER_BEHAVIOR_FOLLOW: 'follow', // 订阅
    USER_BEHAVIOR_CHAT: 'chat', // 答疑
    USER_BEHAVIOR_ME: 'user', // 我
    USER_BEHAVIOR_USER: 'userLoginOrReg', // 用户注册和登录
    USER_BEHAVIOR_USER_REGBYMAIL: 'userRegByMail', // 用户邮件注册
    USER_BEHAVIOR_USER_REGBYPHONE: 'userRegByPhone', // 用户手机注册

    USER_BEHAVIOR_USER_OTHER: 'user', // 其他信息
    USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION: 'userOpenSubscription', // 订阅
    USER_BEHAVIOR_USER_OTHER_FEEDBACK: 'userClickFeedback', // 其他信息
    USER_BEHAVIOR_USER_OTHER_ARTICLE: 'userOpenCollectionArtilce', // 收藏
    USER_BEHAVIOR_USER_OTHER_FRIDEND: 'userShareToFriend', // 分享给朋友

    //用户点击事件跟踪
    userAction: function userAction(action, label, id) {
        var event = weex.requireModule('event');
        if (typeof event.googleTrack != 'undefined') {
            event.googleTrack(this.USER_BEHAVIOR, action, label, id);
        }
        console.log(action + " -- " + label + " -- " + id);
    },
    userHome: function userHome() {
        this.userAction(this.USER_BEHAVIOR_HOME, "", 0);
    },
    userLibrary: function userLibrary() {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF, "", 0);
    },

    //订阅
    userFollow: function userFollow() {
        this.userAction(this.USER_BEHAVIOR_FOLLOW, "", 0);
    },

    //用户点击专栏
    userOpenAlbum: function userOpenAlbum(albumName, albumId) {
        this.userAction(this.USER_BEHAVIOR_ALBUM, albumName, albumId);
    },

    //用户点击文章次数
    userOpenArticle: function userOpenArticle(title, id) {
        this.userAction(this.USER_BEHAVIOR_ARTICLE, title, id);
    },

    //点击阅读书
    userLibraryOpenBook: function userLibraryOpenBook(title) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHELF, title, 0);
    },

    //点击书籍分类
    userLibraryOpenCategory: function userLibraryOpenCategory(categoryName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_CATEGORY, categoryName, 0);
    },

    //点击书籍分类
    userOpenBookIntroduction: function userOpenBookIntroduction(bookName, bookId) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION, bookName, bookId);
    },

    //点击书籍分类
    userOpenBookIntroductionAdd: function userOpenBookIntroductionAdd(bookName, bookId) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOK_INTRODUCTION_ADD, bookName, bookId);
    },

    //点击书单
    userLibraryOpenBookList: function userLibraryOpenBookList() {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST, '', 0);
    },

    //点击书单
    userLibraryOpenBookListByName: function userLibraryOpenBookListByName(name) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKLIST, name, 0);
    },

    //点击书店导航
    userLibraryOpenBookshop: function userLibraryOpenBookshop() {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP, "", 0);
    },

    //点击书店中的更多
    userLibraryOpenBookshopMore: function userLibraryOpenBookshopMore(categoryName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_MORE, categoryName, 0);
    },

    //点击书店中某本书
    userLibraryOpenBookshopDetails: function userLibraryOpenBookshopDetails(bookName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_DETAIL, bookName, 0);
    },

    //点击书店中去购买
    userLibraryOpenBookshopBuy: function userLibraryOpenBookshopBuy(bookName) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_BOOKSHOP_BUY, bookName, 0);
    },

    //点击搜索
    userSearch: function userSearch(keywords) {
        this.userAction(this.USER_BEHAVIOR_LIBRARY_SEARCH, keywords, 0);
    },

    //点击答疑
    userChat: function userChat() {
        this.userAction(this.USER_BEHAVIOR_CHAT, "", 0);
    },

    //点击Me
    userMe: function userMe() {
        this.userAction(this.USER_BEHAVIOR_ME, "", 0);
    },

    //用户点击登录或注册按钮
    userLoginOrReg: function userLoginOrReg() {
        this.userAction(this.USER_BEHAVIOR_USER, "", 0);
    },

    //用户用邮件注册
    userRegByMail: function userRegByMail() {
        this.userAction(this.USER_BEHAVIOR_USER, this.USER_BEHAVIOR_USER_REGBYMAIL, 0);
    },

    //用户用手机注册
    userRegByPhone: function userRegByPhone() {
        this.userAction(this.USER_BEHAVIOR_USER, this.USER_BEHAVIOR_USER_REGBYPHONE, 0);
    },

    //订阅
    userOtherSubscription: function userOtherSubscription() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_SUBSCRIPTION, 0);
    },

    //收藏
    userOtherArticle: function userOtherArticle() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_ARTICLE, 0);
    },

    //其他
    userOtherFeedback: function userOtherFeedback() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_FEEDBACK, 0);
    },
    userOtherShareToFriend: function userOtherShareToFriend() {
        this.userAction(this.USER_BEHAVIOR_USER_OTHER, this.USER_BEHAVIOR_USER_OTHER_FRIDEND, 0);
    }
};

exports.default = googleTrack;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(4);

exports.default = {
    tabItems: [{
        index: 0,
        title: "主页",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-android-home',
        image: (0, _common.getImgPath)("home.png"),
        selectedImage: (0, _common.getImgPath)('homeselected.png'),
        ref: 'home'
    }, {
        index: 1,
        title: "专栏",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-help-buoy',
        image: (0, _common.getImgPath)("library.png"),
        selectedImage: (0, _common.getImgPath)("libraryselected.png"),
        ref: 'album'
    }, {
        index: 2,
        title: "答疑",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-chatboxes',
        image: (0, _common.getImgPath)("subscribe.png"),
        selectedImage: (0, _common.getImgPath)("subscribeselected.png"),
        ref: 'chat'
    }, {
        index: 3,
        title: "图书",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-ios-book',
        image: (0, _common.getImgPath)("chat.png"),
        selectedImage: (0, _common.getImgPath)("chatselected.png"),
        ref: 'books'
    }, {
        index: 4,
        title: "我",
        titleColor: '#666666',
        selectedColor: '#E15D53',
        fontSize: '25px',
        iconWdith: '48px',
        iconHeight: '48px',
        iconFont: 'ion-ios-person',
        image: (0, _common.getImgPath)("me1.png"),
        selectedImage: (0, _common.getImgPath)("meselected.png"),
        ref: 'me'
    }],
    bookstoremessage: { message: "来自合作伙伴的精选书籍推荐给您" },
    bottommessage: { message: "没有更多了!" }

    // 正常模式的tab title配置
    // tabTitles: [
    //   {
    //     title: '主页',
    //     icon: getImgPath("home.png"),
    //     activeIcon: getImgPath('homeselected.png'),
    //   },
    //   {
    //     title: '图书馆',
    //     icon: getImgPath("library.png"),
    //     activeIcon: getImgPath("libraryselected.png")
    //   },
    //   {
    //     title: '订阅',
    //     icon: getImgPath("subscribe.png"),
    //     activeIcon: getImgPath("subscribeselected.png")
    //   },
    //   {
    //     title: '答疑',
    //     icon: getImgPath("chat.png"),
    //     activeIcon: getImgPath("chatselected.png")
    //   },
    //   {
    //     title: '我',
    //     icon: getImgPath("me1.png"),
    //     activeIcon: getImgPath("meselected.png")
    //   }
    // ],
    // tabStyles: {
    //   bgColor: '#FFFFFF',
    //   titleColor: '#666666',
    //   activeTitleColor: '#E15D53',
    //   activeBgColor: '#ffffff',
    //   isActiveTitleBold: true,
    //   iconWidth: 60,
    //   iconHeight: 60,
    //   width: 160,
    //   height: 120,
    //   BottomColor: '#FFC900',
    //   BottomWidth: 0,
    //   BottomHeight: 0,
    //   activeBottomColor: 'green',
    //   activeBottomWidth: 160,
    //   activeBottomHeight: 6,
    //   fontSize: 24,
    //   textPaddingLeft: 10,
    //   textPaddingRight: 10
    // },
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//import UrlParser from 'url-parse';

var Utils = {
  //UrlParser: UrlParser,
  _typeof: function _typeof(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
  },
  isPlainObject: function isPlainObject(obj) {
    return Utils._typeof(obj) === 'object';
  },
  isString: function isString(obj) {
    return typeof obj === 'string';
  },
  isNonEmptyArray: function isNonEmptyArray() {
    var obj = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    return obj && obj.length > 0 && Array.isArray(obj) && typeof obj !== 'undefined';
  },
  isObject: function isObject(item) {
    return item && (typeof item === 'undefined' ? 'undefined' : _typeof2(item)) === 'object' && !Array.isArray(item);
  },
  isEmptyObject: function isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  },
  decodeIconFont: function decodeIconFont(text) {
    // 正则匹配 图标和文字混排 eg: 我去上学校&#xe600;,天天不&#xe600;迟到
    var regExp = /&#x[a-z]\d{3,4};?/;
    if (regExp.test(text)) {
      return text.replace(new RegExp(regExp, 'g'), function (iconText) {
        var replace = iconText.replace(/&#x/, '0x').replace(/;$/, '');
        return String.fromCharCode(replace);
      });
    } else {
      return text;
    }
  },
  mergeDeep: function mergeDeep(target) {
    for (var _len = arguments.length, sources = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      sources[_key - 1] = arguments[_key];
    }

    if (!sources.length) return target;
    var source = sources.shift();
    if (Utils.isObject(target) && Utils.isObject(source)) {
      for (var key in source) {
        if (Utils.isObject(source[key])) {
          if (!target[key]) {
            Object.assign(target, _defineProperty({}, key, {}));
          }
          Utils.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, _defineProperty({}, key, source[key]));
        }
      }
    }
    return Utils.mergeDeep.apply(Utils, [target].concat(sources));
  },
  appendProtocol: function appendProtocol(url) {
    if (/^\/\//.test(url)) {
      var bundleUrl = weex.config.bundleUrl;

      return 'http' + (/^https:/.test(bundleUrl) ? 's' : '') + ':' + url;
    }
    return url;
  },
  encodeURLParams: function encodeURLParams(url) {
    var parsedUrl = new UrlParser(url, true);
    return parsedUrl.toString();
  },
  goToH5Page: function goToH5Page(jumpUrl) {
    var animated = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var callback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    var Navigator = weex.requireModule('navigator');
    var jumpUrlObj = new Utils.UrlParser(jumpUrl, true);
    var url = Utils.appendProtocol(jumpUrlObj.toString());
    Navigator.push({
      url: Utils.encodeURLParams(url),
      animated: animated.toString()
    }, callback);
  },

  env: {
    isTaobao: function isTaobao() {
      var appName = weex.config.env.appName;

      return (/(tb|taobao|淘宝)/i.test(appName)
      );
    },
    isTrip: function isTrip() {
      var appName = weex.config.env.appName;

      return appName === 'LX';
    },
    isBoat: function isBoat() {
      var appName = weex.config.env.appName;

      return appName === 'Boat' || appName === 'BoatPlayground';
    },
    isWeb: function isWeb() {
      var platform = weex.config.env.platform;

      return (typeof window === 'undefined' ? 'undefined' : _typeof2(window)) === 'object' && platform.toLowerCase() === 'web';
    },
    isIOS: function isIOS() {
      var platform = weex.config.env.platform;

      return platform.toLowerCase() === 'ios';
    },

    /**
     * 是否为 iPhone X
     * @returns {boolean}
     */
    isIPhoneX: function isIPhoneX() {
      var deviceHeight = weex.config.env.deviceHeight;

      if (Utils.env.isWeb()) {
        return (typeof window === 'undefined' ? 'undefined' : _typeof2(window)) !== undefined && window.screen && window.screen.width && window.screen.height && parseInt(window.screen.width, 10) === 375 && parseInt(window.screen.height, 10) === 812;
      }
      return Utils.env.isIOS() && deviceHeight === 2436;
    },
    isAndroid: function isAndroid() {
      var platform = weex.config.env.platform;

      return platform.toLowerCase() === 'android';
    },
    isAlipay: function isAlipay() {
      var appName = weex.config.env.appName;

      return appName === 'AP';
    },
    isTmall: function isTmall() {
      var appName = weex.config.env.appName;

      return (/(tm|tmall|天猫)/i.test(appName)
      );
    },
    isAliWeex: function isAliWeex() {
      return Utils.env.isTmall() || Utils.env.isTrip() || Utils.env.isTaobao();
    },
    supportsEB: function supportsEB() {
      var weexVersion = weex.config.env.weexVersion || '0';
      var isHighWeex = Utils.compareVersion(weexVersion, '0.10.1.4') && (Utils.env.isIOS() || Utils.env.isAndroid());
      var expressionBinding = weex.requireModule('expressionBinding');
      return expressionBinding && expressionBinding.enableBinding && isHighWeex;
    },


    /**
     * 判断Android容器是否支持是否支持expressionBinding(处理方式很不一致)
     * @returns {boolean}
     */
    supportsEBForAndroid: function supportsEBForAndroid() {
      return Utils.env.isAndroid() && Utils.env.supportsEB();
    },


    /**
     * 判断IOS容器是否支持是否支持expressionBinding
     * @returns {boolean}
     */
    supportsEBForIos: function supportsEBForIos() {
      return Utils.env.isIOS() && Utils.env.supportsEB();
    },


    /**
     * 获取weex屏幕真实的设置高度，需要减去导航栏高度
     * @returns {Number}
     */
    getPageHeight: function getPageHeight() {
      var env = weex.config.env;

      var navHeight = Utils.env.isWeb() ? 0 : Utils.env.isIPhoneX() ? 176 : 132;
      return env.deviceHeight / env.deviceWidth * 750 - navHeight;
    }
  },

  /**
   * 版本号比较
   * @memberOf Utils
   * @param currVer {string}
   * @param promoteVer {string}
   * @returns {boolean}
   * @example
   *
   * const { Utils } = require('@ali/wx-bridge');
   * const { compareVersion } = Utils;
   * console.log(compareVersion('0.1.100', '0.1.11')); // 'true'
   */
  compareVersion: function compareVersion() {
    var currVer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '0.0.0';
    var promoteVer = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '0.0.0';

    if (currVer === promoteVer) return true;
    var currVerArr = currVer.split('.');
    var promoteVerArr = promoteVer.split('.');
    var len = Math.max(currVerArr.length, promoteVerArr.length);
    for (var i = 0; i < len; i++) {
      var proVal = ~~promoteVerArr[i];
      var curVal = ~~currVerArr[i];
      if (proVal < curVal) {
        return true;
      } else if (proVal > curVal) {
        return false;
      }
    }
    return false;
  },

  /**
   * 分割数组
   * @param arr 被分割数组
   * @param size 分割数组的长度
   * @returns {Array}
   */
  arrayChunk: function arrayChunk() {
    var arr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 4;

    var groups = [];
    if (arr && arr.length > 0) {
      groups = arr.map(function (e, i) {
        return i % size === 0 ? arr.slice(i, i + size) : null;
      }).filter(function (e) {
        return e;
      });
    }
    return groups;
  },
  truncateString: function truncateString(str, len) {
    var hasDot = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

    var newLength = 0;
    var newStr = '';
    var singleChar = '';
    var chineseRegex = /[^\x00-\xff]/g;
    var strLength = str.replace(chineseRegex, '**').length;
    for (var i = 0; i < strLength; i++) {
      singleChar = str.charAt(i).toString();
      if (singleChar.match(chineseRegex) !== null) {
        newLength += 2;
      } else {
        newLength++;
      }
      if (newLength > len) {
        break;
      }
      newStr += singleChar;
    }

    if (hasDot && strLength > len) {
      newStr += '...';
    }
    return newStr;
  }
};

exports.default = Utils;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sync = new BroadcastChannel('christian reader');

var syncAblumInfo = {
  receive: function receive(fun) {
    sync.onmessage = fun;
  },
  follow: function follow(ablumnId) {
    var message = {
      status: 'add',
      id: ablumnId
    };
    sync.postMessage(message);
  },
  cancel: function cancel(ablumnId) {
    var message = {
      status: 'cancel',
      id: ablumnId
    };
    sync.postMessage(message);
  },
  getData: function getData() {
    var message = {
      status: 'getData'
    };
    sync.postMessage(message);
  },
  close: function close(ablumnId, fun) {
    sync.close();
  }
};

exports.default = syncAblumInfo;

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getHost = getHost;
exports.fetch = fetch;
exports.get = get;
exports.post = post;
// const host = "http://file.biblemooc.net/";
// const host = "http://192.168.2.56:1070";
// const host = "http://192.168.31.247:1070";
// const host = "http://192.168.2.100:12000";
//  const host ="http://192.168.2.249:1070";
var host = "http://christian-reading.daddygarden.com";

function getHost() {
    return host;
}

function getStream() {
    var stream = weex.requireModule('stream');
    return stream;
}

function paramsToString(obj) {
    var param = "";
    for (var name in obj) {
        param += "&" + name + "=" + obj[name];
    }
    return param.substring(1);
}

function fetch(path, method, param, successFun) {
    path = host + path;
    getStream().fetch({
        method: method,
        url: path,
        type: 'json',
        body: paramsToString(param)
    }, successFun);
    console.log(path);
}

// export function  get(path,param,fun) {
//     console.log(host+path+"?"+paramsToString(param));
//     return getStream().fetch({
//         method: 'GET',
//         type: 'json',
//         url: host+path+"?"+paramsToString(param),
//     }, fun)
// }

function get(path, param) {
    console.log(host + path + "?" + paramsToString(param));
    return new Promise(function (resolve, reject) {
        getStream().fetch({
            method: 'GET',
            url: host + path + "?" + paramsToString(param),
            type: 'json'
        }, function (ret) {
            resolve(ret);
        });
    });
}
//
// export function post(path,param,successFun)
// {
//     path = host+path;
//     console.log(path);
//     console.log(paramsToString(param));
//     getStream().fetch({
//             method: 'POST',
//             url: path,
//             type: 'json',
//             headers:{'Content-Type':'application/x-www-form-urlencoded'},
//             body: paramsToString(param)
//         }
//         ,successFun);
// }

function post(path, param) {
    path = host + path;
    console.log(path);
    console.log(paramsToString(param));
    return new Promise(function (resolve, reject) {
        getStream().fetch({
            method: 'POST',
            url: path,
            type: 'json',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: paramsToString(param)
        }, function (ret) {
            resolve(ret);
        });
    });
}

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.storageSetItem = storageSetItem;
exports.storageGetItem = storageGetItem;
exports.storageRemoveItem = storageRemoveItem;

var storage = weex.requireModule('storage');

function storageSetItem(key, value) {
    // console.log(' -- storageSetItem -- ');
    // console.log(value);
    return new Promise(function (resolve, reject) {
        storage.setItem(key, value, function (event) {
            // console.log(event);
            resolve(event);
        });
    });
};

function storageGetItem(key) {
    return new Promise(function (resolve, reject) {
        storage.getItem(key, function (event) {
            // console.log(" storage event ");
            // console.log(event);
            resolve(event);
        });
    });
}

function storageRemoveItem(key) {
    return new Promise(function (resolve, reject) {
        storage.removeItem(key, function (event) {
            // console.log(event);
            resolve(event);
        });
    });
}

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(134)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(47),
  /* template */
  __webpack_require__(107),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-057107c2",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/Index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-057107c2", Component.options)
  } else {
    hotAPI.reload("data-v-057107c2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(138)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(48),
  /* template */
  __webpack_require__(111),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/chat.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] chat.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-196a917b", Component.options)
  } else {
    hotAPI.reload("data-v-196a917b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(148)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(49),
  /* template */
  __webpack_require__(121),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/commonFun.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] commonFun.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-64fba0fe", Component.options)
  } else {
    hotAPI.reload("data-v-64fba0fe", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(157)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(50),
  /* template */
  __webpack_require__(130),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/crIconFont.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] crIconFont.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e292274c", Component.options)
  } else {
    hotAPI.reload("data-v-e292274c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(152)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(51),
  /* template */
  __webpack_require__(125),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/defaultImage.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] defaultImage.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-8c481486", Component.options)
  } else {
    hotAPI.reload("data-v-8c481486", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(146)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(52),
  /* template */
  __webpack_require__(119),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/defaultLoading.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] defaultLoading.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-622eaa9e", Component.options)
  } else {
    hotAPI.reload("data-v-622eaa9e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(155)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(53),
  /* template */
  __webpack_require__(128),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-d5b6ba2a",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/followAlbum.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] followAlbum.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d5b6ba2a", Component.options)
  } else {
    hotAPI.reload("data-v-d5b6ba2a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(158)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(54),
  /* template */
  __webpack_require__(131),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-f392ce2e",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/libraryBookShelf.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] libraryBookShelf.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f392ce2e", Component.options)
  } else {
    hotAPI.reload("data-v-f392ce2e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(145)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(55),
  /* template */
  __webpack_require__(118),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-55d0704b",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/libraryNav.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] libraryNav.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-55d0704b", Component.options)
  } else {
    hotAPI.reload("data-v-55d0704b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(140)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(56),
  /* template */
  __webpack_require__(113),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-22ff7e99",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/loadingCircle.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] loadingCircle.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-22ff7e99", Component.options)
  } else {
    hotAPI.reload("data-v-22ff7e99", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(153)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(57),
  /* template */
  __webpack_require__(126),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/me.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] me.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-93795b0a", Component.options)
  } else {
    hotAPI.reload("data-v-93795b0a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(156)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(58),
  /* template */
  __webpack_require__(129),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/netError.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] netError.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dfc39ce4", Component.options)
  } else {
    hotAPI.reload("data-v-dfc39ce4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(139)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(59),
  /* template */
  __webpack_require__(112),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/networkErrorDisplay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] networkErrorDisplay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-222082d6", Component.options)
  } else {
    hotAPI.reload("data-v-222082d6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(150)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(60),
  /* template */
  __webpack_require__(123),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-71f69050",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/progress.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] progress.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-71f69050", Component.options)
  } else {
    hotAPI.reload("data-v-71f69050", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(144)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(61),
  /* template */
  __webpack_require__(117),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-49098c90",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/searchBar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] searchBar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-49098c90", Component.options)
  } else {
    hotAPI.reload("data-v-49098c90", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(141)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(62),
  /* template */
  __webpack_require__(114),
  /* styles */
  injectStyle,
  /* scopeId */
  null,
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/submitting.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] submitting.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35a029e9", Component.options)
  } else {
    hotAPI.reload("data-v-35a029e9", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(133)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(63),
  /* template */
  __webpack_require__(106),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-02bfbb92",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/subscribe.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] subscribe.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-02bfbb92", Component.options)
  } else {
    hotAPI.reload("data-v-02bfbb92", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(151)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(64),
  /* template */
  __webpack_require__(124),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-7c36821a",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/topNavigationWidget.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] topNavigationWidget.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7c36821a", Component.options)
  } else {
    hotAPI.reload("data-v-7c36821a", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(154)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(65),
  /* template */
  __webpack_require__(127),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-a7d346f2",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/topNavigationWidgetWithBack.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] topNavigationWidgetWithBack.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a7d346f2", Component.options)
  } else {
    hotAPI.reload("data-v-a7d346f2", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(147)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(66),
  /* template */
  __webpack_require__(120),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-647bf301",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/topbar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] topbar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-647bf301", Component.options)
  } else {
    hotAPI.reload("data-v-647bf301", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(132)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(67),
  /* template */
  __webpack_require__(105),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-00ff98bc",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/videoPlay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] videoPlay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-00ff98bc", Component.options)
  } else {
    hotAPI.reload("data-v-00ff98bc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(136)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(68),
  /* template */
  __webpack_require__(109),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-14dbd79c",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/weexDialogue.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weexDialogue.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-14dbd79c", Component.options)
  } else {
    hotAPI.reload("data-v-14dbd79c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(142)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(69),
  /* template */
  __webpack_require__(115),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-3858c23c",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/weexOverlay.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weexOverlay.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3858c23c", Component.options)
  } else {
    hotAPI.reload("data-v-3858c23c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(137)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(70),
  /* template */
  __webpack_require__(110),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-1597bcf6",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/weexWebView.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weexWebView.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1597bcf6", Component.options)
  } else {
    hotAPI.reload("data-v-1597bcf6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(135)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(71),
  /* template */
  __webpack_require__(108),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-0ba33bfc",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/03.components/weextabbar.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] weextabbar.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0ba33bfc", Component.options)
  } else {
    hotAPI.reload("data-v-0ba33bfc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var utilFunc = {
    initIconFont: function initIconFont() {
        var domModule = weex.requireModule('dom');
        domModule.addRule('fontFace', {
            'fontFamily': "iconfont",
            'src': "url('http://at.alicdn.com/t/font_404010_jgmnakd1zizr529.ttf')"
        });
    },
    setWXBundleUrl: function setWXBundleUrl(url, jsFile) {
        var bundleUrl = url;
        var host = '';
        var path = '';
        var nativeBase = void 0;
        var isAndroidAssets = bundleUrl.indexOf('your_current_IP') >= 0 || bundleUrl.indexOf('file://assets/') >= 0;
        var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('WeexDemo.app') > 0;
        if (isAndroidAssets) {
            nativeBase = 'file://assets/dist';
        } else if (isiOSAssets) {
            // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
            // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
            nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
        } else {
            var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
            var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
            if (matches && matches.length >= 2) {
                host = matches[1];
            }
            if (matchFirstPath && matchFirstPath.length >= 2) {
                path = matchFirstPath[1];
            }
            nativeBase = 'http://' + host + '/';
        }
        var h5Base = './index.html?page=';
        // in Native
        var base = nativeBase;
        if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
            // check if in weexpack project
            if (path === 'web' || path === 'dist') {
                base = h5Base + '/dist/';
            } else {
                base = h5Base + '';
            }
        } else {
            base = nativeBase + (!!path ? path + '/' : '');
        }

        var newUrl = base + jsFile;
        return newUrl;
    },
    setWebBundleUrl: function setWebBundleUrl(url, jsFile) {
        var bundleUrl = url;
        var host = '';
        var path = '';
        var nativeBase = void 0;
        var isAndroidAssets = bundleUrl.indexOf('your_current_IP') >= 0 || bundleUrl.indexOf('file://assets/') >= 0;
        var isiOSAssets = bundleUrl.indexOf('file:///') >= 0 && bundleUrl.indexOf('WeexDemo.app') > 0;
        if (isAndroidAssets) {
            nativeBase = 'file://assets/dist';
        } else if (isiOSAssets) {
            // file:///var/mobile/Containers/Bundle/Application/{id}/WeexDemo.app/
            // file:///Users/{user}/Library/Developer/CoreSimulator/Devices/{id}/data/Containers/Bundle/Application/{id}/WeexDemo.app/
            nativeBase = bundleUrl.substring(0, bundleUrl.lastIndexOf('/') + 1);
        } else {
            var matches = /\/\/([^\/]+?)\//.exec(bundleUrl);
            var matchFirstPath = /\/\/[^\/]+\/([^\/]+)\//.exec(bundleUrl);
            if (matches && matches.length >= 2) {
                host = matches[1];
            }
            if (matchFirstPath && matchFirstPath.length >= 2) {
                path = matchFirstPath[1];
            }
            nativeBase = 'http://' + host + '/';
        }
        var h5Base = './index.html?page=';
        // in Native
        var base = nativeBase;
        if (typeof navigator !== 'undefined' && (navigator.appCodeName === 'Mozilla' || navigator.product === 'Gecko')) {
            // check if in weexpack project
            if (path === 'web' || path === 'dist') {
                base = h5Base + '/web/';
            } else {
                base = h5Base + '';
            }
        } else {
            base = nativeBase + (!!path ? path + '/' : '');
        }

        var newUrl = base + jsFile;
        return newUrl;
    },
    getUrlSearch: function getUrlSearch(url, name) {
        // debugger;
        var address,
            val = "";
        if (WXEnvironment.platform == 'Web') {
            address = window.location.href;
        } else {
            address = url;
        }

        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = address.slice(address.indexOf('?') + 1).match(reg);

        if (r != null) {
            try {
                val = decodeURIComponent(r[2]);
            } catch (_e) {}
        }
        return val;
    }
};

exports.default = utilFunc;

/***/ }),
/* 41 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var sync = new BroadcastChannel('book');

var syncBookShelf = {
  receive: function receive(fun) {
    sync.onmessage = fun;
  },
  addToShelf: function addToShelf(bookId) {
    var message = {
      status: 'add',
      bookId: bookId
    };
    sync.postMessage(message);
  },
  cancel: function cancel() {
    var message = {
      status: 'cancel'
    };
    sync.postMessage(message);
  },
  getData: function getData() {
    var message = {
      status: 'getData'
    };
    sync.postMessage(message);
  },
  close: function close(ablumnId, fun) {
    sync.close();
  }
};

exports.default = syncBookShelf;

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var EVENT_DOWNLOAD = 'downLoadBook';

var downloadEvent = {
    addDownLoadBookEventListener: function addDownLoadBookEventListener(fun) {
        var event = weex.requireModule('globalEvent');
        event.addEventListener(EVENT_DOWNLOAD, function (value) {
            console.log("get downLoadBook");
            fun(value);
        });
    },
    removeDownLoadBookEventListener: function removeDownLoadBookEventListener() {
        var event = weex.requireModule('globalEvent');
        console.log("remove downLoadBook");
        event.removeEventListener(EVENT_DOWNLOAD);
    }
};

exports.default = downloadEvent;

/***/ }),
/* 44 */
/***/ (function(module, exports) {

module.exports = {"ion-alert":"&#xf101;","ion-alert-circled":"&#xf100;","ion-android-add":"&#xf2c7;","ion-android-add-circle":"&#xf359;","ion-android-alarm-clock":"&#xf35a;","ion-android-alert":"&#xf35b;","ion-android-apps":"&#xf35c;","ion-android-archive":"&#xf2c9;","ion-android-arrow-back":"&#xf2ca;","ion-android-arrow-down":"&#xf35d;","ion-android-arrow-dropdown":"&#xf35f;","ion-android-arrow-dropdown-circle":"&#xf35e;","ion-android-arrow-dropleft":"&#xf361;","ion-android-arrow-dropleft-circle":"&#xf360;","ion-android-arrow-dropright":"&#xf363;","ion-android-arrow-dropright-circle":"&#xf362;","ion-android-arrow-dropup":"&#xf365;","ion-android-arrow-dropup-circle":"&#xf364;","ion-android-arrow-forward":"&#xf30f;","ion-android-arrow-up":"&#xf366;","ion-android-attach":"&#xf367;","ion-android-bar":"&#xf368;","ion-android-bicycle":"&#xf369;","ion-android-boat":"&#xf36a;","ion-android-bookmark":"&#xf36b;","ion-android-bulb":"&#xf36c;","ion-android-bus":"&#xf36d;","ion-android-calendar":"&#xf2d1;","ion-android-call":"&#xf2d2;","ion-android-camera":"&#xf2d3;","ion-android-cancel":"&#xf36e;","ion-android-car":"&#xf36f;","ion-android-cart":"&#xf370;","ion-android-chat":"&#xf2d4;","ion-android-checkbox":"&#xf374;","ion-android-checkbox-blank":"&#xf371;","ion-android-checkbox-outline":"&#xf373;","ion-android-checkbox-outline-blank":"&#xf372;","ion-android-checkmark-circle":"&#xf375;","ion-android-clipboard":"&#xf376;","ion-android-close":"&#xf2d7;","ion-android-cloud":"&#xf37a;","ion-android-cloud-circle":"&#xf377;","ion-android-cloud-done":"&#xf378;","ion-android-cloud-outline":"&#xf379;","ion-android-color-palette":"&#xf37b;","ion-android-compass":"&#xf37c;","ion-android-contact":"&#xf2d8;","ion-android-contacts":"&#xf2d9;","ion-android-contract":"&#xf37d;","ion-android-create":"&#xf37e;","ion-android-delete":"&#xf37f;","ion-android-desktop":"&#xf380;","ion-android-document":"&#xf381;","ion-android-done":"&#xf383;","ion-android-done-all":"&#xf382;","ion-android-download":"&#xf2dd;","ion-android-drafts":"&#xf384;","ion-android-exit":"&#xf385;","ion-android-expand":"&#xf386;","ion-android-favorite":"&#xf388;","ion-android-favorite-outline":"&#xf387;","ion-android-film":"&#xf389;","ion-android-folder":"&#xf2e0;","ion-android-folder-open":"&#xf38a;","ion-android-funnel":"&#xf38b;","ion-android-globe":"&#xf38c;","ion-android-hand":"&#xf2e3;","ion-android-hangout":"&#xf38d;","ion-android-happy":"&#xf38e;","ion-android-home":"&#xf38f;","ion-android-image":"&#xf2e4;","ion-android-laptop":"&#xf390;","ion-android-list":"&#xf391;","ion-android-locate":"&#xf2e9;","ion-android-lock":"&#xf392;","ion-android-mail":"&#xf2eb;","ion-android-map":"&#xf393;","ion-android-menu":"&#xf394;","ion-android-microphone":"&#xf2ec;","ion-android-microphone-off":"&#xf395;","ion-android-more-horizontal":"&#xf396;","ion-android-more-vertical":"&#xf397;","ion-android-navigate":"&#xf398;","ion-android-notifications":"&#xf39b;","ion-android-notifications-none":"&#xf399;","ion-android-notifications-off":"&#xf39a;","ion-android-open":"&#xf39c;","ion-android-options":"&#xf39d;","ion-android-people":"&#xf39e;","ion-android-person":"&#xf3a0;","ion-android-person-add":"&#xf39f;","ion-android-phone-landscape":"&#xf3a1;","ion-android-phone-portrait":"&#xf3a2;","ion-android-pin":"&#xf3a3;","ion-android-plane":"&#xf3a4;","ion-android-playstore":"&#xf2f0;","ion-android-print":"&#xf3a5;","ion-android-radio-button-off":"&#xf3a6;","ion-android-radio-button-on":"&#xf3a7;","ion-android-refresh":"&#xf3a8;","ion-android-remove":"&#xf2f4;","ion-android-remove-circle":"&#xf3a9;","ion-android-restaurant":"&#xf3aa;","ion-android-sad":"&#xf3ab;","ion-android-search":"&#xf2f5;","ion-android-send":"&#xf2f6;","ion-android-settings":"&#xf2f7;","ion-android-share":"&#xf2f8;","ion-android-share-alt":"&#xf3ac;","ion-android-star":"&#xf2fc;","ion-android-star-half":"&#xf3ad;","ion-android-star-outline":"&#xf3ae;","ion-android-stopwatch":"&#xf2fd;","ion-android-subway":"&#xf3af;","ion-android-sunny":"&#xf3b0;","ion-android-sync":"&#xf3b1;","ion-android-textsms":"&#xf3b2;","ion-android-time":"&#xf3b3;","ion-android-train":"&#xf3b4;","ion-android-unlock":"&#xf3b5;","ion-android-upload":"&#xf3b6;","ion-android-volume-down":"&#xf3b7;","ion-android-volume-mute":"&#xf3b8;","ion-android-volume-off":"&#xf3b9;","ion-android-volume-up":"&#xf3ba;","ion-android-walk":"&#xf3bb;","ion-android-warning":"&#xf3bc;","ion-android-watch":"&#xf3bd;","ion-android-wifi":"&#xf305;","ion-aperture":"&#xf313;","ion-archive":"&#xf102;","ion-arrow-down-a":"&#xf103;","ion-arrow-down-b":"&#xf104;","ion-arrow-down-c":"&#xf105;","ion-arrow-expand":"&#xf25e;","ion-arrow-graph-down-left":"&#xf25f;","ion-arrow-graph-down-right":"&#xf260;","ion-arrow-graph-up-left":"&#xf261;","ion-arrow-graph-up-right":"&#xf262;","ion-arrow-left-a":"&#xf106;","ion-arrow-left-b":"&#xf107;","ion-arrow-left-c":"&#xf108;","ion-arrow-move":"&#xf263;","ion-arrow-resize":"&#xf264;","ion-arrow-return-left":"&#xf265;","ion-arrow-return-right":"&#xf266;","ion-arrow-right-a":"&#xf109;","ion-arrow-right-b":"&#xf10a;","ion-arrow-right-c":"&#xf10b;","ion-arrow-shrink":"&#xf267;","ion-arrow-swap":"&#xf268;","ion-arrow-up-a":"&#xf10c;","ion-arrow-up-b":"&#xf10d;","ion-arrow-up-c":"&#xf10e;","ion-asterisk":"&#xf314;","ion-at":"&#xf10f;","ion-backspace":"&#xf3bf;","ion-backspace-outline":"&#xf3be;","ion-bag":"&#xf110;","ion-battery-charging":"&#xf111;","ion-battery-empty":"&#xf112;","ion-battery-full":"&#xf113;","ion-battery-half":"&#xf114;","ion-battery-low":"&#xf115;","ion-beaker":"&#xf269;","ion-beer":"&#xf26a;","ion-bluetooth":"&#xf116;","ion-bonfire":"&#xf315;","ion-bookmark":"&#xf26b;","ion-bowtie":"&#xf3c0;","ion-briefcase":"&#xf26c;","ion-bug":"&#xf2be;","ion-calculator":"&#xf26d;","ion-calendar":"&#xf117;","ion-camera":"&#xf118;","ion-card":"&#xf119;","ion-cash":"&#xf316;","ion-chatbox":"&#xf11b;","ion-chatbox-working":"&#xf11a;","ion-chatboxes":"&#xf11c;","ion-chatbubble":"&#xf11e;","ion-chatbubble-working":"&#xf11d;","ion-chatbubbles":"&#xf11f;","ion-checkmark":"&#xf122;","ion-checkmark-circled":"&#xf120;","ion-checkmark-round":"&#xf121;","ion-chevron-down":"&#xf123;","ion-chevron-left":"&#xf124;","ion-chevron-right":"&#xf125;","ion-chevron-up":"&#xf126;","ion-clipboard":"&#xf127;","ion-clock":"&#xf26e;","ion-close":"&#xf12a;","ion-close-circled":"&#xf128;","ion-close-round":"&#xf129;","ion-closed-captioning":"&#xf317;","ion-cloud":"&#xf12b;","ion-code":"&#xf271;","ion-code-download":"&#xf26f;","ion-code-working":"&#xf270;","ion-coffee":"&#xf272;","ion-compass":"&#xf273;","ion-compose":"&#xf12c;","ion-connection-bars":"&#xf274;","ion-contrast":"&#xf275;","ion-crop":"&#xf3c1;","ion-cube":"&#xf318;","ion-disc":"&#xf12d;","ion-document":"&#xf12f;","ion-document-text":"&#xf12e;","ion-drag":"&#xf130;","ion-earth":"&#xf276;","ion-easel":"&#xf3c2;","ion-edit":"&#xf2bf;","ion-egg":"&#xf277;","ion-eject":"&#xf131;","ion-email":"&#xf132;","ion-email-unread":"&#xf3c3;","ion-erlenmeyer-flask":"&#xf3c5;","ion-erlenmeyer-flask-bubbles":"&#xf3c4;","ion-eye":"&#xf133;","ion-eye-disabled":"&#xf306;","ion-female":"&#xf278;","ion-filing":"&#xf134;","ion-film-marker":"&#xf135;","ion-fireball":"&#xf319;","ion-flag":"&#xf279;","ion-flame":"&#xf31a;","ion-flash":"&#xf137;","ion-flash-off":"&#xf136;","ion-folder":"&#xf139;","ion-fork":"&#xf27a;","ion-fork-repo":"&#xf2c0;","ion-forward":"&#xf13a;","ion-funnel":"&#xf31b;","ion-gear-a":"&#xf13d;","ion-gear-b":"&#xf13e;","ion-grid":"&#xf13f;","ion-hammer":"&#xf27b;","ion-happy":"&#xf31c;","ion-happy-outline":"&#xf3c6;","ion-headphone":"&#xf140;","ion-heart":"&#xf141;","ion-heart-broken":"&#xf31d;","ion-help":"&#xf143;","ion-help-buoy":"&#xf27c;","ion-help-circled":"&#xf142;","ion-home":"&#xf144;","ion-icecream":"&#xf27d;","ion-image":"&#xf147;","ion-images":"&#xf148;","ion-information":"&#xf14a;","ion-information-circled":"&#xf149;","ion-ionic":"&#xf14b;","ion-ios-alarm":"&#xf3c8;","ion-ios-alarm-outline":"&#xf3c7;","ion-ios-albums":"&#xf3ca;","ion-ios-albums-outline":"&#xf3c9;","ion-ios-americanfootball":"&#xf3cc;","ion-ios-americanfootball-outline":"&#xf3cb;","ion-ios-analytics":"&#xf3ce;","ion-ios-analytics-outline":"&#xf3cd;","ion-ios-arrow-back":"&#xf3cf;","ion-ios-arrow-down":"&#xf3d0;","ion-ios-arrow-forward":"&#xf3d1;","ion-ios-arrow-left":"&#xf3d2;","ion-ios-arrow-right":"&#xf3d3;","ion-ios-arrow-thin-down":"&#xf3d4;","ion-ios-arrow-thin-left":"&#xf3d5;","ion-ios-arrow-thin-right":"&#xf3d6;","ion-ios-arrow-thin-up":"&#xf3d7;","ion-ios-arrow-up":"&#xf3d8;","ion-ios-at":"&#xf3da;","ion-ios-at-outline":"&#xf3d9;","ion-ios-barcode":"&#xf3dc;","ion-ios-barcode-outline":"&#xf3db;","ion-ios-baseball":"&#xf3de;","ion-ios-baseball-outline":"&#xf3dd;","ion-ios-basketball":"&#xf3e0;","ion-ios-basketball-outline":"&#xf3df;","ion-ios-bell":"&#xf3e2;","ion-ios-bell-outline":"&#xf3e1;","ion-ios-body":"&#xf3e4;","ion-ios-body-outline":"&#xf3e3;","ion-ios-bolt":"&#xf3e6;","ion-ios-bolt-outline":"&#xf3e5;","ion-ios-book":"&#xf3e8;","ion-ios-book-outline":"&#xf3e7;","ion-ios-bookmarks":"&#xf3ea;","ion-ios-bookmarks-outline":"&#xf3e9;","ion-ios-box":"&#xf3ec;","ion-ios-box-outline":"&#xf3eb;","ion-ios-briefcase":"&#xf3ee;","ion-ios-briefcase-outline":"&#xf3ed;","ion-ios-browsers":"&#xf3f0;","ion-ios-browsers-outline":"&#xf3ef;","ion-ios-calculator":"&#xf3f2;","ion-ios-calculator-outline":"&#xf3f1;","ion-ios-calendar":"&#xf3f4;","ion-ios-calendar-outline":"&#xf3f3;","ion-ios-camera":"&#xf3f6;","ion-ios-camera-outline":"&#xf3f5;","ion-ios-cart":"&#xf3f8;","ion-ios-cart-outline":"&#xf3f7;","ion-ios-chatboxes":"&#xf3fa;","ion-ios-chatboxes-outline":"&#xf3f9;","ion-ios-chatbubble":"&#xf3fc;","ion-ios-chatbubble-outline":"&#xf3fb;","ion-ios-checkmark":"&#xf3ff;","ion-ios-checkmark-empty":"&#xf3fd;","ion-ios-checkmark-outline":"&#xf3fe;","ion-ios-circle-filled":"&#xf400;","ion-ios-circle-outline":"&#xf401;","ion-ios-clock":"&#xf403;","ion-ios-clock-outline":"&#xf402;","ion-ios-close":"&#xf406;","ion-ios-close-empty":"&#xf404;","ion-ios-close-outline":"&#xf405;","ion-ios-cloud":"&#xf40c;","ion-ios-cloud-download":"&#xf408;","ion-ios-cloud-download-outline":"&#xf407;","ion-ios-cloud-outline":"&#xf409;","ion-ios-cloud-upload":"&#xf40b;","ion-ios-cloud-upload-outline":"&#xf40a;","ion-ios-cloudy":"&#xf410;","ion-ios-cloudy-night":"&#xf40e;","ion-ios-cloudy-night-outline":"&#xf40d;","ion-ios-cloudy-outline":"&#xf40f;","ion-ios-cog":"&#xf412;","ion-ios-cog-outline":"&#xf411;","ion-ios-color-filter":"&#xf414;","ion-ios-color-filter-outline":"&#xf413;","ion-ios-color-wand":"&#xf416;","ion-ios-color-wand-outline":"&#xf415;","ion-ios-compose":"&#xf418;","ion-ios-compose-outline":"&#xf417;","ion-ios-contact":"&#xf41a;","ion-ios-contact-outline":"&#xf419;","ion-ios-copy":"&#xf41c;","ion-ios-copy-outline":"&#xf41b;","ion-ios-crop":"&#xf41e;","ion-ios-crop-strong":"&#xf41d;","ion-ios-download":"&#xf420;","ion-ios-download-outline":"&#xf41f;","ion-ios-drag":"&#xf421;","ion-ios-email":"&#xf423;","ion-ios-email-outline":"&#xf422;","ion-ios-eye":"&#xf425;","ion-ios-eye-outline":"&#xf424;","ion-ios-fastforward":"&#xf427;","ion-ios-fastforward-outline":"&#xf426;","ion-ios-filing":"&#xf429;","ion-ios-filing-outline":"&#xf428;","ion-ios-film":"&#xf42b;","ion-ios-film-outline":"&#xf42a;","ion-ios-flag":"&#xf42d;","ion-ios-flag-outline":"&#xf42c;","ion-ios-flame":"&#xf42f;","ion-ios-flame-outline":"&#xf42e;","ion-ios-flask":"&#xf431;","ion-ios-flask-outline":"&#xf430;","ion-ios-flower":"&#xf433;","ion-ios-flower-outline":"&#xf432;","ion-ios-folder":"&#xf435;","ion-ios-folder-outline":"&#xf434;","ion-ios-football":"&#xf437;","ion-ios-football-outline":"&#xf436;","ion-ios-game-controller-a":"&#xf439;","ion-ios-game-controller-a-outline":"&#xf438;","ion-ios-game-controller-b":"&#xf43b;","ion-ios-game-controller-b-outline":"&#xf43a;","ion-ios-gear":"&#xf43d;","ion-ios-gear-outline":"&#xf43c;","ion-ios-glasses":"&#xf43f;","ion-ios-glasses-outline":"&#xf43e;","ion-ios-grid-view":"&#xf441;","ion-ios-grid-view-outline":"&#xf440;","ion-ios-heart":"&#xf443;","ion-ios-heart-outline":"&#xf442;","ion-ios-help":"&#xf446;","ion-ios-help-empty":"&#xf444;","ion-ios-help-outline":"&#xf445;","ion-ios-home":"&#xf448;","ion-ios-home-outline":"&#xf447;","ion-ios-infinite":"&#xf44a;","ion-ios-infinite-outline":"&#xf449;","ion-ios-information":"&#xf44d;","ion-ios-information-empty":"&#xf44b;","ion-ios-information-outline":"&#xf44c;","ion-ios-ionic-outline":"&#xf44e;","ion-ios-keypad":"&#xf450;","ion-ios-keypad-outline":"&#xf44f;","ion-ios-lightbulb":"&#xf452;","ion-ios-lightbulb-outline":"&#xf451;","ion-ios-list":"&#xf454;","ion-ios-list-outline":"&#xf453;","ion-ios-location":"&#xf456;","ion-ios-location-outline":"&#xf455;","ion-ios-locked":"&#xf458;","ion-ios-locked-outline":"&#xf457;","ion-ios-loop":"&#xf45a;","ion-ios-loop-strong":"&#xf459;","ion-ios-medical":"&#xf45c;","ion-ios-medical-outline":"&#xf45b;","ion-ios-medkit":"&#xf45e;","ion-ios-medkit-outline":"&#xf45d;","ion-ios-mic":"&#xf461;","ion-ios-mic-off":"&#xf45f;","ion-ios-mic-outline":"&#xf460;","ion-ios-minus":"&#xf464;","ion-ios-minus-empty":"&#xf462;","ion-ios-minus-outline":"&#xf463;","ion-ios-monitor":"&#xf466;","ion-ios-monitor-outline":"&#xf465;","ion-ios-moon":"&#xf468;","ion-ios-moon-outline":"&#xf467;","ion-ios-more":"&#xf46a;","ion-ios-more-outline":"&#xf469;","ion-ios-musical-note":"&#xf46b;","ion-ios-musical-notes":"&#xf46c;","ion-ios-navigate":"&#xf46e;","ion-ios-navigate-outline":"&#xf46d;","ion-ios-nutrition":"&#xf470;","ion-ios-nutrition-outline":"&#xf46f;","ion-ios-paper":"&#xf472;","ion-ios-paper-outline":"&#xf471;","ion-ios-paperplane":"&#xf474;","ion-ios-paperplane-outline":"&#xf473;","ion-ios-partlysunny":"&#xf476;","ion-ios-partlysunny-outline":"&#xf475;","ion-ios-pause":"&#xf478;","ion-ios-pause-outline":"&#xf477;","ion-ios-paw":"&#xf47a;","ion-ios-paw-outline":"&#xf479;","ion-ios-people":"&#xf47c;","ion-ios-people-outline":"&#xf47b;","ion-ios-person":"&#xf47e;","ion-ios-person-outline":"&#xf47d;","ion-ios-personadd":"&#xf480;","ion-ios-personadd-outline":"&#xf47f;","ion-ios-photos":"&#xf482;","ion-ios-photos-outline":"&#xf481;","ion-ios-pie":"&#xf484;","ion-ios-pie-outline":"&#xf483;","ion-ios-pint":"&#xf486;","ion-ios-pint-outline":"&#xf485;","ion-ios-play":"&#xf488;","ion-ios-play-outline":"&#xf487;","ion-ios-plus":"&#xf48b;","ion-ios-plus-empty":"&#xf489;","ion-ios-plus-outline":"&#xf48a;","ion-ios-pricetag":"&#xf48d;","ion-ios-pricetag-outline":"&#xf48c;","ion-ios-pricetags":"&#xf48f;","ion-ios-pricetags-outline":"&#xf48e;","ion-ios-printer":"&#xf491;","ion-ios-printer-outline":"&#xf490;","ion-ios-pulse":"&#xf493;","ion-ios-pulse-strong":"&#xf492;","ion-ios-rainy":"&#xf495;","ion-ios-rainy-outline":"&#xf494;","ion-ios-recording":"&#xf497;","ion-ios-recording-outline":"&#xf496;","ion-ios-redo":"&#xf499;","ion-ios-redo-outline":"&#xf498;","ion-ios-refresh":"&#xf49c;","ion-ios-refresh-empty":"&#xf49a;","ion-ios-refresh-outline":"&#xf49b;","ion-ios-reload":"&#xf49d;","ion-ios-reverse-camera":"&#xf49f;","ion-ios-reverse-camera-outline":"&#xf49e;","ion-ios-rewind":"&#xf4a1;","ion-ios-rewind-outline":"&#xf4a0;","ion-ios-rose":"&#xf4a3;","ion-ios-rose-outline":"&#xf4a2;","ion-ios-search":"&#xf4a5;","ion-ios-search-strong":"&#xf4a4;","ion-ios-settings":"&#xf4a7;","ion-ios-settings-strong":"&#xf4a6;","ion-ios-shuffle":"&#xf4a9;","ion-ios-shuffle-strong":"&#xf4a8;","ion-ios-skipbackward":"&#xf4ab;","ion-ios-skipbackward-outline":"&#xf4aa;","ion-ios-skipforward":"&#xf4ad;","ion-ios-skipforward-outline":"&#xf4ac;","ion-ios-snowy":"&#xf4ae;","ion-ios-speedometer":"&#xf4b0;","ion-ios-speedometer-outline":"&#xf4af;","ion-ios-star":"&#xf4b3;","ion-ios-star-half":"&#xf4b1;","ion-ios-star-outline":"&#xf4b2;","ion-ios-stopwatch":"&#xf4b5;","ion-ios-stopwatch-outline":"&#xf4b4;","ion-ios-sunny":"&#xf4b7;","ion-ios-sunny-outline":"&#xf4b6;","ion-ios-telephone":"&#xf4b9;","ion-ios-telephone-outline":"&#xf4b8;","ion-ios-tennisball":"&#xf4bb;","ion-ios-tennisball-outline":"&#xf4ba;","ion-ios-thunderstorm":"&#xf4bd;","ion-ios-thunderstorm-outline":"&#xf4bc;","ion-ios-time":"&#xf4bf;","ion-ios-time-outline":"&#xf4be;","ion-ios-timer":"&#xf4c1;","ion-ios-timer-outline":"&#xf4c0;","ion-ios-toggle":"&#xf4c3;","ion-ios-toggle-outline":"&#xf4c2;","ion-ios-trash":"&#xf4c5;","ion-ios-trash-outline":"&#xf4c4;","ion-ios-undo":"&#xf4c7;","ion-ios-undo-outline":"&#xf4c6;","ion-ios-unlocked":"&#xf4c9;","ion-ios-unlocked-outline":"&#xf4c8;","ion-ios-upload":"&#xf4cb;","ion-ios-upload-outline":"&#xf4ca;","ion-ios-videocam":"&#xf4cd;","ion-ios-videocam-outline":"&#xf4cc;","ion-ios-volume-high":"&#xf4ce;","ion-ios-volume-low":"&#xf4cf;","ion-ios-wineglass":"&#xf4d1;","ion-ios-wineglass-outline":"&#xf4d0;","ion-ios-world":"&#xf4d3;","ion-ios-world-outline":"&#xf4d2;","ion-ipad":"&#xf1f9;","ion-iphone":"&#xf1fa;","ion-ipod":"&#xf1fb;","ion-jet":"&#xf295;","ion-key":"&#xf296;","ion-knife":"&#xf297;","ion-laptop":"&#xf1fc;","ion-leaf":"&#xf1fd;","ion-levels":"&#xf298;","ion-lightbulb":"&#xf299;","ion-link":"&#xf1fe;","ion-load-a":"&#xf29a;","ion-load-b":"&#xf29b;","ion-load-c":"&#xf29c;","ion-load-d":"&#xf29d;","ion-location":"&#xf1ff;","ion-lock-combination":"&#xf4d4;","ion-locked":"&#xf200;","ion-log-in":"&#xf29e;","ion-log-out":"&#xf29f;","ion-loop":"&#xf201;","ion-magnet":"&#xf2a0;","ion-male":"&#xf2a1;","ion-man":"&#xf202;","ion-map":"&#xf203;","ion-medkit":"&#xf2a2;","ion-merge":"&#xf33f;","ion-mic-a":"&#xf204;","ion-mic-b":"&#xf205;","ion-mic-c":"&#xf206;","ion-minus":"&#xf209;","ion-minus-circled":"&#xf207;","ion-minus-round":"&#xf208;","ion-model-s":"&#xf2c1;","ion-monitor":"&#xf20a;","ion-more":"&#xf20b;","ion-mouse":"&#xf340;","ion-music-note":"&#xf20c;","ion-navicon":"&#xf20e;","ion-navicon-round":"&#xf20d;","ion-navigate":"&#xf2a3;","ion-network":"&#xf341;","ion-no-smoking":"&#xf2c2;","ion-nuclear":"&#xf2a4;","ion-outlet":"&#xf342;","ion-paintbrush":"&#xf4d5;","ion-paintbucket":"&#xf4d6;","ion-paper-airplane":"&#xf2c3;","ion-paperclip":"&#xf20f;","ion-pause":"&#xf210;","ion-person":"&#xf213;","ion-person-add":"&#xf211;","ion-person-stalker":"&#xf212;","ion-pie-graph":"&#xf2a5;","ion-pin":"&#xf2a6;","ion-pinpoint":"&#xf2a7;","ion-pizza":"&#xf2a8;","ion-plane":"&#xf214;","ion-planet":"&#xf343;","ion-play":"&#xf215;","ion-playstation":"&#xf30a;","ion-plus":"&#xf218;","ion-plus-circled":"&#xf216;","ion-plus-round":"&#xf217;","ion-podium":"&#xf344;","ion-pound":"&#xf219;","ion-power":"&#xf2a9;","ion-pricetag":"&#xf2aa;","ion-pricetags":"&#xf2ab;","ion-printer":"&#xf21a;","ion-pull-request":"&#xf345;","ion-qr-scanner":"&#xf346;","ion-quote":"&#xf347;","ion-radio-waves":"&#xf2ac;","ion-record":"&#xf21b;","ion-refresh":"&#xf21c;","ion-reply":"&#xf21e;","ion-reply-all":"&#xf21d;","ion-ribbon-a":"&#xf348;","ion-ribbon-b":"&#xf349;","ion-sad":"&#xf34a;","ion-sad-outline":"&#xf4d7;","ion-scissors":"&#xf34b;","ion-search":"&#xf21f;","ion-settings":"&#xf2ad;","ion-share":"&#xf220;","ion-shuffle":"&#xf221;","ion-skip-backward":"&#xf222;","ion-skip-forward":"&#xf223;","ion-social-android":"&#xf225;","ion-social-android-outline":"&#xf224;","ion-social-angular":"&#xf4d9;","ion-social-angular-outline":"&#xf4d8;","ion-social-apple":"&#xf227;","ion-social-apple-outline":"&#xf226;","ion-social-bitcoin":"&#xf2af;","ion-social-bitcoin-outline":"&#xf2ae;","ion-social-buffer":"&#xf229;","ion-social-buffer-outline":"&#xf228;","ion-social-chrome":"&#xf4db;","ion-social-chrome-outline":"&#xf4da;","ion-social-codepen":"&#xf4dd;","ion-social-codepen-outline":"&#xf4dc;","ion-social-css3":"&#xf4df;","ion-social-css3-outline":"&#xf4de;","ion-social-designernews":"&#xf22b;","ion-social-designernews-outline":"&#xf22a;","ion-social-dribbble":"&#xf22d;","ion-social-dribbble-outline":"&#xf22c;","ion-social-dropbox":"&#xf22f;","ion-social-dropbox-outline":"&#xf22e;","ion-social-euro":"&#xf4e1;","ion-social-euro-outline":"&#xf4e0;","ion-social-facebook":"&#xf231;","ion-social-facebook-outline":"&#xf230;","ion-social-foursquare":"&#xf34d;","ion-social-foursquare-outline":"&#xf34c;","ion-social-freebsd-devil":"&#xf2c4;","ion-social-github":"&#xf233;","ion-social-github-outline":"&#xf232;","ion-social-google":"&#xf34f;","ion-social-google-outline":"&#xf34e;","ion-social-googleplus":"&#xf235;","ion-social-googleplus-outline":"&#xf234;","ion-social-hackernews":"&#xf237;","ion-social-hackernews-outline":"&#xf236;","ion-social-html5":"&#xf4e3;","ion-social-html5-outline":"&#xf4e2;","ion-social-instagram":"&#xf351;","ion-social-instagram-outline":"&#xf350;","ion-social-javascript":"&#xf4e5;","ion-social-javascript-outline":"&#xf4e4;","ion-social-linkedin":"&#xf239;","ion-social-linkedin-outline":"&#xf238;","ion-social-markdown":"&#xf4e6;","ion-social-nodejs":"&#xf4e7;","ion-social-octocat":"&#xf4e8;","ion-social-pinterest":"&#xf2b1;","ion-social-pinterest-outline":"&#xf2b0;","ion-social-python":"&#xf4e9;","ion-social-reddit":"&#xf23b;","ion-social-reddit-outline":"&#xf23a;","ion-social-rss":"&#xf23d;","ion-social-rss-outline":"&#xf23c;","ion-social-sass":"&#xf4ea;","ion-social-skype":"&#xf23f;","ion-social-skype-outline":"&#xf23e;","ion-social-snapchat":"&#xf4ec;","ion-social-snapchat-outline":"&#xf4eb;","ion-social-tumblr":"&#xf241;","ion-social-tumblr-outline":"&#xf240;","ion-social-tux":"&#xf2c5;","ion-social-twitch":"&#xf4ee;","ion-social-twitch-outline":"&#xf4ed;","ion-social-twitter":"&#xf243;","ion-social-twitter-outline":"&#xf242;","ion-social-usd":"&#xf353;","ion-social-usd-outline":"&#xf352;","ion-social-vimeo":"&#xf245;","ion-social-vimeo-outline":"&#xf244;","ion-social-whatsapp":"&#xf4f0;","ion-social-whatsapp-outline":"&#xf4ef;","ion-social-windows":"&#xf247;","ion-social-windows-outline":"&#xf246;","ion-social-wordpress":"&#xf249;","ion-social-wordpress-outline":"&#xf248;","ion-social-yahoo":"&#xf24b;","ion-social-yahoo-outline":"&#xf24a;","ion-social-yen":"&#xf4f2;","ion-social-yen-outline":"&#xf4f1;","ion-social-youtube":"&#xf24d;","ion-social-youtube-outline":"&#xf24c;","ion-soup-can":"&#xf4f4;","ion-soup-can-outline":"&#xf4f3;","ion-speakerphone":"&#xf2b2;","ion-speedometer":"&#xf2b3;","ion-spoon":"&#xf2b4;","ion-star":"&#xf24e;","ion-stats-bars":"&#xf2b5;","ion-steam":"&#xf30b;","ion-stop":"&#xf24f;","ion-thermometer":"&#xf2b6;","ion-thumbsdown":"&#xf250;","ion-thumbsup":"&#xf251;","ion-toggle":"&#xf355;","ion-toggle-filled":"&#xf354;","ion-transgender":"&#xf4f5;","ion-trash-a":"&#xf252;","ion-trash-b":"&#xf253;","ion-trophy":"&#xf356;","ion-tshirt":"&#xf4f7;","ion-tshirt-outline":"&#xf4f6;","ion-umbrella":"&#xf2b7;","ion-university":"&#xf357;","ion-unlocked":"&#xf254;","ion-upload":"&#xf255;","ion-usb":"&#xf2b8;","ion-videocamera":"&#xf256;","ion-volume-high":"&#xf257;","ion-volume-low":"&#xf258;","ion-volume-medium":"&#xf259;","ion-volume-mute":"&#xf25a;","ion-wand":"&#xf358;","ion-waterdrop":"&#xf25b;","ion-wifi":"&#xf25c;","ion-wineglass":"&#xf2b9;","ion-woman":"&#xf25d;","ion-wrench":"&#xf2ba;","ion-xbox":"&#xf30c;"}

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');
exports.default = {
  props: {
    show: {
      type: Boolean,
      default: true
    },
    hasAnimation: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: Array,
      default: function _default() {
        return ['ease-in', 'ease-out'];
      }
    },
    opacity: {
      type: [Number, String],
      default: 0.6
    },
    canAutoClose: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    overlayStyle: function overlayStyle() {
      return {
        opacity: this.hasAnimation ? 0 : 1,
        backgroundColor: 'rgba(0, 0, 0,' + this.opacity + ')'
      };
    },
    shouldShow: function shouldShow() {
      var _this = this;

      var show = this.show,
          hasAnimation = this.hasAnimation;

      hasAnimation && setTimeout(function () {
        _this.appearOverlay(show);
      }, 50);
      return show;
    }
  },
  methods: {
    overlayClicked: function overlayClicked(e) {
      this.canAutoClose ? this.appearOverlay(false) : this.$emit('wxcOverlayBodyClicked', {});
    },
    appearOverlay: function appearOverlay(bool) {
      var _this2 = this;

      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.duration;
      var hasAnimation = this.hasAnimation,
          timingFunction = this.timingFunction,
          canAutoClose = this.canAutoClose;

      var needEmit = !bool && canAutoClose;
      needEmit && this.$emit('wxcOverlayBodyClicking', {});
      var overlayEl = this.$refs['wxc-overlay'];
      if (hasAnimation && overlayEl) {
        animation.transition(overlayEl, {
          styles: {
            opacity: bool ? 1 : 0
          },
          duration: duration,
          timingFunction: timingFunction[bool ? 0 : 1],
          delay: 0
        }, function () {
          needEmit && _this2.$emit('wxcOverlayBodyClicked', {});
        });
      } else {
        needEmit && this.$emit('wxcOverlayBodyClicked', {});
      }
    }
  }
};

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _wxcOverlay = __webpack_require__(160);

var _wxcOverlay2 = _interopRequireDefault(_wxcOverlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');
var platform = weex.config.env.platform;

var isWeb = (typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && platform.toLowerCase() === 'web';
exports.default = {
  components: { WxcOverlay: _wxcOverlay2.default },
  props: {
    show: {
      type: Boolean,
      default: false
    },
    pos: {
      type: String,
      default: 'bottom'
    },
    popupColor: {
      type: String,
      default: '#FFFFFF'
    },
    overlayCfg: {
      type: Object,
      default: function _default() {
        return {
          hasAnimation: true,
          timingFunction: ['ease-in', 'ease-out'],
          duration: 300,
          opacity: 0.6
        };
      }
    },
    height: {
      type: [Number, String],
      default: 840
    },
    standOut: {
      type: [Number, String],
      default: 0
    },
    width: {
      type: [Number, String],
      default: 750
    },
    animation: {
      type: Object,
      default: function _default() {
        return {
          timingFunction: 'ease-in'
        };
      }
    }
  },
  data: function data() {
    return {
      haveOverlay: true,
      isOverShow: true
    };
  },
  computed: {
    isNeedShow: function isNeedShow() {
      var _this = this;

      setTimeout(function () {
        _this.appearPopup(_this.show);
      }, 50);
      return this.show;
    },
    _height: function _height() {
      this.appearPopup(this.show, 150);
      return this.height;
    },
    padStyle: function padStyle() {
      var pos = this.pos,
          width = this.width,
          height = this.height,
          popupColor = this.popupColor,
          standOut = this.standOut;

      var style = {
        width: width + 'px',
        backgroundColor: popupColor
      };
      pos === 'top' && (style = _extends({}, style, {
        top: -height + standOut + 'px',
        height: height + 'px'
      }));
      pos === 'bottom' && (style = _extends({}, style, {
        bottom: -height + standOut + 'px',
        height: height + 'px'
      }));
      pos === 'left' && (style = _extends({}, style, {
        left: -width + standOut + 'px'
      }));
      pos === 'right' && (style = _extends({}, style, {
        right: -width + standOut + 'px'
      }));
      return style;
    }
  },
  methods: {
    handleTouchEnd: function handleTouchEnd(e) {
      // 在支付宝上面有点击穿透问题
      var platform = weex.config.env.platform;

      platform === 'Web' && e.preventDefault && e.preventDefault();
    },
    hide: function hide() {
      this.appearPopup(false);
      this.$refs.overlay.appearOverlay(false);
    },
    wxcOverlayBodyClicking: function wxcOverlayBodyClicking() {
      this.isShow && this.appearPopup(false);
    },
    appearPopup: function appearPopup(bool) {
      var _this2 = this;

      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 300;

      this.isShow = bool;
      var popupEl = this.$refs['wxc-popup'];
      if (!popupEl) {
        return;
      }
      animation.transition(popupEl, _extends({
        styles: {
          transform: this.getTransform(this.pos, this.width, this.height, !bool)
        },
        duration: duration,
        delay: 0
      }, this.animation), function () {
        if (!bool) {
          _this2.$emit('wxcPopupOverlayClicked', { pos: _this2.pos });
        }
      });
    },
    getTransform: function getTransform(pos, width, height, bool) {
      var _size = pos === 'top' || pos === 'bottom' ? height : width;
      bool && (_size = 0);
      var _transform = void 0;
      switch (pos) {
        case 'top':
          _transform = 'translateY(' + _size + 'px)';
          break;
        case 'bottom':
          _transform = 'translateY(-' + _size + 'px)';
          break;
        case 'left':
          _transform = 'translateX(' + _size + 'px)';
          break;
        case 'right':
          _transform = 'translateX(-' + _size + 'px)';
          break;
      }
      return _transform;
    }
  }
};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(10);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(7);

var _common = __webpack_require__(4);

var _googleTrack = __webpack_require__(9);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            currentPage: 0,
            height: '',
            articles: [],
            clientUrl: "",
            global: _global2.default,
            msg: '首页',
            pageIndex: 1,
            loadinging: false,
            errorType: '',
            errorMsg: '',
            netStatus: 0, //0表示正在加载数据，1表示网络错误，2表示加载数据成功
            shareIcon: 'ion-share'
        };
    },
    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight;
        this.getData(this.pageIndex);
    },

    methods: {
        bannerJump: function bannerJump(event) {
            console.log("11");
        },
        jumpToBookProfile: function jumpToBookProfile() {
            (0, _navigator.jumpSubPage)(this, 'libraryBookIntroduce', { closeThisPage: true });
        },
        share: function share(id, shareCount, title, img_url, des) {
            var _this = this;

            console.log(id, shareCount);
            var url = "http://christian-reading.daddygarden.com/display/index/index?id=" + id;
            shareArticleToFriends(id, shareCount).then(function (res) {
                console.log(res);
                _this.$refs.commonFun.ShareArticle(title, url, img_url, des);
            });
        },
        test: function test() {
            _prompt2.default.toast("测试信息");
        },
        jumpToRead: function jumpToRead(id, title, des, img_url, is_shop) {
            if (is_shop == 1) {
                (0, _navigator.jumpSubPage)(this, 'storeBookDetail', { id: id, title: title, des: des, img_url: img_url, from: 'artilce' });
            } else {
                (0, _navigator.jumpSubPage)(this, 'articleDetails', { id: id, title: title, des: des, img_url: img_url });
            }
        },
        loadData: function loadData() {
            this.getData(this.pageIndex);
        },
        loadMore: function loadMore(event) {
            var self = this;
            self.loadinging = true;
            if (self.articles.length < 10 * self.pageIndex) {
                _prompt2.default.toast("数据已经加载完成");
                setTimeout(function () {
                    self.loadinging = false;
                }, 2000);
            } else {
                self.pageIndex = self.pageIndex + 1;
                self.getData(self.pageIndex);
            }
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData(self.pageIndex);
        },
        arrNoRepeat: function arrNoRepeat(arr) {
            return Array.from(new Set(arr));
        },
        browseInfo: function browseInfo(id) {
            //                jump(this,this.clientUrl+"?id="+id)
            console.log(id);
        },
        getData: function getData(pageIndex) {
            var self = this;
            (0, _creader.getArticleListInfo)(pageIndex).then(function (res) {
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined") {
                    self.loadinging = false;
                    self.netStatus = 2;
                    self.articles = self.articles.concat(res.data.data);
                    self.clientUrl = res.data.client_url;
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        },
        jumpToAuthorColumn: function jumpToAuthorColumn(albumName, albumId) {
            (0, _navigator.jumpSubPage)(this, 'articleAlbumInfo', { id: albumId });
        }
    }
};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(4);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(10);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(7);

var _user = __webpack_require__(8);

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {};
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 96;
    },

    methods: {
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        },
        goToPersonalProfile: function goToPersonalProfile() {
            if (this.personInfo.id > 0) {
                (0, _navigator.jumpSubPage)(this, 'userPersonalProfile');
            } else {
                (0, _navigator.jumpSubPage)(this, 'userLogin');
            }
        },
        goToMySubscribe: function goToMySubscribe() {
            (0, _navigator.jumpSubPage)(this, 'mySubscribe');
        },
        goToMyLists: function goToMyLists() {
            (0, _navigator.jumpSubPage)(this, 'myArticles');
        },
        goToFeedback: function goToFeedback() {
            (0, _navigator.jumpSubPage)(this, 'UserFeedback');
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
        },
        getData: function getData() {
            var _this = this;

            var self = this;
            (0, _user.getUserInfo)().then(function (res) {
                console.log(" --- getUserInfo --- ");
                self.personInfo = res;
                //                        console.log(self.personInfo.nickName);
                self.netStatus = 2;
                (0, _creader.getPersonalData)().then(function (res) {
                    var self = _this;
                    console.log(res);
                    if (res.status != -1 && typeof res.data != "undefined") {
                        self.netStatus = 2;
                        self.personInfo = res.data.data;
                        console.log(self.personInfo);
                    } else {
                        self.netStatus = 1;
                        self.errorType = 'neterror';
                    }
                });
            });
        }
    }

};

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//

exports.default = {};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _common = __webpack_require__(4);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var iconItems = __webpack_require__(44);

var fontFamily = "ionfont";
var domModule = weex.requireModule("dom");
module.exports = {
    data: function data() {
        return {
            iconValue: ''
        };
    },
    beforeCreate: function beforeCreate() {
        var url = weex.config.bundleUrl;
        if (url.indexOf('?') > 0) {
            url = url.substring(0, url.indexOf('?'));
        }
        url = url.split('/').slice(0, -1).join('/');
        var partUrl = (0, _common.getIconFontPath)();
        url = partUrl + "ionicons.ttf";
        domModule.addRule('fontFace', {
            'fontFamily': fontFamily,
            'src': "url('" + url + "')"
        });
    },
    created: function created() {
        this.iconValue = iconItems[this.name];
    },

    props: {
        name: {
            type: String,
            defalut: ''
        },
        color: {
            type: String,
            default: 'white'
        },
        size: {
            type: [Number, String],
            default: '70px'
        },
        activeColor: {
            type: String
        }
    },
    computed: {
        getFontName: function getFontName() {
            var icon = iconItems[this.name];
            return this.decode(icon || '');
        },
        getStyle: function getStyle() {
            var style = {
                'color': this.color,
                'font-size': this.size,
                'font-family': fontFamily
            };
            if (this.activeColor) {
                style["color:active"] = this.activeColor;
            }
            return style;
        }
    },
    methods: {
        _click: function _click(e) {
            this.$emit("click", e);
        },
        decode: function decode(fontCode) {
            if (/^&#x/.test(fontCode)) {
                return String.fromCharCode(fontCode.replace(/^&#x/, '0x').replace(/;$/, ''));
            } else {
                return String.fromCharCode('0x' + fontCode);
            }
        }
    }
};

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        backgroup: { type: String },
        source: { type: String }

    },
    data: function data() {
        return {
            loadFinished: false
        };
    },
    created: function created() {
        console.log(' --- ');
    },

    methods: {
        onImageLoad: function onImageLoad(event) {
            if (event.success) {
                // Do something to hanlde success
                this.loadFinished = true;
            }
        }
    }
};

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    name: 'defaultLoading',
    data: function data() {
        return {
            msg: 'hello vue'
        };
    }
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(4);

var _navigator = __webpack_require__(3);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _user = __webpack_require__(8);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _creader = __webpack_require__(7);

var _syncAblumInfo = __webpack_require__(12);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            app: 'ion-android-apps',
            search: 'ion-search'
        };
    },

    computed: {
        Id: function Id() {
            var self = this;
            return self.albums.length;
        }
    },
    props: {
        albums: {
            type: Array,
            // default: function () {
            //           return []
            // },
            required: true
        },
        name: {
            type: String,
            default: ''
        },
        height: {
            type: String,
            default: '100px'
        },
        textStyle: String,
        textColor: {
            type: String,
            default: 'white'
        },
        bgcolor: {
            type: String,
            default: '#E15D53'
        }
    },
    created: function created() {
        var self = this;
        _syncAblumInfo2.default.receive(function (res) {
            if (res.data.status == 'getData') {
                // self.getData();
            } else {
                for (var i = 0; i < self.albums.length; i++) {
                    if (res.data.id == self.albums[i].id && res.data.status == 'add') {
                        self.albums[i].status = 1;
                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                    }
                    if (res.data.id == self.albums[i].id && res.data.status == 'cancel') {
                        self.albums[i].status = 0;
                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                    }
                }
            }
        });
    },
    mounted: function mounted() {},

    methods: {
        goBack: function goBack() {
            (0, _navigator.back)(this, "");
        },
        toggleSubscribe: function toggleSubscribe(albumId, index) {
            var _this = this;

            var self = this;
            (0, _user.getOpenId)().then(function (openId) {
                if (openId) {
                    var _loop = function _loop(i) {
                        if (i == index) {
                            if (self.albums[i].status == 0) {
                                (0, _creader.userFollow)(albumId).then(function (res) {
                                    _prompt2.default.toast("订阅成功");
                                    if (res.data.status == 1) {
                                        self.albums[i].status = 1;
                                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                                        _syncAblumInfo2.default.follow(albumId);
                                    }
                                });
                            } else {
                                (0, _creader.userRemoveColumn)(albumId).then(function (res) {
                                    _prompt2.default.toast("取消成功");
                                    if (res.data.status == 1) {
                                        self.albums[i].status = 0;
                                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                                        _syncAblumInfo2.default.cancel(albumId);
                                    }
                                });
                            }
                        }
                    };

                    for (var i = 0; i < self.albums.length; i++) {
                        _loop(i);
                    }
                } else {
                    _prompt2.default.toast("请登录");
                    setTimeout(function () {
                        (0, _navigator.jumpSubPage)(_this, 'userLogin');
                    }, 2000);
                }
            });
        },
        jumpToAuthorColumn: function jumpToAuthorColumn(albumId, status) {
            (0, _navigator.jumpSubPage)(this, 'articleAlbumInfo', { id: albumId, status: status });
        }
    }
};

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(4);

var _navigator = __webpack_require__(3);

var _fetch = __webpack_require__(13);

var _weextabbarConfig = __webpack_require__(10);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _creader = __webpack_require__(7);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _globalEvent = __webpack_require__(43);

var _globalEvent2 = _interopRequireDefault(_globalEvent);

var _syncBookShelf = __webpack_require__(42);

var _syncBookShelf2 = _interopRequireDefault(_syncBookShelf);

var _googleTrack = __webpack_require__(9);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            currentPage: 1,
            currentPosition: 0,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            errorType: '',
            errorMsg: '',
            global: global,
            netStatus: 0,
            isShelf: false,
            columnWidth: 'auto',
            height: '',
            title: '!',
            content: '您确定要将本书移出书架吗？',
            confirmText: '确定',
            cancelText: '取消',
            show: false,
            single: false,
            showNoPrompt: false,
            isChecked: false,
            currentBookId: -1,
            currentIndex: -1,
            bookTotal: 0,
            bookIndex: 1,
            books: [],
            isSync: false,
            downLoadIndex: -1,
            downLoadStatus: 0,
            imgDownLoad: false
        };
    },
    created: function created() {
        var self = this;
        // self.getData();
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        self.height = 750 / deviceWidth * deviceHeight - 132;
        _syncBookShelf2.default.receive(function (res) {
            if (res.data.status == 'add') {
                (0, _creader.getBookShelf)().then(function (data) {
                    self.books = [];
                    self.books = data;
                    self.netStatus = 2;
                });
            }
            if (res.data.status == 'getData') {
                self.syncData();
            }
        });
        _globalEvent2.default.addDownLoadBookEventListener(function (res) {

            if (res.status == 1) {
                if (self.books[self.downLoadIndex]['progress'].visible == false) {
                    self.books[self.downLoadIndex]['progress'].visible = true;
                }
                self.books[self.downLoadIndex]['progress'].value = res.progress;
            } else if (res.status == 2) {
                self.books[self.downLoadIndex]['progress'] = { visible: false, value: 0 };
                self.downLoadStatus = 0;
                self.downLoadIndex = 0;
            } else if (res.status == 3) {
                _prompt2.default.toast("下载异常，请稍后再试！");
                self.books[self.downLoadIndex]['progress'] = { visible: false, value: 0 };
                self.downLoadStatus = 0;
                self.downLoadIndex = 0;
            } else if (res.status == 4) {
                _prompt2.default.toast("网络无法连接，请联网后再操作. ");
                self.books[self.downLoadIndex]['progress'] = { visible: false, value: 0 };
                self.downLoadStatus = 0;
                self.downLoadIndex = 0;
            }
            this.$set(self.books, self.downLoadIndex, self.books[self.downLoadIndex]);
            console.log(res);
        });
    },

    methods: {
        onchange: function onchange(event) {
            console.log('changed:', event.index);
        },
        bannerJump: function bannerJump(event) {
            console.log("11");
        },
        loadData: function loadData() {
            console.log("--------------loadData ----------------");
            var self = this;
            self.getData();
        },
        jumpToBookProfile: function jumpToBookProfile(id, title, address, imgUrl) {
            var _this = this;

            (0, _creader.getBookShelf)().then(function (data) {
                var h = -1;
                for (var _i = 0; _i < data.length; _i++) {
                    if (data[_i].id == id) {
                        // data = data.splice(i,1);
                        h = _i;
                    }
                }
                var arr = data.splice(h, 1);
                data.unshift(arr[0]);
                (0, _creader.saveBookShelf)(data);
                if (_this.downLoadStatus == 1) {
                    _prompt2.default.toast("一次只能下载一本书籍，请稍后再试.");
                    return;
                } else {
                    _this.downLoadStatus == 1;
                    var len = _this.books.length;
                    for (var i = 0; i < len; i++) {
                        if (_this.books[i].id == id) {
                            _this.downLoadIndex = i;
                            break;
                        }
                    }
                    _googleTrack2.default.userLibraryOpenBook(title, id);
                    var event = weex.requireModule('event');
                    event.openBook(id, title, address, imgUrl, (0, _fetch.getHost)() + "/display/index/displayBook?id=" + id);
                }
            });
        },
        openPopUp: function openPopUp(id, index) {
            var self = this;
            self.currentBookId = id;
            self.currentIndex = index;
            // prompt.toast(self.currentBookId+'-----'+self.currentIndex);
            self.show = true;
        },
        dialogCancelBtnClick: function dialogCancelBtnClick() {
            this.show = false;
            // prompt.toast("我点了取消");
        },
        dialogConfirmBtnClick: function dialogConfirmBtnClick() {
            var _this2 = this;

            var self = this;
            self.show = false;
            // prompt.toast(self.currentBookId);
            (0, _creader.userRemoveBookToShelf)(self.currentBookId).then(function (res) {
                console.log(res);
                if (res.data.status == 1) {
                    self.books.splice(self.currentIndex, 1);
                    (0, _creader.saveBookShelf)(_this2.books);
                } else {
                    _prompt2.default.toast('移除失败，请稍后再试');
                }
            });
        },
        refresh: function refresh() {
            var self = this;
            global.init();
            self.netStatus = 0;
            self.getData();
        },
        isExist: function isExist(item) {
            var len = this.books.length;
            for (var i = 0; i < len; i++) {
                if (this.books[i].id == item.id) {
                    return true;
                    break;
                }
            }
            return false;
        },
        syncData: function syncData() {
            var _this3 = this;

            //采用循环调用，把所有的数据同步完成
            (0, _creader.getBookListByService)(this.books, this.bookIndex).then(function (res) {
                var res = res.data;
                console.log(" -- this.bookIndex  --");
                console.log(_this3.bookIndex);
                console.log(res);
                if (res.status == 1) {
                    _this3.bookTotal = res.data.total;
                    var tlist = [];
                    res.data.list.forEach(function (item) {
                        //从服务器拿到数据后遍历，是否有新添加的书籍
                        if (_this3.isExist(item) == false) {
                            item['progress'] = { visible: false, value: 0 };
                            item['img_url'] = res.data.host + item['img_url'];
                            item['address'] = res.data.host + item['address'];
                            tlist.push(item);
                        }
                    });
                    if (tlist.length > 0) {
                        _this3.books = _this3.books.concat(tlist);
                    }
                    if (_this3.bookIndex * 50 >= _this3.bookTotal) {
                        //                                console.log(" ----- this.books ---- ");
                        //                                console.log(this.books);
                        (0, _creader.saveBookShelf)(_this3.books);
                        if (_this3.books.length == 0) {
                            _this3.netStatus = 1;
                            _this3.errorType = 'noResult';
                            _this3.errorMsg = "您的书架里还没有添加任何书籍..";
                        } else {
                            _this3.netStatus = 2;
                            _this3.imgDownLoad = true;
                        }
                    } else {
                        _this3.bookIndex = _this3.bookIndex + 1;
                        _this3.syncData();
                    }
                }
            });
        },
        getData: function getData() {
            var self = this;
            if (self.isSync == false) {
                self.isSync = true;
                (0, _creader.getBookShelf)().then(function (data) {
                    self.books = data;
                    if (self.books.length > 0) {
                        self.netStatus = 2;
                    }
                    if (data.length == 0) {
                        setTimeout(self.syncData, 400);
                        self.netStatus = 1;
                        self.errorType = 'noResult';
                        self.errorMsg = "您的书架里还没有添加任何书籍..";
                    } else {
                        setTimeout(self.syncData, 3000);
                    }
                });
            }
        },
        tabBarCurrentTabSelected: function tabBarCurrentTabSelected(e) {
            var page = e.page;
            console.log(page);
            (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        }
    }

}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(41)))

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(4);

var _setting = __webpack_require__(72);

var _setting2 = _interopRequireDefault(_setting);

var _googleTrack = __webpack_require__(9);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var dom = weex.requireModule('dom');
var animation = weex.requireModule('animation');
exports.default = {
  props: {
    currentPage: {
      type: Number
    },
    currentPosition: {
      type: Number
    },
    tabTitles: {
      type: Array,
      default: function _default() {
        return [];
      }
    },
    isShelf: {
      type: Boolean,
      default: true
    },
    tabStyles: {
      type: Object,
      default: function _default() {
        return {
          bgColor: '#FFFFFF',
          titleColor: '#515151',
          activeTitleColor: '#E15D53',
          activeBgColor: '#ffffff',
          isActiveTitleBold: true,
          iconWidth: 60,
          iconHeight: 60,
          width: 160,
          height: 120,
          borderBottomColor: '#FFC900',
          borderBottomWidth: 0,
          borderBottomHeight: 0,
          activeBorderBottomColor: '#E15D53',
          activeBorderBottomWidth: 6,
          activeBorderBottomHeight: 6,
          fontSize: 31,
          textPaddingLeft: 10,
          textPaddingRight: 10
        };
      }
    },
    titleType: {
      type: String,
      default: 'icon'
    },
    isTabView: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: String,
      default: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    }
  },
  data: function data() {
    return {
      topNav: ['书架', '分类', '书单', '书店'],
      getImgPath: _common.getImgPath

    };
  },
  created: function created() {
    var titleType = this.titleType,
        tabStyles = this.tabStyles;
  },

  methods: {
    setPage: function setPage(index) {
      console.log(index + " -- " + this.currentPosition);
      if (index == this.currentPosition) {
        return;
      }
      var page = '';
      switch (index) {
        case 0:
          page = 'libraryBookShelf';
          break;
        case 1:
          page = 'libraryCategory';
          break;
        case 2:
          page = 'libraryBookLists';
          break;
        case 3:
          page = 'storeEntry';
          break;
        case 4:
          page = 'librarySearch';
          break;
      }
      var parm = {};
      if (page == 'libraryBookShelf') {
        this.back();
      } else {
        if (this.currentPosition == 0) {
          (0, _navigator.jumpSubPage)(this, page);
        } else {
          (0, _navigator.jumpSubPage)(this, page, { closeThisPage: true });
        }
      }
    },
    back: function back() {
      if (_setting2.default.isWeb() || _setting2.default.isAndroid()) {
        (0, _navigator.back)(this, "");
      } else {
        (0, _navigator.jumpSubPage)(this, 'libraryBookShelf', { backToRootView: true, closeThisPage: true });
      }
    }
  }

};

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//

var animation = weex.requireModule('animation');
var modal = weex.requireModule('modal');

exports.default = {
    data: function data() {
        return {
            current_rotate: 0,
            loading: false,
            height: ''
        };
    },
    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight;
        var deviceWidth = WXEnvironment.deviceWidth;
        this.height = (deviceHeight * 750 / deviceWidth - 208) / 2;
    },
    mounted: function mounted() {
        this.loading = true;
        var that = this;
        var el = this.$refs.test;
        this.rotate(el, 1);
    },

    methods: {
        rotate: function rotate(el, x) {
            var self = this;
            this.current_rotate += 360;
            animation.transition(el, {
                styles: {
                    opacity: x == 1 ? 0.5 : 1,
                    transform: 'scale(' + (x == 1 ? 0.6 : 1) + ')' + 'rotate(' + self.current_rotate + 'deg)',
                    // transform: 'rotate(' + self.current_rotate + 'deg)',
                    transformOrigin: 'center center'
                },
                duration: 600,
                timingFunction: 'linear',
                delay: 0
            }, function () {
                this.loading && this.rotate(el, -1 * x);
            }.bind(this));
        }
    }
};

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _wxcPopup = __webpack_require__(161);

var _wxcPopup2 = _interopRequireDefault(_wxcPopup);

var _common = __webpack_require__(4);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _fetch = __webpack_require__(13);

var _weextabbarConfig = __webpack_require__(10);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(7);

var _user = __webpack_require__(8);

var _user2 = _interopRequireDefault(_user);

var _googleTrack = __webpack_require__(9);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    components: { WxcPopup: _wxcPopup2.default },
    data: function data() {
        return {
            currentPage: 4,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            storagediv: [],
            networkStatus: 1,
            global: _global2.default,
            personInfo: [],
            errorType: '',
            errorMsg: '',
            netStatus: 0,
            isBottomShow: false,
            height: 100,
            autofocus: false,
            doneteValue: "",
            popuColor: "#FFFFFF"

        };
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight - 132;
        // var self = this;
        // this.getData();
        // syncUserInfo.receive(this.getData);
    },

    methods: {
        oninput: function oninput(event) {
            var self = this;
            self.doneteValue = event.value;
        },
        loadData: function loadData() {
            this.getData();
            _user2.default.receive(this.getData);
        },
        goToPersonalProfile: function goToPersonalProfile() {
            if (this.personInfo.id > 0) {
                (0, _navigator.jumpSubPage)(this, 'userPersonalProfile');
            } else {
                (0, _navigator.jumpSubPage)(this, 'userLogin');
            }
        },
        goToMySubscribe: function goToMySubscribe() {
            (0, _navigator.jumpSubPage)(this, 'mySubscribe');
        },
        goToMyLists: function goToMyLists() {
            (0, _navigator.jumpSubPage)(this, 'myArticles');
        },
        goToFeedback: function goToFeedback() {
            (0, _navigator.jumpSubPage)(this, 'UserFeedback');
            // jump(this,'UserFeedback');
        },
        goToShare: function goToShare() {
            _googleTrack2.default.userOtherShareToFriend();
            this.$refs.commonFun.ShareApp("推荐下载 基督徒阅读APP", (0, _fetch.getHost)() + "/display/index/introduce", (0, _fetch.getHost)() + "/static/default/icon-512.jpg", "各大应用市场中有超过1000+的好评，深受弟兄姊妹的喜爱 是基督徒的必备工具。");
        },
        Donate: function Donate() {
            var self = this;
            self.isBottomShow = true;
        },

        // 捐献调用方法
        goToDonate: function goToDonate() {
            var self = this;
            self.isBottomShow = false;
            try {
                var event = weex.requireModule('event');
                event.gotoDonate(self.doneteValue);
            } catch (error) {}
        },
        popupOverlayBottomClick: function popupOverlayBottomClick() {
            this.isBottomShow = false;
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
        },
        getData: function getData() {
            var _this = this;

            var self = this;
            (0, _user.getUserInfo)().then(function (res) {
                console.log(" --- getUserInfo --- ");
                self.personInfo = res;
                //                        console.log(self.personInfo.nickName);
                self.netStatus = 2;
                (0, _creader.getPersonalData)().then(function (res) {
                    var self = _this;
                    console.log(res);
                    if (res.status != -1 && typeof res.data != "undefined") {
                        self.netStatus = 2;
                        self.personInfo = res.data.data;
                        console.log(self.personInfo);
                    } else {
                        self.netStatus = 1;
                        self.errorType = 'neterror';
                    }
                });
            });
        }
    }

};

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(4);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            noResult: 'noResult',
            neterror: 'neterror'
        };
    },

    props: {
        networkError: String,
        errorType: {
            type: String
        },
        errorMsg: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            this.$emit('toNetError');
        }
    },

    created: function created() {
        _global2.default.init();
    }
};

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        _global2.default.init();
    }
};

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    props: {
        barColor: {
            type: String,
            default: '#9B7B56'
        },
        barWidth: {
            type: Number,
            default: 600
        },
        barHeight: {
            type: Number,
            default: 8
        },
        value: {
            type: Number,
            default: 0
        }
    },
    computed: {
        runWayStyle: function runWayStyle() {
            var barWidth = this.barWidth,
                barHeight = this.barHeight;

            return {
                width: barWidth + 'px',
                height: barHeight + 'px'
            };
        },
        progressStyle: function progressStyle() {
            var value = this.value,
                barWidth = this.barWidth,
                barHeight = this.barHeight,
                barColor = this.barColor;

            var newValue = value < 0 ? 0 : value > 100 ? 100 : value;
            return {
                backgroundColor: barColor,
                height: barHeight + 'px',
                width: newValue / 100 * barWidth + 'px'
            };
        }
    }
};

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(4);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    alwaysShowCancel: {
      type: Boolean,
      default: true
    },
    inputType: {
      type: String,
      default: 'text'
    },
    returnKeyType: {
      type: String,
      default: 'default'
    },
    mod: {
      type: String,
      default: 'default'
    },
    autofocus: {
      type: Boolean,
      default: false
    },
    theme: {
      type: String,
      default: 'gray'
    },
    barStyle: {
      type: Object,
      default: function _default() {
        return {};
      }
    },
    defaultValue: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: '搜索'
    },
    cancelLabel: {
      type: String,
      default: '搜索'
    },
    depName: {
      type: String,
      default: '杭州'
    }
  },
  computed: {
    needShowCancel: function needShowCancel() {
      return this.alwaysShowCancel || this.showCancel;
    },
    buttonStyle: function buttonStyle() {
      var barStyle = this.barStyle;

      if (barStyle.backgroundColor) {
        return { backgroundColor: barStyle.backgroundColor };
      }
      return {};
    }
  },
  data: function data() {
    return {
      getImgPath: _common.getImgPath,
      showCancel: false,
      showClose: false,
      value: ''

    };
  },
  created: function created() {
    this.defaultValue && (this.value = this.defaultValue);
    if (this.disabled) {
      this.showCancel = false;
      this.showClose = false;
    }
  },

  methods: {
    onBlur: function onBlur() {
      var self = this;
      setTimeout(function () {
        self.showCancel = false;
        self.detectShowClose();
        self.$emit('wxcSearchbarInputOnBlur', { value: self.value });
      }, 10);
    },
    goBack: function goBack() {
      (0, _navigator.back)(this, '');
    },
    autoBlur: function autoBlur() {
      this.$refs['search-input'].blur();
    },
    onFocus: function onFocus() {
      if (this.isDisabled) {
        return;
      }
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputOnFocus', { value: this.value });
    },
    closeClicked: function closeClicked() {
      this.value = '';
      // this.showCancel && (this.showCancel = false);
      this.showClose && (this.showClose = false);
      this.$emit('wxcSearchbarCloseClicked', { value: this.value });
      this.$emit('wxcSearchbarInputOnInput', { value: this.value });
    },
    onInput: function onInput(e) {
      this.value = e.value;
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputOnInput', { value: this.value });
    },
    onSubmit: function onSubmit(e) {
      this.onBlur();
      this.value = e.value;
      this.showCancel = true;
      this.detectShowClose();
      this.$emit('wxcSearchbarInputReturned', { value: this.value });
    },
    searchClicked: function searchClicked() {
      // this.showCancel && (this.showCancel = false);
      // this.showClose && (this.showClose = false);
      if (this.value) {
        this.$emit('wxcSearchbarCancelClicked', { value: this.value });
      } else {
        _prompt2.default.toast("请输入关键词");
      }
    },
    detectShowClose: function detectShowClose() {
      this.showClose = this.value.length > 0 && this.showCancel;
    },
    depClicked: function depClicked() {
      this.$emit('wxcSearchbarDepChooseClicked', {});
    },
    inputDisabledClicked: function inputDisabledClicked() {
      this.$emit('wxcSearchbarInputDisabledClicked', {});
    },
    setValue: function setValue(value) {
      this.value = value;
    }
  }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// import { INPUT_ICON, ARROW_ICON, CLOSE_ICON } from './type';

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _navigator = __webpack_require__(3);

var _common = __webpack_require__(4);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            global: _global2.default,
            getImgPath: _common.getImgPath,
            loadinging: true,
            height: ''
        };
    },

    props: {
        networkError: String,
        refresh: String
    },
    methods: {
        Refresh: function Refresh() {
            console.log("1111");
            this.$emit('childMethod');
        }
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight;
        var deviceWidth = WXEnvironment.deviceWidth;
        this.height = (deviceHeight * 750 / deviceWidth - 108) / 2;
        _global2.default.init();
    }
};

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(4);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

var _Utils = __webpack_require__(11);

var _Utils2 = _interopRequireDefault(_Utils);

var _weextabbarConfig = __webpack_require__(10);

var _weextabbarConfig2 = _interopRequireDefault(_weextabbarConfig);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _navigator = __webpack_require__(3);

var _creader = __webpack_require__(7);

var _syncAblumInfo = __webpack_require__(12);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _user = __webpack_require__(8);

var _storage = __webpack_require__(14);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    props: {
        tabbarItemSelected: Number
    },
    data: function data() {
        return {
            topname: '订阅专栏',
            currentPage: 1,
            tabTitles: _weextabbarConfig2.default.tabTitles,
            tabStyles: _weextabbarConfig2.default.tabStyles,
            getImgPath: _common.getImgPath,
            netStatus: 0,
            followStatus: '',
            banners: [],
            albums: [],
            hotArticles: [],
            storagediv: [],
            global: _global2.default,
            clientUrl: "",
            errorType: '',
            errorMsg: '',
            height: '',
            albumInfo: []
        };
    },

    created: function created() {
        var deviceHeight = WXEnvironment.deviceHeight || unknown;
        var deviceWidth = WXEnvironment.deviceWidth || unknown;
        this.height = 750 / deviceWidth * deviceHeight;
        var self = this;
        _syncAblumInfo2.default.receive(function (res) {
            if (res.data.status == 'getData') {
                self.getData();
            } else {
                for (var i = 0; i < self.albums.length; i++) {
                    if (res.data.id == self.albums[i].id && res.data.status == 'add') {
                        self.albums[i].status = 1;
                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                    }
                    if (res.data.id == self.albums[i].id && res.data.status == 'cancel') {
                        self.albums[i].status = 0;
                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                    }
                }
            }
        });
    },
    computed: {
        Id: function Id() {
            var self = this;
            return self.albums.length;
        }
    },
    methods: {
        jumpToRead: function jumpToRead(id, title, des, img_url) {
            (0, _navigator.jumpSubPage)(this, 'articleDetails', { url: this.clientUrl + "?id=" + id, id: id, title: title, des: des, img_url: img_url });
        },
        loadData: function loadData() {
            var self = this;
            self.getData();
        },
        refresh: function refresh() {
            var self = this;
            _global2.default.init();
            self.netStatus = 0;
            self.getData();
            _syncAblumInfo2.default.receive(function (res) {
                for (var i = 0; i < self.albums.length; i++) {
                    if (res.data.id == self.albums[i].id && res.data.status == 'add') {
                        self.albums[i].status = 1;
                        self.albums[i].follow_count = self.albums[i].follow_count + 1;
                    }
                    if (res.data.id == self.albums[i].id && res.data.status == 'cancel') {
                        self.albums[i].status = 0;
                        self.albums[i].follow_count = self.albums[i].follow_count - 1;
                    }
                }
            });
        },
        getData: function getData() {
            var self = this;
            (0, _creader.getSubscribeInfo)().then(function (res) {
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined") {
                    var alreadyFollowList = res.data.data.alreadyFollowList;
                    console.log(alreadyFollowList);
                    var albumArrays = res.data.data.notSubscribedList;
                    self.albums = albumArrays.slice(0, 5);
                    console.log("qsdadk");
                    console.log(self.albums);
                    self.hotArticles = res.data.data.hotArticleList;
                    self.clientUrl = res.data.data.client_url;
                    self.banners = res.data.data.banners;

                    for (var i = 0; i < self.albums.length; i++) {
                        for (var k = 0; k < alreadyFollowList.length; k++) {
                            if (self.albums[i].id == alreadyFollowList[k].id) {
                                self.albums[i].status = 1;
                            }
                        }
                    }
                    self.loadinging = false;
                    self.netStatus = 2;
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        onchange: function onchange(event) {
            console.log('changed:', event.index);
        },
        bannerJump: function bannerJump(id, is_shop) {
            if (is_shop == 1) {
                (0, _navigator.jumpSubPage)(this, 'storeBookDetail', { id: id, title: "", des: "", img_url: '', from: 'shop' });
            }
        },
        jumpToAuthorColumn: function jumpToAuthorColumn(albumId, status) {
            (0, _navigator.jumpSubPage)(this, 'articleAlbumInfo', { id: albumId, status: status });
        },
        goToAllAlbums: function goToAllAlbums() {
            (0, _navigator.jumpSubPage)(this, 'articleAlbumAll');
        }
    }

}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(4);

var _navigator = __webpack_require__(3);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      global: _global2.default,
      getImgPath: _common.getImgPath,
      app: 'ion-android-apps',
      search: 'ion-search'
    };
  },

  props: {
    secondaryPage: {
      type: Boolean,
      default: false
    },
    name: {
      type: String,
      default: ''
    },
    height: {
      type: String,
      default: '100px'
    },
    textStyle: String,
    textColor: {
      type: String,
      default: 'white'
    },
    bgcolor: {
      type: String,
      default: '#E15D53'
    }
  },
  methods: {
    goBack: function goBack() {
      console.log("qingsong1");
      (0, _navigator.back)(this, "");
    },
    jumpToSearch: function jumpToSearch() {
      (0, _navigator.jumpSubPage)(this, "searchPage");
    },
    jumpToMySubscribe: function jumpToMySubscribe() {
      (0, _navigator.jumpSubPage)(this, "mySubscribe");
    }
  }
};

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(4);

var _navigator = __webpack_require__(3);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  data: function data() {
    return {
      global: _global2.default,
      getImgPath: _common.getImgPath
    };
  },

  props: {
    secondaryPage: {
      type: Boolean,
      default: false
    },
    name: {
      type: String,
      default: ''
    },
    height: {
      type: String,
      default: '100px'
    },
    textStyle: String,
    textColor: {
      type: String,
      default: 'white'
    },
    albumInfo: {
      type: Object
    },
    bgcolor: {
      type: String,
      default: '#E15D53'
    }
  },
  methods: {
    goBack: function goBack() {
      (0, _navigator.back)(this, "");
    },
    jumpToMySubscribe: function jumpToMySubscribe() {
      (0, _navigator.jumpSubPage)(this, "mySubscribe");
    },
    share: function share(albumInfo) {
      console.log(albumInfo);
      var url = "";
      this.$refs.commonFun.ShareArticle(albumInfo.author, url + albumInfo.id, albumInfo.img_url, albumInfo.remark);
    }
  }
};

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _common = __webpack_require__(4);

var _navigator = __webpack_require__(3);

var _global = __webpack_require__(5);

var _global2 = _interopRequireDefault(_global);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'topbar',
  data: function data() {
    return {
      global: _global2.default,
      getImgPath: _common.getImgPath
    };
  },

  props: {
    topbarname: String,
    textStyle: String,
    bgcolor: {
      type: String,
      default: '#f6f6f6'
    }
  },
  methods: {
    goBack: function goBack() {
      (0, _navigator.back)(this, "");
    }
  }
}; //
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//


exports.default = {
    props: {
        src: String,
        title: String,
        isLandscape: Boolean
    }
};

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  props: {
    show: {
      type: Boolean,
      default: false
    },
    single: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: ''
    },
    content: {
      type: String,
      default: ''
    },
    top: {
      type: Number,
      default: 400
    },
    cancelText: {
      type: String,
      default: '取消'
    },
    confirmText: {
      type: String,
      default: '确定'
    },
    mainBtnColor: {
      type: String,
      default: '#E15D53'
    },
    secondBtnColor: {
      type: String,
      default: '#666666'
    },
    showNoPrompt: {
      type: Boolean,
      default: false
    },
    noPromptText: {
      type: String,
      default: '不再提示'
    },
    isChecked: {
      type: Boolean,
      default: false
    }
  },
  data: function data() {
    return {
      pageHeight: 1334
    };
  },
  created: function created() {
    var _weex$config$env = weex.config.env,
        deviceHeight = _weex$config$env.deviceHeight,
        deviceWidth = _weex$config$env.deviceWidth;

    this.pageHeight = deviceHeight / deviceWidth * 750;
  },

  methods: {
    secondaryClicked: function secondaryClicked() {
      this.$emit('wxcDialogCancelBtnClicked', {
        type: 'cancel'
      });
    },
    primaryClicked: function primaryClicked(e) {
      this.$emit('wxcDialogConfirmBtnClicked', {
        type: 'confirm'
      });
    }
  }
};

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');
exports.default = {
  props: {
    show: {
      type: Boolean,
      default: true
    },
    hasAnimation: {
      type: Boolean,
      default: true
    },
    duration: {
      type: [Number, String],
      default: 300
    },
    timingFunction: {
      type: Array,
      default: function _default() {
        return ['ease-in', 'ease-out'];
      }
    },
    opacity: {
      type: [Number, String],
      default: 0.6
    },
    canAutoClose: {
      type: Boolean,
      default: true
    }
  },
  computed: {
    overlayStyle: function overlayStyle() {
      return {
        opacity: this.hasAnimation ? 0 : 1,
        backgroundColor: 'rgba(0, 0, 0,' + this.opacity + ')'
      };
    },
    shouldShow: function shouldShow() {
      var _this = this;

      var show = this.show,
          hasAnimation = this.hasAnimation;

      hasAnimation && setTimeout(function () {
        _this.appearOverlay(show);
      }, 50);
      return show;
    }
  },
  methods: {
    overlayClicked: function overlayClicked(e) {
      this.canAutoClose ? this.appearOverlay(false) : this.$emit('wxcOverlayBodyClicked', {});
    },
    appearOverlay: function appearOverlay(bool) {
      var _this2 = this;

      var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.duration;
      var hasAnimation = this.hasAnimation,
          timingFunction = this.timingFunction,
          canAutoClose = this.canAutoClose;

      var needEmit = !bool && canAutoClose;
      needEmit && this.$emit('wxcOverlayBodyClicking', {});
      var overlayEl = this.$refs['wxc-overlay'];
      if (hasAnimation && overlayEl) {
        animation.transition(overlayEl, {
          styles: {
            opacity: bool ? 1 : 0
          },
          duration: duration,
          timingFunction: timingFunction[bool ? 0 : 1],
          delay: 0
        }, function () {
          needEmit && _this2.$emit('wxcOverlayBodyClicked', {});
        });
      } else {
        needEmit && this.$emit('wxcOverlayBodyClicked', {});
      }
    }
  }
};

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//


exports.default = {
    props: {
        src: String,
        web: String
    }
};

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _navigator = __webpack_require__(3);

exports.default = {
    // mixins: [mixins],
    props: {
        tabItems: {
            type: Array,
            default: function _default() {
                return [];
            },
            required: true
        },

        styles: {
            type: Object,
            default: function _default() {
                return {};
            },
            required: false
        },

        height: {
            type: String,
            default: '108px'
        },
        network: {
            default: 1
        }
    },

    data: function data() {
        return {
            selectedTab: 0,
            translateX: 'translateX(0px)',
            deviceWidth: 750,
            titleStyle: {}
        };
    },
    created: function created() {
        var self = this;
        self.totalWidth = self.deviceWidth * self.tabItems.length;
    },
    mounted: function mounted() {
        var self = this;
        if (self.network == 1) {
            self.selectedTab = 0;
        } else if (self.network == 3) {
            self.selectedTab = 1;
            self.$emit('wxChange', self.selectedTab);
        } else {
            self.selectedTab = 3;
            self.$emit('wxChange', self.selectedTab);
        }
        // self.totalWidth = self.deviceWidth * self.tabItems.length;
        self.setTranslateX();
    },


    methods: {
        changeTab: function changeTab(item, index) {
            if (2 == index) {
                this.$emit('wxChange', index);
                (0, _navigator.jumpSubPage)(this, 'chat', {});
            } else {
                this.selectedTab = index;
                this.setTranslateX();
                this.$emit('wxChange', index);
            }
        },
        setTranslateX: function setTranslateX() {
            var x = this.selectedTab * 750; //this.deviceWidth;
            this.translateX = 'translateX(-' + x + 'px)';
        },
        getStyles: function getStyles() {
            var baseStyle = {
                'bottom': 0,
                'height': this.height
            };
            return Object.assign({}, baseStyle, this.styles);
        },
        getIconStyle: function getIconStyle(item) {
            return {
                width: item.iconWdith || '48px',
                height: item.iconHeight || '48px'
            };
        },
        getTitleStyle: function getTitleStyle(item) {
            return {
                'font-size': item.fontSize || '28px',
                'color': this.selectedTab === item.index ? item.selectedColor : item.titleColor
            };
        }
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by zwwill on 2017/8/27.
 */

var setting = {
    getPlatform: function getPlatform() {
        return weex.config.env.platform.toLowerCase();
    },
    isWeb: function isWeb() {
        return this.getPlatform() == "web";
    },
    isAndroid: function isAndroid() {
        return this.getPlatform() == "android";
    },
    isIOS: function isIOS() {
        return this.getPlatform() == "ios";
    }
};

exports.default = setting;

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
    "loading": "Loading",
    "networkError": "Unable to get network data, please connect to network and try again",
    "refresh": "Refresh",
    "columnInfoItro": "Album Introduction",
    "suggestion": "Recommended Album",
    "alreadyFollow": "Aready Follow",
    "videoplay": "Video Playing",
    "follow": "follow",
    "videofrom": "VideoFrom",
    "videoList": "Recommended Videos",
    "recommendation": "Excellent Column Recommendation",
    "recommendedvideos": "Excellent Video Recommendation"

};
exports.lang = lang;

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
					"loading": "加載中",
					"networkError": "無法獲取網絡數據 請連接網絡後重試",
					"refresh": "刷新",
					"columnInfoItro": "專輯介紹",
					"suggestion": "專欄推薦",
					"alreadyFollow": "已訂閱專欄",
					"videoplay": "視頻播放",
					"follow": "訂閱",
					"videofrom": "視頻來源",
					"videoList": "本專欄視頻推薦",
					"recommendation": "優秀專欄推薦",
					"recommendedvideos": "優秀視頻推薦"
};
exports.lang = lang;

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var lang = {
    "loading": "加载中",
    "networkError": "无法获取网络数据 请连接网络后重试",
    "refresh": "刷新",
    "columnInfoItro": "专辑介绍",
    "suggestion": "专栏推荐",
    "alreadyFollow": "已订阅专栏",
    "videoplay": "视频播放",
    "follow": "订阅",
    "videofrom": "视频来源",
    "videoList": "本专栏视频推荐",
    "recommendation": "优秀专栏推荐",
    "recommendedvideos": "优秀视频推荐"
};
exports.lang = lang;

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.video[data-v-00ff98bc] {\n    width: 750px;\n    height: 480px;\n}\n", ""]);

// exports


/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-02bfbb92]{\n    /*position: absolute;\n    left:0;\n    top:0;\n    right:0;\n    bottom:0;*/\n    background-color: #f5f5f5;\n    /*background-color: green;*/\n    width:750px;\n}\n.scroller[data-v-02bfbb92]{\n    position: relative;\n    left:0;\n    top:0;\n    right:0;\n    bottom:120px;\n    /*background-color: lightblue;*/\n}\n.slider[data-v-02bfbb92] {\n    height:320px;\n    width:750px;\n    /*margin-left:15px;*/\n    /*margin-top:20px;*/\n    /*border-radius: 10px;*/\n}\n.image[data-v-02bfbb92] {\n    width: 750px;\n    height:320px;\n    /*border-radius: 10px;*/\n}\n.frame[data-v-02bfbb92] {\n    width: 720px;\n    height:320px;\n    position:relative;\n}\n.indicator[data-v-02bfbb92] {\n   width: 200px;\n   height: 30px;\n   item-color: white;\n   item-selected-color: #E15D53;\n   item-size: 20px;\n   position: absolute;\n   top: 290px;\n   left: 275px;\n}\n.suggestionBar[data-v-02bfbb92]{\n     width:750px;\n     height:116px;\n     display: flex;\n     flex-direction: row;\n     align-items: center;\n     padding-left: 30px;\n     padding-right: 30px;\n     /*border-top-width:20px;\n     border-top-style: solid;\n     border-top-color: white;*/\n     justify-content: space-between;\n     background-color:#f6f6f6;\n}\n.suggestionBar1[data-v-02bfbb92]{\n    width:750px;\n     height:96px;\n     display: flex;\n     flex-direction: row;\n     align-items: center;\n     padding-left: 30px;\n     padding-right: 30px;\n     justify-content: space-between;\n     background-color:#f6f6f6;\n}\n.text_suggestion[data-v-02bfbb92]{\n    color: #959595;\n    font-size:27px;\n}\n.text_suggestion_icon[data-v-02bfbb92]{\n    color: #959595;\n    font-size: 31.83px;\n}\n.suggestionList[data-v-02bfbb92]{\n    height:112px;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n   /* border-bottom-width:1px;\n    border-bottom-style: solid;\n    border-bottom-color: #e5e5e5;\n    border-top-width:1px;\n    border-top-style: solid;\n    border-top-color: #e5e5e5;*/\n    background-color: #ffffff;\n    /*background-color: yellow;*/\n}\n.album_image[data-v-02bfbb92]{\n    width:65.1px;\n    height:65.1px;\n    border-radius: 33px;\n    margin-left: 15px;\n    margin-right:15px;\n}\n.albumInfo[data-v-02bfbb92]{\n    width:650px;\n    height:112px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    /*margin-top: 15px;*/\n    /*margin-right: 35px;*/\n    margin-left:20px;\n    border-bottom-width:1px;\n    border-bottom-style: solid;\n    border-bottom-color: #e5e5e5;\n    /*background-color: lightblue;*/\n}\n.albumInfo_last[data-v-02bfbb92]{\n    width:650px;\n    height:112px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    /*margin-top: 15px;*/\n    /*margin-right: 35px;*/\n    margin-left:20px;\n}\n.albumInfoDetail[data-v-02bfbb92]{\n    height:100px;\n    width:430px;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: flex-start;\n    padding-bottom: 10px;\n    /*background-color: lightgreen;*/\n}\n.article_read_recommend[data-v-02bfbb92]{\n    color: #B6B6B6;\n    font-size: 21px;\n}\n.album_NS[data-v-02bfbb92]{\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: center;\n    padding-top: 5px;\n}\n.album_subscribed[data-v-02bfbb92]{\n    border:1px solid grey;\n    border-width:1px;\n    border-style: solid;\n    border-color: #D2D2D2;\n    border-radius: 3px;\n    padding: 5px;\n    margin-left: 30px;\n}\n.text_album_subscribed[data-v-02bfbb92]{\n    font-size: 18px;\n    color:#B6B6B6;\n}\n.text_album_name[data-v-02bfbb92]{\n    font-size: 29.4px;\n    font-weight: bold;\n    color:#545454;\n    padding-bottom: 8px;\n    width:200px;\n    height:40px;\n    overflow: hidden;\n}\n.album_des[data-v-02bfbb92]{\n    color: #888888;\n    font-size: 21px;\n    padding-top: 8px;\n    lines:1;\n}\n.subscribeBtn[data-v-02bfbb92]{\n    width:132px;\n    height:50px;\n    background-color: #E15D53;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    margin-right: 30px;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 15px;\n    padding-left: 15px;\n    padding-right: 15px;\n    /*background-color: lightblue;*/\n}\n.subscribeBtn-subscribed[data-v-02bfbb92]{\n    width:132px;\n    height:50px;\n    background-color: white;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    margin-right: 30px;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 15px;\n}\n.text_subscribeBtn[data-v-02bfbb92]{\n    color:white;\n    font-size: 24px;\n}\n.text_subscribedBtn_[data-v-02bfbb92]{\n    color:#E15D53;\n    font-size: 24px;\n}\n.img_subscribeBtn[data-v-02bfbb92]{\n    width:24px;\n    height:24px;\n}\n.title[data-v-02bfbb92]{\n    color:#101010;\n    font-size:31px;\n    font-family: \"Helvetica Neue\",sans-serif;\n    font-weight: bold;\n    /*padding-top: 20px;*/\n    padding-bottom: 15px;\n    padding-right: 30px;\n    /*lines:2;*/\n    /*letter-spacing: 10;*/\n}\n.article-des[data-v-02bfbb92]{\n    color:#8f8f8f;\n    font-size: 29px;\n    line-height: 29px;\n    text-overflow: ellipsis;\n    font-family: Segoe UI;\n    lines:2;\n}\n.article-mid[data-v-02bfbb92]{\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: center;\n    padding-left: 30px;\n    padding-right: 30px;\n    padding-top: 25px;\n    padding-bottom: 25px;\n    background-color: #ffffff;\n    /*background-color: lightgreen;*/\n    margin-bottom: 19px;\n}\n.article-content[data-v-02bfbb92]{\n    flex-direction: column;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: flex-start;\n    width:400px;\n    height: 194px;\n    /*padding-top:30px;*/\n    /*padding-bottom: 30px;*/\n    font-family: Segoe UI;\n    /*background-color: lightblue;*/\n}\n\n", ""]);

// exports


/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-057107c2]{\n    background-color: #f5f5f5;\n    width:750px;\n    /*justify-content: center;\n    align-items: center;*/\n}\n.list[data-v-057107c2]{\n    width:750px;\n    position: absolute;\n    top:100px;\n    background-color: #f5f5f5;\n}\n.article[data-v-057107c2]{\n    /*margin-bottom: 19px;*/\n    border-bottom-width: 19px;\n    border-bottom-style: solid;\n    border-bottom-color: #f5f5f5;\n    background-color: #ffffff;\n    padding-bottom: 15px;\n}\n.article-top[data-v-057107c2]{\n    height:98px;\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: center;\n    border-bottom-width: 2px;\n    border-bottom-style: solid;\n    border-bottom-color: #ececec;\n    padding-left: 30px;\n    padding-right: 30px;\n    font-family: Segoe UI;\n    /*background-color: lightgreen;*/\n}\n.article-name[data-v-057107c2]{\n    color:#545454;\n    font-size:28px;\n    margin-left:33px;\n    font-family: Segoe UI;\n    /*letter-spacing: 13px;*/\n    /*line-height: 45px;*/\n}\n.article-tag[data-v-057107c2]{\n    color:#888888;\n    font-size: 25px;\n    margin-left:20px;\n    font-family: Segoe UI;\n    /*background-color: yellow;*/\n}\n.article-time[data-v-057107c2]{\n    width:150px;\n    margin-left: 20px;\n    color:#888888;\n    font-size: 22px;\n    text-align: right;\n    font-family: Segoe UI;\n    /*background-color: red;*/\n}\n.title[data-v-057107c2]{\n    color:#101010;\n    font-size:29.4px;\n    font-family: \"Helvetica Neue\",sans-serif;\n    font-weight: bold;\n    padding-bottom: 15px;\n    /*lines:2;*/\n}\n.title-spread[data-v-057107c2]{\n    color:#2f2f2f;\n    font-size:29.4px;\n    font-family: \"Helvetica Neue\",sans-serif;\n    font-weight: bold;\n    padding-bottom: 15px;\n    /*lines:2;*/\n}\n.article-des[data-v-057107c2]{\n    color:#888888;\n    font-size: 25px;\n    padding: 0;\n    text-overflow: ellipsis;\n    lines:1;\n}\n.article-des-spread[data-v-057107c2]{\n    color:#888888;\n    font-size: 25px;\n    text-overflow: ellipsis;\n    lines:1;\n}\n.article-mid[data-v-057107c2]{\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: flex-start;\n    padding-top: 15px;\n    padding-bottom: 15px;\n    padding-left: 30px;\n    padding-right: 30px;\n    /*background-color: lightblue;*/\n}\n.article-content[data-v-057107c2]{\n    flex-direction: column;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: flex-start;\n    width:420px;\n    height: 195px;\n    /*padding-bottom: 10px;*/\n    padding-right: 34px;\n    /*background-color: lightgreen;*/\n}\n.article-content-spread[data-v-057107c2]{\n    flex-direction: column;\n    flex-wrap: nowrap;\n    justify-content: center;\n    align-items: flex-start;\n    width:700px;\n    /*padding-top:10px;*/\n    /*padding-bottom: 30px;*/\n    padding-right: 34px;\n    font-family: Segoe UI;\n    /*background-color: lightblue;*/\n}\n.article-bottom[data-v-057107c2]{\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: space-between;\n    align-items: center;\n    padding-left: 30px;\n    padding-right: 30px;\n    /*background-color: lightblue;*/\n}\n.article-bottom-left[data-v-057107c2]{\n    flex-direction: row;\n    flex-wrap: nowrap;\n    justify-content: flex-start;\n}\n.img_subscribeBtn[data-v-057107c2]{\n    width:34px;\n    height:34px;\n}\n.loading[data-v-057107c2] {\n    width: 750;\n    display: -ms-flex;\n    display: -webkit-flex;\n    display: flex;\n    -ms-flex-align: center;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    align-items: center;\n}\n.indicator-text[data-v-057107c2] {\n    color: #E15D53;\n    font-size: 42px;\n    text-align: center;\n}\n.indicator[data-v-057107c2] {\n    margin-top: 16px;\n    height: 40px;\n    width: 40px;\n    color: #E15D53;\n}\n.loadingCircle[data-v-057107c2]{\n    position: absolute;\n    top:0;\n    bottom: 0;\n    left:0;\n    right:0;\n    justify-content: center;\n    align-items: center;\n}\n\n", ""]);

// exports


/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wx-tabbar[data-v-0ba33bfc] {\n    width:750px;\n}\n.tab-component[data-v-0ba33bfc] {\n    flex-direction: row;\n}\n.tabbar[data-v-0ba33bfc] {\n    width: 750px;\n    position: fixed;\n    left: 0;\n    right: 0;\n    bottom:0;\n    /*padding-bottom: 15px;*/\n    z-index: 1000;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    border-top-width: 1px;\n    border-top-style: solid;\n    border-top-color: #D8D8D8;\n    background-color: #fff;\n}\n.tabbar-item[data-v-0ba33bfc] {\n    flex: 1;\n    flex-direction: column;\n    align-items: center;\n    justify-content: center;\n}\n.icon[data-v-0ba33bfc] {\n    /*margin-top: 14px;\n    margin-bottom: 10px;*/\n    width: 48px;\n    height: 48px;\n}\n.wx-text[data-v-0ba33bfc] {\n    font-size: 25px;\n    padding-top: 2px;\n    text-align: center;\n    color: #646464;\n}\n", ""]);

// exports


/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.container[data-v-14dbd79c] {\n  position: fixed;\n  width: 750px;\n  /*兼容H5异常*/\n  z-index: 99999;\n}\n.dialog-box[data-v-14dbd79c] {\n  position: fixed;\n  left: 96px;\n  width: 558px;\n  background-color: #FFFFFF;\n}\n.dialog-content[data-v-14dbd79c] {\n  padding-top: 36px;\n  padding-bottom: 36px;\n  padding-left: 36px;\n  padding-right: 36px;\n}\n.content-title[data-v-14dbd79c] {\n  color: #333333;\n  font-size: 36px;\n  text-align: center;\n  margin-bottom: 24px;\n}\n.content-subtext[data-v-14dbd79c] {\n  color: #666666;\n  font-size: 26px;\n  line-height: 36px;\n  text-align: center;\n}\n.dialog-footer[data-v-14dbd79c] {\n  flex-direction: row;\n  align-items: center;\n  border-top-color: #F3F3F3;\n  border-top-width: 1px;\n}\n.footer-btn[data-v-14dbd79c] {\n  flex-direction: row;\n  align-items: center;\n  justify-content: center;\n  flex: 1;\n  height: 90px;\n}\n.cancel[data-v-14dbd79c] {\n  border-right-color: #F3F3F3;\n  border-right-width: 1px;\n}\n.btn-text[data-v-14dbd79c] {\n  font-size: 36px;\n  color: #666666;\n}\n.no-prompt[data-v-14dbd79c] {\n  width: 486px;\n  align-items: center;\n  justify-content: center;\n  flex-direction: row;\n  margin-top: 24px;\n}\n.no-prompt-icon[data-v-14dbd79c] {\n  width: 24px;\n  height: 24px;\n  margin-right: 12px;\n}\n.no-prompt-text[data-v-14dbd79c] {\n  font-size: 24px;\n  color: #A5A5A5;\n}\n", ""]);

// exports


/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n", ""]);

// exports


/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body{\n        /*position: absolute;\n        left:0;\n        top:0;\n        right:0;\n        bottom:0;*/\n        width:750px;\n        /*background-color: purple;*/\n}\n", ""]);

// exports


/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.netDivError{\n    width:750px;\n    display: flex;\n    flex-flow: column;\n    align-items:center;\n    justify-content: center;\n    height:300px;\n}\n.netTextError{\n    padding-top: 10px;\n    padding-bottom: 10px;\n    font-size:35px;\n}\n.button{\n    width: 280px;\n    height:80px;\n    border-width: 1px;\n    border-color: black;\n    border-style:solid;\n    text-align: center;\n    line-height:80px;\n    font-size: 35px;\n    border-radius: 10px;\n    transform: translateY(50px);\n}\n", ""]);

// exports


/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.loadingCircle[data-v-22ff7e99]{\n    position: absolute;\n    bottom: 0;\n    left:0;\n    right:0;\n    justify-content: flex-start;\n    align-items: center;\n    /*background-color: green;*/\n}\n.box[data-v-22ff7e99] {\n    width: 50px;\n    height: 50px;\n    border-top-width: 6px;\n    border-top-left-radius: 25px;\n    border-top-color: #E15D53;\n    border-left-width: 5px;\n    border-left-color: #E15D53;\n    border-radius: 25px;\n}\n", ""]);

// exports


/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.loadingCircle{\n    position: absolute;\n    top: 0px;\n    left:0;\n    right:0;\n    bottom:0;\n    justify-content: center;\n    align-items: center;\n    background-color: rgba(0,0,0,0.2);\n}\n.loading {\n    width: 750;\n    display: -ms-flex;\n    display: -webkit-flex;\n    display: flex;\n    -ms-flex-align: center;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    align-items: center;\n}\n.indicator-text {\n    color: #E15D53;\n    font-size: 42px;\n    text-align: center;\n}\n.indicator {\n    margin-top: 16px;\n    height: 40px;\n    width: 40px;\n    color: #E15D53;\n}\n", ""]);

// exports


/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-overlay[data-v-3858c23c] {\n  width: 750px;\n  position: fixed;\n  left: 0;\n  top: 0;\n  bottom: 0;\n  right: 0;\n}\n", ""]);

// exports


/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-overlay[data-v-3b6fe780] {\n  width: 750px;\n  position: fixed;\n  left: 0;\n  top: 0;\n  bottom: 0;\n  right: 0;\n}\n", ""]);

// exports


/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-search-bar[data-v-49098c90] {\n  padding-left: 30px;\n  padding-right: 30px;\n  background-color: #ffffff;\n  width: 750px;\n  height: 100px;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n.wxc-search-bar-yellow[data-v-49098c90] {\n  background-color: #ffc900;\n}\n.search-bar-input[data-v-49098c90] {\n  /*position: absolute;\n  top: 10px;*/\n  left: ;\n  padding-top: 0;\n  padding-bottom: 0;\n  padding-right: 40px;\n  padding-left: 60px;\n  font-size: 26px;\n  width: 604px;\n  height: 80px;\n  line-height: 80px;\n  background-color: #ffffff;\n  /*background-color: red;*/\n  border-radius: 6px;\n}\n.search-bar-input-yellow[data-v-49098c90] {\n  background-color: #fff6d6;\n}\n.search-bar-icon[data-v-49098c90] {\n}\n.search-bar-close[data-v-49098c90] {\n  position: absolute;\n  width: 30px;\n  height: 30px;\n  right: 120px;\n  top: 36px;\n}\n.search-bar-button[data-v-49098c90] {\n  margin-right: 0;\n  color: #333333;\n}\n.search-bar-button[data-v-49098c90]:active{\n  background-color: #fff6d6;\n}\n.search-bar-button-yellow[data-v-49098c90] {\n  background-color: #FFC900;\n}\n.input-has-dep[data-v-49098c90] {\n  padding-left: 240px;\n  width: 710px;\n}\n.bar-dep[data-v-49098c90] {\n  width: 170px;\n  padding-right: 12px;\n  padding-left: 12px;\n  height: 42px;\n  align-items: center;\n  flex-direction: row;\n  position: absolute;\n  left: 24px;\n  top: 22px;\n  border-right-style: solid;\n  border-right-width: 1px;\n  border-right-color: #C7C7C7;\n}\n.bar-dep-yellow[data-v-49098c90] {\n  border-right-color: #C7C7C7;\n}\n.dep-text[data-v-49098c90] {\n  flex: 1;\n  text-align: center;\n  font-size: 26px;\n  color: #666666;\n  margin-right: 6px;\n  lines: 1;\n  text-overflow: ellipsis;\n}\n.dep-arrow[data-v-49098c90] {\n  width: 24px;\n  height: 24px;\n}\n.icon-has-dep[data-v-49098c90] {\n  left: 214px;\n}\n.disabled-input[data-v-49098c90] {\n  width: 750px;\n  height: 64px;\n  position: absolute;\n  left: 0;\n  background-color: transparent;\n}\n.has-dep-disabled[data-v-49098c90] {\n  width: 550px;\n  left: 200px;\n}\n", ""]);

// exports


/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.back[data-v-55d0704b]{\n  width:40px; height:90px; justify-content: center; align-items: center;\n}\n.wxc-tab-page[data-v-55d0704b] {\n  height:102px;\n  width: 750px;\n  flex-direction: row;\n  justify-content: space-around;\n  align-items: center;\n  border-bottom-style: solid;\n  border-bottom-width: 2px;\n  border-bottom-color: #f0f0f0;\n  /*box-shadow:  0 15px 30px rgba(0, 0, 0, 0.2);*/\n}\n.title-item[data-v-55d0704b] {\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-bottom-style:solid;\n  border-bottom-width:3px;\n}\n.tab-text[data-v-55d0704b]{\n  height:102px;\n  font-weight: bold;\n  line-height: 102px;\n}\n\n", ""]);

// exports


/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n/*.test {*/\n\n    /*height: 500px;*/\n/*}*/\n", ""]);

// exports


/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.back[data-v-647bf301]{\n      width:100px; height:100px; justify-content: center; align-items: center;\n}\n.topbar[data-v-647bf301]{\n  position: fixed;\n  top:0;\n  width: 750px;\n  height:100px;\n  flex-direction: row;\n  justify-content: space-between;\n  /*background-color: #f6f6f6;*/\n}\n.right-part[data-v-647bf301]{\n  width:100px;\n  height:100px;\n}\n.backButton[data-v-647bf301]{\n  width:100px;\n  height:100px;\n  text-align: center;\n  line-height:100px;\n  font-size: 40px;\n  position: absolute;\n  left:0px;\n}\n.appName[data-v-647bf301]{\n  font-size: 45px;\n  font-weight: bold;\n  line-height: 100px;\n  font-family: \"SimSun\";\n}\n", ""]);

// exports


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-popup[data-v-6876521c] {\n  position: fixed;\n  width: 750px;\n}\n.top[data-v-6876521c] {\n  left: 0;\n  right: 0;\n}\n.bottom[data-v-6876521c] {\n  left: 0;\n  right: 0;\n}\n.left[data-v-6876521c] {\n  bottom: 0;\n  top: 0;\n}\n.right[data-v-6876521c] {\n  bottom: 0;\n  top: 0;\n}\n\n", ""]);

// exports


/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.wxc-progress[data-v-71f69050] {\n    background-color: #f2f3f4;\n}\n.progress[data-v-71f69050] {\n    position: absolute;\n    background-color: #FFC900;\n}\n", ""]);

// exports


/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.content[data-v-7c36821a]{\n  flex-direction: row;\n  flex-flow: nowrap;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 30px;\n  padding-right: 30px;\n}\n.title[data-v-7c36821a]{\n   font-size:34px;\n   font-family: Roboto;\n}\n", ""]);

// exports


/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.donateDiv{\n    padding: 30px;\n}\n.donatConformText{\n    \n    font-size: 40px;\n    font-weight: bold;\n    color: #ffffff;\n}\n.donatConform{\n   flex-direction:row;\n    margin-top: 50px;\n    /* background-color:aqua; */\n     background-color: #FF8A80;\n    height: 80px;\n    width: 280px;\n   \n    /* border: #888888 ;\n    border-width: 3px; */\n    display: flex;\n    border-radius:5px;\n    \njustify-content:center;\nalign-items:center;\nalign-self: center ;\n}\n.donatetext{\n    margin-top: 20px;\n    font-size:35px;\n    color:#545454;\n}\n.input { \n    width: 690px;\n     height:80px;\n        border-width:1px;\n        border-style: solid;\n        border-color: #888888;\n        border-radius: 5px;\n        margin-top:50px;\n        padding:10px;\n}\n.body{\n        /*position: absolute;\n        left:0;\n        top:0;\n        right:0;\n        bottom:0;*/\n        background-color: #f8f8f8;\n        width:750px;\n}\n.suggestionBar{\n        width:750px;\n        height:96px;\n        display: flex;\n        flex-direction: row;\n        align-items: center;\n        padding-left: 20px;\n        padding-right: 20px;\n        justify-content: space-between;\n        background-color:#ffffff;\n}\n.suggestionBar1{\n        width:750px;\n        /*height:100px;*/\n        margin-top:20px;\n        flex-direction: row;\n        align-items: center;\n        padding-left: 20px;\n        padding-right: 20px;\n        padding-top: 30px;\n        padding-bottom: 30px;\n        justify-content: space-between;\n        background-color:#ffffff;\n}\n.top_img{\n         width:48px;\n         height:48px;\n}\n.top_img1{\n         width:36px;\n         height:36px;\n}\n.top_icon{\n        width:48px;\n        height:48px;\n        /*background-color: yellow;*/\n}\n.top_icon1{\n        width:36px;\n        height:36px;\n        /*background-color: yellow;*/\n}\n.suggestionList{\n        height:195px;\n        flex-direction: row;\n        justify-content: flex-start;\n        align-items: center;\n        padding:20px;\n        background-color: #ffffff;\n}\n.album_image{\n        width:130px;\n        height:130px;\n        border-radius: 50px;\n        margin-right:20px;\n        /*background-color: green;*/\n}\n.albumInfo{\n        width:580px;\n        height:120px;\n        display: flex;\n        flex-direction: row;\n        justify-content: space-between;\n        padding: 20px;\n        align-items: center;\n        margin-top: 15px;\n        margin-bottom: 15px;\n        margin-right: 35px;\n        /*background-color: lightblue;*/\n}\n.albumInfoDetail{\n        height:120px;\n        width:470px;\n        display: flex;\n        flex-direction: column;\n        justify-content: space-between;\n        align-items: flex-start;\n        padding-bottom: 10px;\n        /*background-color: lightgreen;*/\n}\n.album_NS{\n        display: flex;\n        flex-direction: row;\n        justify-content: flex-start;\n        align-items: center;\n        padding-top: 5px;\n}\n.album_subscribed{\n        border:1px solid grey;\n        border-width:1px;\n        border-style: solid;\n        border-color: #D2D2D2;\n        border-radius: 3px;\n        padding: 5px;\n        margin-left: 30px;\n}\n.text_album_subscribed{\n        font-size: 18px;\n        color:#888888;\n}\n.text_album_name{\n        font-size: 40px;\n        font-weight: bold;\n        color:#202020;\n        padding-bottom: 10px;\n}\n.album_des{\n        color: #9e9e9e;\n        font-size: 29px;\n        padding-top: 8px;\n}\n.img_subscribeBtn{\n        width:36px;\n        height:36px;\n        /*background-color: yellow;*/\n}\n", ""]);

// exports


/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.content[data-v-a7d346f2]{\n  flex-direction: row;\n  flex-flow: nowrap;\n  justify-content: space-between;\n  align-items: center;\n  padding-left: 30px;\n  padding-right: 30px;\n}\n.title[data-v-a7d346f2]{\n   font-size:34px;\n}\n", ""]);

// exports


/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.suggestionList[data-v-d5b6ba2a]{\n    height:112px;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    padding-left: 30px;\n    padding-right: 30px;\n   /* border-bottom-width:1px;\n    border-bottom-style: solid;\n    border-bottom-color: #e5e5e5;\n    border-top-width:1px;\n    border-top-style: solid;\n    border-top-color: #e5e5e5;*/\n    background-color: #ffffff;\n    /*background-color: yellow;*/\n}\n.album_image[data-v-d5b6ba2a]{\n    width:65.1px;\n    height:65.1px;\n    border-radius: 33px;\n    margin-right:15px;\n}\n.albumInfo[data-v-d5b6ba2a]{\n    width:630px;\n    height:112px;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    /*margin-top: 15px;*/\n    /*margin-right: 35px;*/\n    margin-left:10px;\n    border-bottom-width:1px;\n    border-bottom-style: solid;\n    border-bottom-color: #e5e5e5;\n    /*background-color: lightblue;*/\n}\n.albumInfo_last[data-v-d5b6ba2a]{\n    width:630px;\n    height:112px;\n    display: flex;\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    /*margin-top: 15px;*/\n    /*margin-right: 35px;*/\n    margin-left:10px;\n    /*background-color: lightblue;*/\n}\n.albumInfoDetail[data-v-d5b6ba2a]{\n    height:100px;\n    width:430px;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    align-items: flex-start;\n    padding-bottom: 10px;\n    /*background-color: lightgreen;*/\n}\n.article_read_recommend[data-v-d5b6ba2a]{\n    color: #B6B6B6;\n    font-size: 21px;\n}\n.album_NS[data-v-d5b6ba2a]{\n    flex-direction: row;\n    justify-content: flex-start;\n    align-items: center;\n    padding-top: 5px;\n}\n.album_subscribed[data-v-d5b6ba2a]{\n    border:1px solid grey;\n    border-width:1px;\n    border-style: solid;\n    border-color: #D2D2D2;\n    border-radius: 3px;\n    padding: 5px;\n    margin-left: 30px;\n}\n.text_album_subscribed[data-v-d5b6ba2a]{\n    font-size: 18px;\n    color:#B6B6B6;\n}\n.text_album_name[data-v-d5b6ba2a]{\n    font-size: 29.4px;\n    font-weight: bold;\n    color:#545454;\n    padding-bottom: 8px;\n    /*width:200px;*/\n    height:40px;\n    overflow: hidden;\n}\n.album_des[data-v-d5b6ba2a]{\n    color: #888888;\n    font-size: 21px;\n    padding-top: 8px;\n    text-overflow: ellipsis;\n    lines:1;\n}\n.subscribeBtn[data-v-d5b6ba2a]{\n    width:132px;\n    height:50px;\n    background-color: #E15D53;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    margin-right: 30px;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 15px;\n    padding-left: 15px;\n    padding-right: 15px;\n    /*background-color: lightblue;*/\n}\n.subscribeBtn-subscribed[data-v-d5b6ba2a]{\n    width:132px;\n    height:50px;\n    background-color: white;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    margin-right: 30px;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 15px;\n}\n.text_subscribeBtn[data-v-d5b6ba2a]{\n    color:white;\n    font-size: 24px;\n}\n.text_subscribedBtn_[data-v-d5b6ba2a]{\n    color:#888888;\n    font-size: 24px;\n}\n.img_subscribeBtn[data-v-d5b6ba2a]{\n    width:24px;\n    height:24px;\n}\n", ""]);

// exports


/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.netError{\n    position: absolute;\n    top:0;\n    bottom: 0;\n    left:0;\n    right:0;\n    justify-content: center;\n    align-items: center;\n}\n.netError1{\n    justify-content: center;\n    align-items: center;\n}\n.netError2{\n    justify-content: center;\n    align-items: center;\n}\n.errorHint{\n    font-size: 30;\n    color:#8f8f8f;\n    margin-top:30px;\n}\n.refresh{\n    margin-top:50px;\n    font-size: 35;\n    color:white;\n    width:550;\n    height:80;\n    background-color: #E15D53;\n    text-align: center;\n    line-height: 80;\n    border-radius: 30;\n    box-shadow: 0 15px 30px rgba(0, 0, 0, 0.2);\n}\n", ""]);

// exports


/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.icon-block {\n    justify-content: flex-start;\n    /*background-color: red;*/\n}\n.icon {\n    text-align: center;\n}\n", ""]);

// exports


/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-f392ce2e]{\n     width:750px;\n}\n.top[data-v-f392ce2e]{\n     position: absolute;\n     top:0;\n     left: 0;\n     right: 0;\n     flex-direction: row;\n     flex-wrap: nowrap;\n     justify-content: space-around;\n     align-items: center;\n     width:750px;\n     background-color: #fdfdfd;\n     /*border-bottom-color: #e9e9e9;\n     border-bottom-width: 2px;\n     border-bottom-style: solid;*/\n     /* background-color: red; */\n}\n.type[data-v-f392ce2e]{\n     font-size: 38px;\n     color:#636363;\n     font-weight: bold;\n}\n.content[data-v-f392ce2e]{\n     /*flex-direction: row;\n     flex-wrap: wrap;\n     justify-content: flex-start;\n     align-items: flex-start;*/\n     position: relative;\n     top:102px;\n     right:0px;\n     left:0px;\n}\n.netError[data-v-f392ce2e]{\n     position: absolute;\n     top:102;\n     bottom: 0;\n     left:0;\n     right:0;\n     justify-content: center;\n     align-items: center;\n}\n.netError2[data-v-f392ce2e]{\n     justify-content: center;\n     align-items: center;\n}\n.alreadyFollowList_album[data-v-f392ce2e]{\n     width:200px;\n     height: 350px;\n     margin-left:20px;\n     margin-top: 37px;\n     flex-direction: column;\n     justify-content: flex-start;\n     align-items: center;\n     /*background-color: red;*/\n}\n.alreadyFollowList_album_name[data-v-f392ce2e]{\n     font-size: 26px;\n     margin-top: 26px;\n     margin-bottom: 12px;\n     margin-left: 12px;\n     margin-right: 12px;\n     color:#505050;\n}\n", ""]);

// exports


/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(143)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(45),
  /* template */
  __webpack_require__(116),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-3b6fe780",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/node_modules/weex-ui/packages/wxc-overlay/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3b6fe780", Component.options)
  } else {
    hotAPI.reload("data-v-3b6fe780", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(149)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(46),
  /* template */
  __webpack_require__(122),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-6876521c",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/node_modules/weex-ui/packages/wxc-popup/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6876521c", Component.options)
  } else {
    hotAPI.reload("data-v-6876521c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('video', {
    staticClass: "video",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "id": "videPlay",
      "src": _vm.src,
      "autoplay": "",
      "controls": ""
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-00ff98bc", module.exports)
  }
}

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height
    }))
  }, [_c('topNavigationWidget', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": _vm.topname
    }
  }), _vm._v(" "), (_vm.netStatus == 2) ? _c('scroller', {
    staticClass: "scroller",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height - 222
    }))
  }, [_c('slider', {
    staticClass: "slider",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "interval": "5000",
      "infinite": "true",
      "auto-play": "true"
    },
    on: {
      "change": _vm.onchange
    }
  }, [_vm._l((_vm.banners), function(banner) {
    return _c('div', {
      staticClass: "frame",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('image', {
      staticClass: "image",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "resize": "stretch",
        "src": banner.img_url
      },
      on: {
        "click": function($event) {
          _vm.bannerJump(banner.article_id, banner.is_shop)
        }
      }
    })])
  }), _vm._v(" "), _c('indicator', {
    staticClass: "indicator",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  })], 2), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": function($event) {
        _vm.goToAllAlbums()
      }
    }
  }, [_c('text', {
    staticClass: "text_suggestion",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("优秀专栏推荐")]), _vm._v(" "), _c('text', {
    staticClass: "text_suggestion_icon",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(">")])]), _vm._v(" "), _c('followAlbum', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "albums": _vm.albums
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('text', {
    staticClass: "text_suggestion",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("热门文章推荐")])]), _vm._v(" "), _vm._l((_vm.hotArticles), function(item) {
    return _c('div', {
      staticClass: "article-mid",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToRead(item.id, item.title, item.des, item.img_url)
        }
      }
    }, [_c('div', {
      staticClass: "article-content",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "title",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('div', {
      staticStyle: _vm.$processStyle({
        "flex-direction": "row",
        "justify-content": "space-between",
        "width": "390px",
        "padding-right": "30px",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "article_read_recommend",
      staticStyle: _vm.$processStyle({
        "color": "#888888；font-size:25px",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.album_name))]), _vm._v(" "), _c('div', {
      staticStyle: _vm.$processStyle({
        "flex-direction": "row",
        "justify-content": "space-between",
        "overflow": "hidden"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "article_read_recommend",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.browse) + "阅读∙")]), _vm._v(" "), _c('text', {
      staticClass: "article_read_recommend",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.love_count) + "喜欢")])])])]), _vm._v(" "), _c('image', {
      staticStyle: _vm.$processStyle({
        "width": "296px",
        "height": "194px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.img_url,
        "resize": "cover"
      }
    })])
  })], 2) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-02bfbb92", module.exports)
  }
}

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height
    }))
  }, [_c('topNavigationWidget', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "基督徒阅读"
    }
  }), _vm._v(" "), (_vm.netStatus == 2) ? _c('list', {
    staticClass: "list",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height - 222
    }))
  }, [_vm._l((_vm.articles), function(item, index) {
    return _c('cell', {
      key: index,
      staticClass: "article",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [(index % 5 !== 1) ? _c('div', [_c('div', {
      staticClass: "article-top",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticStyle: _vm.$processStyle({
        "flex-direction": "row",
        "justify-content": "space-between",
        "align-items": "center"
      }),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.album_name, item.album_id)
        }
      }
    }, [_c('image', {
      staticStyle: _vm.$processStyle({
        "width": "51px",
        "height": "51px",
        "border-radius": "25.5px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.album_img_url
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "article-name",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.album_name))])]), _vm._v(" "), _c('text', {
      staticClass: "article-time",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.publish_dated))])]), _vm._v(" "), _c('div', {
      staticClass: "article-mid",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToRead(item.id, item.title, item.des, item.img_url, item.is_shop)
        }
      }
    }, [_c('div', {
      staticClass: "article-content",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "title",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('text', {
      staticClass: "article-des",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.des))])]), _vm._v(" "), _c('image', {
      staticStyle: _vm.$processStyle({
        "width": "252px",
        "height": "195px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.img_url,
        "resize": "stretch"
      }
    })]), _vm._v(" "), _c('div', {
      staticClass: "article-bottom",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticClass: "article-bottom-left",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticStyle: _vm.$processStyle({
        "font-size": "21px",
        "color": "#B6B6B6",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.browse) + "阅读・")]), _vm._v(" "), _c('text', {
      staticStyle: _vm.$processStyle({
        "font-size": "21px",
        "color": "#B6B6B6",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.love_count) + "喜欢")])]), _vm._v(" "), _c('crIconFont', {
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "name": _vm.shareIcon,
        "size": "40",
        "color": "#B6B6B6"
      },
      on: {
        "click": function($event) {
          _vm.share(item.id, item.share_count, item.title, item.des, item.img_url, item.is_shop)
        }
      }
    })], 1)]) : _c('div', [_c('div', {
      staticClass: "article-top",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticStyle: _vm.$processStyle({
        "flex-direction": "row",
        "justify-content": "space-between",
        "align-items": "center"
      }),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.album_name, item.album_id)
        }
      }
    }, [_c('image', {
      staticStyle: _vm.$processStyle({
        "width": "62.5px",
        "height": "62.5px",
        "border-radius": "50px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.album_img_url
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "article-name",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.album_name))])]), _vm._v(" "), _c('text', {
      staticClass: "article-time",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.publish_dated))])]), _vm._v(" "), _c('div', {
      staticClass: "article-mid",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToRead(item.id, item.title, item.des, item.img_url, item.is_shop)
        }
      }
    }, [_c('div', {
      staticClass: "article-content-spread",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "title-spread",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('image', {
      staticStyle: _vm.$processStyle({
        "width": "690px",
        "height": "349px",
        "margin-bottom": "15px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.img_url,
        "resize": "stretch"
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "article-des-spread",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.des))])])]), _vm._v(" "), _c('div', {
      staticClass: "article-bottom",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticClass: "article-bottom-left",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticStyle: _vm.$processStyle({
        "font-size": "21px",
        "color": "#B6B6B6",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.browse) + "阅读・")]), _vm._v(" "), _c('text', {
      staticStyle: _vm.$processStyle({
        "font-size": "21px",
        "color": "#B6B6B6",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.love_count) + "喜欢")])]), _vm._v(" "), _c('crIconFont', {
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "name": _vm.shareIcon,
        "size": "40",
        "color": "#B6B6B6"
      },
      on: {
        "click": function($event) {
          _vm.share(item.id, item.share_count, item.title, item.des, item.img_url)
        }
      }
    })], 1)])])
  }), _vm._v(" "), _c('loading', {
    staticClass: "loading",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": _vm.loadMore
    }
  }, [_c('text', {
    staticClass: "indicator-text",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("Loading ...")]), _vm._v(" "), _c('loading-indicator', {
    staticClass: "indicator",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  })], 1)], 2) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-057107c2", module.exports)
  }
}

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wx-tabbar",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('div', {
    staticClass: "tab-component",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      'transform': _vm.translateX,
      width: _vm.totalWidth + 'px'
    }))
  }, [_vm._t("default")], 2), _vm._v(" "), _c('div', {
    staticClass: "tabbar",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.getStyles()))
  }, _vm._l((_vm.tabItems), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "tabbar-item",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.changeTab(item, index)
        }
      }
    }, [_c('crIconFont', {
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "name": item.iconFont,
        "size": "40",
        "color": _vm.selectedTab === item.index ? item.selectedColor : item.titleColor
      },
      on: {
        "click": function($event) {
          _vm.changeTab(item, index)
        }
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "wx-text",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(_vm.getTitleStyle(item)))
    }, [_vm._v(_vm._s(item.title))])], 1)
  }))])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-0ba33bfc", module.exports)
  }
}

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [(_vm.show) ? _c('div', {
    staticClass: "dialog-box",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      top: _vm.top + 'px'
    }))
  }, [_c('div', {
    staticClass: "dialog-content",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._t("title", [_c('text', {
    staticClass: "content-title",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.title))])]), _vm._v(" "), _vm._t("content", [_c('text', {
    staticClass: "content-subtext",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.content))])])], 2), _vm._v(" "), _c('div', {
    staticClass: "dialog-footer",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [(!_vm.single) ? _c('div', {
    staticClass: "footer-btn cancel",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.secondaryClicked
    }
  }, [_c('text', {
    staticClass: "btn-text",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.secondBtnColor
    }))
  }, [_vm._v(_vm._s(_vm.cancelText))])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "footer-btn confirm",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.primaryClicked
    }
  }, [_c('text', {
    staticClass: "btn-text",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.mainBtnColor
    }))
  }, [_vm._v(_vm._s(_vm.confirmText))])])])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-14dbd79c", module.exports)
  }
}

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('web', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.src
    }
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-1597bcf6", module.exports)
  }
}

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height
    }))
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-196a917b", module.exports)
  }
}

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "netDivError",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('text', {
    staticClass: "netTextError",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "lines": "2"
    }
  }, [_vm._v(_vm._s(_vm.global.display('networkError')))]), _vm._v(" "), _c('text', {
    staticClass: "button",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.Refresh
    }
  }, [_vm._v(_vm._s(_vm.global.display('refresh')))])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-222082d6", module.exports)
  }
}

/***/ }),
/* 113 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loadingCircle",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      top: _vm.height
    }))
  }, [_c('div', {
    ref: "test",
    staticClass: "box",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-22ff7e99", module.exports)
  }
}

/***/ }),
/* 114 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "loadingCircle",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('div', {
    staticStyle: _vm.$processStyle({
      "backgroundColor": "rgba(255,255,255,0.3)",
      "height": "300px",
      "width": "300px",
      "justify-content": "center",
      "align-items": "center",
      "borderRadius": "10px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticStyle: _vm.$processStyle({
      "width": "80px",
      "height": "80px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('load.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "font-size": "30px",
      "color": "#8f8f8f",
      "margin-top": "20px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("提交中...")])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-35a029e9", module.exports)
  }
}

/***/ }),
/* 115 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.show) ? _c('div', {
    ref: "wxc-overlay",
    staticClass: "wxc-overlay",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.overlayStyle)),
    attrs: {
      "hack": _vm.shouldShow
    },
    on: {
      "click": _vm.overlayClicked
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3858c23c", module.exports)
  }
}

/***/ }),
/* 116 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.show) ? _c('div', {
    ref: "wxc-overlay",
    staticClass: "wxc-overlay",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.overlayStyle)),
    attrs: {
      "hack": _vm.shouldShow
    },
    on: {
      "click": _vm.overlayClicked
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3b6fe780", module.exports)
  }
}

/***/ }),
/* 117 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.mod === 'default') ? _c('div', {
    class: ['wxc-search-bar', 'wxc-search-bar-' + _vm.theme],
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.barStyle))
  }, [_c('crIconFont', {
    staticClass: "search-bar-icon",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "ion-android-arrow-back",
      "size": "50px",
      "color": "#101010"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  }), _vm._v(" "), _c('input', {
    ref: "search-input",
    class: ['search-bar-input', 'search-bar-input-' + _vm.theme],
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      width: _vm.needShowCancel ? '604px' : '690px'
    })),
    attrs: {
      "autofocus": _vm.autofocus,
      "disabled": _vm.disabled,
      "type": _vm.inputType,
      "placeholder": _vm.placeholder
    },
    domProps: {
      "value": _vm.value
    },
    on: {
      "blur": _vm.onBlur,
      "focus": _vm.onFocus,
      "input": _vm.onInput
    }
  }), _vm._v(" "), (_vm.disabled) ? _c('div', {
    staticClass: "disabled-input",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.inputDisabledClicked
    }
  }) : _vm._e(), _vm._v(" "), (_vm.showClose) ? _c('image', {
    staticClass: "search-bar-close",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "aria-hidden": true,
      "src": _vm.getImgPath('close.png')
    },
    on: {
      "click": _vm.closeClicked
    }
  }) : _vm._e(), _vm._v(" "), _c('crIconFont', {
    staticClass: "search-bar-button",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "ion-search",
      "size": "50px",
      "color": "#101010"
    },
    on: {
      "click": _vm.searchClicked
    }
  })], 1) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-49098c90", module.exports)
  }
}

/***/ }),
/* 118 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wxc-tab-page",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [(_vm.isShelf) ? _c('div', {
    staticClass: "back",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.back
    }
  }, [(_vm.isShelf) ? _c('image', {
    staticStyle: _vm.$processStyle({
      "width": "90px",
      "height": "90px",
      "border-radius": "5px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('backarrow.png')
    },
    on: {
      "click": _vm.back
    }
  }) : _vm._e()]) : _vm._e(), _vm._v(" "), _vm._l((_vm.topNav), function(item, index) {
    return _c('div', {
      key: index,
      staticClass: "title-item",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle({
        borderBottomColor: _vm.currentPosition == index ? _vm.tabStyles.activeBorderBottomColor : 'rgba(0, 0, 0, 0)'
      })),
      on: {
        "click": function($event) {
          _vm.setPage(index)
        }
      }
    }, [_c('text', {
      staticClass: "tab-text",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle({
        fontSize: _vm.tabStyles.fontSize + 'px',
        fontWeight: (_vm.currentPosition == index && _vm.tabStyles.isActiveTitleBold) ? 'bold' : 'bold',
        color: _vm.currentPosition == index ? _vm.tabStyles.activeTitleColor : _vm.tabStyles.titleColor,
        paddingLeft: _vm.tabStyles.textPaddingLeft + 'px',
        paddingRight: _vm.tabStyles.textPaddingRight + 'px'
      }))
    }, [_vm._v(_vm._s(item))])])
  }), _vm._v(" "), _c('div', {
    staticClass: "back",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": function($event) {
        _vm.setPage(4)
      }
    }
  }, [_c('image', {
    staticStyle: _vm.$processStyle({
      "width": "35px",
      "height": "35px",
      "border-radius": "5px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('search.png')
    }
  })])], 2)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-55d0704b", module.exports)
  }
}

/***/ }),
/* 119 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('loading-indicator', {
    staticStyle: _vm.$processStyle({
      "height": "80px",
      "width": "80px",
      "color": "#7ec9c2"
    }),
    style: (_vm.$processStyle(undefined))
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-622eaa9e", module.exports)
  }
}

/***/ }),
/* 120 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "topbar",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      backgroundColor: _vm.bgcolor
    }))
  }, [_c('div', {
    staticClass: "back",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goBack
    }
  }, [_c('image', {
    staticStyle: _vm.$processStyle({
      "width": "90px",
      "height": "90px",
      "border-radius": "5px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('backarrow.png')
    }
  })]), _vm._v(" "), _c('text', {
    staticClass: "appName",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.textStyle
    })),
    attrs: {
      "topbarname": _vm.topbarname
    }
  }, [_vm._v(_vm._s(_vm.topbarname))]), _vm._v(" "), _c('text', {
    staticClass: "right-part",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.bgcolor
    }))
  }, [_vm._v(".")])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-647bf301", module.exports)
  }
}

/***/ }),
/* 121 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticStyle: _vm.$processStyle({
      "display": "none"
    }),
    style: (_vm.$processStyle(undefined))
  })
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-64fba0fe", module.exports)
  }
}

/***/ }),
/* 122 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "touchend": _vm.handleTouchEnd
    }
  }, [(_vm.show) ? _c('wxc-overlay', _vm._b({
    ref: "overlay",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "show": _vm.haveOverlay && _vm.isOverShow
    },
    on: {
      "wxcOverlayBodyClicking": _vm.wxcOverlayBodyClicking
    }
  }, 'wxc-overlay', _vm.overlayCfg, false)) : _vm._e()], 1), _vm._v(" "), (_vm.show) ? _c('div', {
    ref: "wxc-popup",
    class: ['wxc-popup', _vm.pos],
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.padStyle)),
    attrs: {
      "height": _vm._height,
      "hack": _vm.isNeedShow
    },
    on: {
      "click": function () {}
    }
  }, [_vm._t("default")], 2) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-6876521c", module.exports)
  }
}

/***/ }),
/* 123 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wxc-progress",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.runWayStyle)),
    attrs: {
      "accessible": true,
      "aria-label": ("进度为百分之" + _vm.value)
    }
  }, [_c('div', {
    staticClass: "progress",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.progressStyle))
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-71f69050", module.exports)
  }
}

/***/ }),
/* 124 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('div', {
    staticClass: "content",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.bgcolor,
      height: _vm.height,
      backgroundColor: _vm.bgcolor
    }))
  }, [(_vm.secondaryPage) ? _c('crIconFont', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "ion-android-menu",
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  }) : _c('crIconFont', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": _vm.app,
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.jumpToMySubscribe()
      }
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "title",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.textColor
    }))
  }, [_vm._v(_vm._s(_vm.name))]), _vm._v(" "), _c('crIconFont', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": _vm.search,
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.jumpToSearch()
      }
    }
  })], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-7c36821a", module.exports)
  }
}

/***/ }),
/* 125 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [(_vm.loadFinished) ? _c('image', {
    staticStyle: _vm.$processStyle({
      "width": "160px",
      "height": "220px",
      "border-radius": "5px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.source
    },
    on: {
      "load": _vm.onImageLoad
    }
  }) : _vm._e(), _vm._v(" "), (_vm.loadFinished == false) ? _c('image', {
    staticStyle: _vm.$processStyle({
      "width": "160px",
      "height": "220px",
      "border-radius": "5px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.backgroup
    }
  }) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-8c481486", module.exports)
  }
}

/***/ }),
/* 126 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height
    }))
  }, [(_vm.netStatus == 2) ? _c('div', [_c('div', {
    staticClass: "suggestionList",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "album_image",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.personInfo.head_icon
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "albumInfo",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('div', {
    staticClass: "albumInfoDetail",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goToPersonalProfile
    }
  }, [_c('div', {
    staticClass: "album_NS",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('div', {
    staticClass: "album_name",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('text', {
    staticClass: "text_album_name",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.personInfo.nickName))])])]), _vm._v(" "), _c('div', [_c('text', {
    staticClass: "album_des",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.personInfo.profile))])])]), _vm._v(" "), _c('image', {
    staticClass: "img_subscribeBtn",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('go.png')
    },
    on: {
      "click": _vm.goToPersonalProfile
    }
  })])]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goToMySubscribe
    }
  }, [_c('div', {
    staticStyle: _vm.$processStyle({
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "top_img",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('focus.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("我的订阅")])]), _vm._v(" "), _c('image', {
    staticClass: "top_icon1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('go.png')
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goToMyLists
    }
  }, [_c('div', {
    staticStyle: _vm.$processStyle({
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "top_img",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('articles.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("我的收藏")])]), _vm._v(" "), _c('image', {
    staticClass: "top_icon1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('go.png')
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goToFeedback
    }
  }, [_c('div', {
    staticStyle: _vm.$processStyle({
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "top_img",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('feedback.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("反馈")])]), _vm._v(" "), _c('image', {
    staticClass: "top_icon1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('go.png')
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goToShare
    }
  }, [_c('div', {
    staticStyle: _vm.$processStyle({
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "top_img",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('setting_push.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("分享给朋友")])])]), _vm._v(" "), _c('div', {
    staticClass: "suggestionBar1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.Donate
    }
  }, [_c('div', {
    staticStyle: _vm.$processStyle({
      "flex-direction": "row",
      "justify-content": "flex-start",
      "align-items": "center"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "top_img",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('focus.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "margin-left": "20px",
      "color": "#2e2e2e",
      "font-size": "30px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("捐赠")])])])]) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e(), _vm._v(" "), _c('commonFun', {
    ref: "commonFun",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }), _vm._v(" "), _c('wxc-popup', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "popup-color": _vm.popuColor,
      "show": _vm.isBottomShow,
      "pos": "bottom",
      "height": _vm.height
    }
  }, [_c('div', {
    staticClass: "donateDiv",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('text', {
    staticClass: "donatetext",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("     资金是事工运作与发展的一个非常重要的条件，如果您认为这项事工有价值、有感动，请支持我们。捐助金额主要用于支付服务器费用及工价。\n\n奉献支持\n        ")]), _vm._v(" "), _c('input', {
    staticClass: "input",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "type": "number",
      "placeholder": "您要捐献的金额",
      "autofocus": true,
      "value": ""
    },
    on: {
      "input": _vm.oninput
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "donatConform",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.goToDonate
    }
  }, [_c('text', {
    staticClass: "donatConformText",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("微信支付")])])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-93795b0a", module.exports)
  }
}

/***/ }),
/* 127 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('div', {
    staticClass: "content",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.bgcolor,
      height: _vm.height,
      backgroundColor: _vm.bgcolor
    }))
  }, [_c('crIconFont', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "ion-android-arrow-back",
      "size": "50px"
    },
    on: {
      "click": function($event) {
        _vm.goBack()
      }
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "title",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      color: _vm.textColor
    }))
  }, [_vm._v(_vm._s(_vm.name))]), _vm._v(" "), _c('crIconFont', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "ion-android-share-alt",
      "size": "40px"
    },
    on: {
      "click": function($event) {
        _vm.share(_vm.albumInfo)
      }
    }
  })], 1), _vm._v(" "), _c('commonFun', {
    ref: "commonFun",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-a7d346f2", module.exports)
  }
}

/***/ }),
/* 128 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "wrapper",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, _vm._l((_vm.albums), function(item, index) {
    return _c('div', {
      staticClass: "suggestionList",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('image', {
      staticClass: "album_image",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.img_url
      },
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.id, item.status)
        }
      }
    }), _vm._v(" "), _c('div', {
      class: [_vm.Id === index + 1 ? 'albumInfo_last' : 'albumInfo'],
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticClass: "albumInfoDetail",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToAuthorColumn(item.id, item.status)
        }
      }
    }, [_c('div', {
      staticClass: "album_NS",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticClass: "album_name",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "text_album_name",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.name))])]), _vm._v(" "), _c('div', {
      staticClass: "album_subscribed",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticClass: "text_album_subscribed",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.follow_count) + "人订阅")])])]), _vm._v(" "), _c('div', [_c('text', {
      staticClass: "album_des",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.author))])])]), _vm._v(" "), (item.status == 1) ? _c('div', {
      staticClass: "subscribeBtn-subscribed",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.toggleSubscribe(item.id, index)
        }
      }
    }, [_c('image', {
      staticClass: "img_subscribeBtn",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": _vm.getImgPath('followok.png')
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "text_subscribedBtn_",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v("已订阅")])]) : _c('div', {
      staticClass: "subscribeBtn",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.toggleSubscribe(item.id, index)
        }
      }
    }, [_c('crIconFont', {
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "name": "ion-android-add-circle",
        "size": "30px"
      }
    }), _vm._v(" "), _c('text', {
      staticClass: "text_subscribeBtn",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v("订阅")])], 1)])])
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-d5b6ba2a", module.exports)
  }
}

/***/ }),
/* 129 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "netError",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [(_vm.errorType == _vm.neterror) ? _c('div', {
    staticClass: "netError1",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticStyle: _vm.$processStyle({
      "width": "150px",
      "height": "150px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('neterror.png')
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "errorHint",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("网络不给力，请稍后重试")]), _vm._v(" "), _c('text', {
    staticClass: "refresh",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": _vm.Refresh
    }
  }, [_vm._v("刷新")])]) : _vm._e(), _vm._v(" "), (_vm.errorType == _vm.noResult) ? _c('div', {
    staticClass: "netError2",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticStyle: _vm.$processStyle({
      "width": "150px",
      "height": "150px"
    }),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('noresult.png')
    }
  }), _vm._v(" "), _c('text', {
    staticStyle: _vm.$processStyle({
      "fontSize": "30px",
      "color": "#8f8f8f",
      "margin-top": "50px"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.errorMsg) + "...")])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-dfc39ce4", module.exports)
  }
}

/***/ }),
/* 130 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "icon-block",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      width: _vm.size,
      height: _vm.size
    })),
    on: {
      "click": function($event) {
        _vm._click($event)
      }
    }
  }, [_c('text', {
    staticClass: "icon",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(_vm.getStyle)),
    attrs: {
      "value": _vm.getFontName
    },
    on: {
      "click": function($event) {
        _vm._click($event)
      }
    }
  })])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-e292274c", module.exports)
  }
}

/***/ }),
/* 131 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height
    }))
  }, [(_vm.netStatus == 1) ? _c('netError', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e(), _vm._v(" "), (_vm.netStatus == 2) ? _c('waterfall', {
    ref: "waterfall",
    staticClass: "content",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle({
      height: _vm.height - 122
    })),
    attrs: {
      "loadmoreoffset": "3000",
      "column-width": "auto",
      "column-count": "3",
      "column-gap": "5px",
      "show-scrollbar": "false",
      "scrollable": "true"
    }
  }, _vm._l((_vm.books), function(item, index) {
    return _c('cell', {
      key: index,
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      key: index,
      staticClass: "alreadyFollowList_album",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToBookProfile(item.id, item.title, item.address, item.img_url)
        },
        "longpress": function($event) {
          _vm.openPopUp(item.id, index)
        }
      }
    }, [_c('image', {
      staticStyle: _vm.$processStyle({
        "width": "160px",
        "height": "220px",
        "border-radius": "5px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.img_url,
        "placeholder": _vm.getImgPath('cover_default_new.png')
      }
    }), _vm._v(" "), (item.progress.visible == false) ? _c('text', {
      staticClass: "alreadyFollowList_album_name",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.title))]) : _vm._e(), _vm._v(" "), (item.progress.visible) ? _c('progress', {
      staticStyle: _vm.$processStyle({
        "margin-top": "20px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "bar-width": 200,
        "barHeight": 20
      },
      domProps: {
        "value": item.progress.value
      }
    }) : _vm._e()])])
  })) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), _vm._v(" "), (_vm.show) ? _c('weexOverlay', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "show": true,
      "hasAnimation": false
    }
  }) : _vm._e(), _vm._v(" "), _c('weexDialogue', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "title": _vm.title,
      "content": _vm.content,
      "confirm-text": _vm.confirmText,
      "cancel-text": _vm.cancelText,
      "show": _vm.show,
      "single": _vm.single
    },
    on: {
      "wxcDialogCancelBtnClicked": _vm.dialogCancelBtnClick,
      "wxcDialogConfirmBtnClicked": function($event) {
        _vm.dialogConfirmBtnClick()
      }
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "top",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('libraryNav', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "currentPosition": _vm.currentPosition,
      "isShelf": _vm.isShelf
    }
  })], 1)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-f392ce2e", module.exports)
  }
}

/***/ }),
/* 132 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(76);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("3cb74bb6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-00ff98bc\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./videoPlay.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-00ff98bc\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./videoPlay.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 133 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(77);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("7d7e547c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-02bfbb92\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subscribe.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-02bfbb92\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./subscribe.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 134 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(78);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("76b6edd4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-057107c2\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Index.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-057107c2\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(79);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("342fe5bd", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0ba33bfc\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weextabbar.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0ba33bfc\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weextabbar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 136 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(80);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("ab1469a2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-14dbd79c\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weexDialogue.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-14dbd79c\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weexDialogue.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 137 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(81);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("0d6ffd24", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1597bcf6\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weexWebView.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1597bcf6\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weexWebView.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 138 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(82);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("a2aac678", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-196a917b\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./chat.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-196a917b\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./chat.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 139 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(83);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("43c23cc2", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-222082d6\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./networkErrorDisplay.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-222082d6\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./networkErrorDisplay.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 140 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(84);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("cf3eebde", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-22ff7e99\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./loadingCircle.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-22ff7e99\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./loadingCircle.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 141 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(85);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("39fbbc4c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35a029e9\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./submitting.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35a029e9\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./submitting.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 142 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(86);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("5ff7271e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3858c23c\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weexOverlay.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3858c23c\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./weexOverlay.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 143 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(87);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("080b26b7", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../css-loader/index.js!../../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b6fe780\",\"scoped\":true,\"hasInlineConfig\":false}!../../../vue-loader/lib/selector.js?type=styles&index=0!./index.vue", function() {
     var newContent = require("!!../../../css-loader/index.js!../../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3b6fe780\",\"scoped\":true,\"hasInlineConfig\":false}!../../../vue-loader/lib/selector.js?type=styles&index=0!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 144 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(88);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("9c78f0a6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-49098c90\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./searchBar.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-49098c90\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./searchBar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 145 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(89);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("5e85ef3a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-55d0704b\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./libraryNav.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-55d0704b\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./libraryNav.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 146 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(90);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("fed881ee", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-622eaa9e\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./defaultLoading.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-622eaa9e\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./defaultLoading.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 147 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(91);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("1bccc2d4", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-647bf301\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./topbar.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-647bf301\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./topbar.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 148 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(92);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("0b7022f8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-64fba0fe\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./commonFun.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-64fba0fe\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./commonFun.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 149 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(93);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("96ca5868", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../css-loader/index.js!../../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6876521c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../vue-loader/lib/selector.js?type=styles&index=0!./index.vue", function() {
     var newContent = require("!!../../../css-loader/index.js!../../../vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6876521c\",\"scoped\":true,\"hasInlineConfig\":false}!../../../vue-loader/lib/selector.js?type=styles&index=0!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 150 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(94);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("4abef451", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71f69050\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./progress.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-71f69050\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./progress.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 151 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(95);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("795207b0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7c36821a\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./topNavigationWidget.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7c36821a\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./topNavigationWidget.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 152 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(96);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("29e0091e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8c481486\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./defaultImage.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-8c481486\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./defaultImage.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 153 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(97);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("5f800188", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-93795b0a\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./me.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-93795b0a\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./me.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(98);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("32bea028", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a7d346f2\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./topNavigationWidgetWithBack.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-a7d346f2\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./topNavigationWidgetWithBack.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(99);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("2f9005a0", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d5b6ba2a\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./followAlbum.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d5b6ba2a\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./followAlbum.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(100);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("44af6659", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dfc39ce4\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./netError.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-dfc39ce4\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./netError.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(101);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("0ab02d30", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e292274c\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./crIconFont.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e292274c\",\"scoped\":false,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./crIconFont.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(102);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("08277e9c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f392ce2e\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./libraryBookShelf.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f392ce2e\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./libraryBookShelf.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),
/* 159 */
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),
/* 160 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__index_vue__);
/* harmony reexport (default from non-hamory) */ __webpack_require__.d(__webpack_exports__, "default", function() { return __WEBPACK_IMPORTED_MODULE_0__index_vue___default.a; });


/***/ }),
/* 161 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__index_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__index_vue__);
/* harmony reexport (default from non-hamory) */ __webpack_require__.d(__webpack_exports__, "default", function() { return __WEBPACK_IMPORTED_MODULE_0__index_vue___default.a; });



/***/ }),
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(309)
}
var Component = __webpack_require__(1)(
  /* script */
  __webpack_require__(192),
  /* template */
  __webpack_require__(283),
  /* styles */
  injectStyle,
  /* scopeId */
  "data-v-646799ed",
  /* moduleIdentifier (server only) */
  null
)
Component.options.__file = "/Users/james/200--Work/202--Front End/WEEX CReader/src/04.views/articleAlbumInfo.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] articleAlbumInfo.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-646799ed", Component.options)
  } else {
    hotAPI.reload("data-v-646799ed", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */,
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */,
/* 183 */,
/* 184 */,
/* 185 */,
/* 186 */,
/* 187 */,
/* 188 */,
/* 189 */,
/* 190 */,
/* 191 */,
/* 192 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _common = __webpack_require__(4);

var _util = __webpack_require__(40);

var _util2 = _interopRequireDefault(_util);

var _prompt = __webpack_require__(6);

var _prompt2 = _interopRequireDefault(_prompt);

var _creader = __webpack_require__(7);

var _navigator = __webpack_require__(3);

var _syncAblumInfo = __webpack_require__(12);

var _syncAblumInfo2 = _interopRequireDefault(_syncAblumInfo);

var _user = __webpack_require__(8);

var _googleTrack = __webpack_require__(9);

var _googleTrack2 = _interopRequireDefault(_googleTrack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
    data: function data() {
        return {
            topName: '专栏主页',
            id: 0,
            getImgPath: _common.getImgPath,
            textStyle: '',
            pageIndex: 1,
            albumInfo: [],
            shareAlbumInfo: {},
            articleInfo: [],
            storagediv: [],
            networkStatus: 1,
            global: global,
            loadinging: false,
            netStatus: 0,
            errorType: '',
            errorMsg: '',
            followStatus: '',
            clientUrl: ""
        };
    },

    created: function created() {
        var self = this;

        self.id = _util2.default.getUrlSearch(weex.config.bundleUrl, 'id');
        self.followStatus = _util2.default.getUrlSearch(weex.config.bundleUrl, 'status');
        console.log(self.id);
        self.getData(self.id, self.pageIndex);
    },
    computed: {
        albumName: function albumName() {
            var self = this;
            return self.albumInfo.name;
        }
    },
    methods: {
        onchange: function onchange(event) {
            console.log('changed:', event.index);
        },
        bannerJump: function bannerJump(event) {
            console.log("11");
        },
        getData: function getData(id, pageIndex) {
            var self = this;
            (0, _creader.getAlbumInfoById)(id, pageIndex).then(function (res) {
                console.log(res);
                if (res.status != -1 && typeof res.data != "undefined") {
                    self.netStatus = 2;
                    self.albumInfo = res.data.data.albumInfo;
                    self.shareAlbumInfo = { "author": self.albumInfo.author,
                        "id": self.albumInfo.id,
                        "img_url": self.albumInfo.img_url,
                        "remark": self.albumInfo.remark
                    };
                    self.articleInfo = self.articleInfo.concat(res.data.data.articleInfo);
                    self.clientUrl = res.data.data.client_url;
                    for (var i = 0; i < self.articleInfo.length; i++) {
                        self.articleInfo[i].publish_dated = self.articleInfo[i].publish_dated.slice(0, 10);
                    }
                    self.loadinging = false;
                    _googleTrack2.default.userOpenAlbum(self.albumInfo.name, self.albumInfo.id);
                } else {
                    self.netStatus = 1;
                    self.errorType = 'neterror';
                }
            });
        },
        jumpToRead: function jumpToRead(id, title, des, img_url, is_shop) {
            if (is_shop == 1) {
                (0, _navigator.jumpSubPage)(this, 'storeBookDetail', { id: id, title: title, des: des, img_url: img_url, from: 'article' });
            } else {
                (0, _navigator.jumpSubPage)(this, 'articleDetails', { id: id, title: title, des: des, img_url: img_url });
            }
        },
        refresh: function refresh() {
            var self = this;
            // global.init();
            _prompt2.default.toast("hi");
            self.netStatus = 0;
            self.getData(self.pageIndex);
        },
        loadMore: function loadMore(event) {
            var self = this;
            self.loadinging = true;
            if (self.articleInfo.length < 10 * self.pageIndex) {
                _prompt2.default.toast("已经到底了");
                setTimeout(function () {
                    self.loadinging = false;
                }, 2000);
            } else {
                self.pageIndex = self.pageIndex + 1;
                self.getData(self.id, self.pageIndex);
            }
        },
        toggleSubscribe: function toggleSubscribe(albumId) {
            var _this = this;

            var self = this;
            (0, _user.getOpenId)().then(function (openId) {
                if (openId) {
                    if (self.followStatus == 0) {
                        (0, _creader.userFollow)(albumId).then(function (res) {
                            console.log(res);
                            if (res.data.status == 1) {
                                self.followStatus = 1;
                                self.albumInfo.follow_count = self.albumInfo.follow_count + 1;
                                _syncAblumInfo2.default.follow(albumId);
                            }
                        });
                    } else {
                        (0, _creader.userRemoveColumn)(albumId).then(function (res) {
                            console.log(res);
                            if (res.data.status == 1) {
                                self.followStatus = 0;
                                self.albumInfo.follow_count = self.albumInfo.follow_count - 1;
                                _syncAblumInfo2.default.cancel(albumId);
                            }
                        });
                    }
                } else {
                    _prompt2.default.toast("请登录");
                    setTimeout(function () {
                        (0, _navigator.jumpSubPage)(_this, 'userLogin');
                    }, 2000);
                }
            });
        }
    }

};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(41)))

/***/ }),
/* 193 */,
/* 194 */,
/* 195 */,
/* 196 */,
/* 197 */,
/* 198 */,
/* 199 */,
/* 200 */,
/* 201 */,
/* 202 */,
/* 203 */,
/* 204 */,
/* 205 */,
/* 206 */,
/* 207 */,
/* 208 */,
/* 209 */,
/* 210 */,
/* 211 */,
/* 212 */,
/* 213 */,
/* 214 */,
/* 215 */,
/* 216 */,
/* 217 */,
/* 218 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var App = __webpack_require__(166);
var defaultLoading = __webpack_require__(20);
Vue.component('defaultLoading', defaultLoading);
var videoPlay = __webpack_require__(35);
Vue.component('videoPlay', videoPlay);
var webView = __webpack_require__(38);
Vue.component('webView', webView);
var networkError = __webpack_require__(27);
Vue.component('networkError', networkError);
var topbar = __webpack_require__(34);
Vue.component('topbar', topbar);
var commonFun = __webpack_require__(17);
Vue.component('commonFun', commonFun);
var weextabbar = __webpack_require__(39);
Vue.component('weextabbar', weextabbar);
var libraryNav = __webpack_require__(23);
Vue.component('libraryNav', libraryNav);
var searchBar = __webpack_require__(29);
Vue.component('searchBar', searchBar);
var netError = __webpack_require__(26);
Vue.component('netError', netError);
var loadingCircle = __webpack_require__(24);
Vue.component('loadingCircle', loadingCircle);
var progress = __webpack_require__(28);
Vue.component('progress', progress);
var weexDialogue = __webpack_require__(36);
Vue.component('weexDialogue', weexDialogue);
var weexOverlay = __webpack_require__(37);
Vue.component('weexOverlay', weexOverlay);
var defaultImage = __webpack_require__(19);
Vue.component('defaultImage', defaultImage);
var submitting = __webpack_require__(30);
Vue.component('submitting', submitting);
var Index = __webpack_require__(15);
Vue.component('Index', Index);
var me = __webpack_require__(25);
Vue.component('me', me);
var chat = __webpack_require__(16);
Vue.component('chat', chat);
var subscribe = __webpack_require__(31);
Vue.component('subscribe', subscribe);
var libraryBookShelf = __webpack_require__(22);
Vue.component('libraryBookShelf', libraryBookShelf);
var crIconFont = __webpack_require__(18);
Vue.component('crIconFont', crIconFont);
var topNavigationWidget = __webpack_require__(32);
Vue.component('topNavigationWidget', topNavigationWidget);
var topNavigationWidgetWithBack = __webpack_require__(33);
Vue.component('topNavigationWidgetWithBack', topNavigationWidgetWithBack);
var followAlbum = __webpack_require__(21);
Vue.component('followAlbum', followAlbum);
App.el = '#root';
new Vue(App);

/***/ }),
/* 219 */,
/* 220 */,
/* 221 */,
/* 222 */,
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)();
// imports


// module
exports.push([module.i, "\n.body[data-v-646799ed]{\n    position: absolute;\n    left:0;\n    top:0;\n    right:0;\n    bottom:0;\n    background-color: #F0F0F2;\n}\n.suggestionList[data-v-646799ed]{\n    position: absolute;\n    left:0;\n    top:100px;\n    right:0;\n    height: 487px;\n    flex-direction: column;\n    justify-content: center;\n    align-items: center;\n    background-color: #ffffff;\n}\n.album_image[data-v-646799ed]{\n    width:150px;\n    height: 150px;\n    border-radius: 75px;\n}\n.album-title[data-v-646799ed]{\n    font-size: 34px;\n    color: #101010;\n    font-weight: bold;\n    margin-top:30px;\n}\n.author[data-v-646799ed]{\n    font-size: 25px;\n    color: #101010;\n     margin-top:30px;\n}\n.follow_count[data-v-646799ed]{\n    font-size: 21px;\n    color: #888888;\n     margin-top:30px;\n}\n.subscribeBtn[data-v-646799ed]{\n    width:115px;\n    height:50px;\n    background-color: #E15D53;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 15px;\n    padding-left: 15px;\n    padding-right: 15px;\n    margin-top:30px;\n    /*background-color: lightblue;*/\n}\n.subscribeBtn-subscribed[data-v-646799ed]{\n    width:115px;\n    height:50px;\n    background-color: white;\n    flex-direction: row;\n    justify-content: space-around;\n    align-items: center;\n    border-width:1px;\n    border-style: solid;\n    border-color: #e5e5e5;\n    border-radius: 15px;\n    margin-top:30px;\n}\n.text_subscribeBtn[data-v-646799ed]{\n    color:white;\n    font-size: 24px;\n}\n.text_subscribedBtn_[data-v-646799ed]{\n    color:#888888;\n    font-size: 24px;\n}\n.img_subscribeBtn[data-v-646799ed]{\n    width:24px;\n    height:24px;\n}\n.content[data-v-646799ed]{\n    position: absolute;\n    left:0;\n    right:0;\n    top:698px;\n    bottom: 0;\n    background-color: #ffffff;\n}\n.suggestionBar[data-v-646799ed]{\n     width:750px;\n     height:96px;\n     position: absolute;\n     top:602px;\n     flex-direction: row;\n     align-items: center;\n     padding-left: 30px;\n     padding-right: 30px;\n     justify-content: space-between;\n     border-bottom-width:2px;\n     border-bottom-style: solid;\n     border-bottom-color: #F0F0F2;\n     background-color:#ffffff;\n}\n.text_suggestion[data-v-646799ed]{\n    color: #888888;\n    font-size:25px;\n}\n.article[data-v-646799ed]{\n    border-bottom-width:2px;\n    border-bottom-style: solid;\n    border-bottom-color: #F0F0F2;\n    padding-bottom: 25px;\n    /*background-color: lightblue;*/\n}\n.article-mid[data-v-646799ed]{\n    flex-direction: row;\n    justify-content: space-between;\n    align-items: center;\n    width: 750px;\n    padding-left: 30px;\n    padding-right: 30px;\n    padding-top:20px;\n    padding-bottom: 20px;\n    /*background-color: lightgreen;*/\n}\n.title[data-v-646799ed]{\n    width: 440px;\n    height: 150px;\n    font-size: 35px;\n    color: #101010;\n    font-weight: bold;\n}\n.loading[data-v-646799ed] {\n    width: 750;\n    display: -ms-flex;\n    display: -webkit-flex;\n    display: flex;\n    -ms-flex-align: center;\n    -webkit-align-items: center;\n    -webkit-box-align: center;\n    align-items: center;\n}\n.indicator-text[data-v-646799ed] {\n    color: #E15D53;\n    font-size: 42px;\n    text-align: center;\n}\n.indicator[data-v-646799ed] {\n    margin-top: 16px;\n    height: 40px;\n    width: 40px;\n    color: #E15D53;\n}\n  \n", ""]);

// exports


/***/ }),
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */,
/* 267 */,
/* 268 */,
/* 269 */,
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "body",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('topNavigationWidgetWithBack', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": _vm.topName,
      "albumInfo": _vm.shareAlbumInfo
    }
  }), _vm._v(" "), (_vm.netStatus == 2) ? _c('div', {
    staticClass: "suggestionList",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('image', {
    staticClass: "album_image",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.albumInfo.img_url
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "album-title",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.albumInfo.name))]), _vm._v(" "), _c('text', {
    staticClass: "author",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.albumInfo.author))]), _vm._v(" "), _c('text', {
    staticClass: "follow_count",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v(_vm._s(_vm.albumInfo.follow_count) + "位订阅者")]), _vm._v(" "), (_vm.followStatus == 1) ? _c('div', {
    staticClass: "subscribeBtn-subscribed",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": function($event) {
        _vm.toggleSubscribe(_vm.albumInfo.id, _vm.index)
      }
    }
  }, [_c('image', {
    staticClass: "img_subscribeBtn",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "src": _vm.getImgPath('followok.png')
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "text_subscribedBtn_",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("已订阅")])]) : _c('div', {
    staticClass: "subscribeBtn",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    on: {
      "click": function($event) {
        _vm.toggleSubscribe(_vm.albumInfo.id, _vm.index)
      }
    }
  }, [_c('crIconFont', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "name": "ion-android-add-circle",
      "size": "25px"
    }
  }), _vm._v(" "), _c('text', {
    staticClass: "text_subscribeBtn",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("订阅")])], 1)]) : _vm._e(), _vm._v(" "), (_vm.netStatus == 2) ? _c('div', {
    staticClass: "suggestionBar",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_c('text', {
    staticClass: "text_suggestion",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("本专栏文章列表")])]) : _vm._e(), _vm._v(" "), (_vm.netStatus == 2) ? _c('list', {
    staticClass: "content",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  }, [_vm._l((_vm.articleInfo), function(item, index) {
    return _c('cell', {
      key: index,
      staticClass: "article",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticClass: "article-mid",
      staticStyle: _vm.$processStyle(undefined),
      style: (_vm.$processStyle(undefined)),
      on: {
        "click": function($event) {
          _vm.jumpToRead(item.id, item.title, item.des, item.img_url, item.is_shop)
        }
      }
    }, [_c('text', {
      staticClass: "title",
      staticStyle: _vm.$processStyle({
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.title))]), _vm._v(" "), _c('image', {
      staticStyle: _vm.$processStyle({
        "width": "240px",
        "height": "150px",
        "border-radius": "5px"
      }),
      style: (_vm.$processStyle(undefined)),
      attrs: {
        "src": item.img_url
      }
    })]), _vm._v(" "), _c('div', {
      staticStyle: _vm.$processStyle({
        "flex-direction": "row",
        "justify-content": "space-between",
        "align-items": "center",
        "padding-left": "25px",
        "padding-right": "40px"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_c('div', {
      staticStyle: _vm.$processStyle({
        "flex-direction": "row",
        "justify-content": "space-between",
        "align-items": "center"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_c('text', {
      staticStyle: _vm.$processStyle({
        "color": "#B6B6B6",
        "font-size": "21px",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.browse) + "阅读•")]), _vm._v(" "), _c('text', {
      staticStyle: _vm.$processStyle({
        "color": "#B6B6B6",
        "font-size": "21px",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.love_count) + "喜欢")])]), _vm._v(" "), _c('text', {
      staticStyle: _vm.$processStyle({
        "color": "#B6B6B6",
        "font-size": "21px",
        "font-family": "Roboto"
      }),
      style: (_vm.$processStyle(undefined))
    }, [_vm._v(_vm._s(item.publish_dated))])])])
  }), _vm._v(" "), _c('loading', {
    staticClass: "loading",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": _vm.loadMore
    }
  }, [_c('text', {
    staticClass: "indicator-text",
    staticStyle: _vm.$processStyle({
      "font-family": "Roboto"
    }),
    style: (_vm.$processStyle(undefined))
  }, [_vm._v("Loading ...")]), _vm._v(" "), _c('loading-indicator', {
    staticClass: "indicator",
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined))
  })], 1)], 2) : _vm._e(), _vm._v(" "), (_vm.netStatus == 0) ? _c('loadingCircle') : _vm._e(), _vm._v(" "), (_vm.netStatus == 1) ? _c('netError', {
    staticStyle: _vm.$processStyle(undefined),
    style: (_vm.$processStyle(undefined)),
    attrs: {
      "errorType": _vm.errorType,
      "errorMsg": _vm.errorMsg
    },
    on: {
      "toNetError": _vm.refresh
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-646799ed", module.exports)
  }
}

/***/ }),
/* 284 */,
/* 285 */,
/* 286 */,
/* 287 */,
/* 288 */,
/* 289 */,
/* 290 */,
/* 291 */,
/* 292 */,
/* 293 */,
/* 294 */,
/* 295 */,
/* 296 */,
/* 297 */,
/* 298 */,
/* 299 */,
/* 300 */,
/* 301 */,
/* 302 */,
/* 303 */,
/* 304 */,
/* 305 */,
/* 306 */,
/* 307 */,
/* 308 */,
/* 309 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(257);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("06a94968", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-646799ed\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./articleAlbumInfo.vue", function() {
     var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-646799ed\",\"scoped\":true,\"hasInlineConfig\":false}!../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./articleAlbumInfo.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })
/******/ ]);