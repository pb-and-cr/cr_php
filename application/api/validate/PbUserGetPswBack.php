<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 2017/12/6
 * Time: 下午3:00
 */

namespace app\api\validate;
use think\Validate;


class PbUserGetPswBack extends Validate {

    protected $rule = [
        'email' => 'require|email',
    ];

    protected $message = [
        'email.require' =>'请输入邮箱',
        'email.email' => '请检查邮箱格式',
    ];


}