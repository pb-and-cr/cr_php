<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 2017/12/6
 * Time: 下午3:00
 */

namespace app\api\validate;
use think\Validate;


class PbUserUpdateProfile extends Validate {

    protected $rule = [
        'email' => 'email',
        // 'name' => 'require|max:25',
    ];

    protected $message = [
        'email' => '请检查邮箱格式',
        // 'name.require' => '请输入用户名',
        // 'name.max' => '名称最多不能超过25个字符',
    ];


}