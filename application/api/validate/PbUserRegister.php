<?php
namespace app\api\validate;

use think\Validate;

//为pressbible_user_info定义一个registerCheck类
class PbUserRegister extends Validate {

    protected $rule = [
        'nickName' => 'require|max:50',
        'email' => 'require|email',
        'password' => 'require',
    ];

    protected $message = [
        'nickName.require' => '请输入昵称',
        'nickName.max' => '名称最多不能超过25个字符',
        'password.require' => '请输入密码',
        'email.require' => '请输入邮箱。如果您不小心忘记账号，这将帮助您在将来找回账号',
        'email.email' => '请检查邮箱格式',
    ];

}
