<?php
namespace app\api\validate;

use think\Validate;

//为pressbible_user_info定义一个loginCheck类
class PbUserLogin extends Validate {

    protected $rule = [
        'userName' => 'require',
        'password' => 'require',
    ];

    protected $message = [
        'userName.require' => '请输入用户名',
        'password.require' => '请输入密码',
    ];
}