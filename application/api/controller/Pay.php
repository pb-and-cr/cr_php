<?php
/**
 * Created by PhpStorm.
 * User: henry
 * Date: 2018/7/10
 * Time: 11:46
 */

namespace app\api\controller;

use app\lib\business\WechatAppPayInfo;

/**
* 微信支付请求类
*
* @author    Henry
* @version     1.0 版本号
*/

class Pay extends BaseController
{
    //发送微信服务器的参数集合
    private $params = array();
    //返回本软件客户端的参数集合
    private $data=array();

    /**
     * 微信支付统一下以及返回客户端数据
     *
     * @access public
     * @param String $total  参数一客户端请求的支付金额
     * @return array  客户端发起微信客户端支付所需要的参数
     */
    public function getAppPayParams(){
        $total=$_POST["total"];
        $params['body'] = '捐赠'; //商品描述
        $params['out_trade_no'] = WechatAppPayInfo::getWechatAppPayInstance()->getTrade(); //自定义的订单号
        $params['total_fee'] = $total."00"; //订单金额 只能为整数 单位为分
        $params['trade_type'] = 'APP'; //交易类型 JSAPI | NATIVE | APP | WAP
        $result=WechatAppPayInfo::getWechatAppPayInstance()->unifiedOrder($params);
//        $this->simpleAjax($params['out_trade_no']);
        if($result ["return_code"]=="SUCCESS"){
            $data['appid'] = $result['appid'];
            $data['partnerid'] =$result['mch_id'];
            $data['prepayid'] = $result['prepay_id'];
            $data['package'] = 'Sign=WXPay';
            $data['noncestr'] = WechatAppPayInfo::getWechatAppPayInstance()->genRandomString();
            $data['timestamp'] = time();
            $data['sign'] = WechatAppPayInfo::getWechatAppPayInstance()->MakeSign( $data );
            $this->ajax($data,'',200);
           $this->simpleAjax($data);

        }else{
            WechatAppPayInfo::getWechatAppPayInstance()->error_code($result['err_code']);
        }

    }
    /**
     * 微信支付成功后，微信服务器返回来的数据接收控制器
     *
     * @access public
     * @param 'xarry  账单的一些数据
     * @return String
     */
    public function Notice(){
        //获取通知的数据
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        $data1 = array();
        $data=array();
        if( empty($xml) ){
            return false;
        }
        $data1 = WechatAppPayInfo::getWechatAppPayInstance()->xml_to_data( $xml );
        if( !empty($data1['return_code']) ){
            if( $data1['return_code'] == 'FAIL' ){
                return false;
            }else{
                $data['return_code'] = 'SUCCESS';
                $data['return_msg'] = 'OK';
                $xml = WechatAppPayInfo::getWechatAppPayInstance()->data_to_xml( $data );
                $this->ajax($xml);
            }
        }

    }




}