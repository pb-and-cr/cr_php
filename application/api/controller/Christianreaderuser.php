<?php
namespace app\api\controller;

//use think\Config;
//use think\Controller;
//use think\Session;
//use think\Lang;
//use think\Request;
use app\lib\common\StringTool;
use app\lib\model\UserInfoModel;
use think\Db;
use think\Validate;
use think\config;
use think\Cache;
use app\lib\common\MySendEmail;
use app\lib\common\Email;
use app\lib\business\ChristianReaderUserInfo;
use app\lib\model\ChristianReaderUserModel;
use app\lib\common\sms\SmsSingleSender;


class Christianreaderuser extends BaseController
{
    //用户登录,需要传入三个参数，用户名，密码，语言
    public function login(){
        //header("Access-Control-Allow-Origin: *");
        // curl -d "name=James&password=1" "http://localhost:1080/api/ChristianReaderUser/login"
        //curl -d "userName=9096796231@qq.com&password=1" "http://192.168.2.100:12000/api/ChristianReaderUser/login"
        //localhost:1066/api/ChristianReaderUser/login?loginName=13730804091&password=1
        if($this->request->isPost()){
            $data = $this->request->post();
            $info = $this->validate($data,'app\api\validate\PbUserLogin');
            if (true !== $info) {
                $this->ajax($data,$info,0);
            }
            else{
                $data['password'] = StringTool::MD5Code($data['password']);
                $message = ChristianReaderUserInfo::isErrorLogin($data);
                $this->simpleAjax($message);

            }
        }
    }

    //用户登录,需要传入三个参数，用户名，密码，语言
    public function getUserInfo(){
        //header("Access-Control-Allow-Origin: *");
        //http://localhost:1062/api/ChristianReaderUser/getUserInfo?userToken=24856cfeef943733baada9d9ef2fa0f6
        $token = $this->param('userToken');
        $user = ChristianReaderUserModel::get(['user_token' => $token]);
        if($user){
            $this->ajax($user->getData(),'',1);
        }
        else{
            $this->ajax('','',0);
        }
    }



    //用来测试的方法，返回10个用户信息
    public function userTest() {
        header("Access-Control-Allow-Origin: *");
        $re = Db::query("select * from user_info order by created_time desc limit 10");
        $this->ajax($re,'测试',1);
    }


    //用来测试的方法，接收json 数组数据,前端传过来的是json数组[{},{},{}]
    public function userInfo() {
        header("Access-Control-Allow-Origin: *");
        $data = $this->request->post();
        foreach($data as &$item) {
            ChristianReaderUserInfo::newUserRegist($item);
        };
        // dump($data);
    }

    //用户注册,云端神学院的用户注册
    public function userRegist() {
        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){

            $data = $this->request->post();

            try {
                foreach($data as &$param) {

                    //判断数据库是否已经存在该用户名
                    $msg = Db::table('user_info')->where('name',$param['name'])->find();

                    if($msg) {
                        $this->ajax('',$param['name'].'--此用户名已存在',0);
                        break;
                    }else {
                        $re = ChristianReaderUserInfo::newUserRegist($param);
                    }
                }
                $this->ajax('','导入成功',1);

            }catch(\Exception $e) {
                echo var_dump($e);
            }  
            
        }
    }

    //用户使用手机注册(接收五个参数：昵称，手机号码，验证码，密码，语言信息)
    public function registerByPhone(){
        header("Access-Control-Allow-Origin: *");
        //http://localhost:1066/api/ChristianReaderUser/registerByPhone?nickname=%E6%9D%BE%E6%9D%BE&mobile=13699482803&password=123456&verifyCode=5064
        // http://localhost:1062/api/ChristianReaderUser/registerByPhone?nickName=%E6%9D%BE%E6%9D%BE&mobile=13699482803&password=123456&verifyCode=5064
        if($this->request->isPost()){
            $data = $this->request->post();
            $rule = [
                'nickName'  => 'require|max:25',
                'mobile'   => 'require|length:7,14',
                'verifyCode' => 'require|length:4',
                'password'  => 'require',
            ];

            $msg = [
                'nickName.require' => '请输入昵称',
                'nickName.max' => '昵称最多不能超过25个字符',
                'mobile.require' => '请输入手机号码',
                'mobile.length' => '请检查手机号码是否正确',
                'verifyCode.require' => '请输入验证码',
                'verifyCode.length' => '请输入正确的验证码',
                'password.require' => '请输入密码',
            ];
            $validate = Validate::make($rule)->message($msg);
            $result = $validate->check($data);
            if (!$result) {
                $info = $validate->getError();
                $this->ajax('',$info,0);
            }
            else{
                $code = Cache::get($data['mobile']);
                if($code){
                    $userRegister = new ChristianReaderUserModel();
                    $userMobile = $userRegister->where('mobile',$data['mobile'])->find();
                    if($userMobile){
                        $info = "该手机已注册，请登陆";
                        $this->ajax('',$info,0);
                    }else{
                        $info = "恭喜，注册成功,请登录";
                        ChristianReaderUserInfo::createUserByPhone($data);
                        $this->ajax('',$info,1);
                    }
                }
                else{
                    $info = "您输入的验证码不正确或已过期";
                    $this->ajax('',$info,0);
                }

            }

        }
    }
    //用户使用邮箱注册(接收四个参数：昵称，邮箱，密码，语言信息)
    public function registerByEmail(){
//        header("Access-Control-Allow-Origin: *");
        //http://localhost:1066/api/ChristianReaderUser/registerByEmail?nickname=%E5%90%89%E5%A7%86&email=1521170630@qq.com&password=123456
        // curl -d 'nickName=vincent&email=909679623@qq.com&password=1' http://192.168.31.126:12000/api/ChristianReaderUser/registerByEmail
        if($this->request->isPost()){
            $data = $this->request->post();
            $info = $this->validate($data,'app\api\validate\PbUserRegister');
            if($info!== true){
                $this->ajax('',$info,0);
            }else{
                $userRegister = new ChristianReaderUserModel();
                $userEmail = $userRegister->where('email',$data['email'])->find();
                if($userEmail){
                    $info = "邮箱已注册，请登录";
                    $this->ajax('',$info,0);
                }else{
                    $message = ChristianReaderUserInfo::createUserByEmail($data,$this);
                    $this->simpleAjax($message);
                }

            }
        }
    }

    //用户更新资料
    public function updatePersonalProfile(){
        //http://localhost:1066/api/ChristianReaderUser/updatePersonalProfile?nickname=%E8%BD%BB%E6%9D%BE&user_token=b1d3d16099ed8ef237dfbad2c8024c47e1c167ba
        if($this->request->isPost()){
            $data = $this->request->post();
            if(empty($data['user_token'])){
                $info = '请先登录';
                $this->ajax('',$info,0);
            }else{
                $info = $this->validate($data,'app\api\validate\PbUserUpdateProfile');
                if($info!==true){
                    $this->ajax('',$info,0);
                }else{
                    $message = ChristianReaderUserInfo::updateProfile($data);
                    $this->simpleAjax($message);
                    dump($message);
                }
            }

        }
    }

    //邮箱找回密码
    public function getPasswordBack(){
        // curl -d '1003200758@qq.com' http://localhost:1070/api/ChristianReaderUser/getPasswordBack
        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){
            $data = $this->request->post();
            $info = $this->validate($data,'app\api\validate\PbUserGetPswBack');
            if($info!==true){
                $this->ajax('',$info,0);
            }else{
                $message = ChristianReaderUserInfo::getPassword($data,$this);
                $this->simpleAjax($message);
            }
        }

    }
    //邮箱里,点击进入设置密码界面
    public function setNewPsw(){
        header("Access-Control-Allow-Origin: *");
        //$time = date('Y-m-d H:i:s');
        $host = Config::get('IMG_DOMAIN');
        if($this->request->isGet()){
            $user_token = $this->param("user_token");
            $time = $this->param("time");
            $this->assign([
                "user_token" => $user_token,
                "time" => $time,
                "info" => '',
                "host" => $host,
            ]);
            return $this->fetch(APP_PATH.request()->module().'/view/resetPsw.html');
        }
        else
        {
            $data = $this->request->post();
            $time = $this->param("time");
            $this->assign([
                "user_token" => $data['user_token'],
                "time" => $time,
                "info" => '',
            ]);
            return $this->fetch(APP_PATH.request()->module().'/view/resetPsw.html');

        }
    }
    //邮箱跳转到网页，设置新的密码
    public function updatePsw(){

        if($this->request->isPost()){
            $time = date('Y-m-d H:i:s');
            $data = $this->request->post();
            $user_token = $data['user_token'];

            if($data['password']!=''&&$data['psw2']!=''){
                if($data['password']===$data['psw2']){
                    unset($data['psw2']);
                    $data['password'] = StringTool::MD5Code($data['password']);
                    $message = ChristianReaderUserInfo::updateProfile($data);
                    if($message['status']===1){
                        return $this->fetch(APP_PATH.request()->module().'/view/successful.html');
                    }else{
                        $this->assign([
                            "user_token" => $user_token,
                            "time" => $time,
                            "info" => $message['info'],
                        ]);
                        return $this->fetch(APP_PATH.request()->module().'/view/resetPsw.html');
                    }
                }
                else{
                    $this->assign([
                        "user_token" => $user_token,
                        "time" => $time,
                        "info" => '密码不一致',
                    ]);
                    return $this->fetch(APP_PATH.request()->module().'/view/resetPsw.html');
                }
            }
            else{
                $this->assign([
                    "user_token" => $user_token,
                    "time" => $time,
                    "info" => '密码不能为空',
                ]);
                return $this->fetch(APP_PATH.request()->module().'/view/resetPsw.html');
            }
        }
    }

    //手机找回密码
    public function getPasswordBackByPhone(){
        // curl -d "mobile=13730804091&verifyCode=1189" "http://localhost:12000/api/ChristianReaderUser/getPasswordBackByPhone"
        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){
           $data = $this->request->post();
//            $this->ajax($data,'验证通过',1);
           $code = Cache::get($data['mobile']);
            if($code===$data['verifyCode']){
                $this->ajax($data,'验证通过',1);
            }else{
                $this->ajax($data,'验证码错误或者过期',0);
            }
        }
    }
    //手机方式跳转到新页面设置新密码
    public function updatePswByPhone(){

        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){
            $data = $this->request->post();
            $data['password'] = StringTool::MD5Code($data['password']);
            $message = ChristianReaderUserInfo::setNewPasswordByPhone($data);
            $this->simpleAjax($message);
        }
    }
    //登陆后,在个人中心修改密码
    public function reSetPsw(){
        if($this->request->isPost()){
            $data = $this->request->post();
            $data['password'] = StringTool::MD5Code($data['password']);
            dump($data);
            if(empty($data['user_token'])){
                $info = "非法修改";
                $this->ajax('',$info,0);
            }else{
                $message = ChristianReaderUserInfo::updateProfile($data);
                $this->simpleAjax($message);
                //dump($message);
            }
        }
    }

    //发送短信
    public function getSms(){
        //http://localhost:1066/api/ChristianReaderUser/getSms
        //curl -d 'mobile=13730804091' http://localhost:12000/api/ChristianReaderUser/getSms
        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){
            $data = $this->request->post();
//            $this->ajax($data,'--01--',1);
            $appid = Config::get('appid');
            $appkey = Config::get('appkey');
            $templId = 96005;
            $code = ChristianReaderUserInfo::getRandStr(4);//获取随机4位数字验证码
            try {
                $sender = new SmsSingleSender($appid, $appkey);
                $params = [$code];
                $result = $sender->sendWithParam("86", $data['mobile'], $templId,
                    $params, "", "", "");
                $rsp = json_decode($result);
                if($rsp->result==0){
                    $info = "验证码已发送，请查收";
                    Cache::set($data['mobile'],$code,3000);
                    $this->ajax($rsp,$info,1);
                }else{
                    $info = "验证码发送失败";
                    $this->ajax($rsp,$info,0);
                }

            } catch(\Exception $e) {
                $info = "验证码发送失败...";
                $this->ajax($e,$info,0);
            }

        }


    }

    //更新用户信息
    public function update(){
        //curl -d 'name=13699482803&nick_name=&website=&birthday=&brief_introduction=&email=&user_token=&language' http://localhost:1066/api/bibleuser/update
        $nick_name = $this->param('nickName');
        $user_token = $this->param('userToken');
        $name = $this->param('name');
        $brief_introduction = $this->param('briefIntroduction');

        if($this->isNull($user_token)){
            $this->ajax('','',0);
        }

        $entity = ChristianReaderUserModel::get(['user_token' => $user_token]);

        if($entity!=null){

            $data['nickName'] = $nick_name;
            $data['name'] = $name;
            $data['brief_personal_des'] = $brief_introduction;
            $data['user_token'] = $user_token;
            $user = new ChristianReaderUserModel();
            $user->save($data,['id' => $entity->id]);

            $data['brief_personal_des'] = $brief_introduction;
            $data['userToken'] = $user_token;
            $data['nickName'] = $nick_name;
            $data['status'] = 1;
            $data['avatar'] = $entity->avatar;
            $this->ajax($data,$this->getErrorCodeInfo('006'),1);
        }
        else
        {
            $this->ajax('',$this->getErrorCodeInfo('001'),0);
        }
    }

    public function getErrorCodeInfo($code)
    {
        $language = $this->param('language');

        $errorInfo = array(
            '000'=>array('zh'=>'系统错误,稍后再试!','en'=>'System error, try again later!'),
            '001'=>array('zh'=>'用户不存在','en'=>'User does not exist'),
            '002'=>array('zh'=>'注册成功','en'=>'registration success'),
            '003'=>array('zh'=>'登录成功','en'=>'login successful.'),
            '004'=>array('zh'=>'登录名或密码错误','en'=>'Login name or password is incorrect.'),
            '005'=>array('zh'=>'邮件或者手机号码,已经被注册.','en'=>'E-mail or mobile phone number has been registered.'),
            '006'=>array('zh'=>'操作成功.','en'=>'Successful operation.'),
            '007'=>array('zh'=>'老密码错误.','en'=>'Old password is incorrect.'),
            '008'=>array('zh'=>'该邮件或手机号，被其他用户使用.','en'=>'The e-mail or phone number, be used by other users.'),
        );

        if(!(stripos($language,'zh')===false) ){
            $language = 'zh';
        }
        else
        {
            $language = 'en';
        }

        $errorInfo = $errorInfo[$code];
        if(count($errorInfo)>0){
            if($language=='zh'){
                return $errorInfo['zh'];
            }
            else
            {
                return $errorInfo['en'];
            }
        }
        return '';
    }
    public function getPersonalData(){
        header("Access-Control-Allow-Origin: *");
        $user_token = $this->param('openId');
        // $user_id = $this->param('user_id');
//        $user_token = '8A471FD74AF017EE0680381611D442CD';
        $result = ChristianReaderUserInfo::UserInfoData($user_token);
        $this->simpleAjax($result);
    }
    public function feedback()
    {
        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){
            $data = $this->request->post();
            $msg = "userInfo: ".$data['ContactInformation'].'  <br/>';
            $msg = $msg."content: ".$data['Content'].'  <br/>';
            $msg = $msg."appInfo: ".$data['AppInfo'];

            $mail = new Email();
            $mail->sendEmailByTitle("dev@jdtapps.me",$msg,"基督徒阅读（用户反馈）");

            $this->simpleAjax(1);
        }
    }
}
