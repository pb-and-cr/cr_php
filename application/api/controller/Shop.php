<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 2018/3/6
 * Time: 下午2:27
 */

namespace app\api\controller;


use app\lib\business\ShopInfo;


class Shop extends BaseController
{

    public function getBooksInShop(){
        header("Access-Control-Allow-Origin: *");
        // $id = $this->param('id');
        // $pageIndex = $this->param('pageIndex');
        $tags = ShopInfo::getBookTags();
        $books = ShopInfo::getAllBooksInShop();
        // $booksInfo = array();
        // $booksInfo['tags'] = $tags;
        // $booksInfo['books']  = $books;
        
        $result= [];
        $result['tags'] = $tags;
        $result['books'] = $books;
        $this->ajax($result,'',1);
    }

    public function getBooksInShopByTag(){
        header("Access-Control-Allow-Origin: *");
        $tag_id = $this->param('tag_id');
        $result = ShopInfo::getShopBookInfoByTag($tag_id);
        $this->ajax($result,'',1);
    }

    public function getBookProfile(){

        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $openId = $this->param('openId');
        $resualt = ShopInfo::getProfileById($id,$openId);
        $this->ajax($resualt);
    }

}