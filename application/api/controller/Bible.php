<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\api\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\BibleVersionInfo;


class Bible extends BaseController
{
    public function index()
    {
        //http://localhost:8080/smallprogram/index/index
        $num = $this->param("num");

        $bid = 40;
        $cid = 1;

        if($this->isNull($cid)==true){
            $chapter = Smallprogram::$BibleChapters[$num];
            if(count($chapter)>0){
                $bid = $chapter[1];
                $cid = $chapter[2];
            }
        }

        $sql = "select * from bible_cnv where bookid =".$bid." and chapterid = ".$cid;
        $list = Db::query($sql);

        $array = [];
        $array_id = 0;
        $sum = count($list);
        $subverseId  = 0;
        $suborderId  = 0;
        $subverseSum  = 0;
        $tempArr = [];
        $tempArrOrder = 0;
        for ($i=0; $i < $sum ; $i++){
            $status = intval($list[$i]['status']);
            $verseid = intval($list[$i]['verseid']);
            $content = $list[$i]['content'];
//            $content = '-';
            if($status == 3)
            {
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = [];
                $array_id++;
            }
            else if($status == 2){
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = [];
                $array_id++;
            }
            else if($status == 1){
                $subverseId = $verseid;
                $subverseSum = 1;
                $suborderId = $i;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = [];
                $array_id++;
            }
            else
            {
                if($subverseId>0 && $subverseId + $subverseSum == $verseid){
                    $tempArr[$tempArrOrder] = [$subverseId,$verseid,$content,$status,0];
                    $tempArrOrder++;
                    $subverseSum ++;
                }
            }
        }

        $sum = count($array);
        $tempSum = count($tempArr);
        for ($i=0; $i < $sum ; $i++){
            if($array[$i][2] == 1){

                $array[$i][4] = [];
                $num = 0;
                for ($k=0; $k < $tempSum ; $k++){
                    if($tempArr[$k][0] == $array[$i][0]){
                        $array[$i][4][$num] = [$tempArr[$k][1],$tempArr[$k][2],$tempArr[$k][3],0];
                        $array[$i][3] = 1;
                        $num++;
                    }
                }
            }
        }

        $this->simpleAjax($array);
    }

    public function bibleVerseOrder($list){
        $array = [];
        $array_id = 0;
        $sum = count($list);
        $subverseId  = 0;
        $suborderId  = 0;
        $subverseSum  = 0;
        $tempArr = [];
        $tempArrOrder = 0;
        for ($i=0; $i < $sum ; $i++){
            $status = intval($list[$i]['status']);
            $verseid = intval($list[$i]['verseid']);
            $content = $list[$i]['content'];
//            $content = '-';
            if($status == 3)
            {
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = [];
                $array_id++;
            }
            else if($status == 2){
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = [];
                $array_id++;
            }
            else if($status == 1){
                $subverseId = $verseid;
                $subverseSum = 1;
                $suborderId = $i;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = [];
                $array_id++;
            }
            else
            {
                if($subverseId>0 && $subverseId + $subverseSum == $verseid){
                    $tempArr[$tempArrOrder] = [$subverseId,$verseid,$content,$status,0];
                    $tempArrOrder++;
                    $subverseSum ++;
                }
            }
        }

        $sum = count($array);
        $tempSum = count($tempArr);
        for ($i=0; $i < $sum ; $i++){
            if($array[$i][2] == 1){

                $array[$i][4] = [];
                $num = 0;
                for ($k=0; $k < $tempSum ; $k++){
                    if($tempArr[$k][0] == $array[$i][0]){
                        $array[$i][4][$num] = [$tempArr[$k][1],$tempArr[$k][2],$tempArr[$k][3],0];
                        $array[$i][3] = 1;
                        $num++;
                    }
                }
            }
        }
        return $array;
    }

    public function get_by_num()
    {
        //http://localhost:8080/smallprogram/index/v1/get_by_num

        $num = $this->_get("num");

        $bid = 40;
        $cid = 1;

        $number = split(',', $num);
        $sum = count($number);
        $sql="";
        $resultList = [$sum];
        for ($i=0; $i < $sum ; $i++){
            $chapter = Smallprogram::$BibleChapters[$number[$i]];
            if(count($chapter)>0){
                if($sql==""){
                    $sql = "( bookid = ".$chapter[1]." and chapterid = ".$chapter[2]." )";
                }
                else{
                    $sql = $sql." or ( bookid = ".$chapter[1]." and chapterid = ".$chapter[2]." ) ";
                }
            }
            $resultList[$i] = $chapter;
        };

        $sql = "select * from weixin_bible_cnv where ".$sql;

        $db = M();
        $list = $db->query($sql);
        $sum = count($list);

        for($i=0;$i<count($resultList);$i++){
            $verseList = [];
            for ($k=0; $k < $sum ;$k++){
                if($resultList[$i][1]==$list[$k]['bookid'] && $resultList[$i][2]==$list[$k]['chapterid']){
                    array_push($verseList,$list[$k]);
                }
            }
            $resultList[$i][3] = $this->bibleVerseOrder($verseList);
        }

        $this->ajax($resultList,"","");
    }

    public function bibleVerseOrderV1($list,$videoList){
        $array = [];
        $array_id = 0;
        $sum = count($list);
        $videoSum = count($videoList);
//        dump($videoList);
        $subverseId  = 0;
        $suborderId  = 0;
        $subverseSum  = 0;
        $tempArr = [];
        $tempArrOrder = 0;
        for ($i=0; $i < $sum ; $i++){
            $status = intval($list[$i]['status']);
            $verseid = $list[$i]['verseid'];
            $content = $list[$i]['content'];
            $videoinfo = [];

            if(strripos($verseid, "-")===false){

            }
            else
            {
                $verseid = "";
            }

            $isExist = false;
            for ($k=0; $k < $videoSum ; $k++){
                if($i==0 && $videoList[$k]["verse_id"]<=0){
                    $subverseId = -1;
                    $subverseSum = 0;
                    $suborderId = 0;

                    $array[$array_id][0] = $verseid;
                    $array[$array_id][1] = '';
                    $array[$array_id][2] = 4;
                    $array[$array_id][3] = 0;
                    $array[$array_id][4] = '';
                    $array[$array_id][5] = [$videoList[$k]["id"],$videoList[$k]["title"]];
                    $array[$array_id][6] = [];
                    $array_id++;
                }
                else if($videoList[$k]["verse_id"] == $verseid)
                {
                    $isExist = true;
                    $videoinfo = [$videoList[$k]["id"],$videoList[$k]["title"]];
                }
                else if($videoList[$k]["verse_id"]+1 == $verseid)
                {
                    if($status==0){
                        $status = 1;
                    }
                }
            }

            if($status == 3)
            {
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = '';
                $array[$array_id][5] = [];
                $array[$array_id][6] = [];
                $array_id++;
            }
            else if($status == 2){
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = '';
                $array[$array_id][5] = [];
                $array[$array_id][6] = [];
                $array_id++;
            }
            else if($status == 1){
                $subverseId = $verseid;
                $subverseSum = 1;
                $suborderId = $i;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = $content;
                $array[$array_id][2] = $status;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = '';
                $array[$array_id][5] = [];
                $array[$array_id][6] = [];
                $array_id++;
            }
            else
            {

                if($subverseId>0 && $subverseId + $subverseSum == $verseid){
                    $tempArr[$tempArrOrder] = [$subverseId,$verseid,$content,$status,0];
                    $tempArrOrder++;
                    $subverseSum ++;
                }

            }
            if($isExist==true){
                $subverseId = -1;
                $subverseSum = 0;
                $suborderId = 0;

                $array[$array_id][0] = $verseid;
                $array[$array_id][1] = '';
                $array[$array_id][2] = 4;
                $array[$array_id][3] = 0;
                $array[$array_id][4] = '';
                $array[$array_id][5] = $videoinfo;
                $array[$array_id][6] = [];
                $array_id++;
            }
        }

        $sum = count($array);
        $tempSum = count($tempArr);
        for ($i=0; $i < $sum ; $i++){
            if($array[$i][2] == 1){

                $array[$i][4] = [];
                $num = 0;
                for ($k=0; $k < $tempSum ; $k++){
                    if($tempArr[$k][0] == $array[$i][0]){
                        $array[$i][6][$num] = [$tempArr[$k][1],$tempArr[$k][2],$tempArr[$k][3],0,''];
                        $array[$i][3] = 1;
                        $num++;
                    }
                }
            }
        }
//        dump($array);
        return $array;
    }

    public function get_by_num_v1()
    {
        //http://localhost:1060/smallprogram/index/get_by_num

        $num = $this->param("num");

        $bibleversion = $this->param("bibleversion");
        $bibledb = "";
        $bibleVideoLanguage = "";
        $bibleVideoTitle = "";
        if($this->isNull($bibleversion)==true){
            $bibledb = "bible_cnv";
            $bibleVideoLanguage = " language_type = 'zh-cn' ";
            $bibleVideoTitle = "title";
        }
        else if ($bibleversion == 0)
        {
            $bibledb = "bible_cnv";
            $bibleVideoLanguage = " language_type = 'zh-cn' ";
            $bibleVideoTitle = "title as title";
        }
        else if ($bibleversion == 1)
        {
            $bibledb = "bible_nlt";
            $bibleVideoLanguage = "language_type like  '%en%'";
            $bibleVideoTitle = "title_en as title";
        }


        $bid = 40;
        $cid = 1;

        $number = explode(',', $num);
        $sum = count($number);
        $sql="";
        $videoSql = "";
        $resultList = [$sum];
        for ($i=0; $i < $sum ; $i++){
            $chapter = BibleVersionInfo::$BibleChapters[$number[$i]];
            if(count($chapter)>0){
                if($sql==""){
                    $sql = "( bookid = ".$chapter[1]." and chapterid = ".$chapter[2]." )";
                    $videoSql = "( book_id = ".$chapter[1]." and chapter_id = ".$chapter[2]." )";
                }
                else{
                    $sql = $sql." or ( bookid = ".$chapter[1]." and chapterid = ".$chapter[2]." ) ";
                    $videoSql = $videoSql." or ( book_id = ".$chapter[1]." and chapter_id = ".$chapter[2]." )";
                }
            }
            $resultList[$i] = $chapter;
        };

        $sql = "select * from ".$bibledb." where ".$sql;
        $videoSql = "select id,".$bibleVideoTitle.",book_id,chapter_id,verse_id,video_address from bible_video_info where (".$videoSql.") and is_published = 1 AND publish_dated<NOW() and ".$bibleVideoLanguage." and video_type = 1";
//        echo $videoSql;

        $list = Db::query($sql);
        $videolist = Db::query($videoSql);
        $sum = count($list);
        $videoSum = count($videolist);

        for($i=0;$i<count($resultList);$i++){
            $verseList = [];
            $videotempList = [];
            for ($k=0; $k < $sum ;$k++){
                if($resultList[$i][1]==$list[$k]['bookid']
                    && $resultList[$i][2]==$list[$k]['chapterid']){
                    array_push($verseList,$list[$k]);
                }
            }
            for ($k=0; $k < $videoSum ;$k++){
                if($resultList[$i][1]==$videolist[$k]['book_id']
                    && $resultList[$i][2]==$videolist[$k]['chapter_id']){
                    array_push($videotempList,$videolist[$k]);
                }
            }

            $resultList[$i][3] = $this->bibleVerseOrderV1($verseList,$videotempList);
        }

        $result  =  array();
        $result['status']  =  1;
        $result['info'] =  '';
        $result['data'] = $resultList;
        $result['v'] = 1;
        $this->simpleAjax($result);
    }

    public function search()
    {
        //http://localhost:8080/smallprogram/index/search?words=%E6%88%91&pageindex=1&bibleversion=0

        $words = $this->param("words");
        $pageIndex = $this->param("pageindex");
        $bibleversion = $this->param("bibleversion");
        $bibledb = "";
        if($this->isNull($bibleversion)==true){
            $bibledb = "bible_cnv";
        }
        else if ($bibleversion == 0)
        {
            $bibledb = "bible_cnv";
        }
        else if ($bibleversion == 1)
        {
            $bibledb = "bible_nlt";
        }
        else
        {
            $bibledb = "bible_cnv";
        }

        if($this->isNull($words)==true){
            $words = "";
        };

        $limit = "";
        if($this->isNull($pageIndex)==true){
            $pageIndex  = 1;
            $index=1;
            $limit = 0;
            $limit = ' limit '.$limit.','.(20);
        }
        else
        {
            $limit = ($pageIndex-1)*20;
            $limit = ' limit '.$limit.','.(20);
        }

        $sql = " select bookid,chapterid,verseid,content,'' as n from ".$bibledb." where (status = 1 or status = 0) and ";
//        dump($sql . " content like '%".$words."%' ".$limit);
        $list = DB::query($sql . " content like '%".$words."%' ".$limit);
        $sql = " select count(*) as t from ".$bibledb." where (status = 1 or status = 0) and ";
        $sum = DB::query($sql . " content like '%".$words."%' ");
//        echo $sum[0]['t'];
        $result = [];
        $result["total"] = $sum[0]['t'];
        $result["pageindex"] = $pageIndex;
        $result["data"] = $list;
//        dump($result);
        $this->simpleAjax($result);
    }

    public function get_video_by_id()
    {
        //http://localhost:1060/smallprogram/index/get_video_by_id?id=1

        $id = $this->param("id");
        $sql = "select title,book_id,chapter_id,verse_id,video_address from bible_video_info where id=".$id;
        $list = Db::query($sql);
        if(count($list)>0){
            $list = $list[0];
        }
        $this->simpleAjax($list);
    }
}