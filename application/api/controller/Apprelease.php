<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\api\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\AppReleaseInfo;


class Apprelease extends BaseController
{

    public function getReleaseInfo(){
        // http://localhost:1062/api/apprelease/getreleaseInfo?package_name=com.pressbible.view&channel=
        $model = AppReleaseInfo::getInfoByPackagename($this->param('package_name'),"");
        if(empty($model)){
            $this->ajax('','',0);
        }
        else
        {
            $this->ajax($model,'',1);
        }
    }
}