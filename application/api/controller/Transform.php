<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\api\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\BibleVideoInfo;
use app\lib\business\AlbumInfo;
use app\lib\business\TagsInfo;
use app\lib\business\FollowInfo;
use app\lib\business\LanguageInfo;
use app\lib\model\BibleVideoInfoModel;
use app\lib\model\AblumnInfoModel;
use app\lib\business\UserInfo;

class Transform extends BaseController
{
    public function index(){
        // http://192.168.31.126:1062/api/video/v1/index?openId=123456789&pageindex=1&tagsId=-1&language=zh
        $language = 'zh';
        $openId = $this->param('openId');
        $tagsId = $this->param('tagsId');
        $pageindex = $this->param('pageindex');

        //设置标签
        if($this->isNull($tagsId)){
            $tagsId = 0;
        }

        $tagsId = intval($tagsId);
        $tgId = -1;
        switch($tagsId){
            case 0: $tgId = 0;break;
            case 1: $tgId = 14;break;
            case 2: $tgId = 1;break;
            case 3: $tgId = 2;break;
            case 4: $tgId = 3;break;
            case 5: $tgId = 4;break;
            case 6: $tgId = 5;break;
            case 7: $tgId = 6;break;

        }
        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        //获得标签
        $tags = TagsInfo::getTagsByLanguageId($language_id);
        $result['tags'] = $tags;

        //获得视频
        $result = BibleVideoInfo::getVideoList($tgId.'',$pageindex,$openId,$language_id);
        $result['tags'] = $tags;
        if($result['label']=='follow'){
            $result["tagsId"]  = 1;
        }
        else
        {
            $result["tagsId"]  = $tagsId;
        }

        $tagList = [];
        if($result['tags']){
            foreach ($result['tags'] as $item){
               array_push($tagList,$item['name']);
            }
        }
        $result['tags'] = $tagList;
        if($result['data']){
            foreach ($result['data'] as &$item){
                $item['video_length'] = $item['length'];
                $item['video_des'] = $item['des'];
                $item['video_author'] = $item['author'];
                if(array_key_exists('album_name',$item)) {
                    $item['abulum_name'] = $item['album_name'];
                }
            }
        }
        $result['v'] = 1;
        $this->simpleAjax($result);
    }

    public function getListById(){
        // http://192.168.31.126:1062/api/video/v1/play?openId=a9e76fa20f9137c594ac674a95721901&id=2278&language=zh
        $id = $this->param('id');
        $openId = $this->param('openId');
        $language = 'zh';
        $result = [];

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        $result = BibleVideoInfo::getPlay($id,$openId,$language_id);
        if($result['videoInfo']){
            $result['videoInfo']['video_length'] = $result['videoInfo']['length'];
            $result['videoInfo']['video_des'] = $result['videoInfo']['des'];
            $result['videoInfo']['video_author'] = $result['videoInfo']['author'];
            $result['videoInfo']['video_address'] = $result['videoInfo']['address'];
            $result['videoInfo']['des'] = '';
            $result['videoInfo']['author'] = '';
            $result['videoInfo']['address'] = '';
        }

        if($result['albumInfo']){
            $result['albumInfo']['image_url'] = $result['albumInfo']['img_url'];
            $result['albumInfo']['des'] = $result['albumInfo']['remark'];
            $result['albumInfo']['img_url'] = '';
            $result['albumInfo']['remark'] = '';
        }
        if($result['data']){
            foreach ($result['data'] as &$item){
                $item['video_length'] = $item['length'];
                $item['video_des'] = $item['des'];
                $item['video_author'] = $item['author'];
                $item['video_address'] =  $item['address'];
                if(array_key_exists('album_name',$item)) {
                    $item['abulum_name'] = $item['album_name'];
                }
            }
        }

//        dump($result['data']);

        $result['v'] = 1;
//        dump($result);
        $out  =  array();
        $out['status']  =  1;
        $out['info'] =  '';
        $out['data'] = $result;
        $out['v'] = 1;
        $this->simpleAjax($out);
    }

    public function follow(){
        // http://192.168.31.126:1062/api/video/v1/play?openId=a9e76fa20f9137c594ac674a95721901&id=2278&language=zh
        $openId = $this->param("openId");
        $language = "zh";

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        if($this->isNull($openId)==true){
            $openId = "";
        };

        $result = FollowInfo::getAllInfo($openId,$language_id);

        if($result['notsubscribedList']){
            foreach ($result['notsubscribedList'] as &$item) {
                $item['image_url'] = $item['img_url'];
                $item['des'] = $item['remark'];
                $item['img_url'] = '';
                $item['remark'] = '';
            }
        }

        if($result['hotVideoList']){
            foreach ($result['hotVideoList'] as &$item){
                $item['video_length'] = $item['length'];
                $item['video_author'] = $item['author'];
                if(array_key_exists('album_name',$item)) {
                    $item['abulum_name'] = $item['album_name'];
                }
            }
        }

//        dump($result);

        $this->ajax($result,"",1);
    }

    public function getColumnInfo(){

        $columnId = $this->param("columnId");
        $openId = $this->param("openId");
        $language = 'zh';

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        if($this->isNull($columnId)==true){
            $openId = 0;
        };

        $result = FollowInfo::getColumnInfo($columnId,$openId,$language_id);
        if($result['columnInfo']){
            $result['columnInfo']['image_url'] = $result['columnInfo']['img_url'];
            $result['columnInfo']['des'] = $result['columnInfo']['remark'];
            $result['columnInfo']['img_url'] = '';
            $result['columnInfo']['remark'] = '';
        }
        if($result['videoList']){
            foreach ($result['videoList'] as &$item){
                $item['video_length'] = $item['length'];
                $item['video_des'] = $item['des'];
                $item['video_author'] = $item['author'];
                if(array_key_exists('album_name',$item)) {
                    $item['abulum_name'] = $item['album_name'];
                }
            }
        }
//        dump($result);
        $this->ajax($result,"",1);
    }

    public function notsubscribedList()
    {
        //http://localhost:1062/api/follow/v1/notsubscribedList?openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs&language=zh
        $openId = $this->param("openId");
        $language = 'zh';

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        $result = FollowInfo::notsubscribedList($openId,$language_id);

        foreach ($result as &$item) {
            $item['image_url'] = $item['img_url'];
            $item['des'] = $item['remark'];
            $item['img_url'] = '';
            $item['remark'] = '';
        }
        $this->ajax($result,"",1);
    }

    public function followColumnInfo()
    {
        //http://localhost:1062/api/follow/v1/followColumnInfo?openId=a9e76fa20f9137c594ac674a95721901&language=zh
        $openId = $this->param("openId");
        $language = 'zh';

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        if($this->isNull($openId)==true){
            $openId = "";
        };

        $alreadyfollowlist = UserInfo::getFollowAlbumByOpenId($openId);
        $albumInfo = AlbumInfo::getAlbumByCache();
        $result = [];
        foreach ($alreadyfollowlist as $item){
            foreach ($albumInfo as $tempItem){
                if($item['album_id'] == $tempItem['id']){
                    $tempItem['image_url'] = $tempItem['img_url'];
                    $tempItem['img_url'] = '';
                    $tempItem['remark'] = '';
                    array_push($result,$tempItem);
                    break;
                }
            }
        }

//        dump($result);
        $this->ajax($result,"",1);
//        $this->simpleAjax($alreadyfollowlist);

    }

    //user follow column
    public function userFollow(){
        //http://192.168.2.127:1062/api/follow/v1/userFollow?columnId=1651&openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs
        $columnId = $this->param("columnId");
        $openId = $this->param("openId");

        if($this->isNull($columnId)==true){
            $openId = "";
        };
        if($this->isNull($openId)==true){
            $openId = "";
        };

        if(strlen($openId)<=0 || strlen($openId) <= 0){

            $this->ajax("","",0);

        }
        else
        {
            AlbumInfo::followAlbum($openId,$columnId);
            $this->ajax("","",1);

        }
    }

    //user remove follow column
    public function userRemoveColumn(){
        //http://192.168.2.127:1062/api/follow/v1/userRemoveColumn?columnId=1651&openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs
        $columnId = $this->param("columnId");
        $openId = $this->param("openId");

        if($this->isNull($columnId)==true){
            $openId = "";
        };
        if($this->isNull($openId)==true){
            $openId = "";
        };

        if(strlen($openId)<=0 || $columnId == 0){
            $this->ajax("","",0);
        }
        else
        {
            AlbumInfo::userRemoveAblum($openId,$columnId);
            $this->ajax("","",1);
        }
    }

}