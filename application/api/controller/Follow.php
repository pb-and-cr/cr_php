<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\api\controller;

use app\lib\business\AlbumInfo;
use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\FollowInfo;
use app\lib\business\BooksInfo;
use app\lib\business\ArticleInfo;
use app\lib\business\UserInfo;
use app\lib\business\LanguageInfo;


class Follow extends BaseController
{

    public function index(){

        // http://localhost:1062/api/follow/v1/index?openId=a9e76fa20f9137c594ac674a95721901&language=zh
        header("Access-Control-Allow-Origin: *");
        $openId = $this->param("openId");
        if($this->isNull($openId)==true){
            $openId = "";
        };
        $results = FollowInfo::getAllInfo($openId);
        $results['client_url'] = Config::get('CLIENT_URL');
        $this->ajax($results,"",1);
    }

    public function queryAllAlbumLists(){
        header("Access-Control-Allow-Origin: *");
        $openId = $this->param("openId");
        if($this->isNull($openId)==true){
            $openId = "";
        };
        $results = FollowInfo::allAlbumLists($openId);
        $this->ajax($results,"",1);

    }


    public function notsubscribedList()
    {
        //http://localhost:1062/api/follow/v1/notsubscribedList?openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs&language=zh
        $openId = $this->param("openId");
        $language = $this->param("language");

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        $results = FollowInfo::notsubscribedList($openId,$language_id);

        $this->ajax($results,"",1);
    }

    public function followColumnInfo()
    {
        //http://localhost:1062/api/follow/v1/followColumnInfo?openId=a9e76fa20f9137c594ac674a95721901&language=zh
        $openId = $this->param("openId");
        if($this->isNull($openId)==true){
            $openId = "";
        };

        $alreadyFollowList = UserInfo::getFollowAlbumByOpenId($openId);
        $albumInfo = AlbumInfo::getAlbumByCache();
        $result = [];
        foreach ($alreadyFollowList as $item){
            foreach ($albumInfo as $tempItem){
                if($item['album_id'] == $tempItem['id']){
                    array_push($result,$tempItem);
                    break;
                }
            }
        }

        $this->ajax($result,"",1);
//        $this->simpleAjax($alreadyfollowlist);

    }

    public function getOpenId(){
//        http://localhost:1060/smallprogram/follow/getOpenId?code=003JbK9y1VCDRe0AGxay16hW9y1JbK9S

        $code = $this->param("code");
//        echo $code;
        $appid = 'wxefaedf69c3a2332e';
        $appsecret = '1d406847001abb493751b25c4e44ec39';
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$appid.'&secret='.$appsecret.'&js_code='.$code.'&grant_type=authorization_code';
//        echo $url;
        $weixin =  file_get_contents($url);//通过code换取网页授权access_token
        $jsondecode = json_decode($weixin); //对JSON格式的字符串进行编码
        $array = get_object_vars($jsondecode);//转换成数组
        $this->ajax($array);
    }

    public function getColumnInfo()
    {
        //http://localhost:1060/api/follow/getColumnInfo?columnId=1651&openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs
        //http://localhost:1062/api/follow/v1/getColumnInfo?columnId=1651&openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs&language=zh

        $columnId = $this->param("columnId");
        $openId = $this->param("openId");
        $language = $this->param("language");

        //获取语言id
        $language_id = LanguageInfo::getLanguageIdByLanguageCode($language);

        if($this->isNull($columnId)==true){
            $openId = 0;
        };

        $results = FollowInfo::getColumnInfo($columnId,$openId,$language_id);

        $this->ajax($results,"",1);
    }

    //user follow column
    public function userFollow(){
        //http://192.168.2.127:1062/api/follow/v1/userFollow?columnId=1651&openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs
        header("Access-Control-Allow-Origin: *");
        $columnId = $this->param("columnId");
        $openId = $this->param("openId");

        if($this->isNull($columnId)==true){
            $openId = "";
        };
        if($this->isNull($openId)==true){
            $openId = "";
        };

        if(strlen($openId)<=0 || strlen($openId) <= 0){

            $this->ajax("","",0);

        }
        else
        {
            AlbumInfo::followAlbum($openId,$columnId);
            $this->ajax("","",1);

        }
    }

    //user remove follow column
    public function userRemoveColumn(){
        //http://192.168.2.127:1062/api/follow/v1/userRemoveColumn?columnId=1651&openId=oCQUJ0WWohvDkKju8G0zyQ9o87Cs
        header("Access-Control-Allow-Origin: *");
        $columnId = $this->param("columnId");
        $openId = $this->param("openId");

        if($this->isNull($columnId)==true){
            $openId = "";
        };
        if($this->isNull($openId)==true){
            $openId = "";
        };

        if(strlen($openId)<=0 || $columnId == 0){
            $this->ajax("","",0);
        }
        else
        {
            AlbumInfo::userRemoveAblum($openId,$columnId);
            $this->ajax("","",1);
        }
    }

    public function searchResult(){
        //type = 1查询书;2表示查询文章;3，表示查询专辑
        header("Access-Control-Allow-Origin: *");
        $searchValue = $this->param("title");
        $pageIndex = $this->param('pageIndex');
        $type = $this->param("type");
        $user_id = $this->param("user_id");
        if($type == 1){
            $results = BooksInfo::getSearchResult($searchValue,$pageIndex);
        }else if($type == 2){
            $results = ArticleInfo::searchArticle($searchValue,$pageIndex);
        }else{
            // $results = 0;
            $results = FollowInfo::searchAlbum($searchValue,$pageIndex,$user_id);
        }
        $this->ajax($results,"",1);
    }

}