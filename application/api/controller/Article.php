<?php
namespace app\api\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\AlbumInfo;
use app\lib\business\ArticleInfo;
use app\lib\model\ArticleInfoModel;
use app\lib\model\ArticleInfoContentModel;
use app\lib\common\TimeHelper;

class Article extends BaseController
{
    public function index(){
        // curl -d 'pageIndex=1' http://localhost:1070/api/Article/v1/index
        //$openId = $this->param('openId');
        header("Access-Control-Allow-Origin: *");
        $pageIndex = $this->param('pageIndex');
        //dump($pageIndex);
        //$pageIndex = 2;
        //获取文章
        $result['data'] = ArticleInfo::getArticleLists($pageIndex);
        $result['client_url'] = Config::get('CLIENT_URL');
        $this->simpleAjax($result);
    }
    public function test() {
        phpinfo();
    }

    public function getArticleListByBrowse(){
		header("Access-Control-Allow-Origin: *");
		$pageIndex = $this->param('pageIndex');
		$result = ArticleInfo::getArticleListsByMostBrowse($pageIndex);
		if($result){
			$this->ajax($result,'',1);
		}else{
			$this->ajax($result,'',0);
		}

	}

    // 排行榜
    public function reak(){
        // curl -d 'pageIndex=1' http://localhost:1070/api/Article/v1/index

        header("Access-Control-Allow-Origin: *");
        $begDate = $this->param('begDate');
        $endDate = $this->param('endDate');

        //获取文章
        $result['data'] = ArticleInfo::getReakLists($begDate,$endDate);
        $this->simpleAjax($result);
    }


    public function album(){
        // http://192.168.31.126:1062/api/video/v1/play?openId=a9e76fa20f9137c594ac674a95721901&id=2278&language=zh
        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $pageIndex = $this->param('pageIndex');
        //$openId = $this->param('openId');
        $result = [];

        $albumInfo = AlbumInfo::getCacheAlbumById($id);
        $articleInfo = ArticleInfo::getArticleListsByAlbumId($id,$pageIndex);
        $result['albumInfo'] = $albumInfo;
        $result['articleInfo'] = $articleInfo;
        $result['client_url'] = Config::get('CLIENT_URL');
        $this->ajax($result);
    }

    public function articleInfoById(){
        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $user_id = $this->param('user_id');
        $articleInfo = ArticleInfo::getArticleInfoById($id,$user_id);
        $albumInfo = AlbumInfo::getAlbumInfoById($articleInfo[0]['album_id'],$user_id);
        $result['article_info'] = $articleInfo;
        $result['album_info'] = $albumInfo;
        $this->simpleAjax($result);
    }

    // 获取文章的相关信息,
    public function getArticleOtherInfo(){
        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $user_id = $this->param('user_id');
        $album_id = $this->param('album_id');
        $articleInfo = ArticleInfo::getArticleOtherInfo($id,$user_id,$album_id);
        $result['articleInfo'] = $articleInfo;
        $this->simpleAjax($result);
    }

    public function addArticleToLove(){
        header("Access-Control-Allow-Origin: *");
        if($this->request->isGet()) {
            $data = $this->request->get();
            $msg = ArticleInfo::addArticleToMyLove($data);
            if($msg==1){
                $info = "添加成功";
                $this->ajax($msg,$info,1);
            }else{
                $info = "已收藏";
                $this->ajax($msg,$info,0);
            }

        }

    }
    public function isArticleInLove(){
        $id = $this->param('article_book_id');
        $user_id = $this->param('user_id');
        $msg['isInLove'] = ArticleInfo::checkArtIfInLove($id,$user_id);
        if($msg['isInLove']==1){
            $this->ajax($msg,'',1);
        }else{
            $this->ajax($msg,'',0);
        }

    }
    public function removeArticleFromLove(){
        header("Access-Control-Allow-Origin: *");
        if($this->request->isGet()) {
            $data = $this->request->get();
            $love_count = $data['love_count'];
            unset($data['love_count']);
            $msg = ArticleInfo::removeArticleFromMyLove($data,$love_count);
            if($msg==0){
                $info = "请重试";
                $this->ajax($msg,$info,0);
            }else{
                $info = "移除成功";
                $this->ajax($msg,$info,1);
            }

        }
    }
    public function articleInLove(){
        header("Access-Control-Allow-Origin: *");
        if($this->request->isGet()) {
            $data = $this->request->get();
            $resualt['data'] = ArticleInfo::getMyLovedArticles($data);
            $resualt['client_url'] = Config::get('CLIENT_URL');
            if($resualt['data']){
                $this->ajax($resualt,'',1);
            }else{
                $this->ajax($resualt,'您还没添加文章',0);
            }

        }
    }
    public function shareArticles(){
        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $result = ArticleInfo::updateShareCount($id);
        $this->ajax('','',1);
    }

    public function updateArticleContent(){
//            curl -d  http://localhost:1070/api/Article/updateArticleContent
            $content = Db::table('article_content')->page(7, 1000)->select();
//        echo mb_substr("php中文字符encode",0,4,"utf-8").'<br/>';
            foreach ($content as &$item) {
                if(empty($item['content'])){

                }
                else{
                    $item['content'] = strip_tags($item['content']);
                    $item['content'] = trim($item['content']);
                    if(strlen($item['content'])>0){
                        if(strlen($item['content'])>30){
                            $item['content'] = mb_substr($item['content'], 0, 30,'utf-8');
                        }
                        else{
                            $item['content'] = mb_substr($item['content'], 0, strlen($item['content']-1),'utf-8');
                        }
                        echo $item['content'].'<br/>';
                        Db::execute("update article_info set des = ?  where id=? and album_id != 2275",[$item['content'],$item['article_id']]);
                    }
                }

            }

    }

}