<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 2018/3/6
 * Time: 下午2:27
 */

namespace app\api\controller;


use app\lib\business\BooksInfo;


class Books extends BaseController
{
    public function getBooksTags(){
        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $pageIndex = $this->param('pageIndex');
        $tags = BooksInfo::getTags();
        $books = BooksInfo::getBooksByTagId($id,$pageIndex);
        $result= [];
        $result['tags'] = $tags;
        $result['books'] = $books;
        $this->ajax($result,'',1);
    }
    public function getBooksInShop(){
        header("Access-Control-Allow-Origin: *");
        // $id = $this->param('id');
        // $pageIndex = $this->param('pageIndex');
        $tags = BooksInfo::getTags();
        $books = BooksInfo::getAllBooksInShop();
        // $booksInfo = array();
        // $booksInfo['tags'] = $tags;
        // $booksInfo['books']  = $books;

        $result= [];
        $result['tags'] = $tags;
        $result['books'] = $books;
        $this->ajax($result,'',1);
    }


    //添加神学院书籍
    public function addNewBook() {
        header("Access-Control-Allow-Origin: *");
        if($this->request->isPost()){
            $data = $this->request->post();
            // dump($data);
            // return;

            try {
                foreach($data as &$param) {
                     $re = BooksInfo::addNewEpubBook($param);
                     if($re == 1) {

                     }else {
                         $info = "遗憾,操作失败";
                         $this->ajax($re,$info,0);
                         break;
                     }
                };
                $this->ajax('','导入成功',1);
            } catch (\Exception $e) {
                echo var_dump($e);
            };


            

        }
    }

    public function getBookProfile(){

        header("Access-Control-Allow-Origin: *");
        $id = $this->param('id');
        $openId = $this->param('openId');
        $resualt = BooksInfo::getProfileById($id,$openId);
        $this->ajax($resualt);
    }
    public function addBookToShelf(){
        header("Access-Control-Allow-Origin: *");
        if($this->request->isGet()) {
            $data = $this->request->get();
            $msg = BooksInfo::addBookToMyShelf($data);
            if($msg==1){
                $info = "添加成功";
                $this->ajax($msg,$info,1);
            }else{
                $info = "已经添加到书架";
                $this->ajax($msg,$info,0);
            }

        }

    }
    public function removeBookFromShelf(){
        header("Access-Control-Allow-Origin: *");
        if($this->request->isGet()) {
            $data = $this->request->get();
            $msg = BooksInfo::removeBookFromMyShelf($data);
            if($msg==0){
                $info = "请重试";
                $this->ajax($msg,$info,0);
            }else{
                $info = "移除成功";
                $this->ajax($msg,$info,1);
            }
        }
    }
    public function getBooksOnShelf(){
        header("Access-Control-Allow-Origin: *");
        $user_id = $this->param('user_id');
        $page_index = $this->param('page_index');
        if(intval($user_id)<=0){
            $this->ajax('','',0);
        }
        else {
            $msg = BooksInfo::getBooksOnShelfByUserId($user_id, $page_index);
            $this->ajax($msg, '', 1);
        }
    }
    public function getSuggestBookLists(){
        $list = BooksInfo::suggestBookLists();
        $this->ajax($list,'',1);
    }
    public function getBookListsInfo(){
        header("Access-Control-Allow-Origin: *");
        $bookList_id = $this->param('bookList_id');
        $msg = BooksInfo::bookListsInfo($bookList_id);
        $this->ajax($msg,'',1);
    }
    public function getBanners(){
        header("Access-Control-Allow-Origin: *");
        $bookList_id = $this->param('bookList_id');
        $msg = BooksInfo::bookListsInfo($bookList_id);
        $this->ajax($msg,'',1);
    }
    public function searchBook(){
        header("Access-Control-Allow-Origin: *");
        $name = $this->param('title');
        $name = trim($name);
        $pageIndex = $this->param('pageIndex');
        $msg = BooksInfo::getSearchResult($name,$pageIndex);
        if($msg==0){
            $this->ajax($msg,'没有查询到相关书籍',0);
        }else{
            $this->ajax($msg,'',1);
        }
    }
    public function searchSpecifyBook (){
        header("Access-Control-Allow-Origin: *");
        if($this->request->isGet()) {
            $data = $this->request->get();
            $msg = BooksInfo::getSearchResult($data['title'],$data['pageIndex']);
            if($msg==0){
                $this->ajax($msg,'没有查询到相关书籍',0);
            }else{
                $this->ajax($msg,'',1);
            }
        }
    }
}
