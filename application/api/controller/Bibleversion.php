<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\api\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\BibleVideoInfo;
use app\lib\business\AlbumInfo;
use app\lib\business\TagsInfo;
use app\lib\business\LanguageInfo;
use app\lib\model\BibleVideoInfoModel;
use app\lib\model\AblumnInfoModel;

class Bibleversion extends BaseController
{
    public function get()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），k
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），k
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:8080/api/bibleversion/get"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip','version'=>2.1
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts."+
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>2
            ),
        );
        $this->ajax($list,'',1);
    }

    public function get_v1()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:8080/api/bibleversion/get_v1"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>2.1
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts."+
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.1
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.1
            ),
        );
        $this->ajax($list,'',1);
    }


    public function get_v2()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:8080/api/bibleversion/get_v2"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>2.1,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/NLT/'
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts.".
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.1,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/KJV/'
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.1,'audioAddress'=>''
            ),
        );
        $this->ajax($list,'',1);
    }

    public function get_v3()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:8080/api/bibleversion/get_v2"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>2.1,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/NLT/'
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts.".
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.1,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/KJV/'
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>6,'name'=>'圣经当代译本修订版','abbName'=>'当代译本'
            ,'fileName'=>'bible_zh.ccbs','desc'=>'The Chinese Contemporary Bible (圣经当代译本修订版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (圣经当代译本修订版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (圣经当代译本修订版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (圣经当代译本修订版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>7,'name'=>'聖經當代譯本修訂版','abbName'=>'當代譯本'
            ,'fileName'=>'bible_zh.ccbt','desc'=>'The Chinese Contemporary Bible (聖經當代譯本修訂版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (聖經當代譯本修訂版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (聖經當代譯本修訂版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-tw','language'=>'繁體中文','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (聖經當代譯本修訂版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbt.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
        );
        $this->ajax($list,'',1);
    }

    public function get_v4()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:8080/api/bibleversion/get_v2"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>2.1,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/NLT/'
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts.".
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.2,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/KJV/'
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>6,'name'=>'圣经当代译本修订版','abbName'=>'当代译本'
            ,'fileName'=>'bible_zh.ccbs','desc'=>'The Chinese Contemporary Bible (圣经当代译本修订版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (圣经当代译本修订版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (圣经当代译本修订版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (圣经当代译本修订版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>7,'name'=>'聖經當代譯本修訂版','abbName'=>'當代譯本'
            ,'fileName'=>'bible_zh.ccbt','desc'=>'The Chinese Contemporary Bible (聖經當代譯本修訂版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (聖經當代譯本修訂版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (聖經當代譯本修訂版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-tw','language'=>'繁體中文','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (聖經當代譯本修訂版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbt.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>8,'name'=>'中文和合本拼音版','abbName'=>'拼音'
            ,'fileName'=>'bible_zh.py','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.py.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>9,'name'=>'丁道尔圣经注释','abbName'=>'丁道尔注释'
            ,'fileName'=>'commentary_zh.tcs','desc'=>'丁道尔系列是合乎时代的解经丛书，是广博崇高的神学智慧，不但具备循序渐进，由浅而深的条件，且整体构架着力于坚实的神学学术根基上，是一套内容最精简、数据最新的解经丛书。帮助读者确实了解圣经的真理，灵活应用圣经的原则。
丁道尔系列自出版以来，深获好评，被喻为「无价之宝」，值得华人教会在圣经导读上推广使用，建立良好的读经习惯。内容主要分成两大部：
1.导论：简洁、详尽地介绍作者、写作日期及时代背景，不但能帮助读者一窥该卷书的主旨及全貌，也能提供有心钻研的学者宝贵的数据。
2.注释：按主题分段，逐节详解；较难懂的经节也会特别加注说明，让圣经的信息浅白易懂。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.tcs.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>10,'name'=>'马唐纳圣经注释','abbName'=>'马唐纳注释'
            ,'fileName'=>'commentary_zh.wmd','desc'=>'这本新旧约的逐节注释书，为马唐纳四十多年来圣经研究、传道和写作的硕果。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.wmd.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>11,'name'=>'पवित्र बाइबिल','abbName'=>'HHBD'
            ,'fileName'=>'bible_hi.hhbd','desc'=>''
            ,'languageCode'=>'hi','language'=>'Hindi','type'=>'Bible','copyRight'=>'Public Domain'
            ,'briefCopyRight'=>'Public Domain','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_hi.hhbd.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/HHBD/'
            ),
            array(
                'serverId'=>12,'name'=>'Reina-Valera Antigua','abbName'=>'RVA'
            ,'fileName'=>'bible_sp.rva','desc'=>'The Reina-Valera Antigua was first translated and published in 1569 by Casiodoro de Reina, after twelve years of intensive work, and later put out in 1602 in revised form by Cipriano de Valera, who gave more than twenty years of his life to its revision and improvement.'
            ,'languageCode'=>'sp','language'=>'Spanish','type'=>'Bible','copyRight'=>'No copyright information available.'
            ,'briefCopyRight'=>'No copyright information available.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_sp.rva.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/RVA/'
            ),
        );
        $this->ajax($list,'',1);
    }

    public function get_v5()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:8080/api/bibleversion/get_v2"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','languageNameEnglish'=>'Chinese','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>2.1,'languageNameEnglish'=>'English','audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/NLT/'
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts.".
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.2,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/KJV/'
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>6,'name'=>'圣经当代译本修订版','abbName'=>'当代译本'
            ,'fileName'=>'bible_zh.ccbs','desc'=>'The Chinese Contemporary Bible (圣经当代译本修订版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (圣经当代译本修订版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (圣经当代译本修订版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageNameEnglish'=>'Chinese','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (圣经当代译本修订版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>7,'name'=>'聖經當代譯本修訂版','abbName'=>'當代譯本'
            ,'fileName'=>'bible_zh.ccbt','desc'=>'The Chinese Contemporary Bible (聖經當代譯本修訂版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (聖經當代譯本修訂版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (聖經當代譯本修訂版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-tw','language'=>'繁體中文','languageNameEnglish'=>'Chinese','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (聖經當代譯本修訂版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbt.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>8,'name'=>'中文和合本拼音版','abbName'=>'拼音'
            ,'fileName'=>'bible_zh.py','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageNameEnglish'=>'Chinese','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.py.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>9,'name'=>'丁道尔圣经注释','abbName'=>'丁道尔注释'
            ,'fileName'=>'commentary_zh.tcs','desc'=>'丁道尔系列是合乎时代的解经丛书，是广博崇高的神学智慧，不但具备循序渐进，由浅而深的条件，且整体构架着力于坚实的神学学术根基上，是一套内容最精简、数据最新的解经丛书。帮助读者确实了解圣经的真理，灵活应用圣经的原则。
丁道尔系列自出版以来，深获好评，被喻为「无价之宝」，值得华人教会在圣经导读上推广使用，建立良好的读经习惯。内容主要分成两大部：
1.导论：简洁、详尽地介绍作者、写作日期及时代背景，不但能帮助读者一窥该卷书的主旨及全貌，也能提供有心钻研的学者宝贵的数据。
2.注释：按主题分段，逐节详解；较难懂的经节也会特别加注说明，让圣经的信息浅白易懂。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageNameEnglish'=>'Chinese','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.tcs.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>10,'name'=>'马唐纳圣经注释','abbName'=>'马唐纳注释'
            ,'fileName'=>'commentary_zh.wmd','desc'=>'这本新旧约的逐节注释书，为马唐纳四十多年来圣经研究、传道和写作的硕果。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageNameEnglish'=>'Chinese','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.wmd.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>11,'name'=>'पवित्र बाइबिल','abbName'=>'HHBD'
            ,'fileName'=>'bible_hi.hhbd','desc'=>''
            ,'languageCode'=>'hi','language'=>'Hindi','languageNameEnglish'=>'Hindi','type'=>'Bible','copyRight'=>'Public Domain'
            ,'briefCopyRight'=>'Public Domain','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_hi.hhbd.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/HHBD/'
            ),
            array(
                'serverId'=>12,'name'=>'Reina-Valera Antigua','abbName'=>'RVA'
            ,'fileName'=>'bible_sp.rva','desc'=>'The Reina-Valera Antigua was first translated and published in 1569 by Casiodoro de Reina, after twelve years of intensive work, and later put out in 1602 in revised form by Cipriano de Valera, who gave more than twenty years of his life to its revision and improvement.'
            ,'languageCode'=>'sp','language'=>'Spanish','languageNameEnglish'=>'Spanish','type'=>'Bible','copyRight'=>'No copyright information available.'
            ,'briefCopyRight'=>'No copyright information available.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_sp.rva.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/RVA/'
            ),
        );
//        dump($list);
        $this->ajax($list,'',1);
    }


    public function get_v6()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:1060/api/bibleversion/get_v5"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>2.1,'languageNameEnglish'=>'English','languageCodeDBP'=>'ENG','audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/NLT/'
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts.".
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','languageCodeDBP'=>'ENG','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','languageCodeDBP'=>'ENG','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.2,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/KJV/'
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','languageCodeDBP'=>'ENG','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>6,'name'=>'圣经当代译本修订版','abbName'=>'当代译本'
            ,'fileName'=>'bible_zh.ccbs','desc'=>'The Chinese Contemporary Bible (圣经当代译本修订版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (圣经当代译本修订版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (圣经当代译本修订版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (圣经当代译本修订版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>7,'name'=>'聖經當代譯本修訂版','abbName'=>'當代譯本'
            ,'fileName'=>'bible_zh.ccbt','desc'=>'The Chinese Contemporary Bible (聖經當代譯本修訂版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (聖經當代譯本修訂版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (聖經當代譯本修訂版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-tw','language'=>'繁體中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Traditional','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (聖經當代譯本修訂版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbt.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>8,'name'=>'中文和合本拼音版','abbName'=>'拼音'
            ,'fileName'=>'bible_zh.py','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.py.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>9,'name'=>'丁道尔圣经注释','abbName'=>'丁道尔注释'
            ,'fileName'=>'commentary_zh.tcs','desc'=>'丁道尔系列是合乎时代的解经丛书，是广博崇高的神学智慧，不但具备循序渐进，由浅而深的条件，且整体构架着力于坚实的神学学术根基上，是一套内容最精简、数据最新的解经丛书。帮助读者确实了解圣经的真理，灵活应用圣经的原则。
丁道尔系列自出版以来，深获好评，被喻为「无价之宝」，值得华人教会在圣经导读上推广使用，建立良好的读经习惯。内容主要分成两大部：
1.导论：简洁、详尽地介绍作者、写作日期及时代背景，不但能帮助读者一窥该卷书的主旨及全貌，也能提供有心钻研的学者宝贵的数据。
2.注释：按主题分段，逐节详解；较难懂的经节也会特别加注说明，让圣经的信息浅白易懂。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.tcs.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>10,'name'=>'马唐纳圣经注释','abbName'=>'马唐纳注释'
            ,'fileName'=>'commentary_zh.wmd','desc'=>'这本新旧约的逐节注释书，为马唐纳四十多年来圣经研究、传道和写作的硕果。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.wmd.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>11,'name'=>'पवित्र बाइबिल','abbName'=>'HHBD'
            ,'fileName'=>'bible_hi.hhbd','desc'=>''
            ,'languageCode'=>'hi','languageCodeDBP'=>'HND','language'=>'हिंदी','languageNameEnglish'=>'Hindi','type'=>'Bible','copyRight'=>'Public Domain'
            ,'briefCopyRight'=>'Public Domain','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_hi.hhbd.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/HHBD/'
            ),
            array(
                'serverId'=>12,'name'=>'Reina-Valera Antigua','abbName'=>'RVA'
            ,'fileName'=>'bible_sp.rva','desc'=>'The Reina-Valera Antigua was first translated and published in 1569 by Casiodoro de Reina, after twelve years of intensive work, and later put out in 1602 in revised form by Cipriano de Valera, who gave more than twenty years of his life to its revision and improvement.'
            ,'languageCode'=>'es','languageCodeDBP'=>'SPN','language'=>'Español','languageNameEnglish'=>'Spanish','type'=>'Bible','copyRight'=>'No copyright information available.'
            ,'briefCopyRight'=>'No copyright information available.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_sp.rva.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/RVA/'
            ),
        );
//        dump($list);
        $this->ajax($list,'',1);
    }

    public function get_v7()
    {
//        serverId（服务器ID）
//        name（全名），
//        abbName（缩略名）
//        fileName（文件名）
//        desc（描述）
//        languageCode（语言代码）
//        language（语言），
//        type（Bible），
//        copyRight（版权），
//        briefCopyRight（简写版权），
//        url（下载地址），
//        version（版本号），
//        cache（本地存储0/1）
        // curl -d last_date=1294887259 "http://localhost:1060/api/bibleversion/get_v5"
        $list = array
        (
            array(
                'serverId'=>1,'name'=>'简体中文和合本','abbName'=>'和合本'
            ,'fileName'=>'bible_zh.cuvs','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','language'=>'简体中文','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.cuvs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>2,'name'=>'New Living Translation','abbName'=>'NLT'
            ,'fileName'=>'bible_en.nlt','desc'=>'The goal of any Bible translation is to convey the meaning of the ancient Hebrew and Greek texts as accurately as possible to the modern reader. The New Living Translation is based on the most recent scholarship in the theory of translation. The challenge for the translators was to create a text that would make the same impact in the life of modern readers that the original text had for the original readers. In the New Living Translation, this is accomplished by translating entire thoughts (rather than just words) into natural, everyday English. The end result is a translation that is easy to read and understand and that accurately communicates the meaning of the original text.'
            ,'languageCode'=>'en','language'=>'English','type'=>'Bible','copyRight'=>'Holy Bible, New Living Translation copyright © 1996, 2004, 2007, 2013 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved. New Living, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers.
The text of the Holy Bible, New Living Translation, may be quoted in any form (written, visual, electronic, or audio) up to and inclusive of five hundred (500) verses without express written permission of the publisher, provided that the verses quoted do not account for more than 25 percent of the work in which they are quoted, and provided that a complete book of the Bible is not quoted.
When the Holy Bible, New Living Translation, is quoted, one of the following credit lines must appear on the copyright page or title page of the work:
Scripture quotations marked (NLT) are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Scripture quotations are taken from the Holy Bible, New Living Translation, copyright ©1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
Unless otherwise indicated, all Scripture quotations are taken from the Holy Bible, New Living Translation, copyright © 1996, 2004, 2007 by Tyndale House Foundation. Used by permission of Tyndale House Publishers, Inc., Carol Stream, Illinois 60188. All rights reserved.
When quotations from the NLT text are used in non-salable media, such as church bulletins, orders of service, newsletters, transparencies, or similar media, a complete copyright notice is not required, but the initials (NLT) must appear at the end of each quotation.
Publication of any commentary or other Bible reference work produced for commercial sale that uses the New Living Translation requires written permission for use of the NLT text.
New Living Translation, NLT, and the New Living Translation logo are registered trademarks of Tyndale House Publishers, Inc.
Tyndale House Publishers and Wycliffe Bible Translators share the vision for an understandable, accurate translation of the Bible for every person in the world. Each sale of the Holy Bible, New Living Translation, benefits Wycliffe Bible Translators. Wycliffe is working with partners around the world to accomplish Vision 2025 – an initiative to start a Bible translation program in every language group that needs it by the year 2025.'
            ,'briefCopyRight'=>'Holy Bible, New Living Translation, copyright © 1996, 2004, 2015 by Tyndale House Foundation. Used by permission of Tyndale House Publishers Inc., Carol Stream, Illinois 60188. All rights reserved.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.nlt.db.zip'
            ,'version'=>4,'languageNameEnglish'=>'English','languageCodeDBP'=>'ENG','audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/NLT/'
            ),
            array(
                'serverId'=>3,'name'=>'Contemporary English Version','abbName'=>'CEV'
            ,'fileName'=>'bible_en.cev','desc'=>"Uncompromising simplicity marked the American Bible Society's (ABS) translation of the Contemporary English Version (CEV) that was first published in 1995. The text is easily read by grade schoolers, second language readers, and those who prefer the more contemporized form. The CEV is not a paraphrase. It is an accurate and faithful translation of the original manuscripts.".
                'The CEV began as a result of studies conducted by biblical scholar Dr. Barclay M. Newman in 1984 into speech patterns used in books, magazines, newspapers, and television. These studies focused on how English was read and heard, especially by children. This led to a series of test volumes being published in the late 1980s and early 1990s.
The CEV New Testament was released in 1991, the 175th anniversary of ABS. The CEV Old Testament was released in 1995 and the Apocryphal/Deuterocanonical Books were published in 1999.
The translators of the CEV followed three principles; that the CEV:
must be understood by people without stumbling in speech
must be understood by those with little or no comprehension of "Bible" language
must be understood by all.
"The drafting, reviewing, editing, revising, and refining the text of the Contemporary English Version has been a worldwide process extending over a period of slightly more than ten years. It has involved a wide variety of persons beyond the core team of ABS translators and the consultant experts who have worked closely with the team. The creative process has also involved scholar consultants and reviewers representing a wide range of church traditions and with expertise in such areas as Old Testament, New Testament, Hebrew language, Greek language, English language, linguistics, and poetry. In all, this process involved more than a hundred people in the various stages of the text creation and review process. And it is this process, carried out in constant prayer for the guidance of the Spirit of God, that guarantees the accuracy, integrity and trustworthiness of the CEV Bible" (from Creating and Crafting the Contemporary English Version: A New Approach to Bible Translation—New York: American Bible Society, 1996).'
            ,'languageCode'=>'en','language'=>'English','languageCodeDBP'=>'ENG','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'Contemporary English Version® Copyright © 1995 American Bible Society. All rights reserved.Bible text from the Contemporary English Version (CEV) is not to be reproduced in copies or otherwise by any means except as permitted in writing by American Bible Society, 1865 Broadway, New York, NY 10023 (www.americanbible.org).'
            ,'briefCopyRight'=>'Copyright © 1995 by American Bible Society','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.cev.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>4,'name'=>'King James Version','abbName'=>'KJV'
            ,'fileName'=>'bible_en.kjv','desc'=>'In 1604, King James I of England authorized that a new translation of the Bible into English be started. It was finished in 1611, just 85 years after the first translation of the New Testament into English appeared (Tyndale, 1526). The Authorized Version, or King James Version, quickly became the standard for English-speaking Protestants. Its flowing language and prose rhythm has had a profound influence on the literature of the past 400 years.'
            ,'languageCode'=>'en','language'=>'English','languageCodeDBP'=>'ENG','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'The KJV is public domain in the United States.'
            ,'briefCopyRight'=>'The KJV is public domain in the United States.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.kjv.db.zip','version'=>1.2,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/KJV/'
            ),
            array(
                'serverId'=>5,'name'=>'American Standard Version (1901)','abbName'=>'ASV'
            ,'fileName'=>'bible_en.asv'
            ,'desc'=>'The American Standard Version (ASV) of the Holy Bible is in the Public Domain. Please feel free to copy it, give it away, memorize it, publish it, sell it, or whatever God leads you to do with it.'.
                'The American Standard Version of 1901 is an Americanization of the English Revised Bible, which is an update of the KJV to less archaic spelling and greater accuracy of translation. It has been called \'The Rock of Biblical Honesty.\' It is the product of the work of over 50 Evangelical Christian scholars.'.
                'While the ASV retains many archaic word forms, it is still more understandable to the modern reader than the KJV in many passages. The ASV also forms the basis for several modern English translations, including the World English Bible (http://www.eBible.org/bible/WEB), which is also in the Public Domain. The ASV uses \'Jehovah\' for God\'s proper name. While the current consensus is that this Holy Name was more likely pronounced \'Yahweh,\' it is refreshing to see this rendition instead of the overloading of the word \'Lord\' that the KJV, NASB, and many others do.'.
                'Pronouns referring to God are not capitalized in the ASV, as they are not in the NIV and some others, breaking the tradition of the KJV. Since Hebrew has no such thing as tense, and the oldest Greek manuscripts are all upper case, anyway, this tradition was based only on English usage around 1600, anyway. Not capitalizing these pronouns solves some translational problems, such as the coronation psalms, which refer equally well to an earthly king and to God.'.
                'American Standard Version (1901)'
            ,'languageCode'=>'en','language'=>'English','languageCodeDBP'=>'ENG','languageNameEnglish'=>'English','type'=>'Bible','copyRight'=>'American Standard Version (1901)'
            ,'briefCopyRight'=>'American Standard Version (1901)','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_en.asv.db.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>6,'name'=>'圣经当代译本修订版','abbName'=>'当代译本'
            ,'fileName'=>'bible_zh.ccbs','desc'=>'The Chinese Contemporary Bible (圣经当代译本修订版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (圣经当代译本修订版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (圣经当代译本修订版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (圣经当代译本修订版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbs.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>7,'name'=>'聖經當代譯本修訂版','abbName'=>'當代譯本'
            ,'fileName'=>'bible_zh.ccbt','desc'=>'The Chinese Contemporary Bible (聖經當代譯本修訂版), text may be quoted in any form (written, visual, electronic or audio), up to and inclusive of five hundred (500) verses without the express written permission of the publisher, providing the verses quoted do not amount to a complete book of the Bible nor do the verses quoted account for twenty-five percent (25%) or more of the total text of the work in which they are quoted. Permission requests that exceed the above guidelines must be directed to and approved in writing by Biblica, Inc.®, 1820 Jet Stream Drive, Colorado Springs, CO 80921, USA. Biblica.com

Notice of copyright must appear on the title or copyright page as follows:

Scripture quotations taken from:
Chinese Contemporary Bible (聖經當代譯本修訂版)
Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc.®

When quotations from the Chinese Contemporary Bible (聖經當代譯本修訂版) text are used by a local church in non-saleable media such as church bulletins, orders of service, posters, overhead transparencies, or similar materials, a complete copyright notice is not required, but the title or initials (CCB) must appear at the end of each quotation.

Biblica provides God’s Word to people through Bible translation & Bible publishing, and Bible engagement in Africa, Asia Pacific, Europe, Latin America, Middle East, North America, and South Asia. Through its worldwide reach, Biblica engages people with God’s Word so that their lives are transformed through a relationship with Jesus Christ.'
            ,'languageCode'=>'zh-tw','language'=>'繁體中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Traditional','type'=>'Bible','copyRight'=>'Chinese Contemporary Bible (聖經當代譯本修訂版) Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc® Used by permission. All rights reserved worldwide.'
            ,'briefCopyRight'=>'Copyright © 1979, 2005, 2007, 2011 by Biblica, Inc®','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.ccbt.db.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/CCB/'
            ),
            array(
                'serverId'=>8,'name'=>'中文和合本拼音版','abbName'=>'拼音'
            ,'fileName'=>'bible_zh.py','desc'=>'《圣经和合本》（简称和合本；今指国语和合本，旧称官话和合本），是今日华语人士最普遍使用的《圣经》译本。此译本的出版起源自1890年在上海举行的传教士大会，会中各差会派代表成立了三个委员会，各自负责翻译官话（白话文）、浅文理及深文理（文言文）译本。于1904年，《浅文理和合译本》出版《新约》。《深文理和合译本》于1906年出版《新约》。1907年大会计划只译一部文理译本，于1919年出版《文理和合译本》。1906年，官话的翻译工作完成了《新约》；1919年，《旧约》的翻译工作完成。在1919年正式出版时，《圣经》译本名为《官话和合译本》，从此就成了现今大多数华语教会采用的和合本《圣经》。 ——摘自维基百科'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Bible','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_zh.py.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net//mp3/CUVS/'
            ),
            array(
                'serverId'=>9,'name'=>'丁道尔圣经注释','abbName'=>'丁道尔注释'
            ,'fileName'=>'commentary_zh.tcs','desc'=>'丁道尔系列是合乎时代的解经丛书，是广博崇高的神学智慧，不但具备循序渐进，由浅而深的条件，且整体构架着力于坚实的神学学术根基上，是一套内容最精简、数据最新的解经丛书。帮助读者确实了解圣经的真理，灵活应用圣经的原则。
丁道尔系列自出版以来，深获好评，被喻为「无价之宝」，值得华人教会在圣经导读上推广使用，建立良好的读经习惯。内容主要分成两大部：
1.导论：简洁、详尽地介绍作者、写作日期及时代背景，不但能帮助读者一窥该卷书的主旨及全貌，也能提供有心钻研的学者宝贵的数据。
2.注释：按主题分段，逐节详解；较难懂的经节也会特别加注说明，让圣经的信息浅白易懂。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.tcs.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>10,'name'=>'马唐纳圣经注释','abbName'=>'马唐纳注释'
            ,'fileName'=>'commentary_zh.wmd','desc'=>'这本新旧约的逐节注释书，为马唐纳四十多年来圣经研究、传道和写作的硕果。'
            ,'languageCode'=>'zh-cn','language'=>'简体中文','languageCodeDBP'=>'CH1','languageNameEnglish'=>'Chinese, Simplified','type'=>'Commentary','copyRight'=>'公共领域'
            ,'briefCopyRight'=>'公共领域','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/commentary_zh.wmd.zip','version'=>1.0,'audioAddress'=>''
            ),
            array(
                'serverId'=>11,'name'=>'पवित्र बाइबिल','abbName'=>'HHBD'
            ,'fileName'=>'bible_hi.hhbd','desc'=>''
            ,'languageCode'=>'hi','languageCodeDBP'=>'HND','language'=>'हिंदी','languageNameEnglish'=>'Hindi','type'=>'Bible','copyRight'=>'Public Domain'
            ,'briefCopyRight'=>'Public Domain','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_hi.hhbd.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/HHBD/'
            ),
            array(
                'serverId'=>12,'name'=>'Reina-Valera Antigua','abbName'=>'RVA'
            ,'fileName'=>'bible_sp.rva','desc'=>'The Reina-Valera Antigua was first translated and published in 1569 by Casiodoro de Reina, after twelve years of intensive work, and later put out in 1602 in revised form by Cipriano de Valera, who gave more than twenty years of his life to its revision and improvement.'
            ,'languageCode'=>'es','languageCodeDBP'=>'SPN','language'=>'Español','languageNameEnglish'=>'Spanish','type'=>'Bible','copyRight'=>'No copyright information available.'
            ,'briefCopyRight'=>'No copyright information available.','url'=>'http://bible12e12ascq1.biblemooc.net/BibleDatabase/bible_sp.rva.zip','version'=>1.0,'audioAddress'=>'http://bible12e12ascq1.biblemooc.net/mp3/RVA/'
            ),
        );
//        dump($list);
        $this->ajax($list,'',1);
    }
}