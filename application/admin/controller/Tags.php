<?php
namespace app\admin\controller;

use app\lib\business\AlbumInfo;
use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use app\lib\business\TagsInfo;
use app\lib\business\LanguageInfo;

class Tags extends BaseController
{
    public function mlist()
    {
        $this->assign("action","nav_tag_List");
        return $this->fetch(APP_PATH.request()->module().'/view/tagsList.html');
    }

    public function queryLanguage(){
        $name = $this->param('name');
        $family_code = $this->param('family_id');
        $this->simpleAjax(LanguageInfo::queryByName($name,$family_code));
    }

    public function queryTags(){
        $language_family_id = $this->param('language_family_id');
        $language_id = $this->param('language_id');
        $this->simpleAjax(TagsInfo::queryTags($language_family_id,$language_id));
    }

    public function videoQuery(){
        $page = $this->param('page');
        $rows = $this->param("rows");
        $this->simpleAjax(TagsInfo::query($page,$rows));
    }

    public function view(){
        $id = $this->param('id');
        $tags = array();

        if($this->isNull($id)==false)
        {
            //表示编辑
            $tags = TagsInfo::getInfoById($id);
            $this->assign('edit_tips',Lang::get("video_edit"));
        }
        else
        {
            //表示新建
            $tags['id']=0;
            $tags['name']='';
            $tags['album_name']='';
            $tags['order_by']=0;
        }

        $this->assign("action","nav_tag_List");
        $this->assign("tags",$tags);
        $this->assign("parentList",TagsInfo::parentAll());
        $this->assign('edit_tips',"新增或编辑");

        return $this->fetch(APP_PATH.request()->module().'/view/tagsEdit.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            //验证
            $validate_result = $this->validate($data, 'Tags');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }
            else
            {
                $id = TagsInfo::updateOrCreated($data);
                $this->ajax($id,'',1);
            }
        }
    }

    //删除信息
    public function delete(){
        $id = $this->param("id");
        TagsInfo::delete($id);
        $this->ajax('','',1);
    }
}
