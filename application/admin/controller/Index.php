<?php
namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use app\lib\business\UserInfo;

class Index extends BaseController
{
    public function index()
    {

        return $this->fetch(APP_PATH.request()->module().'/view/login.html');
    }

    public function Login()
    {
        // curl -d username=13730804091 -d password=1 "http://localhost:1062/manage/index/Login"
        // http://localhost:1062/manage/index/Login?username=1&password=1
        if($this->request->isPost()){
            $data            = $this->request->only(['username', 'password']);
            $validate_result = $this->validate($data, 'Login');

            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $where['username'] = $data['username'];
                $where['password'] = md5($data['password'] . Config::get('salt'));

                if(UserInfo::login($data['username'],$data['password'])){
                    $this->ajax('','', 1);
                }
                else
                {
                    $this->ajax('',Lang::get('login_error'), 0);
                }
            }
        }
        else
        {
            return $this->fetch(APP_PATH.request()->module().'/view/login.html');
        }
    }

    public function Logout(){
        UserInfo::logout();
        $this->redirect('/admin/index/Login');
    }

    public function findnot(){
        return $this->fetch(APP_PATH.request()->module().'/view/404.html');
    }
}
