<?php
namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use app\lib\business\SMSInfo;

class SMS extends BaseController
{
    public function mlist()
    {
        $this->assign("action","nav_sms_list");
        return $this->fetch(APP_PATH.request()->module().'/view/smsList.html');
    }

    public function videoQuery(){
        $number = $this->param('number');
        $page = $this->param('page');
        $rows = $this->param("rows");
        $this->simpleAjax(SMSInfo::query($number,$page,$rows));
    }

    public function view(){
        $id = $this->param('id');
        // $bibleinfo = new BibleModel();
        $tags = array();

        if($this->isNull($id)==false)
        {
            //表示编辑
            $tags = SMSInfo::getInfoById($id);
        }
        else
        {
            //表示新建
            $tags['id']=0;
            $tags['number']="";
            $tags['content']="";
        }

        $this->assign("action","nav_sms_view");
        $this->assign("tags",$tags);
        $this->assign('edit_tips',"新增或编辑");

        return $this->fetch(APP_PATH.request()->module().'/view/smsEdit.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            //验证
            $validate_result = $this->validate($data, 'SMS');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }
            else
            {
                if(count(explode(';',$data['content']))!=4){
                    $this->ajax("content-数据格式不对",'',1);
                    return;
                }
                //判断专辑信息
                $id = SMSInfo::updateOrCreated($data);
                $this->ajax($id,'',1);
            }
        }
    }

    //删除信息
    public function delete(){
        $id = $this->param("id");
        TagsInfo::delete($id);
        $this->ajax('','',1);
    }
}
