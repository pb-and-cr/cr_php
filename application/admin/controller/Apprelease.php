<?php

namespace app\admin\controller;

use think\Config;
use app\lib\common\Picture;
use app\lib\business\AppReleaseInfo;

class Apprelease extends BaseController {

    //上传电子书
    public function mlist()
    {
        $this->assign("action","nav_apprelease_list");
        return $this->fetch(APP_PATH.request()->module().'/view/appReleaseList.html');
    }

    public function view()
    {
        $id = $this->param('id');
        // $bibleinfo = new BibleModel();
        $app = array();

        if($this->isNull($id)==false)
        {
            //表示编辑
            $app = AppReleaseInfo::getInfoById($id);
            $this->assign('edit_tips',"查看");
        }
        else
        {
            //表示新建
            $app['id']=0;
            $app['name']='';
            $app['channel']='';
            $app['package_name']='';
            $app['version_id']='';
            $app['title']='';
            $app['content']='';
            $app['platform']='';
            $app['des']='';
            $app['url']='';

            $this->assign('edit_tips',"编辑");
        }

        $this->assign("app",$app);
        $this->assign("action","nav_apprelease_view");
        return $this->fetch(APP_PATH.request()->module().'/view/appReleaseEdit.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            //验证

            $validate_result = $this->validate($data, 'AppRelease');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }
            else
            {
                $id = AppReleaseInfo::updateOrCreated($data);
                $this->ajax($id,'',1);
            }
        }
    }


    public function videoQuery(){

        $result = [];
        $list = AppReleaseInfo::query();
        $result['total'] = count($list);
        $result['page'] = 1;
        $result['records'] = 1;
        $result['rows'] = $list;

        $this->simpleAjax($result);
    }

}