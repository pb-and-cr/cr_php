<?php

namespace app\admin\controller;

use app\api\controller\ChristianReaderUser;
use app\lib\model\ChristianReaderUserModel;
use think\Config;
use app\lib\common\Picture;
use think\Db;


class Uploads extends BaseController {


    //上传图片
    public function image()
    {
        // if ($_FILES)
        // {
        // 	$uploads_dir = $_SERVER['DOCUMENT_ROOT'].'/uploads/';
        // 	$uploadname = $_FILES["file"]["name"];
        // 	if (move_uploaded_file($_FILES['file']['tmp_name'], $uploads_dir. $uploadname))
        // 		$response["success"] = 1;
        // 	else
        // 		$response["success"] = 0;

        // 	$json = new Services_JSON();
        // 	echo $json->encode(array('error' => 0, 'url' => $response));
        // 	exit;
        // 	echo json_encode($response);
        // 	file_put_contents("log.txt",serialize($_FILES));
        // }

//        $php_path = Config::get('IMG_PATH')."Uploads";
        $php_path =  $_SERVER['DOCUMENT_ROOT'].'/Uploads';
        $ask = $this->param('ask');
        $dir = date("Ymd");

        //最大文件大小 2M以内
        $max_size = 1024*1024*2;
        $error = '';
        //PHP上传失败
        if (!empty($_FILES['imgFile']['error'])) {
            switch($_FILES['imgFile']['error']){
                case '1':
                    $error = '超过php.ini允许的大小。';
                    break;
                case '2':
                    $error = '超过表单允许的大小。';
                    break;
                case '3':
                    $error = '图片只有部分被上传。';
                    break;
                case '4':
                    $error = '请选择图片。';
                    break;
                case '6':
                    $error = '找不到临时目录。';
                    break;
                case '7':
                    $error = '写文件到硬盘出错。';
                    break;
                case '8':
                    $error = 'File upload stopped by extension。';
                    break;
                case '999':
                default:
                    $error = '未知错误。';
            }
        }

        //有上传文件时
        if (empty($_FILES) === false) {
            //原文件名
            $file_name = $_FILES['imgFile']['name'];
            //服务器上临时文件名
            $tmp_name = $_FILES['imgFile']['tmp_name'];
            //文件大小
            $file_size = $_FILES['imgFile']['size'];
            //检查文件名

            //检查是否已上传
            if (@is_uploaded_file($tmp_name) === false) {
                alert("上传失败。");
            }
            //检查文件大小
            if ($file_size > $max_size) {
                alert("上传文件大小超过限制。");
            }

            //获得文件扩展名
            $temp_arr = explode(".", $file_name);
            $file_ext = array_pop($temp_arr);
            $file_ext = trim($file_ext);
            $file_ext = strtolower($file_ext);

            //检查扩展名
            $isImg = false;
            $isTxt = false;
            if ($file_ext == 'gif' ||$file_ext == 'jpg' ||$file_ext == 'jpeg' ||$file_ext == 'png' ||$file_ext == 'bmp') {
                $save_path = $php_path.'/images/backstage/';
                $save_url =  Config::get('IMG_URL').'/images/backstage/';
                $isImg = true;
            }else if ($file_ext == 'swf' ||$file_ext == 'flv' ||$file_ext == 'wav' ||$file_ext == 'wma' ||$file_ext == 'wmv' ||$file_ext == 'mid' ||$file_ext == 'avi' ||$file_ext == 'mpg' ||$file_ext == 'asf' ||$file_ext == 'rm' ||$file_ext == 'rmvb'){
                $save_path = $php_path.'/video/';
                $save_url =  Config::get('IMG_URL').'/video/';
            }else if ($file_ext=='mp3'){
                $save_path = $php_path.'/music/';
                $save_url =  Config::get('IMG_URL').'/music/';
            }else if ($file_ext == 'doc' ||$file_ext == 'docx' ||$file_ext == 'xls' ||$file_ext == 'xlsx'
                ||$file_ext == 'ppt' ||$file_ext == 'htm' ||$file_ext == 'html' ||$file_ext == 'txt'
                ||$file_ext == 'zip' ||$file_ext == 'rar' ||$file_ext == 'gz' ||$file_ext == 'bz2' ||$file_ext == 'epub' ){
                $save_path = $php_path.'/file/';
                $save_url =  Config::get('IMG_URL').'/file/';
                $isTxt = True;
            }else
            {
                alert("上传文件扩展名是不允许的扩展名。\n只允许");
            }
            //文件夹 和 文件名称

            if($this->param('ask')=="book" && $file_ext != 'epub')
            {
                alert("必须上传epub格式的文件！");
            }

            $save_path = $save_path.$dir.'/';
//            exit($this->ajax($save_path));
            if (!file_exists($save_path)) {
                mkdir($save_path);
            }
            if (!$file_name) {
                alert("请选择文件。");
            }
            //检查目录

            if (@is_dir($save_path) === false) {
                alert("上传目录不存在。".$save_path);
            }
            //检查目录写权限
            if (@is_writable($save_path) === false) {
                alert("上传目录没有写权限。".$save_path);
            }

            $save_url = $save_url.$dir.'/';

            $new_file_name = date("YmdHis") .'_'. rand(10000, 99999);
            $temp_new_file_name = $new_file_name;
            $new_file_name = $new_file_name. '.' . $file_ext;

            //移动文件
            $file_path = $save_path . $new_file_name;
            if (move_uploaded_file($tmp_name, $file_path) === false) {
                alert("上传文件失败。");
            }

            $filesize=abs(filesize($file_path));
//                        if($filesize>=102400){
//                            alert("图片大于100K 不允许上传");
//                            return;
//                        }


            if ($isImg==true){

                if($ask=="article"){
                    $fileName =  $save_path.$temp_new_file_name;
                    Picture::resizeImage($file_path,640,480,$fileName,'.'.$file_ext,$this->param('set'));
                    $fileName =  $save_path.$temp_new_file_name.'_min_';
                    Picture::resizeImage($file_path,320,240,$fileName,'.'.$file_ext,$this->param('set'));
                }
                if($ask=="book_img"){
                    $fileName =  $save_path.$temp_new_file_name;
                    Picture::resizeImage($file_path,640,480,$fileName,'.'.$file_ext,$this->param('set'));
                    $fileName =  $save_path.$temp_new_file_name.'_min_';
                    Picture::resizeImage($file_path,150,400,$fileName,'.'.$file_ext,$this->param('set'));
                }
            }

            @chmod($file_path, 0644);
            $file_url = $save_url . $new_file_name;

            if($ask=="headportra")
            {
                $user_id=$this->param('user_id');

                $result  =  array();
                $result['status']  =  1;
                $result['info'] =  '';
                $result['data'] = array(
                    'id' => $user_id,
                    'head_portra' => Config::get('ApiPhone').$file_url, // 采用新规则输出__PUBLIC__字符串
                );

                //修改数据库
                $user = new RegisterUserModel();
                $data['id'] = $user_id;
                $data['head_portra'] = $file_url;
                $user->save($data);

                //扩展ajax返回数据, 在Action中定义function ajaxAssign(&$result){} 方法 扩展ajax返回数据。
                exit($this->ajax($result));
            }
            else if($ask == "kindEdit"){
                echo $this->simpleAjax(array('error' => 0, 'url' => $file_url));
            }
            else
            {
                echo $this->ajax(array('error' => 0, 'url' => $file_url));
            }
            exit;

        }
        else
        {
            alert('上传失败！');
        }
    }

    //上传头像
    public function headportrait()
    {
        $php_path = Config::get('IMG_PATH')."Uploads";
        $user_token=$this->param('user_token');
        $ask = $this->param('ask');

        if($this->isNull($ask) ==true){$ask = $this->param('ask'); }
        if($this->isNull($user_token)==true){$user_token = $this->param('user_token'); }

        $dir = date("Ymd");

        header('Content-type: text/html; charset=UTF-8');

        $result  =  array();
        $result['status']  =  1;
        $result['info'] =  $this->getErrorCodeInfo('001');
        $result['data'] = array(
            'user_token' => $user_token,
            $ask => "", // 采用新规则输出__PUBLIC__字符串
        );

        //最大文件大小 2M以内
        $max_size = 1024*1024*2;
        $error = '';
        //PHP上传失败
        if (!empty($_FILES['imgFile']['error'])) {
            switch($_FILES['imgFile']['error']){
                case '1':
                    $error = '超过php.ini允许的大小。';
                    break;
                case '2':
                    $error = '超过表单允许的大小。';
                    break;
                case '3':
                    $error = '图片只有部分被上传。';
                    break;
                case '4':
                    $error = '请选择图片。';
                    break;
                case '6':
                    $error = '找不到临时目录。';
                    break;
                case '7':
                    $error = '写文件到硬盘出错。';
                    break;
                case '8':
                    $error = 'File upload stopped by extension。';
                    break;
                case '999':
                default:
                    $error = '未知错误。';
            }
        }

        if($error!=""){
            $result['status']  =  0;
            $result['info'] = $error;
            exit(json_encode($result));
        }

        //有上传文件时
        if (empty($_FILES) === false) {
            //原文件名
            $file_name = $_FILES['imgFile']['name'];
            //服务器上临时文件名
            $tmp_name = $_FILES['imgFile']['tmp_name'];
            //文件大小
            $file_size = $_FILES['imgFile']['size'];
            //检查文件名

            //检查是否已上传
            if (@is_uploaded_file($tmp_name) === false) {
                $result['status']  =  0;
                $result['info'] = "上传失败.";
                exit(json_encode($result));
            }
            //检查文件大小
            if ($file_size > $max_size) {
                $result['status']  =  0;
                $result['info'] = "上传文件大小超过限制。.";
                exit(json_encode($result));

            }

            //获得文件扩展名
            $temp_arr = explode(".", $file_name);
            $file_ext = array_pop($temp_arr);
            $file_ext = trim($file_ext);
            $file_ext = strtolower($file_ext);

            //检查扩展名
            $isImg = false;
            $isTxt = false;
            if ($file_ext == 'gif' ||$file_ext == 'jpg' ||$file_ext == 'jpeg' ||$file_ext == 'png' ||$file_ext == 'bmp') {
                $save_path = $php_path.'/images/backstage/';
                $save_url =  Config::get('IMG_URL').'/images/backstage/';
                $isImg = true;
            }else if ($file_ext == 'swf' ||$file_ext == 'flv' ||$file_ext == 'wav' ||$file_ext == 'wma' ||$file_ext == 'wmv' ||$file_ext == 'mid' ||$file_ext == 'avi' ||$file_ext == 'mpg' ||$file_ext == 'asf' ||$file_ext == 'rm' ||$file_ext == 'rmvb'){
                $save_path = $php_path.'/video/';
                $save_url =  Config::get('IMG_URL').'/video/';
            }else if ($file_ext=='mp3'){
                $save_path = $php_path.'/music/';
                $save_url =  Config::get('IMG_URL').'/music/';
            }else if ($file_ext == 'doc' ||$file_ext == 'docx' ||$file_ext == 'xls' ||$file_ext == 'xlsx'
                ||$file_ext == 'ppt' ||$file_ext == 'htm' ||$file_ext == 'html' ||$file_ext == 'txt'
                ||$file_ext == 'zip' ||$file_ext == 'rar' ||$file_ext == 'gz' ||$file_ext == 'bz2' ||$file_ext == 'epub' ){
                $save_path = $php_path.'/file/';
                $save_url =  Config::get('IMG_URL').'/file/';
                $isTxt = True;
            }else
            {
                $result['status']  =  0;
                $result['info'] = "上传文件必须为图片.";
                exit(json_encode($result));

            }

            $save_path = $save_path.$dir.'/';
            if (!file_exists($save_path)) {
                mkdir($save_path);
            }
            if (!$file_name) {
                $result['status']  =  0;
                $result['info'] = "上传图片为空.";
                exit(json_encode($result));
            }
            //检查目录

            if (@is_dir($save_path) === false) {
                $result['status']  =  0;
                $result['info'] = "上传目录不存在。.";
                exit(json_encode($result));
            }
            //检查目录写权限
            if (@is_writable($save_path) === false) {
                $result['status']  =  0;
                $result['info'] = "上传目录没有写权限。.";
                exit(json_encode($result));
            }

            $save_url = $save_url.$dir.'/';

            $new_file_name = date("YmdHis") .'_'. rand(10000, 99999);
            $temp_new_file_name = $new_file_name;
            $new_file_name = $new_file_name. '.' . $file_ext;

            //移动文件
            $file_path = $save_path . $new_file_name;
            if (move_uploaded_file($tmp_name, $file_path) === false) {
                $result['status']  =  0;
                $result['info'] = $this->getErrorCodeInfo('000');
                exit(json_encode($result));
            }


            @chmod($file_path, 0644);
            $file_url = $save_url . $new_file_name;

//                        alert($file_path." - ".$fileName." - ".$file_ext);
            $fileName =  $save_path.$temp_new_file_name;
            Picture::resizeImage($file_path,720,720,$fileName,'.'.$file_ext,"");

            if($ask=="cover")
            {

                Db::execute("UPDATE user_info set cover = ? where user_token=?",[$file_url,$user_token]);

                $result['data']['cover'] = Config::get('IMG_DOMAIN').$file_url;
                //扩展ajax返回数据, 在Action中定义function ajaxAssign(&$result){} 方法 扩展ajax返回数据。
                header("Content-Type:text/html; charset=utf-8");
                exit(json_encode($result));
            }
            else if ($ask=="avatar") {

                Db::execute("UPDATE user_info set avatar = ? where user_token=?",[$file_url,$user_token]);

                $result['data']['avatar'] = Config::get('IMG_DOMAIN').$file_url;
                //扩展ajax返回数据, 在Action中定义function ajaxAssign(&$result){} 方法 扩展ajax返回数据。
                header("Content-Type:text/html; charset=utf-8");
                exit(json_encode($result));
            }
            else
            {
                $result['status']  =  0;
                $result['info'] = $this->getErrorCodeInfo('000');
                exit(json_encode($result));
            }
            exit;

        }
        else
        {
            $result['status']  =  0;
            $result['info'] = $this->getErrorCodeInfo('000');
            exit(json_encode($result));
        }
    }

    //上传电子书
    public function book()
    {
        $php_path = Config::get('IMG_PATH')."Uploads";
        $user_token=$this->param('user_token');
        $ask = $this->param('ask');

        if($this->isNull($ask) ==true){$ask = $this->param('ask'); }
        if($this->isNull($user_token)==true){$user_token = $this->param('user_token'); }

        $dir = date("Ymd");

        header('Content-type: text/html; charset=UTF-8');

        $result  =  array();
        $result['status']  =  1;
        $result['info'] =  $this->getErrorCodeInfo('001');
        $result['data'] = array(
            'user_token' => $user_token,
            $ask => "", // 采用新规则输出__PUBLIC__字符串
        );

        //最大文件大小 2M以内
        $max_size = 1024*1024*5;
        $error = '';
        //PHP上传失败
        if (!empty($_FILES['imgFile']['error'])) {
            switch($_FILES['imgFile']['error']){
                case '1':
                    $error = '超过php.ini允许的大小。';
                    break;
                case '2':
                    $error = '超过表单允许的大小。';
                    break;
                case '3':
                    $error = '图片只有部分被上传。';
                    break;
                case '4':
                    $error = '请选择图片。';
                    break;
                case '6':
                    $error = '找不到临时目录。';
                    break;
                case '7':
                    $error = '写文件到硬盘出错。';
                    break;
                case '8':
                    $error = 'File upload stopped by extension。';
                    break;
                case '999':
                default:
                    $error = '未知错误。';
            }
        }

        if($error!=""){
            $result['status']  =  0;
            $result['info'] = $error;
            exit(json_encode($result));
        }

        //有上传文件时
        if (empty($_FILES) === false) {
            //原文件名
            $file_name = $_FILES['imgFile']['name'];
            //服务器上临时文件名
            $tmp_name = $_FILES['imgFile']['tmp_name'];
            //文件大小
            $file_size = $_FILES['imgFile']['size'];
            //检查文件名

            //检查是否已上传
            if (@is_uploaded_file($tmp_name) === false) {
                $result['status']  =  0;
                $result['info'] = "上传失败.";
                exit(json_encode($result));
            }
            //检查文件大小
            if ($file_size > $max_size) {
                $result['status']  =  0;
                $result['info'] = "上传文件大小超过限制。.";
                exit(json_encode($result));

            }

            //获得文件扩展名
            $temp_arr = explode(".", $file_name);
            $file_ext = array_pop($temp_arr);
            $file_ext = trim($file_ext);
            $file_ext = strtolower($file_ext);

            //检查扩展名
            $isImg = false;
            $isTxt = false;
            if ($file_ext == 'doc' ||$file_ext == 'docx' ||$file_ext == 'xls' ||$file_ext == 'xlsx'
                ||$file_ext == 'ppt' ||$file_ext == 'htm' ||$file_ext == 'html' ||$file_ext == 'txt'
                ||$file_ext == 'zip' ||$file_ext == 'rar' ||$file_ext == 'gz' ||$file_ext == 'bz2' ||$file_ext == 'epub' ){
                $save_path = $php_path.'/file/';
                $save_url =  Config::get('IMG_URL').'/file/';
                $isTxt = True;
            }else
            {
                $result['status']  =  0;
                $result['info'] = "上传文件必须为图片.";
                exit(json_encode($result));

            }

            $save_path = $save_path.$dir.'/';

            if (!file_exists($save_path)) {
                mkdir($save_path);
            }
            if (!$file_name) {
                $result['status']  =  0;
                $result['info'] = "上传图片为空.";
                exit(json_encode($result));
            }
            //检查目录

            if (@is_dir($save_path) === false) {
                $result['status']  =  0;
                $result['info'] = "上传目录不存在。.";
                exit(json_encode($result));
            }
            //检查目录写权限
            if (@is_writable($save_path) === false) {
                $result['status']  =  0;
                $result['info'] = "上传目录没有写权限。.";
                exit(json_encode($result));
            }


            $save_url = $save_url.$dir.'/';

            $new_file_name = date("YmdHis") .'_'. rand(10000, 99999);
            $temp_new_file_name = $new_file_name;
            $new_file_name = $new_file_name. '.' . $file_ext;

            //移动文件
            $file_path = $save_path . $new_file_name;
            if (move_uploaded_file($tmp_name, $file_path) === false) {
                alert("上传文件失败。");
            }

            $fileName =  $save_path.$temp_new_file_name;

            @chmod($file_path, 0644);
            $file_url = $save_url . $new_file_name;
            echo $this->ajax(array('error' => 0, 'url' => $file_url));
            exit;
        }
        else
        {
            $result['status']  =  0;
            $result['info'] = $this->getErrorCodeInfo('000');
            exit(json_encode($result));
        }
    }


    public function getErrorCodeInfo($code)
    {
        $language = $this->param('language');
        if($this->isNull($language) ==true){$language = $this->param('language'); }

        $errorInfo = array(
            '000'=>array('zh'=>'上传文件失败,请稍后再试！','en'=>'Failed to upload file. Please try again later.'),
            '001'=>array('zh'=>'上传成功.','en'=>'Upload success.'),
        );

        if(!(stripos($language,'zh')===false) ){
            $language = 'zh';
        }
        else
        {
            $language = 'en';
        }

        $errorInfo = $errorInfo[$code];
        if(count($errorInfo)>0){
            if($language=='zh'){
                return $errorInfo['zh'];
            }
            else
            {
                return $errorInfo['en'];
            }
        }

        return '';
    }
}