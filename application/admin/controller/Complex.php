<?php
namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use app\lib\business\ComplexInfo;
use app\lib\business\AlbumInfo;

class Complex extends BaseController
{
    public function mlist()
    {
        $this->assign("action","nav_complex_List");
        return $this->fetch(APP_PATH.request()->module().'/view/complexList.html');
    }

    public function videoQuery(){
        $page = $this->param('page');
        $rows = $this->param("rows");
        $this->simpleAjax(ComplexInfo::query($page,$rows));
    }

    public function view(){

        $id = $this->param('id');
        // $bibleinfo = new BibleModel();
        $column = array();

        if($this->isNull($id)==false)
        {
            //表示编辑
            $column = ComplexInfo::getInfoById($id);
            $artilelist = '';
            foreach ($column['list'] as $item){
                if($artilelist==""){
                    $artilelist = "{id:".$item['article_id'].",title:'".$item['title']."'}";
                }
                else
                {
                    $artilelist = $artilelist.",{id:".$item['article_id'].",title:'".$item['title']."'}";
                }
            }
            $this->assign('artilelist',"[".$artilelist."]");
//            dump("[".$artilelist."]");
            $this->assign('edit_tips',Lang::get("video_edit"));
        }
        else
        {
            //表示新建
            $column['id']=0;
            $column['name']='';
            $column['order_by']=0;
            $column['browse']=0;
            $column['img_url']='';
            $column['is_published']=1;
            $column['publish_dated']='';
            $column['update_dated']='';
            $column['content']='';
            $column['catetory_id']=-1;
            $this->assign('artilelist','[]');
        }

        $clist = AlbumInfo::queryFollowAlbum();
        $this->assign('columnlist',$clist);
        $this->assign("action","nav_complex_List");
        $this->assign("column",$column);
        $this->assign('edit_tips',"新增或编辑");
        $this->assign('imgDomain',Config::get('IMG_DOMAIN'));
        return $this->fetch(APP_PATH.request()->module().'/view/complexEdit.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            //验证
            $validate_result = $this->validate($data, 'Complex');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }
            else
            {
                //判断专辑信息
                $id = ComplexInfo::updateOrCreated($data);
                $this->ajax($id,'',1);
            }
        }
    }

    //删除信息
    public function delete(){
        $id = $this->param("id");
        ComplexInfo::delete($id);
        $this->ajax('','',1);
    }
}
