<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

use app\lib\business\ArticleInfo;
use app\lib\business\AlbumInfo;
use app\lib\model\ArticleInfoModel;
use app\lib\model\AlbumInfoModel;
use app\lib\business\TagsInfo;


class Article extends BaseController
{
    public function mlist()
    {   
        // if($this->request->isPost()) {
        //     $albumName = $this->request->Post();

            // return $albumName;
//            $this->assign("action","nav_video_List");
            $str = '';
            $clist = AlbumInfo::queryFollowAlbum();
            $this->assign('columnlist',$clist);
            $this->assign("action","nav_video_List");
            return $this->fetch(APP_PATH.request()->module().'/view/arrticleList.html');
    }
    public function query(){
        $title = $this->param('title');
        $isAll = $this->param('isAll');
        $index = $this->param('page');
        $rows = $this->param("rows");
        $album_id = $this->param("album_id");
        $this->simpleAjax(ArticleInfo::query($title,$index,$rows,$album_id,$isAll));
    }


    public function view(){
        $id = $this->param('id');
        // $bibleinfo = new BibleModel();
        $bibleinfo = array();

        if($this->isNull($id)==false)
        {
            //表示编辑
            $list = Db::query('SELECT a.*,b.name as column_name,c.`name` as tag_name,d.content from article_info a 
                                LEFT JOIN article_content d on d.article_id = a.id
                                LEFT JOIN  album_info b on a.album_id = b.id
                                LEFT JOIN  article_tags c on a.tag_id = c.id where a.id=? ',[$id]);
//            dump($list);
            if ($list)
            {
                $bibleinfo = $list[0];
//                $this->assign("tagslist",TagsInfo::queryTags($bibleinfo['language_family_id'],$bibleinfo['language_id']));
            }
            $bibleinfo['isGetWebSite'] = false;
            if(strlen($bibleinfo['bak_address'])>10){
                $bibleinfo['isGetWebSite'] = true;
            }

            $this->assign('edit_tips',"编辑");
        }
        else
        {
            //表示新建
            $bibleinfo['id']=0;
            $bibleinfo['isGetWebSite'] = false;
            $bibleinfo['title']='';
            $bibleinfo['is_published']=1;
            $bibleinfo['is_top']=0;
            $bibleinfo['publish_dated']='';
            $bibleinfo['book_address']='';
            $bibleinfo['audio_address']='';
            $bibleinfo['des']='';
            $bibleinfo['content']='';
            $bibleinfo['bak_address']='';
            $bibleinfo['author']='';
            $bibleinfo['album_id']='0';
            $bibleinfo['column_name']='';
            $bibleinfo['img_url']='';
            $bibleinfo['tag_id']=-1;
            $this->assign('edit_tips',"添加");
        }

        //添加专辑信息
        $str = '';
        $clist = AlbumInfo::queryFollowAlbum();
        foreach($clist as $item){
            $name = $item['name'];
            if ($this->isNull($str)==true){
                $str = $name;
            }
            else
            {
                $str = $str.",".$name;
            }
        }
        $str = str_replace("\n", "", $str);
        $str = str_replace("\r", "", $str);
        $this->assign('christian_info',$str);
        $this->assign('bibleinfo',$bibleinfo);
        $this->assign('tagslist',TagsInfo::getBookTags());
        $this->assign('imgDomain',Config::get('IMG_DOMAIN'));
        $this->assign('biblebook',$this->isChinese()?"zh-cn":"en-us");
        $this->assign("action","nav_video_edit_view");
        $this->assign("title",$bibleinfo['title']);
//        $this->assign("album_name","");
//        $this->assign("des","");
        return $this->fetch(APP_PATH.request()->module().'/view/arrticleEdit.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            //验证
            $validate_result = $this->validate($data, 'ArticleInfo');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }
            else
            {
                //判断专辑信息
                if($this->isNull($data['column_name'])){
                    $data['column_id'] = 0;
                }
                else{
                    $ablumn = new AlbumInfoModel();
                    $id = $ablumn->where('name', $data['column_name'])->value('id');

                    if($id){
                        $data['album_id'] = $id;
                    }
                    else
                    {
                        $this->ajax('album_id-'.Lang::get('video_validate_video_ablum_name'),'',0);
                    }
                }

                if($this->isNull($data['bak_address'])==false){
                    $data['tag_id'] = 0;
                }

                if($this->isNull($data['contentBak'])==false){
                    $data['content'] = $data['contentBak'];
                }

                if(empty($data['id'])){
                    $result = ArticleInfo::compareDate($data['title']);
                    if($result == 0){
                        $this->ajax('','本文章在一个月内已添加，请勿重复添加',0);
                    }else{
                        $id = ArticleInfo::updateOrCreated($data);
                        $this->ajax($id,'添加成功',1);
                    }
                }else{
                    $id = ArticleInfo::updateOrCreated($data);
                    $this->ajax($id,'更新成功',1);
                }



            }
        }
    }

    //删除信息
    public function delete(){
        $id = $this->param("id");
        $model = new ArticleInfoModel();
        $condition['id'] = $id;
        $model->where($condition)->delete();
        $this->ajax('','',1);
    }
}