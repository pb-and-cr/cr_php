<?php
namespace app\admin\controller;

use app\lib\business\ShopInfo;
use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;
use app\lib\business\AlbumInfo;
use app\lib\model\ArticleInfoModel;
use app\lib\model\AlbumInfoModel;
use app\lib\business\TagsInfo;


class Shop extends BaseController
{
    public function view(){
        $id = $this->param('id');
        // $bibleinfo = new BibleModel();
        $bibleinfo = array();
        $bookTags = ShopInfo::getBookTags();
        if($this->isNull($id)==false)
        {
            //表示编辑
            $list = Db::query('SELECT a.*,b.name as column_name,c.`name` as tag_name,c.`id` as tag_id,d.content from article_info a 
                                LEFT JOIN article_content d on d.article_id = a.id
                                LEFT JOIN  album_info b on a.album_id = b.id
                                LEFT JOIN  article_tags c on a.tag_id = c.id where a.id=? ',[$id]);
            // dump($list);
            if ($list)
            {
                $bibleinfo = $list[0];
                // $this->assign("tag_name",$list['tag_name']);
                // $this->assign("tagslist",TagsInfo::queryTags($bibleinfo['language_family_id'],$bibleinfo['language_id']));
            }
            $bibleinfo['isGetWebSite'] = false;
            if(strlen($bibleinfo['bak_address'])>10){
                $bibleinfo['isGetWebSite'] = true;
            }

            $this->assign('edit_tips',"编辑");
            
        }
        else
        {
            //表示新建
            $bibleinfo['id']=0;
            $bibleinfo['isGetWebSite'] = false;
            $bibleinfo['title']='';
            $bibleinfo['is_published']=1;
            $bibleinfo['publish_dated']='';
            $bibleinfo['book_address']='';
            $bibleinfo['audio_address']='';
            $bibleinfo['des']='';
            $bibleinfo['content']='';
            $bibleinfo['bak_address']='';
            $bibleinfo['author']='';
            $bibleinfo['album_id']='0';
            $bibleinfo['column_name']='';
            $bibleinfo['img_url']='';
            $bibleinfo['tag_id']='';
            $this->assign('edit_tips',"添加");
        }

        //添加专辑信息
        $str = '';
        $clist = AlbumInfo::queryShopBookAll();
        foreach($clist as $item){
            $name = $item['name'];
            if ($this->isNull($str)==true){
                $str = $name;
            }
            else
            {
                $str = $str.",".$name;
            }
        }
        //获取书籍分类信息
        

//        dump($bookTags);
//        return;
        $str = str_replace("\n", "", $str);
        $str = str_replace("\r", "", $str);
        $this->assign('christian_info',$str);
        $this->assign('bibleinfo',$bibleinfo);
        $this->assign('tagslist',TagsInfo::getBookTags());
        $this->assign('imgDomain',Config::get('IMG_DOMAIN'));
        $this->assign('biblebook',$this->isChinese()?"zh-cn":"en-us");
        $this->assign("action","nav_video_edit_view");
        $this->assign("title",$bibleinfo['title']);
        $this->assign("tagslist",$bookTags);
        $this->assign("action","nav_addBook");

        return $this->fetch(APP_PATH.request()->module().'/view/shopEdit.html');
    }

    public function query(){
        $title = $this->param('title');
        $album_id = $this->param('album_id');
        $index = $this->param('page');
        $rows = $this->param("rows");
//        $album_id = AlbumInfo::$ALBUM_SHOP_ID;
        $this->simpleAjax(ShopInfo::queryBooksInShop($title,$index,$rows,$album_id));
    }

    public function mlist(){
        $str = '';
        $clist = AlbumInfo::queryShopBookAll();
        $this->assign('columnlist',$clist);
        $this->assign("action","nav_bookLists");
        return $this->fetch(APP_PATH.request()->module().'/view/shopLists.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $validate_result = $this->validate($data, 'ShopInfo');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }else{
                if($this->isNull($data['column_name'])){
                    $data['column_id'] = 0;
                    $this->ajax('album_id-专辑名不能为空','',0);
                    return;
                }
                else{
                    $ablumn = new AlbumInfoModel();
                    $id = $ablumn->where('name', $data['column_name'])->value('id');

                    if($id){
                        $data['album_id'] = $id;
                    }
                    else
                    {
                        $this->ajax('album_id-专辑名不能为空','',0);
                        return;
                    }
                }

                if($this->isNull($data['contentBak'])==false){
                    $data['content'] = $data['contentBak'];
                }

                $id = ShopInfo::updateBookOrCreateShopData($data);
                if($id == 1){
                    $this->ajax($id,'添加成功',1);
                }else if($id == 2){
                    $this->ajax($id,'更新成功',1);
                }else{
                    $this->ajax('','操作失败，稍后重试',0);
                }
            }
        }
    }
}
