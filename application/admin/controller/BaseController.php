<?php
namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Request;
use app\lib\business\UserInfo;

class BaseController extends Controller
{

    public function _initialize()
    {
        $this->checkAuth();

    }


    /**
     * 权限检查
     * @return bool
     */
    protected function checkAuth() {

        $module     = $this->request->module();
        $controller = $this->request->controller();
        $action     = $this->request->action();

//        echo ' -- '.Session::has('admin_id').' -- '.$module . '/' . $controller . '/' . $action;

        // 排除权限
        $not_check = ['admin/index/index'
            ,'admin/index/findnot'
            ,'admin/index/login'
            ,'admin/index/','admin/index'
            ,'admin/uploads/image'
            ,'admin/uploads/headportrait'];

//        echo strtolower($module . '/' . $controller . '/' . $action).'<br/>';
//        dump(in_array(strtolower($module . '/' . $controller . '/' . $action), $not_check));

        if (in_array(strtolower($module . '/' . $controller . '/' . $action), $not_check)) {
//            echo ' yes ';
        }
        else
        {
//            echo ' no ';
            if (!UserInfo::isSuperAdministratorBySession()) {
                $this->redirect('admin/index/index');
            }
            else{
                $this->assign("isSuperAdmin",UserInfo::isSuperAdministratorBySession());
                $this->assign("isAppUpdate",UserInfo::isAppUpdate());
                $this->assign("isOfflineAdmin",UserInfo::isOfflineAdministratorBySession());
            }
        }
    }

    public function ajax($data,$info='',$status=0,$type='') {
        $result  =  array();
        $result['status']  =  $status;
        $result['info'] =  $info;
        $result['data'] = $data;

        //扩展ajax返回数据, 在Action中定义function ajaxAssign(&$result){} 方法 扩展ajax返回数据。
        if(method_exists($this,"ajaxAssign"))
            $this->ajaxAssign($result);
        exit($this->json_encode_ex($result));
    }

    function json_encode_ex($array) {
        header("Access-Control-Allow-Origin: *");
        if (version_compare(PHP_VERSION,'5.4.0','<')) {
            $str = json_encode($array);
            $str = preg_replace_callback (
                "#\\\u([0-9a-f]{4})#i",
                function($matchs) {
                    return iconv('UCS-2BE', 'UTF-8',  pack('H4',  $matchs[1]));
                },
                $str
            );
            return $str;
        } else {
            return json_encode($array, JSON_UNESCAPED_UNICODE);
        }
    }

    public function simpleAjax($data) {
        header("Access-Control-Allow-Origin: *");
        exit($this->json_encode_ex($data,JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS|JSON_HEX_QUOT));
    }

    public function isNull($fields){
        if (is_null($fields)) return true;
        if (!is_string($fields)) return true; //是否是字符串类型
        if (empty($fields)) return true; //是否已设定
        if ($fields=='') return true; //是否为空
        return false;
    }

    public function param($var){
        return Request::instance()->param($var);
    }

    public function isChinese(){
        if(Request::instance()->langset()=="zh-cn"){
            return True;
        }
        else{
            return false;
        }
    }
    public function isEnglish(){
        if(Request::instance()->langset()=="en-us"){
            return True;
        }
        else{
            return false;
        }
    }

    public function spliceSql($where,$condition){
        if ($where==""){
            $where = " where ".$condition;
        }else
        {
            $where = $where." and ".$condition;
        }
        return $where;
    }
}
