<?php
namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use app\lib\business\AlbumInfo;
use app\lib\business\LanguageInfo;

class Album extends BaseController
{
    public function mlist()
    {
        $this->assign("action","nav_column_List");
        return $this->fetch(APP_PATH.request()->module().'/view/albumList.html');
    }

    public function queryLanguage(){
        $name = $this->param('name');
        $this->simpleAjax(LanguageInfo::queryByName($name,$family_code));
    }

    public function videoQuery(){
        $page = $this->param('page');
        $rows = $this->param("rows");
        $this->simpleAjax(AlbumInfo::query($page,$rows));
    }

    public function view(){
        $id = $this->param('id');
        // $bibleinfo = new BibleModel();
        $column = array();

        if($this->isNull($id)==false)
        {
            //表示编辑
            $column = AlbumInfo::getInfoById($id);
            $this->assign('edit_tips',Lang::get("video_edit"));
        }
        else
        {
            //表示新建
            $column['id']=0;
            $column['name']='';
            $column['order_by']=0;
            $column['browse']=0;
            $column['img_url']='';
            $column['is_published']=1;
            $column['publish_dated']='';
            $column['author']='';
            $column['remark']='';
            $column['updated_at']='';
            $column['follow_count']=0;
            $this->assign("languageName",[]);
        }

        $this->assign("action","nav_column_List");
        $this->assign("column",$column);
        $this->assign('edit_tips',"新增或编辑");
        $this->assign('imgDomain',Config::get('IMG_DOMAIN'));
        return $this->fetch(APP_PATH.request()->module().'/view/albumEdit.html');
    }

    public function edit(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            //验证
            $validate_result = $this->validate($data, 'Column');
            if ($validate_result !== true) {
                $this->ajax($validate_result);
            }
            else
            {

                //判断专辑信息
                $id = AlbumInfo::updateOrCreated($data);
                $this->ajax($id,'',1);
            }
        }
    }

    //删除信息
    public function delete(){
        $id = $this->param("id");
        AlbumInfo::delete($id);
        $this->ajax('','',1);
    }
}
