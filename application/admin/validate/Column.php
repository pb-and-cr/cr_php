<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class Column extends BaseValidate {

    protected $rule = [
        'name' => 'requireInput',
        'order_by' => 'requireInput|between:1,100000',
        'author' => 'requireInput',
        'remark' => 'requireInput',
        'img_url' => 'requireInput',
        'publish_dated' => 'requireInput',
    ];

    protected $message  =   [
        'order_by.between'  => 'order_by-please select',
    ];

}