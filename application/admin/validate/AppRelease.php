<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class AppRelease extends BaseValidate {

    protected $rule = [
        'name' => 'requireInput',
        'channel' => 'requireInput',
        'version_id' => 'requireInput',
        'package_name' => 'requireInput',
        'title' => 'requireInput',
        'content' => 'requireInput',
        'platform' => 'requireInput',
        'url' => 'requireInput',
        'des' => 'requireInput',
    ];

}