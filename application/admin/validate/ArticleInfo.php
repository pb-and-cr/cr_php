<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class ArticleInfo extends BaseValidate {

    protected $rule = [
        'title' => 'requireInput',
        'author' => 'requireInput',
        'des' => 'requireInput',
        'is_published' => 'requireInput',
        'publish_dated' => 'requireInput',
        'img_url' => 'requireInput',
    ];

//    protected $message = [
//        'title.require' => 'title',
//        'title_en.require' => 'title_en',
//        'is_published.require' => 'is_published',
//        'publish_dated.require' => 'publish_dated',
//        'language_type.require' => 'language_type',
//        'video_address.require' => 'video_address',
//        'video_des.require' => 'video_des',
//        'video_author.require' => 'video_author',
//        'video_type.require' => 'video_type',
//        'img_url.require' => 'img_url',
//        'video_length.require' => 'video_length',
//        'video_address_en.require' => 'video_address_en',
//        'tags.require' => 'tags',
//        'is_banner.require' => 'is_banner',
//    ];

}