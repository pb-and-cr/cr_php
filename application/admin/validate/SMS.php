<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class SMS extends BaseValidate {

    protected $rule = [
        'number' => 'requireInput',
        'content' => 'requireInput',
        'template_name' =>'requireInput'
    ];
}