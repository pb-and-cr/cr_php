<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class Complex extends BaseValidate {

    protected $rule = [
        'name' => 'requireInput',
        'catetory_id' => 'requireInput',
        'publish_dated' => 'requireInput',
        'is_published' => 'requireInput',
        'content' => 'requireInput',
        'articleIds' => 'requireInput',
    ];

}