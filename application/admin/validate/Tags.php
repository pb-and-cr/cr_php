<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class Tags extends BaseValidate {

    protected $rule = [
        'name' => 'requireInput',
        'order_by' => 'requireInput|between:1,120',
    ];

    protected $message  =   [
        'order_by.between'  => 'order_by-please select',
    ];

}