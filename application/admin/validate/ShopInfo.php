<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class ShopInfo extends BaseValidate {

    protected $rule = [
        'author' => 'requireInput',
        'des' => 'requireInput',
        'is_published' => 'requireInput',
        'publish_dated' => 'requireInput',
        'img_url' => 'requireInput',
        'tag_id' => 'requireInput',
        'audio_address'  => 'requireInput',
    ];
}