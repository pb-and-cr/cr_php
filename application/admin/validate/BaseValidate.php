<?php
namespace app\admin\validate;

use think\Validate;
use think\Lang;

/**
 * 后台登录验证
 * Class Login
 * @package app\admin\validate
 */
class BaseValidate extends Validate {

     // 自定义验证规则
    protected function requireInput($value,$rule,$data,$field)
    {
        if ($value=='') {
            return $field.'-'.Lang::get('video_validate_is_empty');
        } //是否为空

        return True;
    }

    // 自定义验证规则
    protected function requireSelected($value,$rule,$data,$field)
    {
        if ($value=='') {
            return $field.'-'.Lang::get('video_validate_check_tip');
        } //是否为空

        return True;
    }
}