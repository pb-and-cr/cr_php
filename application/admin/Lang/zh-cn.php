<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/9
 * Time: 下午2:16
 */

// eg: http://localhost:1062/admin/video/view?lang=en-us

return array(
    //Validate
    'validate_is_not_empty'=>'',

    'login_tip'=>'请输入你的信息',
    'login_username'=>'用户名',
    'login_password'=>'密码',
    'login_login'=>'登陆',
    'login_forgot'=>'忘记密码',
    'login_register'=>'我要注册',
    'login_error'=>'用户名或者密码错误',
    'login_logout'=>'退出登录',

    // admin menu
    'video'=>'文章',
    'dashboard'=>'控制台',
    'menu_video_new'=>'新文章',
    'menu_video_list'=>'文章列表',
    'menu_video_home'=>'主页',

    // video menu
    'video_eidt'=>'文章编辑',
    'video_title'=>'标题',
    'video_search'=>'搜索',
    'video_add'=>'添加',
    'video_id'=>'编号',
    'video_album'=>'专辑',
    'video_tags'=>'标签',
    'video_status'=>'状态',
    'video_album'=>'专辑',
    'video_browse'=>'浏览量',
    'video_publish_dated'=>'发布时间',
    'video_operating'=>'操作',
    'video_publish_dated'=>'发布时间',
    'video_submit'=>'提交',
    'video_reset'=>'重置',

    'video_language_chinese'=>'中文',
    'video_language_english'=>'english',
    'video_edit_or_now'=>'查看和编辑',
    'video_now'=>'新',
    'video_edit'=>'Edit',
    'video_tips'=>'我们的信息会鼓舞到很多人',
    'video_base_info'=>'基本信息',
    'video_title_for_china'=>'标题',
    'video_title_for_english'=>'English Title',
    'video_support_language'=>'Support language',
    'video_author'=>'作者',
    'video_column'=>'专辑名称',
    'video_des'=>'文章描述',
    'video_setting'=>'设置',
    'video_status_open'=>'公开',
    'video_status_close'=>'私有',
    'video_format'=>'格式',
    'video_format_video'=>'video',
    'video_format_audio'=>'audio',
    'video_categories'=>'分类',
    'video_advertising'=>'Advertising',
    'video_advertising_yes'=>'yes',
    'video_advertising_no'=>'no',
    'video_release_time'=>'发布时间',
    'video_bible'=>'圣经',
    'video_video_length'=>'视频时长',
    'video_video_address'=>'文章地址',
    'video_other_address'=>'English video address',
    'video_validate_check_tip'=>'please choose',
    'video_validate_is_empty'=>'不能为空',
    'video_validate_title_check'=>'English or Chinese title, at least need to fill in one',
    'video_validate_video_address_check'=>'English or Chinese video address, at least need to fill in one',
    'video_validate_video_ablum_name'=>'album name is wrong',


    'video_submit_success_tip'=>'提交成功，您需要继续添加吗？',
    'video_submit_success_yes'=>'是',
    'video_submit_success_no'=>'否',
    'video_delete_success_tip'=>'成功删除',

    'video_select_book'=>'选择书名',
    'video_select_chapter'=>'选择章',
    'video_select_verse'=>'选择节',

    'tags_admin' => '标签管理',
    'tags_list' => '标签列表',
    'column_admin' => '专栏列表',
);

?>