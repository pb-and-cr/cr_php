<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\display\controller;

use app\lib\business\ArticleInfo;
use app\lib\business\AlbumInfo;
use app\lib\common\AnalyticsTracker;
use app\lib\common\Jssdk;
use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

class index extends Controller
{
    //文章展示
    public function index(){
        $id = Request::instance()->param('id');
        $article = ArticleInfo::getInfoById($id);
        $videoSrc = ArticleInfo::getVideoSrc();
        if($article) {
            if ($article['id'] < 6536) {
                $article['content'] = str_replace("\n", "<br/>", $article['content']);
                //$list['content'] = str_replace(" ","&nbsp;",$list['content']);
                $article['content'] = str_replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;", $article['content']);
            }

            if (strlen($article['img_url']) < 10) {
                $article['img_url'] = "";
            }

            $isWeixinRequest = 0;
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
                $isWeixinRequest = 1;
            }

            if($isWeixinRequest==1){
                $signPackage = [];
                try{
                    $jssdk = new Jssdk(Config::get('dgAppID'), Config::get('dgAppSecret'));
                    $signPackage = $jssdk->GetSignPackage();
                }
                catch(Exception $e){
                    $signPackage = array(
                        "appId" => '121',
                        "nonceStr" => 'nonceStr',
                        "timestamp" => 'timestamp',
                        "url" => 'url',
                        "signature" => 'signature',
                        "rawString" => 'rawString'
                    );
                }
//                dump($signPackage);
                $this->assign('signPackage', $signPackage);

            }

            $this->assign('isWeixinRequest', $isWeixinRequest);
            $this->assign('article', $article);
            $this->assign('video_src', $videoSrc);
            $this->assign('download', $this->getAppDownLoadAddress());
            if (strlen($article['bak_address']) > 0) {
                $albumArticleList = AlbumInfo::getAlbumOtherArticleList($article['album_id'],$article['id']);
//                dump($albumArticleList);
                $this->assign('albumArticleList', $albumArticleList);
                return $this->fetch(APP_PATH . request()->module() . '/view/article_iframe.html');
            } else {
                return $this->fetch(APP_PATH . request()->module() . '/view/article.html');
            }
        }
    }

    //书籍展示
    public function displayBook(){
//        $this->assign('book_id',$book_id);
        return $this->fetch(APP_PATH.request()->module().'/view/bookShare.html');

    }

    //书籍展示
    public function aboutCheckIn(){
//        $this->assign('book_id',$book_id);
        return $this->fetch(APP_PATH.request()->module().'/view/aboutCheckIn.html');

    }

    //专辑分享
    public function displayAlbumInfo(){
        return $this->fetch(APP_PATH.request()->module().'/view/albumShare.html');
    }

    function getGuid() {
        $charid = strtoupper(md5(uniqid(mt_rand(), true)));

        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);

        return $uuid;
    }

    public function downloadAddress(){
        //是微信
        $isWeixinRequest = false;
        $isChristianReader = false;
        $isApply = false;
        $isRand = rand(10,12);
        $gospelUrl = "http://yesu.me";
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            $isWeixinRequest = true;
        }
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') !== false || strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') !== false) {
            $isApply = true;
        }
        $host = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        if (strpos($host, '_browser') !== false) {
            $isChristianReader = true;
        }

        $appInfo = array(
            'apk'=>'http://bible12e12ascq1.biblemooc.net/apk/jdt-release.apk'
            ,'apply'=>'https://itunes.apple.com/us/app/ji-du-tu-yue-du-sheng-jing/id959472974?mt=8'
            ,'qq'=>'http://a.app.qq.com/o/simple.jsp?pkgname=com.christiansread'
            ,'name'=>'深受好评的基督徒阅读工具','address'=>'http://read.jdtapps.me/wap/pressbible/introduction'
        );

        if($isApply)
        {
            $appInfo['address'] = $appInfo['apply'];
        }
        else if($isWeixinRequest == true)
        {
            $appInfo['address'] = $appInfo['qq'];
        }
        else
        {
            $appInfo['address'] = $appInfo['apk'];
        }
        Header("HTTP/1.1 303 See Other");
        Header("Location: ".$appInfo['address']);
        exit;
    }

    //发送gospel信息到GMO
    public function sendStar(){
        echo AnalyticsTracker::postGospel();
    }

    //拉取微信新
    public function collectionWeChat(){

        // http://localhost:12000/display/index/collectionWeChat?https://mp.weixin.qq.com/s?timestamp=1522721418&src=3&ver=1&signature=K-tfNPG7uY7cJR4uNQoWMHkMeZp9TSrZodjevNw4XigrF5RfEs1mKvkXCLppkmzwlZQmf2EjQDsjT0vIdOXoEBOCKRZlNsBPd4SvczrOHwbLkdd*PBYNhQ8*c9YhihjOXoDPQwA8BYw6LtQlD3NIpn1mvNI6CX*EJ9MGAu1D0-A=

        $url = $_SERVER["QUERY_STRING"];
//        echo $_SERVER["QUERY_STRING"];
//        $ch = curl_init();
//        // 设置URL和相应的选项
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_HEADER, false);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 抓取URL并把它传递给浏览器
        $data = file_get_contents($url);
//        curl_close($ch);
//        preg_match('<iframe [a-z0-9A-W \-\"\=&\:/\.\?\;>]+</iframe>',$data,$result);
//        preg_match('<iframe (.*)</iframe>',$data,$result);
//        $data = preg_replace("\n", "",$data);
//        $data = trim($data);
//$data = strip_tags($str,"");

//        text = text.replaceAll("<!--\\S*?-->", "");
//        text = text.replaceAll("", "");
//        text = text.replaceAll(" {2,}", " ");
//        text = text.replaceAll("\\s{2,}", "\n");
//        text = text.replaceAll(">\\s+?<", "><");
//        text = text.replaceAll("^\\s+?", "");

//        $data = preg_replace("<!--\\S*?-->","",$data);
//        $data = preg_replace(" {2,}","",$data);
//        $data = preg_replace("\\s{2,}","",$data);
//        $data = preg_replace(">\\s+?<","",$data);
//        $data = preg_replace("^\\s+?"," ",$data);
        $array =[];
        $data = preg_replace("/\t/","",$data);
        $data = preg_replace("/\r\n/","",$data);
        $data = preg_replace("/\r/","",$data);
        $data = preg_replace("/\n/","",$data);
        $data = preg_replace("/ {2,}/","",$data);


        $titleBeginStr = "<h2 class=\"rich_media_title\" id=\"activity-name\">";
        $titleBeginIndex = stripos($data,$titleBeginStr);
        $length = mb_strlen($titleBeginStr,"UTF8");
        $titleEndIndex = stripos($data,'</h2><div id="meta_content"');
        $title = substr($data,$titleBeginIndex+$length,$titleEndIndex-$titleBeginIndex-$length);
        $array['title'] = $title;
//        echo $title;

        preg_match_all('/<em id=[\w\-" =]+>([\d\-]+)<\/em>/',$data,$result);
        if(empty($result[1][0])){
            $array['date'] = "没有获取到发布时间,请手动添加";
        }else{
            $date = $result[1][0];
            $t = time();
            $time = date('Y-m-d H:i:s',$t);
            $time = substr($time,11);
            $date = $date.' '.$time;
            $array['date'] = $date;
        }

        preg_match_all('/<strong class="profile_nickname">([\w]+)<\/strong>/u',$data,$result);
        if(empty($result[1][0])){
            $array['albumName'] = "没有获取到专辑名称,请手动添加";
        }else{
            $albumName = $result[1][0];
            $array['albumName'] = $albumName;
        }
        $beginIndex = stripos($data,'<div id="page-content" class="rich_media_area_primary">');
        $endIndex = stripos($data,'</div><div class="rich_media_area_primary sougou"');
//        echo $beginIndex.' -- '. $endIndex .' <br/>';
//        echo $data;
        $data = substr($data,$beginIndex,$endIndex-$beginIndex);
//        echo $data;

        preg_match_all('/<iframe[\w \-\=\"\:\=\/\?\;\.\&>!%]+<\/iframe>/',$data,$result);
//        dump($result);
        if($result!=null){
            for($i=0;$i<count($result[0]);$i++){
                $content = $result[0][$i];
//                dump($content);
                preg_match('/vid=([\w\d]+)/',$content,$tempRusult);
//                dump($tempRusult);
//                echo $content;
                if(count($tempRusult)==2){
//                    echo $content;
//                    echo "<iframe width='100%' src='http://v.qq.com/iframe/player.html?vid=".$tempRusult[1]."&tiny=0&auto=0' frameborder=0 'allowfullscreen'></iframe>";
//                    $data = str_ireplace($content,"<iframe frameborder='0' width='100%' src='http://v.qq.com/iframe/player.html?vid=".$tempRusult[1]."&tiny=0&auto=0' allowfullscreen></iframe>", $data);
                    $data = str_ireplace($content,"<iframe width='100%' src='http://v.qq.com/iframe/player.html?vid=".$tempRusult[1]."&tiny=0&auto=0' frameborder=0 'allowfullscreen'></iframe>", $data);
                }
            }
        }

        preg_match_all('/<img [\w \-\=\"\:\=\/\?\;\.\&>!%,(\)]+/',$data,$result);
//        dump($result);
        if($result!=null){
            for($i=0;$i<count($result[0]);$i++){
                $str = $result[0][$i];
//                echo strripos($str, "data-src");
                if(strripos($str, "data-src")!==false){
                    $newImag = $this->getGuid();
                    preg_match('/data-src=([\w\-\=\"\:\=\/\?\;\.]+)/',$str,$imgs);
                    $imgUrl = preg_replace('["]',"",$imgs[1]);
//                    dump($imgUrl);
                    if($imgUrl!=null && strlen($imgUrl)>3){
//                        echo $imgs[0].'<br/>';
                        preg_match_all('/mmbiz_(\w+)/',$imgUrl,$imgType);

                        if($imgType!=null && count($imgType)==2 && empty($imgType[1])==false ){
                            if($imgType[1]!=null) {
                                $newImag = $newImag . '.' . $imgType[1][0];
                                $dir = date("Ymd");
                                $save_path = $_SERVER['DOCUMENT_ROOT'] . '/Uploads/images/backstage/' . $dir;

                                if (!file_exists($save_path)) {
                                    // dump($save_path);
                                    mkdir($save_path);
                                }
                                $save_path = $save_path . '/' . $newImag;
                                $file = file_get_contents($imgUrl);
                                file_put_contents($save_path, $file);
                                $saveUrl = "src='" . '/Uploads/images/backstage/' . $dir . '/' . $newImag . "'";
                                $data = str_ireplace($imgs[0], $saveUrl, $data);
                            }
                        }
                        else
                        {

                            preg_match_all('/mmbiz/',$imgUrl,$imgType);
                            if($imgType!=null && count($imgType)==1 ){
                                $imgType = '.jpg';

                                $newImag = $newImag.$imgType;
                                $dir = date("Ymd");
                                $save_path = $_SERVER['DOCUMENT_ROOT'] . '/Uploads/images/backstage/' . $dir;

                                if (!file_exists($save_path)) {
                                    mkdir($save_path);
                                }
                                $save_path = $save_path . '/' . $newImag;
                                try {
                                    $file = file_get_contents($imgUrl);
                                    file_put_contents($save_path, $file);
                                    $saveUrl = "src='" . '/Uploads/images/backstage/' . $dir . '/' . $newImag . "'";
                                    $data = str_ireplace($imgs[0], $saveUrl, $data);
                                }
                                catch(Exception $e){

                                }
                            }
                        }
                    }

                }

            }
        }



//        dump($data);
//        preg_match('/[s]iframe',$data,$result);
//        dump($result);
//        $data = str_ireplace("<!--\\S*?-->", "",$data);
//        $data = str_ireplace("<!--\\S*?-->", "",$data);
//        $data = str_ireplace("<!--\\S*?-->", "",$data);
//        $data = str_ireplace("<!--\\S*?-->", "",$data);
//        $data = str_ireplace("<!--\\S*?-->", "",$data);
//        $data = str_ireplace("<!--\\S*?-->", "",$data);
//        $data = preg_replace("", "",$data);
//        $data = preg_replace(" {2,}", " ",$data);
//        $data = preg_replace("\\s{2,}", "\n",$data);
//        $data = preg_replace(">\\s+?<", "><",$data);
//        $data = preg_replace("^\\s+?", "",$data);
//        echo $data;
//        dump($data);
//        $data = str_ireplace("https://", "http://", $data);
//        $data = str_ireplace("data-src=", "src=", $data);
//        file_put_contents("/Users/vincent/DashBoard/100 - Work  工作/120 - 基督徒阅读/jdt-thinkphp5/public/Uploads/t.html", $data);

        //echo $data;
        // $d = str_replace('<h2 class="rich_media_title" id="activity-name"><script .*</script></h2>','<h2>'.$title.'</h2>',$data);

        // dump($d);
        // return;
        $h2Beginning = '<h2 class="rich_media_title" id="activity-name">';
        // dump(strlen($h2Beginning));
        $h2Ending = '</h2>';
        $h2_start_position = stripos($data,$h2Beginning);
        $h2_end_position = stripos($data,$h2Ending);
        $data1 = substr($data,0,strlen($h2Beginning)+$h2_start_position);
        $data2 = substr($data,$h2_end_position);
        // dump($data2);
        $data = $data1.$title.$data2;
        // echo $data;
        // dump($data);
        // dump($h2_start_position);
        // dump($h2_end_position);
        // return;
        $array["content"] = $data;
        // dump($array);
        // return;
        exit(json_encode($array, JSON_UNESCAPED_UNICODE));
    }

    //基督徒阅读介绍
    public function introduce(){
        $this->assign('download',$this->getAppDownLoadAddress());
        return $this->fetch(APP_PATH.request()->module().'/view/introduction.html');
    }

    //基督徒阅读介绍
    public function about(){
        $this->assign('download',$this->getAppDownLoadAddress());
        return $this->fetch(APP_PATH.request()->module().'/view/introduction.html');
    }

    public function wechat(){
        return $this->fetch(APP_PATH.request()->module().'/view/wechat.html');
    }

    //商品介绍界面(基督徒阅读书店中的分享,书籍介绍)
    public function commodityDetails(){
        $id = Request::instance()->param('id');
        $from = Request::instance()->param('f');
        $article = ArticleInfo::getInfoById($id);

        $isWeixinRequest = 0;
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            $isWeixinRequest = 1;
        }

//        $jssdk = new Jssdk(Config::get('dgAppID'), Config::get('dgAppSecret'));
//        $signPackage = $jssdk->GetSignPackage();
        $signPackage = array(
            "appId"     => '121',
            "nonceStr"  => 'nonceStr',
            "timestamp" => 'timestamp',
            "url"       => 'url',
            "signature" => 'signature',
            "rawString" => 'rawString'
        );
        $this->assign('signPackage',$signPackage);

        $this->assign('isWeixinRequest',$isWeixinRequest);


        $this->assign('download',$this->getAppDownLoadAddress());
        if(strlen($article['bak_address'])>0)
        {
            if($from!='article'){
                if($article){
                    $article['content'] = str_replace($article['title'],$article['audio_address'],$article['content']);
//                    $article['title'] = $article['audio_address'];
//                    dump($article);
                }
            }

            $this->assign('article',$article);
            return $this->fetch(APP_PATH.request()->module().'/view/commodityDetails_iframe.html');
        }
        else
        {
//            dump($article);
            $article['title'] = $article['audio_address'];
            $this->assign('article',$article);
            return $this->fetch(APP_PATH.request()->module().'/view/commodityDetails.html');
        }
    }

    //获取微信的信息,透过jssdk
    public function getWeChatToken(){
//        $jssdk = new Jssdk(Config::get('dgAppID'), Config::get('dgAppSecret'));
//        $signPackage = $jssdk->GetSignPackage();
        $signPackage = array(
            "appId"     => '121',
            "nonceStr"  => 'nonceStr',
            "timestamp" => 'timestamp',
            "url"       => 'url',
            "signature" => 'signature',
            "rawString" => 'rawString'
        );
        exit(json_encode($signPackage, JSON_UNESCAPED_UNICODE));
    }

    public function getAppDownLoadAddress(){
        $isWeixinRequest = false;
        $isApply = false;
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            $isWeixinRequest = true;
        }
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Mac') !== false) {
            $isApply = true;
        }

        $appInfo = array('apk'=>'http://prebible.oss-cn-shanghai.aliyuncs.com/apk/jdt-release.apk'
        ,'weixin_apply'=>'http://a.app.qq.com/o/simple.jsp?pkgname=com.christiansread'
        ,'other_apply'=>'https://itunes.apple.com/cn/app/%E5%9F%BA%E7%9D%A3%E5%BE%92%E9%98%85%E8%AF%BB/id959472974?mt=8'
        ,'qq'=>'http://a.app.qq.com/o/simple.jsp?pkgname=com.christiansread'
        ,'name'=>'让您享受优质资讯的快乐(基督徒阅读)','address'=>'http://christian-reading.daddygarden.com/display/index/introduction');

        $address = "";
        if($isApply == true){
            if($isWeixinRequest==true){
                $address = $appInfo['weixin_apply'];
            }
            else
            {
                $address = $appInfo['other_apply'];
            }
        }
        else if($isWeixinRequest == true){
            $address = $appInfo['qq'];
        }
        else
        {
            $address = $appInfo['apk'];
        }
        return $address;
    }
}