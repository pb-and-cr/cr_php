<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/14
 * Time: 下午3:50
 */

namespace app\display\controller;

use app\lib\business\ArticleInfo;
use app\lib\common\AnalyticsTracker;
use app\lib\common\Jssdk;
use think\Config;
use think\Controller;
use think\Db;
use think\Session;
use think\Lang;
use think\Request;
use think\Validate;

class Grab extends Controller
{

    //拉取微信新
    public function collectionWeChat(){


        $url = $_SERVER["QUERY_STRING"];
        $data = file_get_contents($url);

        $array =[];
        $data = preg_replace("/\t/","",$data);
        $data = preg_replace("/\r\n/","",$data);
        $data = preg_replace("/\r/","",$data);
        $data = preg_replace("/\n/","",$data);
        $data = preg_replace("/ {2,}/","",$data);


        $titleBeginStr = "<h2 class=\"rich_media_title\" id=\"activity-name\">";
        $titleBeginIndex = stripos($data,$titleBeginStr);
        $length = mb_strlen($titleBeginStr,"UTF8");
        $titleEndIndex = stripos($data,'</h2><div id="meta_content"');
        $title = substr($data,$titleBeginIndex+$length,$titleEndIndex-$titleBeginIndex-$length);
        $array['title'] = $title;

        preg_match_all('/<em id=[\w\-" =]+>([\d\-]+)<\/em>/',$data,$result);
        if(empty($result[1][0])){
            $array['date'] = "没有获取到发布时间,请手动添加";
        }else{
            $date = $result[1][0];
            $t = time();
            $time = date('Y-m-d H:i:s',$t);
            $time = substr($time,11);
            $date = $date.' '.$time;
            $array['date'] = $date;
        }

        preg_match_all('/<strong class="profile_nickname">([\w]+)<\/strong>/u',$data,$result);
        if(empty($result[1][0])){
            $array['albumName'] = "没有获取到专辑名称,请手动添加";
        }else{
            $albumName = $result[1][0];
            $array['albumName'] = $albumName;
        }
        $beginIndex = stripos($data,'<div id="page-content" class="rich_media_area_primary">');
        $endIndex = stripos($data,'</div><div class="rich_media_area_primary sougou"');

        $data = substr($data,$beginIndex,$endIndex-$beginIndex);
//        echo $data;

        preg_match_all('/<iframe[\w \-\=\"\:\=\/\?\;\.\&>!%]+<\/iframe>/',$data,$result);
        if($result!=null){
            for($i=0;$i<count($result[0]);$i++){
                $content = $result[0][$i];

                preg_match('/vid=([\w\d]+)/',$content,$tempRusult);
                if(count($tempRusult)==2){
                    $data = str_ireplace($content,"<iframe width='100%' src='http://v.qq.com/iframe/player.html?vid=".$tempRusult[1]."&tiny=0&auto=0' frameborder=0 'allowfullscreen'></iframe>", $data);
                }
            }
        }

        preg_match_all('/<img [\w \-\=\"\:\=\/\?\;\.\&>!%,(\)]+/',$data,$result);
//        dump($result);
        if($result!=null){
            for($i=0;$i<count($result[0]);$i++){
                $str = $result[0][$i];
//                echo strripos($str, "data-src");
                if(strripos($str, "data-src")!==false){
                    $newImag = $this->getGuid();
                    preg_match('/data-src=([\w\-\=\"\:\=\/\?\;\.]+)/',$str,$imgs);
                    $imgUrl = preg_replace('["]',"",$imgs[1]);
//                    dump($imgUrl);
                    if($imgUrl!=null && strlen($imgUrl)>3){
//                        echo $imgs[0].'<br/>';
                        preg_match_all('/mmbiz_(\w+)/',$imgUrl,$imgType);

                        if($imgType!=null && count($imgType)==2 && empty($imgType[1])==false ){
                            if($imgType[1]!=null) {
                                $newImag = $newImag . '.' . $imgType[1][0];
                                $dir = date("Ymd");
                                $save_path = $_SERVER['DOCUMENT_ROOT'] . '/Uploads/images/backstage/' . $dir;

                                if (!file_exists($save_path)) {
                                    // dump($save_path);
                                    mkdir($save_path);
                                }
                                $save_path = $save_path . '/' . $newImag;
                                $file = file_get_contents($imgUrl);
                                file_put_contents($save_path, $file);
                                $saveUrl = "src='" . '/Uploads/images/backstage/' . $dir . '/' . $newImag . "'";
                                $data = str_ireplace($imgs[0], $saveUrl, $data);
                            }
                        }
                        else
                        {

                            preg_match_all('/mmbiz/',$imgUrl,$imgType);
                            if($imgType!=null && count($imgType)==1 ){
                                $imgType = '.jpg';

                                $newImag = $newImag.$imgType;
                                $dir = date("Ymd");
                                $save_path = $_SERVER['DOCUMENT_ROOT'] . '/Uploads/images/backstage/' . $dir;

                                if (!file_exists($save_path)) {
                                    mkdir($save_path);
                                }
                                $save_path = $save_path . '/' . $newImag;
                                try {
                                    $file = file_get_contents($imgUrl);
                                    file_put_contents($save_path, $file);
                                    $saveUrl = "src='" . '/Uploads/images/backstage/' . $dir . '/' . $newImag . "'";
                                    $data = str_ireplace($imgs[0], $saveUrl, $data);
                                }
                                catch(Exception $e){

                                }
                            }
                        }
                    }

                }

            }
        }

        // dump($d);
        // return;
        $h2Beginning = '<h2 class="rich_media_title" id="activity-name">';
        // dump(strlen($h2Beginning));
        $h2Ending = '</h2>';
        $h2_start_position = stripos($data,$h2Beginning);
        $h2_end_position = stripos($data,$h2Ending);
        $data1 = substr($data,0,strlen($h2Beginning)+$h2_start_position);
        $data2 = substr($data,$h2_end_position);
        // dump($data2);
        $data = $data1.$title.$data2;
        // return;
        $array["content"] = $data;
        // dump($array);
        // return;
        exit(json_encode($array, JSON_UNESCAPED_UNICODE));
    }

    public function syncData(){
//        http://localhost:12000/display/grab/syncData?des=des&img=img&albumId=12&publishDate=2018-01-01
//        $article = $this->collectionWeChat();

        $des = Request::instance()->param("des");
        $imgUrl = Request::instance()->param("imgUrl");
        $url = Request::instance()->param("url");
        $albumId = Request::instance()->param("albumId");
        $publishDate = Request::instance()->param("publishDate");

        echo ' ------------- '.$albumId;


        $article = [];
        $article['url'] = "bak_address";
        $article['title'] = "title";
        $article['content'] = "content";
        $article['title'] = "title";
        $article['albumName'] = "albumName";

        $video = [];
        $video['title'] = $article['title'];
        $video['is_published'] = 1;
        $video['is_top'] = 0;
        $video['tag_id'] = 0;
        $video['id'] = 0;
        $video['publish_dated'] = $publishDate;
        $video['bak_address'] = $article['url'];
        $video['book_address'] = "";
        $video['audio_address'] = "";
        $video['des'] = $des;
        $video['author'] = "";
        $video['content'] = $article['content'];
        $video['album_id'] = $albumId;
        $video['img_url'] = $imgUrl;
        $video['update_dated'] = date("Y-m-d H:i:s");


        $id = 1;//ArticleInfo::updateOrCreated($video);
        if($id>0){
            $id = 1;
        }
        else
        {
            $id = 0;
        }
        exit(json_encode($id, JSON_UNESCAPED_UNICODE));
    }
}