<?php
namespace app\lib\business;

use app\lib\model\TagsModel;
use think\Db;
use think\Cache;
use app\lib\common\StringTool;
use app\lib\business\LanguageInfo;
use app\lib\model\languageISOModel;


class TagsInfo{

    const CATCH_TAG_KEY='CATCH_TAG_KEY';

    public static $TAG_SHOP_ID = 1; //商城分类父类Id
    public static $TAG_BOOK_ID = 2; //电子书父类Id
    public static $TAG_HISTORY_ID = 3; //历史文章父类Id,对应专辑中的历史文章
    public static $TAG_BOOK_AlBUM_ID = 4; //书店专辑归类


    const CATCH_KEY = 'article_tags';

    public static function cacheClear(){
        Cache::rm(self::CATCH_KEY);
    }

    public static function getArticleTags(){
        $list =  Cache::get(self::CATCH_KEY);
        if ($list)
        {
            return $list;
        }
        else
        {
            $list = Db::query("select * from article_tags ");
            Cache::set(self::CATCH_KEY,$list,60*60*24);
            return $list;
        }
    }

    // 获得所有书籍的标签
    public static function getBookTags(){
        $list = self::getArticleTags();
        $boobList = [];

        foreach ($list as $item){
            if($item['parent_id'] == TagsInfo::$TAG_BOOK_ID){
                array_push($boobList,$item);
            }
        }
        return $boobList;
    }

    // 获得所有书店的标签
    public static function getShopTags(){
        $list = self::getArticleTags();
        $boobList = [];

        foreach ($list as $item){
            if($item['parent_id'] == TagsInfo::$TAG_SHOP_ID){
                array_push($boobList,$item);
            }
        }
        return $boobList;
    }

    public static function getTagsByCache(){
        $list =  Cache::get(self::CATCH_TAG_KEY);
        if ($list)
        {
            return $list;
        }
        else
        {
            $list = Db::query("SELECT * from article_tags order by order_by");
            Cache::set(self::CATCH_TAG_KEY,$list,60*60*24);
            return $list;
        }
    }

    public static function queryTags(){
        $list = TagsModel::select();
        return $list;
    }

    public static function parentAll(){
        return Db::query("SELECT * from article_tags where parent_id = 0 order by order_by");
    }

    public static function query($page,$rows){
        $sql = ' where 1 = 1 ';
        $param = [];

        if(empty($page)==false){
            $limit = ($page-1)*$rows;
            $sql = $sql.' order by a.order_by desc  limit '.$limit.','.($rows);
        }

        $count = Db::query('SELECT count(*) as total from article_tags a ');

        $list = Db::query('select a.*,b.name as album_name from article_tags a left join article_tags b on a.parent_id = b.id '.$sql,$param);

        $result = [];
        $result['records'] = $count[0]['total'];
        $result['page'] = $page;
        $result['total'] = intval($count[0]['total']/$rows)+1;
        $result['rows'] = $list;
        return $result;
    }

    public static function queryById($id){
        $list = Db::query(' select a.* from article_tags a WHERE a.id = ?',[$id]);
        return $list;
    }

    public static function updateOrCreated($data){
        //数据格式的转化
        $app = array();
        $app['name'] = $data['name'];
        $app['parent_id'] = $data['parent_id'];
        $app['order_by'] = $data['order_by'];

        Cache::rm(self::CATCH_TAG_KEY);
        if(intval($data['id'])>0){
            $entity = new TagsModel();
            $entity->save($app,['id' => $data['id'] ]);
            return $data['id'];
        }
        else
        {
            $entity = new TagsModel($app);
            $entity->save();
            return $entity->getLastInsID();
        }

    }

    public static function getInfoById($id){
        $list = Db::query("select a.*,b.name as album_name from article_tags a 
                          left join album_info b on a.parent_id = b.id where a.id = ?",[$id]);;
        if(count($list)>0){
            return $list[0];
        }
        else
        {
            return null;
        }
    }

    public static function delete($id){
        Cache::rm(self::CATCH_TAG_KEY);
        $tags = TagsModel::get($id);
        $tags->delete();
    }
}









