<?php
namespace app\lib\business;

use app\lib\model\ComplexInfoModel;
use app\lib\model\ComplexContentInfoModel;
use think\Db;
use think\Cache;
use think\Config;
use app\lib\common\StringTool;
use app\lib\common\TimeHelper;
use app\lib\business\UserInfo;


class ComplexInfo{

    const CATCH_KEY='CATCH_COMPLEX_KEY';
    const CATEGORY_BOOK_ID=1;    // 书籍合集id
    const CATEGORY_ARTICLE_ID=2; // 文章合集id
    const CATEGORY_AUDIO_ID=3;   // 音频合集id
    const CATEGORY_BNNER_ID=4;   // banner合集id

    
    public static function getComplexByCache(){
        $list =  Cache::get(self::CATCH_KEY);
        if ($list)
        {
            return $list;
        }
        else
        {
//            $imgDomain = Config::get('IMG_DOMAIN');
            $IMG_HOST = Config::get('IMG_HOST');
            $list = Db::query("select * from article_complex where is_published = 1 order by order_by");
            foreach ($list as &$item){
//                $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
                $item['img_url'] = $IMG_HOST.$item['img_url'];
            }
//            Cache::set(self::CATCH_KEY,$list,60*60*24);
            return $list;
        }
    }

    public static function getBannerList(){
        $IMG_HOST = Config::get('IMG_HOST');
        $list = Db::query("select a.img_url,b.article_id,c.tag_id,c.title,c.audio_address from article_complex a
            INNER join article_complex_content b on a.id = b.article_complex_id
            INNER join article_info c on b.article_id = c.id
            where a.catetory_id = 4 and a.is_published = 1 order by a.publish_dated desc LIMIT 5");

        $shopBook = TagsInfo::getShopTags();

        foreach ($list as &$item){
            $item['img_url'] = $IMG_HOST.$item['img_url'];
            $item['is_shop'] = 0;

            foreach ($shopBook as $shop){
                //临时使用
                $item['is_shop'] = 1;
                break;

                if($item['tag_id'] == $shop['id'] ){
                    $item['is_shop'] = 1;
                    $item['title'] = $item['audio_address'];
                    break;
                }

            }
        }
        return $list;
    }

    public static function getCacheAlbumById($id){
        $list = self::getComplexByCache();
        foreach($list as $item){
            if($item["id"] == $id ){
                return $item;
            }
        }
        return null;
    }

    public static function followCountDisplay($follow_count){
        $follow_count = intval($follow_count);
        return $follow_count;
    }
    public static function query($page,$rows){
        $sql = ' where 1 = 1 ';
        $param = [];

//        echo $sql;
//        echo '<br/>';
//
//        echo 'select a.*,b.code as language_family_name,c.`name` as language_name  from video_tags a
//                        LEFT JOIN language_family_code b on a.language_family_id = b.id
//                        LEFT JOIN language_iso_info c on a.language_id = c.id  '.$sql;
//        echo '<br/>';

        $count = Db::query('select count(1) as total  from article_complex a '.$sql,$param);

        if(empty($page)==false){
            $limit = ($page-1)*$rows;
            $sql = $sql.' order by a.update_dated desc  limit '.$limit.','.($rows);
        }

        $list = Db::query('select a.* from article_complex a '.$sql,$param);

        $result = [];
        $result['total'] = intval($count[0]['total']/$rows)+1;
        $result['page'] = $page;
        $result['records'] = 13;
        $result['rows'] = $list;
        return $result;
    }

    public static function queryAll(){

        $list = Db::query('select a.*  from album_info a  ');
        return $list;
    }

    public static function queryById($id){
        $list = Db::query(' select a.* from album_info a
            WHERE a.id = ?',[$id]);
        return $list;
    }

    public static function updateOrCreated($data){
        //数据格式的转化
        $app = array();
        $app['name'] = $data['name'];
        $app['catetory_id'] = $data['catetory_id'];
        $app['content'] = $data['content'];
        $app['img_url'] = $data['img_url'];
        $app['is_published'] = $data['is_published'];
        $app['publish_dated'] = $data['publish_dated'];
        $app['update_dated'] = (new TimeHelper())->getCurrentTime();
        Cache::rm(self::CATCH_KEY);
        $id = 0;
        if(intval($data['id'])>0){
            $entity = new ComplexInfoModel();
            $entity->save($app,['id' => $data['id'] ]);
            $id = $data['id'];
        }
        else
        {
            $app['browse'] = 0;
            $entity = new ComplexInfoModel($app);
            $entity->save();
            $id = $entity->getLastInsID();
        }

        if($id>0){
            Db::execute("delete from article_complex_content where article_complex_id = ?",[$id]);
            $arr = explode(",", $data['articleIds']);
            foreach($arr as $item){
                $content = new ComplexContentInfoModel([
                    'article_complex_id'  =>  $id,
                    'article_id' =>  $item
                ]);
                $content->save();
            }
        }
    }

    public static function getInfoById($id){
        $entity =  ComplexInfoModel::get(['id'=>$id]);;

        $entity['list'] = Db::query("SELECT b.article_id,c.title from article_complex a
INNER JOIN article_complex_content b on a.id = b.article_complex_id
INNER JOIN article_info c on b.article_id = c.id  where a.id = ?",[$id]);
        return $entity;
    }

    public static function delete($id){
        $tags = ComplexInfoModel::get($id);
        $tags->delete();
        Cache::rm(self::CATCH_KEY);
    }

}









