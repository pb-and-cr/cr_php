<?php
namespace app\lib\business;

use app\demo\controller\DBQuery;
use app\lib\model\ArticleInfoModel;
use app\lib\model\ArticleInfoContentModel;
use app\lib\common\StringTool;
use app\lib\common\TimeHelper;
use app\lib\business\AlbumInfo;
use think\Db;
use think\Config;
use think\Cache;
use think\Model;

class ShopInfo
{

    const CATCH_BANNER_KEY='CATCH_BANNER_KEY';
    const CR_INDEX='CR_INDEX';
    const CR_AlbumArticles = 'CR_AlbumArticles';
    const CATCH_BOOK_TAG='CATCH_BOOK_TAG';

    public static function cacheClear(){
        Cache::rm(self::CATCH_BANNER_KEY);
        Cache::rm(self::CR_INDEX.'1');
        Cache::rm(self::CR_INDEX.'2');
        Cache::rm(self::CR_INDEX.'3');
        Cache::rm(self::CR_INDEX.'4');
        Cache::rm(self::CR_INDEX.'5');
        Cache::rm(self::CR_INDEX.'6');
    }

    public static function getBookTags(){
        $tags =  Cache::get(self::CATCH_BOOK_TAG);
        if($tags){
            return $tags;
        }else{
            $tags = Db::query("select id,name from article_tags where parent_id = ".TagsInfo::$TAG_SHOP_ID.' order by order_by ');
            Cache::get(self::CATCH_BOOK_TAG,$tags,3600*24);
            return $tags;
        }
        return $allTags;
    }

    public static function queryBooksInShop($title,$index,$rows,$album_id){
        $sql = ' where c.parent_id = '.TagsInfo::$TAG_SHOP_ID;
        $param = [];
        // array_push($param,$album_id);
        if(!StringTool::isNull($title)){
            $sql = $sql." and a.title like ? ";
            array_push($param,'%'.$title.'%');
        }

        if(!StringTool::isNull($album_id) && intval($album_id)>0){
            $sql = $sql." and a.album_id = ? ";
//            dump($column_id);
            array_push($param,$album_id);
        }


        $count = Db::query('SELECT count(*) as total from article_info a 
                            INNER JOIN  album_info b on a.album_id = b.id
                            INNER JOIN  article_tags c on a.tag_id = c.id '.$sql,$param);
//
//        echo  'SELECT count(*) as total from article_info a
//                            LEFT JOIN  album_info b on a.album_id = b.id
//                            LEFT JOIN  article_tags c on a.tag_id = c.id '.$sql;

        $limit = ($index-1)*$rows;
        $sql = $sql.' order by a.publish_dated desc  limit '.$limit.','.($rows);

        $list = Db::query('SELECT a.*,b.name,b.`name` as column_name, c.`name` as tag_name from article_info a 
                            INNER JOIN  album_info b on a.album_id = b.id
                            INNER JOIN  article_tags c on a.tag_id = c.id 
                            -- where a.album_id = 2285
                            '.$sql,$param);

//        echo 'SELECT a.*,b.name,b.`name` as column_name, c.`name` as tag_name from article_info a
//                            INNER JOIN  album_info b on a.album_id = b.id
//                            INNER JOIN  article_tags c on a.tag_id = c.id
//                            '.$sql;

//        echo 'SELECT a.*,b.name,c.`name` as column_name, c.`name` as tag_name,e.content from article_info a
//                            LEFT JOIN article_content e on e.article_id = a.id
//                            LEFT JOIN  album_info b on a.album_id = b.id
//                            LEFT JOIN  article_tags c on a.tag_id = c.id
//                              '.$sql;

//        dump($count);

        $result = [];
        $result['records'] = $count[0]['total'];
        $result['page'] = $index;
        $result['total'] = intval($count[0]['total']/$rows)+1;

        $size = count($list);
        for($i = 0; $i < $size; $i++) {
//            echo $i.'<br/>';
//            if($list[$i]['book_id']!=0) {
//                foreach ($bookList as $book) {
//                    if ($list[$i]['book_id'] == $book['book_id']) {
//                        $list[$i]['book_id'] = $book['book_name'];
//                        break;
//                    }
//                }
//            }
            if($list[$i]['is_published']==1){
                $list[$i]['is_published'] = '发布';
            }
            else{
                $list[$i]['is_published'] = '关闭';
            }
        }
        $result['rows'] = $list;
        return $result;
    }

    public static function updateBookOrCreateShopData($data){
        $dataForArticleInfo = array();
        $dataForArticleInfo['title'] = $data['title'];
        $dataForArticleInfo['is_published'] = $data['is_published'];
        $dataForArticleInfo['publish_dated'] = $data['publish_dated'];
        $dataForArticleInfo['audio_address'] = $data['audio_address'];
        $dataForArticleInfo['book_address'] = $data['book_address'];
        $dataForArticleInfo['bak_address'] = $data['bak_address'];

        $dataForArticleInfo['des'] = $data['des'];
        $dataForArticleInfo['author'] = $data['author'];
        $dataForArticleInfo['album_id'] = $data['album_id'];
        $dataForArticleInfo['tag_id'] = $data['tag_id'];
        $dataForArticleInfo['img_url'] = $data['img_url'];
        $dataForArticleInfo['update_dated'] = date("Y-m-d H:i:s");

        // $data['content'] = strip_tags($data['content']);

        //更新数据库书店专辑下的书本信息
        if(empty($data['id'])){
            //创建书店新数据信息
            $entity = new ArticleInfoModel($dataForArticleInfo);
            $entity->save();
            $article_id =  $entity->getLastInsID();
            // $dataForArticleContent['article_id'] = $article_id ;
            self::updateOrCreateArticleContent($article_id,$data["content"]);
            return 1;//返回1表示创建新数据
        }else{
            //更新书店书籍信息
            $entity = new ArticleInfoModel();
            $entity->save($dataForArticleInfo,['id' => $data['id'] ]);
            self::updateOrCreateArticleContent($data['id'],$data["content"]);
            return 2;//返回2表示更新新数据
        }       
    }

    public static function deleteBookShopData(){

    }

    public static function updateOrCreateArticleContent($id,$content){
        $article = ArticleInfoContentModel::get(['article_id' => $id]);
        $data['content'] = $content;
         // dump($content);
        //  echo $content;
        //  return;
        if($article){
            //更新
            $entity = new ArticleInfoContentModel();
            $entity->save($data,['article_id' => $id ]);
        }
        else{
            //创建
            $data['article_id'] = $id;
            $entity = new ArticleInfoContentModel($data);
            $entity->save();
        }
    }

    public static function getBooksByTagId($id,$pageIndex){
        $books =  Cache::get('BOOKS_BY_TAG_ID'.$pageIndex);
//        $IMG_HOST = Config::get('IMG_HOST');
        $IMG_DOMAIN = Config::get('IMG_DOMAIN');
        if($books){
            return $books;
        }else{
            if($id) {
                $books = Db::table('article_info')
                    ->where('tag_id', $id)
                    ->page($pageIndex, 10)
                    ->order('publish_dated', 'desc')
                    ->select();
            }
            else
            {
                $books = Db::table('article_info')
                    ->page($pageIndex, 10)
                    ->order('publish_dated', 'desc')
                    ->select();
            }
            foreach ($books as &$item){
                $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
            }

//            Cache::set('BOOKS_BY_TAG_ID'.$pageIndex,$books,3600*24);
            return $books;

        }

    }

    public static function getAllBooksInShop(){
        $imgDomain = Config::get('IMG_DOMAIN');
        $books = Db::query("select a.audio_address as title,a.img_url,a.tag_id,a.id from article_info a
                INNER JOIN article_tags b on a.tag_id = b.id
                where b.parent_id = ? and a.is_published = 1 order by publish_dated desc",[TagsInfo::$TAG_SHOP_ID]);
        foreach($books as &$item){
            $item['img_url'] = $imgDomain.$item['img_url'];
        }
        return $books;
    }
    public static function getShopBookInfoByTag($tag_id){
        $imgDomain = Config::get('IMG_DOMAIN');
        $books = Db::query(" select audio_address as title,img_url,id from article_info where tag_id = ? and is_published = 1 order by publish_dated desc ",[$tag_id]);
        foreach($books as &$item){
            $item['img_url'] = $imgDomain.$item['img_url'];
        }
        return $books;
    }

    public static function getProfileById($id,$openId){
        $imgDomain = Config::get('IMG_DOMAIN');
        $books = Db::query(" select audio_address as title,a.img_url,a.des,a.book_address,a.id,c.name as album_name from article_info a
                        INNER join article_content b on a.id = b.article_id
                        INNER JOIN album_info c on a.album_id = c.id
                    where a.id = ? and a.is_published = 1 ",[$id]);
        foreach($books as &$item){
            $item['img_url'] = $imgDomain.$item['img_url'];
        }
        if($books){
            $books = $books[0];
        }
        return $books;
    }
}

?>