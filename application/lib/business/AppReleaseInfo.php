<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/12/8
 * Time: 下午2:40
 */


namespace app\lib\business;


use app\lib\model\AppReleaseModel;
use think\Db;

class AppReleaseInfo
{
    public static function query(){
        $list = Db::query('select * from app_release_info');
        return $list;
    }

    public static function updateOrCreated($data){
        //数据格式的转化
        $app = array();
        $app['name'] = $data['name'];
        $app['channel'] = $data['channel'];
        $app['package_name'] = $data['package_name'];
        $app['version_id'] = $data['version_id'];
        $app['title'] = $data['title'];
        $app['content'] = $data['content'];
        $app['platform'] = $data['platform'];
        $app['url'] = $data['url'];
        $app['des'] = $data['des'];

        if(intval($data['id'])>0){
            $entity = new AppReleaseModel();
            $entity->save($app,['id' => $data['id'] ]);
            return $data['id'];
        }
        else{

            $entity = new AppReleaseModel($app);
            $entity->save();
            return $entity->getLastInsID();
        }
    }


    public static function getInfoById($id){
        return AppReleaseModel::get(['id'=>$id]);;
    }

    public static function getInfoByPackagename($name,$channel){
        $model = new AppReleaseModel();
        $model = $model::where('package_name',$name);
        if(!empty($channel)){
            $model->where("channel",$channel);
        }
        else
        {
            $model->where("channel","other");
        }
        return $model->find();
    }

    public static function update($arr){

    }
}