<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/15
 * Time: 下午5:01
 */

namespace app\lib\business;


use app\lib\model\languageISOModel;
use app\lib\business\ArticleInfo;
use app\lib\common\StringTool;
use app\lib\business\AlbumInfo;
use app\lib\business\BooksInfo;
use app\lib\business\UserInfo;
use think\Db;
use think\Config;
use think\Model;

class FollowInfo
{
    public static function getAllInfo($openId){

        // http://localhost:1062/api/follow/v1/index?openId=a9e76fa20f9137c594ac674a95721901&language=zh
        // get banners
        //$list = ArticleInfo::getBanners();

        //已经订阅的
        $alreadyFollowList = UserInfo::getFollowAlbumByOpenId($openId);
        $allAlbumList = AlbumInfo::getAlbumByCache();

        //找到未订阅的
        $notSubscribedList = [];
        foreach ($allAlbumList as $item){
            if($item['is_published'] ==1){
                $item['follow_count'] = AlbumInfo::followCountDisplay($item['follow_count']);
                $item['status'] = 0;
                array_push($notSubscribedList,$item);
            }
        }
        foreach ($alreadyFollowList as &$item){
            $item['id'] = $item['album_id'];
        }
        $hotArticleList = ArticleInfo::getHotArticlesBy30Days();
        $results = array();
//        $results['banners'] =  $list;

        //bookList_id,默认是10；
        $bookList_id = 10;
        $banners = ComplexInfo::getBannerList();
        $results['notSubscribedList'] =  $notSubscribedList;
        $results['hotArticleList'] = $hotArticleList;
        $results['alreadyFollowList']  =  $alreadyFollowList;
        $results['banners']  =  $banners;
        return $results;

    }

    public static function allAlbumLists($openId){
        // $allAlbumList = AlbumInfo::getAlbumByCache();
        // $alreadyFollowList = UserInfo::getFollowAlbumByOpenId($openId);
        $alreadyFollowList = UserInfo::getFollowAlbumByOpenId($openId);
        $allAlbumList = AlbumInfo::getAlbumByCache();

        //找到未订阅的
        $notSubscribedList = [];
        foreach ($allAlbumList as $item){
            if($item['is_published'] ==1){
                $item['follow_count'] = AlbumInfo::followCountDisplay($item['follow_count']);
                $item['status'] = 0;
                array_push($notSubscribedList,$item);
            }
        }
        foreach ($alreadyFollowList as &$item){
            $item['id'] = $item['album_id'];
        }
        $results = array();
        $results['alreadyFollowList']  =  $alreadyFollowList;
        $results['notSubscribedList'] =  $notSubscribedList;
        return $results;
    }
    public static function notsubscribedList($openId,$language_id){

        $alreadyfollowlist = UserInfo::getFollowAlbumByOpenId($openId);
        $Notubscribedlist = AlbumInfo::getCacheAlbumByLanguage($language_id);
        foreach ($Notubscribedlist as &$item){
            $isFollow = false;
            foreach ($alreadyfollowlist as &$itemalreadyfollowitem){
                if($itemalreadyfollowitem['album_id'] == $item['id']){
                    $isFollow = true;
                    break;
                }
            }
            if($isFollow){
                $item['status'] = 1;
            }
            else
            {
                $item['status'] = 0;
            }
        }
        
        return $Notubscribedlist;
    }

    public static function getColumnInfo($columnId,$openId,$language_id){

        $columnInfo = AlbumInfo::getCacheAlbumByIdAndLanguage($columnId,$language_id);
        if($columnInfo){
            $columnInfo['status'] = UserInfo::isFollow($columnInfo['id'],$openId)==true?1:0;
        }

        $videolist = BibleVideoInfo::GetVideoListByColoumnId($columnId,$language_id);
        $result = array();
        $result['columnInfo']  =  $columnInfo;
        $result['videoList'] =  $videolist;

        return $result;
    }

    public static function searchAlbum($searchValue,$pageIndex,$user_id){
        $msg = Db::query("select a.* from album_info a where  a.name like ?",['%'.$searchValue.'%']);
        $IMG_HOST = Config::get('IMG_HOST');
        foreach ($msg as &$item){
            $item['img_url'] = $IMG_HOST.$item['img_url'];
            $status = Db::table('user_ablum_info')->where('album_id',$item['id'])->where('user_id',$user_id)->find();
            if($status){
                $item['status'] = 1;
            }else{
                $item['status'] = 0;
            }
        }
        return $msg;
    }

}

?>