<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/15
 * Time: 下午5:01
 */

namespace app\lib\business;

use app\lib\model\ArticleInfoModel;
use app\lib\model\ArticleInfoContentModel;
use app\lib\common\StringTool;
use app\lib\common\TimeHelper;
use app\lib\business\AlbumInfo;
use think\Db;
use think\Config;
use think\Cache;
use think\Model;

class ArticleInfo
{

    const CATCH_BANNER_KEY='CATCH_BANNER_KEY';
    const CR_INDEX='CR_INDEX';
    const CR_AlbumArticles = 'CR_AlbumArticles';
    const CR_GOSPELArticles = 'CR_GOSPEL';

    public static function cacheClear(){
        Cache::rm(self::CATCH_BANNER_KEY);
        Cache::rm(self::CR_INDEX.'0');
        Cache::rm(self::CR_INDEX.'1');
        Cache::rm(self::CR_INDEX.'2');
        Cache::rm(self::CR_INDEX.'3');
        Cache::rm(self::CR_INDEX.'4');
        Cache::rm(self::CR_INDEX.'5');
        Cache::rm(self::CR_INDEX.'6');
    }

    public static function getArticleLists($pageIndex){
        $result =  Cache::get(self::CR_INDEX.$pageIndex);
        $timeHelper = new TimeHelper();
    
        if ($result)
        {
            return $result;
        }
        else
        {
            $rows = 20;
            $limit = ($pageIndex-1)*$rows;

            $imgDomain = Config::get('IMG_DOMAIN');
            $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
            $IMG_HOST = Config::get('IMG_HOST');
            $result = Db::query("select a.title,a.browse,a.share_count,a.is_top,a.love_count,a.publish_dated,a.des,b.`name` as album_name,b.img_url as album_img_url,a.img_url,a.id,a.album_id from article_info a
                            INNER join album_info b on a.album_id = b.id 
                            WHERE b.is_published = 1 and a.is_published = 1 and a.publish_dated < now()
                            ORDER BY a.publish_dated desc LIMIT ?,?",[$limit,$rows]);
            $time1 = date('Y-m-d H:i:s');
            $shopBook = AlbumInfo::queryShopBookAll();

            foreach ($result as &$item){
                if($item['img_url']){
                    $item['img_url'] = $imgDomain.$item['img_url'];
                }else{
                    $radNum = rand(1,10);
                    $item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
                }
                if($item['album_img_url']){
                    $item['album_img_url'] = $IMG_HOST.$item['album_img_url'];
                }else{
                    $radNum = rand(1,10);
                    $item['album_img_url']=$Img_URL_DEFAULT.$radNum.'_min_.jpg';
                }
                $item['browse'] = self::dispalyBrowseCount($item['browse']);
                $time = $timeHelper->compareDate($time1,$item['publish_dated']);
                $item['publish_dated']  = $time;
                $item['is_shop'] = 0;

                foreach ($shopBook as $shop){
                    if($item['album_id'] == $shop['id'] ){
                        $item['is_shop'] = 1;
                        break;
                    }
                }
            }
            Cache::set(self::CR_INDEX.$pageIndex,$result,60*5);
            return $result;
        }
    }

    // 一周排行
    public static function getReakLists($begDate,$endDate){
        $timeHelper = new TimeHelper();
        $pageIndex = 1;

        $rows = 20;
        $limit = ($pageIndex-1)*$rows;

        $shopBook = AlbumInfo::queryShopBookAll();
        $shopId = "0";
        foreach ($shopBook as $shop){
            $shopId = $shopId." ,".$shop['id'];
        }

        $imgDomain = Config::get('IMG_DOMAIN');
        $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
        $IMG_HOST = Config::get('IMG_HOST');
        $result = Db::query("select a.title,a.browse,a.share_count,a.is_top,a.love_count,a.publish_dated,a.des,b.`name` as album_name,b.img_url as album_img_url,a.img_url,a.id,a.album_id from article_info a
                            INNER join album_info b on a.album_id = b.id 
                            WHERE b.is_published = 1 and a.is_published = 1
                            and a.publish_dated>=? and a.publish_dated<? and a.album_id not in (".$shopId.")
                            ORDER BY a.browse desc LIMIT ?,?",[$begDate,$endDate,$limit,$rows]);

        $time1 = date('Y-m-d H:i:s');
        $shopBook = AlbumInfo::queryShopBookAll();
        foreach ($result as &$item){
            if($item['img_url']){
                $item['img_url'] = $imgDomain.$item['img_url'];
            }else{
                $radNum = rand(1,10);
                $item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
            }
            if($item['album_img_url']){
                $item['album_img_url'] = $IMG_HOST.$item['album_img_url'];
            }else{
                $radNum = rand(1,10);
                $item['album_img_url']=$Img_URL_DEFAULT.$radNum.'_min_.jpg';
            }
            $item['browse'] = self::dispalyBrowseCount($item['browse']);
            $time = $timeHelper->compareDate($time1,$item['publish_dated']);
            $item['publish_dated']  = $time;
            $item['is_shop'] = 0;
        }
        return $result;
    }

    public static function dispalyBrowseCount($browseCount){
        $browseCount = intval($browseCount);
        if($browseCount>1000){
            $browseCount = round($browseCount/1000,1);
            $browseCount = $browseCount.'K';
            return $browseCount;
        }else{
            return $browseCount;
        }
    }

    public  static function getArticleListsByAlbumId($id,$pageIndex){
        $imgDomain = Config::get('IMG_DOMAIN');
        $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
        $list = Db::table('article_info')
            ->where('album_id',$id)
            ->page($pageIndex,50)
            ->order('publish_dated', 'desc')
            ->select();

        $shopBook = AlbumInfo::queryShopBookAll();

        foreach ($list as &$item){
            if($item['img_url']){
                $item['img_url'] = $imgDomain.$item['img_url'];
            }else{
                $radNum = rand(1,10);
                $item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
            }

            $item['is_shop'] = 0;

            foreach ($shopBook as $shop){
                if($item['album_id'] == $shop['id'] ){
                    $item['is_shop'] = 1;
                    break;
                }
            }
        }
        return $list;
    }

    public static function setImageHost($list,$newList){
        foreach($list as $item){
            if ($item['img_url']!=""){
                $item['img_url'] = C('CReadFileAddress').$item['img_url'];
            }
            if ($item['main_img_url']!=""){
                $item['main_img_url'] = C('CReadFileAddress').$item['main_img_url'];
            }
            else
            {
                $rad = rand(1,10);
                $item['main_img_url'] = C('CReadFileAddress')."/Uploads/images/default/".$rad.'.jpg';
            }
            if ($item['browse']!=""){
                $item['browse'] = '阅读次数：'.$item['browse'];
            }

            if($item['author']==''){
                $item['author'] = '佚名';
            }

            $item['m_url'] = C('ApiPhone')."/api/p_".$item["id"].'.html';

            $is_exist = false;
            foreach($newList as $sitem){
                if($sitem["id"]==$item["id"]){
                    $is_exist = true;
                }
            }
            if($is_exist==false)
            {
                array_push($newList,$item);
            }
        }
        return $newList;
    }

    public static function getArticleInfoById($id,$user_id){
        $msg = Db::query("select id,title,des,browse,love_count,share_count,album_id from article_info where id =?",[$id]);
        $msg[0]['browse'] = self::dispalyBrowseCount($msg[0]['browse']);
        // dump($msg);
        // return;
        // $msg['album_info'] = Db::query("select a.* from album_info a inner join article_info b on b.album_id = a.id where b.id =?",[$id]);
        // $status = Db::table('user_book_info')->where('article_book_id',$id)->where('user_id',$user_id)->find();
        return $msg;
    }

	public static function getArticleListsByMostBrowse($pageIndex){
//    	$lists = Db::query("select * from article_info where  DATEDIFF(NOW(),publish_dated)<32
//								and DATEDIFF(NOW(),publish_dated)>2
//								order by browse desc limit ?,10 ",[($pageIndex-1)*10]);
		$lists = Db::query("select a.title,a.browse,a.share_count,a.is_top,a.love_count,a.publish_dated,a.des,b.`name` as album_name,b.img_url as album_img_url,a.img_url,a.id,a.album_id from article_info a
                            INNER join album_info b on a.album_id = b.id 
                            WHERE b.is_published = 1 and a.is_published = 1 
                            and DATEDIFF(NOW(),a.publish_dated)<32
                            and DATEDIFF(NOW(),a.publish_dated)>2
                            ORDER BY a.browse desc LIMIT ?,10",[($pageIndex-1)*10]);
		$timeHelper = new TimeHelper();
		$time1 = date('Y-m-d H:i:s');
		$imgDomain = Config::get('IMG_DOMAIN');
		$Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
		$IMG_HOST = Config::get('IMG_HOST');

		foreach ($lists as &$item){
			if($item['img_url']){
				$item['img_url'] = $imgDomain.$item['img_url'];
			}else{
				$radNum = rand(1,10);
				$item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
			}
			if($item['album_img_url']){
				$item['album_img_url'] = $IMG_HOST.$item['album_img_url'];
			}else{
				$radNum = rand(1,10);
				$item['album_img_url']=$Img_URL_DEFAULT.$radNum.'_min_.jpg';
			}
			$item['browse'] = self::dispalyBrowseCount($item['browse']);
			$time = $timeHelper->compareDate($time1,$item['publish_dated']);
			$item['publish_dated']  = $time;
			$item['is_shop'] = 0;
		}
		return $lists;
	}

    public static function getArticleOtherInfo($id,$user_id,$album_id){
        $msg = Db::query("select id,browse,love_count,share_count from article_info where id =?",[$id]);
        if(count($msg)) {
            $msg[0]['browse'] = self::dispalyBrowseCount($msg[0]['browse']);
            $msg[0]['is_follow'] = 0; // 0 表示未订阅 1 表示已经订阅
            $msg[0]['is_love'] = 0; // 0 表示未收藏 1 表示已经收藏
            $isFollow = Db::query("select 1 from user_ablum_info where album_id = ? and user_id = ?", [$album_id, $user_id]);
            if (count($isFollow) > 0) {
                $msg[0]['is_follow'] = 1;
            }
            $isLove = Db::query("select 1 from user_book_info where article_book_id = ? and user_id = ?", [$id, $user_id]);
            if (count($isLove) > 0) {
                $msg[0]['is_love'] = 1;
            }

            $albumInfo = Db::query("select author as album_author,follow_count as album_follow_count from `album_info` where id = ?", [$album_id]);
            $msg[0]['album_author'] = '';
            $msg[0]['album_follow_count'] = 0;
            if (count($albumInfo) > 0) {
                $msg[0]['album_author'] = $albumInfo[0]['album_author'];
                $msg[0]['album_follow_count'] = $albumInfo[0]['album_follow_count'];
            }

            $msg = $msg[0];
        }
        else
        {
            $msg ="{}";
        }
        return $msg;
    }

    //收藏文章
    public static function addArticleToMyLove($data){
        $result = Db::table('user_book_info')
            ->where('article_book_id',$data['article_book_id'])
            ->where('type','2')
            ->where('user_id',$data['user_id'])
            ->find();
        if($result){
            $msg = 0;
            return $msg;
        }else{
            $insertDate = [];
            $insertDate['article_book_id'] = $data['article_book_id'];
            $insertDate['type'] = $data['type'];
            $insertDate['user_id'] = $data['user_id'];
            $msg = Db::name('user_book_info')
                ->data($insertDate)
                ->insert();
            Db::execute("update article_info set love_count = love_count+1 where id = ? ",[$data['article_book_id']]);
            return $msg;
        }
    }

    //从收藏文章中删除
    public static function removeArticleFromMyLove($data,$love_count){

        if($data['user_id']) {
            //没有删除，返回数据0；删除返回删除条数
            $resualt = Db::table('user_book_info')
                ->where('article_book_id', $data['article_book_id'])
                ->where('type', 2)
                ->where('user_id', $data['user_id'])
                ->delete();
            Db::execute("update article_info set love_count = love_count-1 where id = ? and love_count>0", [$data['article_book_id']]);
            return $resualt;
        }
        else {
            return 0;
        }
    }

    //检查文章是否在收藏中
    public static function checkArtIfInLove($id,$user_id){
        $isInShelf = Db::table('user_book_info')
            ->where('article_book_id',$id)
            ->where('type','2')
            ->where('user_id',$user_id)
            ->where('user_id','<>',-1)
            ->find();
        if($isInShelf){
            return 1;//表示已收藏
        }else{
            return 0;
        }
    }

    //从收藏中删除文章
    public static function getMyLovedArticles($data){
        $imgDomain = Config::get('IMG_DOMAIN');
        $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
//        $IMG_HOST = Config::get('IMG_HOST');
//        $resualt = Db::view('user_book_info','user_id,article_book_id')
//            ->view('article_info','title,img_url,des','article_info.id=user_book_info.article_book_id')
//            ->where('user_book_info.user_id',$data['user_id'])
//            ->where('user_book_info.type',$data['type'])
//            ->where('user_id','<>','-1')
//            ->page($data['pageIndex'],10)
//            ->select();
        $startPosition = ($data['pageIndex']-1)*10;
        $resualt = Db::query("select b.id,a.user_id,a.article_book_id,b.title,b.des,b.img_url,c.`name` as album_name,c.img_url as album_img_url,c.id as album_id,c.img_url as album_img_url from user_book_info a
                                    inner join article_info b on a.article_book_id = b.id
                                    INNER join album_info c on b.album_id = c.id 
                                    where a.user_id !=-1 and a.type =? and a.user_id =? order by a.id desc  limit ?,200",[$data['type'],$data['user_id'],$startPosition]);

//        $result = Db::query("select a.title,a.browse,a.share_count,a.is_top,a.love_count,a.publish_dated,a.des,b.`name` as album_name,b.img_url as album_img_url,a.img_url,a.id,a.album_id from article_info a
//                            INNER join album_info b on a.album_id = b.id
//                            WHERE b.is_published = 1 and a.is_published = 1 and a.publish_dated < now()
//                            ORDER BY a.publish_dated desc LIMIT ?,?",[$limit,$rows]);

        if($resualt){

            $shopBook = AlbumInfo::queryShopBookAll();
            foreach ($resualt as &$item){
                if($item['album_img_url']){
                    $item['album_img_url'] = $imgDomain.$item['album_img_url'];
                }

                if($item['img_url']){
                    $item['img_url'] = $imgDomain.$item['img_url'];
                }else{
                    $radNum = rand(1,10);
                    $item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
                }

                $item['is_shop'] = 0;

                foreach ($shopBook as $shop){
                    if($item['album_id'] == $shop['id'] ){
                        $item['is_shop'] = 1;
                        break;
                    }
                }
            }

            return $resualt;
        }else{
            $resualt = '';
            return $resualt;
        }


    }

    public static function updateShareCount($id){
        $msg = Db::execute("update article_info set share_count = share_count+1 where id = ?",[$id]);
        return $msg;
    }

    public static function getVideoSrc(){
       $list =  Cache::get(self::CR_GOSPELArticles);
        if ($list)
        {

        }
        else{
            $list =  Db::query("SELECT a.id ,b.title,b.audio_address from album_info a inner join article_info b on a.id = b.album_id where a.id = 1654");
        }
        $index = rand(0,count($list)-1);
        Cache::set(self::CATCH_BANNER_KEY,$list,60*60*24);
        return $list[$index];
    }

    public static function getBanners(){
        $list =  Cache::get(self::CATCH_BANNER_KEY);
        if ($list)
        {
            return $list;
        }
        else
        {
            $imgDomain = Config::get('IMG_DOMAIN');
            $list = Db::query("SELECT a.id,a.title,a.address,a.img_url, a.des as nav_path FROM video_info a
                INNER join album_info b on a.album_id = b.id
                WHERE a.is_published = 1 and a.publish_dated<now() and b.id = 2272 order by a.publish_dated desc");
            foreach ($list as &$item){
                $item['img_url'] = $imgDomain.$item['img_url'];
            }
            Cache::set(self::CATCH_BANNER_KEY,$list,60*60*24);
            return $list;
        }
    }

    public static function query($title,$index,$rows,$album_id,$isAll){

        $sql = '  ';

        if(StringTool::isNull($isAll)==false && $isAll==1){
            $sql = ' where 1 = 1 ';
        }
        else{
            $sql = ' where a.album_id not in( select name from article_tags where parent_id ='.TagsInfo::$TAG_BOOK_AlBUM_ID.' )';
        }
        $param = [];
        // array_push($param,$album_id);
        if(!StringTool::isNull($title)){
            $sql = $sql." and a.title like ? ";
            array_push($param,'%'.$title.'%');
        }

        if(!StringTool::isNull($album_id) && intval($album_id)>0){
            $sql = $sql." and a.album_id = ? ";
            array_push($param,$album_id);
        }

        if(!StringTool::isNull($title)){
            $sql = $sql." and a.title like ? ";
            array_push($param,'%'.$title.'%');
        }

        $count = Db::query('SELECT count(*) as total from article_info a 
                            LEFT JOIN  album_info b on a.album_id = b.id
                            LEFT JOIN  article_tags c on a.tag_id = c.id '.$sql,$param);


        $limit = ($index-1)*$rows;
        $sql = $sql.' order by a.publish_dated desc  limit '.$limit.','.($rows);

        $list = Db::query('SELECT a.*,b.name,b.`name` as column_name, c.`name` as tag_name from article_info a 
                            LEFT JOIN  album_info b on a.album_id = b.id
                            LEFT JOIN  article_tags c on a.tag_id = c.id 
                            '.$sql,$param);

        $result = [];
        $result['records'] = $count[0]['total'];
        $result['page'] = $index;
        $result['total'] = intval($count[0]['total']/$rows)+1;

        $size = count($list);
        for($i = 0; $i < $size; $i++) {
            if($list[$i]['is_published']==1){
                $list[$i]['is_published'] = '发布';
            }
            else{
                $list[$i]['is_published'] = '关闭';
            }
        }
        $result['rows'] = $list;
        return $result;
    }

    public static function updateOrCreated($data){

        $data['audio_address'] = str_replace("jfilm.oss-cn-shanghai.aliyuncs.com", "jfilm.biblemooc.net", $data['audio_address']);
        $data['audio_address'] = str_replace("smallprogram.oss-cn-shanghai.aliyuncs.com", "smalmqwq.biblemooc.net", $data['audio_address']);

        //数据格式的转化
        $video = array();
        $video['title'] = $data['title'];
        $video['is_published'] = $data['is_published'];
        $video['is_top'] = $data['is_top'];
        $video['publish_dated'] = $data['publish_dated'];
        $video['audio_address'] = $data['audio_address'];
        $video['book_address'] = $data['book_address'];
        $video['des'] = $data['des'];
        $video['author'] = $data['author'];
        $video['album_id'] = $data['album_id'];
        $video['img_url'] = $data['img_url'];
        $video['update_dated'] = date("Y-m-d H:i:s");
        if(in_array('tag_id',$data)){
            $video['tag_id'] = $data['tag_id'];
        }
        else{
            $video['tag_id'] = 0;
        }
        $video['bak_address'] = $data['bak_address'];

        if(intval($data['id'])>0){
            $entity = new ArticleInfoModel();
            $entity->save($video,['id' => $data['id'] ]);
            ArticleInfo::updateArticleContent($data['id'],$data['content']);
            ArticleInfo::removeArticleContentById($data['id']);
            return $data['id'];
        }
        else{

            $video['browse'] = 0;
            $video['id'] = 0;
            $entity = new ArticleInfoModel($video);
            $entity->save();
            $id =  $entity->getLastInsID();
            ArticleInfo::updateArticleContent($id,$data['content']);
            ArticleInfo::removeArticleContentById($id);
            return $id;
        }
        self::cacheClear();

    }

    public static function updateArticleContent($id,$content){
        $article = ArticleInfoContentModel::get(['article_id' => $id]);
        $data['content'] = $content;
       
        if($article){
            $entity = new ArticleInfoContentModel();
            $entity->save($data,['article_id' => $id ]);
        }
        else{

            $data['article_id'] = $id;
            $entity = new ArticleInfoContentModel($data);
            $entity->save();
        }
    }

    //根据 专辑名称 获取专辑Id
    public static function getAblumnByName(){

    }

    public static function datadapter($data,$language_id){
        $imgDomain = Config::get('IMG_DOMAIN');
        $albumnList = AlbumInfo::getAlbumByCache();
        $currentDate=strtotime(date("Y-m-d H:i"));
        $currentHours = date("H");
        foreach($data as &$item){

            if ($item['img_url']!=""){
                $item['img_url'] = $imgDomain.$item['img_url'];
            }

            foreach ($albumnList as $album){
                if($album['id'] == $item["album_id"]){
                    $item['album_name'] = $album['name'];
                    $item['album_img_url'] = $album['img_url'];
                    break;
                }
            }

            $item['browse'] = self::browseDisplay($language_id,$item['browse']);
            $item['id'] = intval($item['id']);
            $item['display_video'] = 0;

            $startdate=strtotime($item['publish_dated']);

            $days= round(($currentDate-$startdate)/3600/24) ;
            if($days<=0){
                $item['publish_dated'] = date(LanguageInfo::displaylanguage($language_id,'time_format'), $startdate);
            }
            else
            {
                $item['publish_dated'] = date(LanguageInfo::displaylanguage($language_id,'date_format'), $startdate); //self::toDateByLanguage($item['language_id'],$startdate);
            }

            $item['length'] = str_replace(":","′",$item['length']).'″';
        }
//        dump($data);
        return $data;
    }

    public static function getHotArticlesBy30Days(){
        $currentDate = date("Y-m-d h:i:s");
        $lastMonth = date("Y-m-d",strtotime("-30 day"));

        $list = Db::query("select id,title,publish_dated,`author`,`des`,`img_url`,browse,love_count,album_id from article_info "
            . "where is_published = 1 and publish_dated > '".$lastMonth."' and publish_dated<'".$currentDate."' order by browse desc LIMIT 10");

        $albumList = AlbumInfo::getAlbumByCache();

        foreach($list as &$item){
            if ($item['img_url']!=""){
                $item['img_url'] = Config::get('IMG_DOMAIN').$item['img_url'];
            }

            foreach ($albumList as $album){
                if($album['id'] == $item["album_id"]){
                    $item['album_name'] = $album['name'];
                    $item['album_img_url'] = $album['img_url'];
                    break;
                }
            }
            $item['id'] = intval($item['id']);
            // $item['browse'] = self::browseDisplay($item['browse']);
            $item['browse'] = self::dispalyBrowseCount($item['browse']);

        }
        return $list;
    }

    public static function getVideoList($tagsId,$pageindex,$openId,$language_id){

        // 初始化值
        if(StringTool::hasValue($tagsId) == false){
            $tagsId = 0;
        }
        else{
            $tagsId = intval($tagsId);
        }

        if($tagsId==15){
            $tagsId = 0;
        }

        if(StringTool::hasValue($pageindex) == false){
            $pageindex = 1;
        }
        else{
            $pageindex = intval($pageindex);
        }

        $albumList = [];
        if($pageindex==1){
            $albumList = UserInfo::getFollowAlbumByOpenId($openId);
//            dump($albumList);
        }

        if($tagsId>-1 && $tagsId!=14){
            $result = self::getVideoListByTags($tagsId,$pageindex,$language_id);
            $result['label'] = 'tags';
        }
        else
        {
            if(count($albumList)>0){
                $result = self::getVideoListByAlbumList($albumList,$pageindex,$language_id);
                $result['label'] = 'follow';
            }
            else
            {
                $tagsId = 0;
                $result = self::getVideoListByTags($tagsId,$pageindex,$language_id);
                $result['label'] = 'tags';
            }
        }
        $result['data'] = self::datadapter($result['data'],$language_id);
        if($pageindex<=1) {
            if(count($result['data'])>5){
                $gospel = self::getGospel($language_id);
                array_splice($result['data'], 4, 0, [$gospel]);
            }
//            $result['gospel'] = self::getGospel($language_id);
        }
//        dump($result);
        return $result;
    }

    public static function getGospel($language_id){
        $where = "";
        $param = [];
        if($language_id>0){
            $where = $where." and a.language_id = ? ";
            array_push($param,$language_id);
        }
        $sql = " SELECT a.id,a.title,a.address,a.author,a.length,a.browse,a.publish_dated,a.des,a.img_url,a.album_id,'isGospel' as c
from video_info a where a.is_published = 1 and a.album_id=1654 and a.publish_dated<NOW() ";

        $list = Db::query($sql.$where,$param);
        $list = self::datadapter($list,$language_id);

        if(count($list)>0){
            return $list[round(1,count($list))-1];
        }
        else {
            return $list;
        }
    }

    public static function getPlay($id,$openId,$language_id){

        $result = [];

        //获取视频信息
        $videoInfo = BibleVideoInfo::getVideoInfoById($id);

        //获取对应标签下的视频
        $list = [];
        $album = null;
        if($videoInfo){
            $list = BibleVideoInfo::getVideoListByTags($videoInfo['tag_id'],1,$language_id);
            $list['data'] = self::datadapter($list['data'],$language_id);
            $album = AlbumInfo::getCacheAlbumById($videoInfo['album_id']);
            // 判断订阅
            if($album){
                $album['status'] = UserInfo::isFollow($album['id'],$openId)==true?1:0;
                $album['follow_count'] = AlbumInfo::followCountDisplay($language_id,$album['follow_count']);
//                $album['img_url'] = Config::get('IMG_DOMAIN').$album['img_url'];
            }

            $count = count($list['data']);
            $gospel = self::getGospel($language_id);
            if($count>5){
                array_splice($list['data'], 4, 0, [$gospel]);
            }
            else
            {
                array_push($list['data'],[$gospel]);
            }
        }
        $result["albumInfo"] = $album;
        $videoInfo['browse'] = self::browseDisplay($language_id,$videoInfo['browse']+1);
        $result["videoInfo"] = $videoInfo;
        $result["data"] = $list['data'];

        return $result;
    }

    public static function getVideoListByTags($tagId,$pageIndex,$language_id){

        $limit = "";
        $result = [];
        $where = "";
        $param = [];

        if($pageIndex <= 1 ){
            $pageIndex  = 1;
            $limit = 0;
            $limit = ' limit '.$limit.','.(10);
        }
        else
        {
            $limit = ($pageIndex-1)*10;
            $limit = ' limit '.$limit.','.(10);
        }

        if($tagId>0){
            $where = $where." and a.tag_id = ? ";
            array_push($param,$tagId);
        }
        if($language_id>0){
            $where = $where." and a.language_id = ? ";
            array_push($param,$language_id);
        }

        $sum = Db::query(" SELECT count(1) as t from video_info a where a.is_published = 1 and a.publish_dated<NOW() ".$where, $param);

//        $sql = " SELECT count(1) as t from video_info a where a.is_published = 1 and a.publish_dated<NOW() and a.tag_id = ".$tagId." and a.language_id = ".$language_id;
//        dump($sql);

        $result["total"] = intval($sum[0]['t']);
        $result["pageindex"] = $pageIndex;

        $list = Db::query(" SELECT a.id,a.title,a.address,a.author,a.length,a.browse,a.publish_dated,a.des,a.img_url,a.album_id,' ' as c
                 from video_info a where a.is_published = 1 and a.publish_dated<NOW() ".$where." order by publish_dated desc ".$limit, $param);
        $result["data"] = $list;
        return $result;
    }

    public static function browseDisplay($browse){
        $browse = intval($browse);
        if($browse<10000){
            return $browse;// '次观看';
        }else{
            return round(($browse/10000),1); // "万人观看";
        }
    }

    public static function getVideoListByAlbumList($albumList,$pageIndex,$language_id){

        $album = "0";
        $result = [];

        if($pageIndex <= 1 ){
            $pageIndex  = 1;
            $limit = 0;
            $limit = ' limit '.$limit.','.(10);
        }
        else
        {
            $limit = ($pageIndex-1)*10;
            $limit = ' limit '.$limit.','.(10);
        }

        foreach ($albumList as $item){
            $album = $album.','.$item['album_id'];
        }

//        $sql = " SELECT a.id,a.title,a.address,a.author,a.length,a.browse,a.publish_dated,a.des,a.img_url,a.album_id,' ' as c
//                 from video_info a where a.is_published = 1 and publish_dated<NOW() and a.album_id in (".$album.") order by publish_dated desc ".$limit;

        $sum = Db::query(" SELECT count(1) as t
                 from video_info a where a.is_published = 1 and publish_dated<NOW() and a.album_id in (".$album.") and a.language_id =?  ",[$language_id]);

        $result["total"] = intval($sum[0]['t']);
        $result["pageindex"] = $pageIndex;

        $list = Db::query(" SELECT a.id,a.title,a.address,a.author,a.length,a.browse,a.publish_dated,a.des,a.img_url,a.album_id,' ' as c
                 from video_info a where a.is_published = 1 and publish_dated<NOW() and a.album_id in (".$album.") and a.language_id =? order by publish_dated desc ".$limit,[$language_id]);
        $result["data"] = $list;

        return $result;
    }

    public static function GetVideoListByColoumnId($albumId,$language_id){
        $list = Db::query(" SELECT a.id,a.title,a.address,a.author,a.length,a.browse,a.publish_dated,a.des,a.img_url,a.album_id,' ' as c
                 from video_info a where a.is_published = 1 and a.album_id=? and a.publish_dated<NOW() order by publish_dated desc limit 20 ", [$albumId]);


        $list = self::datadapter($list,$language_id);
        return $list;
    }

    //根据ID来获取视频播放页面信息
    public static function getInfoById($id){
        $article = Db::query("select * from article_info  where id = ?",[$id]);
        if(count($article) >0 ) {
            $article = $article[0];
            if ($article['img_url']!=""){
                $article['img_url'] = Config::get('IMG_DOMAIN').$article['img_url'];
            }
            $browse = $article['browse'];
            self::updateBrowseById($id, $browse);
            $article['content'] = self::getArticleContentById($id);

        }
        return $article;
    }

    //删除本次磁盘中的文章内容
    public static function removeArticleContentById($id){
        $filePath = $_SERVER['DOCUMENT_ROOT'].'/html/'.$id.'.txt';
        if(file_exists($filePath)){
            unlink($filePath);
        }
    }

    //根据ID文章的内容信息
    public static function getArticleContentById($id){
        $filePath = $_SERVER['DOCUMENT_ROOT'].'/html/'.$id.'.txt';
        if(!file_exists($filePath)){
            $article = Db::query("select content from article_content where article_id = ?",[$id]);
            if(count($article) >0 ) {
                $article = $article[0];
            }
            $myfile = fopen($filePath, "w") or die("Unable to open file!");
            fwrite($myfile, $article['content']);
            fclose($myfile);
            return $article['content'];
        }
        else
        {
            return file_get_contents($filePath);
        }
    }

    //更加id查询视频新
    public static function getGospelInfo(){

        $sql = " SELECT a.id,a.title,a.video_address,a.video_author,a.video_length,a.publish_dated,a.video_des,a.img_url,b.`name` as abulum_name,b.`image_url` as album_img_url,b.`name` as album_name,a.video_ablum_id,' ' as c,a.browse from small_program_info a
left JOIN app_christian_info b on a.video_ablum_id = b.id 
WHERE b.id = 1654 ";
        $db = M();
        $list = $db->query($sql);
        if(count($list)>0){
//            $list = $list[0];
            $list[0]['img_url'] = C('SmallprogramUrl').$list[0]['img_url'];
            $list[0]['album_img_url'] = C('SmallprogramUrl').$list[0]['album_img_url'];
            $list[0]['browse'] = Smallprogram::browseDisplay($list[0]['browse']);
        }
        return $list;
    }

    //更新流量量
    public static function updateBrowseById($id,$browse){
        Db::table('article_info')->where('id', $id)->update(['browse' => $browse+1]);
    }

    public static function compareDate($title){
//        $timeHelper = new TimeHelper();
//        $currentDate = date("Y-m-d h:i:s");
        $lastMonth = date("Y-m-d",strtotime("-30 day"));
        $result = Db::table('article_info')->where('title',$title)->where('publish_dated','>',$lastMonth)->find();
        if($result){
            return 0;//表示30天内已存在同一标题的文章
        }else{
            return 1;
        }
    }

    public static function searchArticle($searchValue,$pageIndex){
        $IMG_HOST = Config::get('IMG_HOST');
        $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
        $position = ($pageIndex-1)*10;
        $lists = Db::query("select a.*,b.`name` as album_name,b.img_url as album_img_url,a.album_id from article_info a 
                                inner join album_info b on a.album_id = b.id 
                                where a.title like ? and a.album_id != 2275 and a.is_published = 1 order by a.publish_dated desc limit ?,50",['%'.$searchValue.'%',$position]);
        foreach($lists as &$item){
            if($item['album_img_url']){
                $item['album_img_url'] = $IMG_HOST.$item['album_img_url'];
            }

            if($item['img_url']){
                $item['img_url'] = $IMG_HOST.$item['img_url'];
            }else{
                $radNum = rand(1,10);
                $item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
            }
            $item['browse'] = self::dispalyBrowseCount($item['browse']);

        }
        $len = count($lists);
        if($len == 0){
            return $len;
        }else{
            return $lists;
        }
    }
}

?>