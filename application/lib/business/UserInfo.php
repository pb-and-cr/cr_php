<?php
namespace app\lib\business;

use app\lib\common\StringTool;
use app\lib\model\UserInfoModel;
use think\Db;
use app\lib\common\Json;
use app\lib\common\Email;
use think\Session;
use think\Cache;
use app\lib\business\AlbumInfo;

class UserInfo{

    const CATCH_USER_ALBUM_KEY='CAlbum';

    public static function getAdminUserList(){
        $userList = [
            ['name'=>'admin123','pwd'=>'garden123','roleId'=>2], // 2 表示超级管理员
            ['name'=>'ruth','pwd'=>'ruth123qaz','roleId'=>1],
            ['name'=>'tom','pwd'=>'qeru123','roleId'=>1],
            ['name'=>'julia','pwd'=>'julia98i','roleId'=>1],
            ['name'=>'jimmy','pwd'=>'jimmy963','roleId'=>1],
            ['name'=>'masion','pwd'=>'masion1542','roleId'=>1],
            ['name'=>'dev159','pwd'=>'garden123','roleId'=>1],
            ['name'=>'lucy','pwd'=>'ilovegod','roleId'=>1],
            ['name'=>'daniel','pwd'=>'ilovegod','roleId'=>1],
        ];
        return $userList;
    }

    public static function isSuperdministrator($userName){
        $userList = getAdminUserList();
        foreach ($userList as $item){
            if($item['name']==$userName){
                return $item['roleId'];
                break;
            }
        }
        return false;
    }


    public static function isAppUpdate(){
        if (Session::has('admin_role_id')) {
            if(Session::get('admin_role_id') == 2 ){
                return true;
            }
        }
        return false;
    }

    public static function isSuperAdministratorBySession(){
        if (Session::has('admin_role_id')) {
            if(Session::get('admin_role_id') == 1 || Session::get('admin_role_id') == 2 ){
                return true;
            }
        }
        return false;
    }

    public static function isOfflineAdministratorBySession(){
        if (Session::has('admin_role_id')) {
            return true;
        }
        return false;
    }

    public static function login($userName,$userPwd){
        foreach (UserInfo::getAdminUserList() as $user){
//            echo '---';
            if($user['name']==$userName && $userPwd == $user['pwd'] ){
                Session::set('admin_role_id', $user['roleId']);
                Session::set('admin_name', $userName);
//                        Db::name('admin_user')->update(
//                            [
//                                'last_login_time' => date('Y-m-d H:i:s', time()),
//                                'last_login_ip'   => $this->request->ip(),
//                                'id'              => $admin_user['id']
//                            ]
//                        );
                return true;
                break;
            }
        }
        return false;
    }

    public static function logout(){
        Session::delete('admin_role_id');
        Session::delete('admin_name');
    }


    //获取微信的openId
    public function getWeChatOpenId(){
//        http://localhost:1060/smallprogram/follow/getOpenId?code=003JbK9y1VCDRe0AGxay16hW9y1JbK9S

        $code = $this->_get("code");
//        echo $code;
        $appid = 'wxefaedf69c3a2332e';
        $appsecret = '1d406847001abb493751b25c4e44ec39';
        $url = 'https://api.weixin.qq.com/sns/jscode2session?appid='.$appid.'&secret='.$appsecret.'&js_code='.$code.'&grant_type=authorization_code';
//        echo $url;
        $weixin =  file_get_contents($url);//通过code换取网页授权access_token
        $jsondecode = json_decode($weixin); //对JSON格式的字符串进行编码
        $array = get_object_vars($jsondecode);//转换成数组
        $this->ajax($array);
    }

    // 获得用户订阅id礼拜
    public static function getFollowAlbumByOpenId($openId){
//        echo ' --- 01 -- '.$openId.'<br/>';
        if(StringTool::hasValue($openId) == false){
            return [];
        }

//        echo ' --- 02 -- <br/>';
        $list =  Cache::get(self::CATCH_USER_ALBUM_KEY.$openId);
        if ($list)
        {
            return $list;
        }
        else
        {
            $albumList = Db::query("SELECT b.album_id From user_info a
                INNER JOIN user_ablum_info b on a.id = b.user_id
                WHERE a.user_token = ?",[$openId]);
            Cache::set(self::CATCH_USER_ALBUM_KEY.$openId,$albumList,60*60);
            return $albumList;
        }
    }

    public static function isFollow($album_id,$openId){
        $list = self::getFollowAlbumByOpenId($openId);
        foreach ($list as $item){
            if($album_id == $item['album_id']){
                return true;
                break;
            }
        }
        return false;
    }

    public static function removeCacheByAlbumOpenId($openId){
        Cache::rm(self::CATCH_USER_ALBUM_KEY.$openId);
    }

    public static function getUserInfoByOpenId($openId){
        if(StringTool::hasValue($openId) == false){
            return [];
        }
        $userInfo =  Cache::get($openId);
        if ($userInfo)
        {
            return $userInfo;
        }
        else
        {
            Db::execute("INSERT INTO user_info(
                `password`,
                nickName,
                sex,
                email,
                mobile,
                user_token,
                password_attempts,
                is_active,
                head_icon,
                brief_personal_des,
                created_time,
                update_time,
                last_login_time,
                error_login_time,
                platform,
                role_id
                )
                SELECT  
                '' AS `password`,
                '' AS nickName,
                0 AS sex,
                '' AS email,
                '' AS mobile,
                ? AS user_token,
                0 AS password_attempts,
                1 AS is_active,
                '' AS head_icon,
                '' AS brief_personal_des,
                NOW() AS created_time,
                NOW() AS update_time,
                NOW() AS last_login_time,
                NOW() AS error_login_time,
                '' AS platform,
                0 AS role_id",[$openId]);
            $userInfo = UserInfoModel::get(['user_token'=>$openId]);;
            Cache::set($openId,$userInfo,60*60);
            return $userInfo;
        }
    }

    public static function removeCacheByUserOpenId($openId){
        Cache::rm($openId);
    }
}









