<?php
namespace app\lib\business;

use app\lib\model\AlbumInfoModel;
use think\Db;
use think\Cache;
use think\Config;
use app\lib\common\StringTool;
use app\lib\common\TimeHelper;
use app\lib\business\UserInfo;


class AlbumInfo{

    const CATCH_ALBUM_KEY='CATCH_ALBUM_KEY';

    //商城专辑ID
    public static function getAlbumByCache(){
        $list =  Cache::get(self::CATCH_ALBUM_KEY);
        if ($list)
        {
            return $list;
        }
        else
        {
//            $imgDomain = Config::get('IMG_DOMAIN');
            $IMG_HOST = Config::get('IMG_HOST');
            $list = Db::query(" SELECT * from album_info where is_published = 1 order by order_by ");
            foreach ($list as &$item){
//                $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
                $item['img_url'] = $IMG_HOST.$item['img_url'];
            }
            Cache::set(self::CATCH_ALBUM_KEY,$list,60*60*24);
            return $list;
        }
    }

    //从缓存中读取专辑
    public static function getCacheAlbumById($id){
        $list = self::getAlbumByCache();
        foreach($list as $item){
            if($item["id"] == $id ){
                $item['status'] = 0;
                return $item;
            }
        }
        return null;
    }

    //获取用户订阅的专辑
    public static function getAlbumInfoById($id,$user_id){
        $img_path = Config::get("IMG_HOST");
        
        $msg = Db::query("select * from album_info where id = ?",[$id]);
        // dump($msg[0]['id']);
        // return;
        $msg[0]['img_url'] = $img_path.$msg[0]['img_url'];
        $status = Db::table('user_ablum_info')->where('album_id',$msg[0]['id'])->where('user_id',$user_id)->find();
        if($status){
            $msg[0]['status'] = 1;
        }else{
            $msg[0]['status'] = 0;
        }
        return $msg;
    }

    public static function getCacheAlbumByLanguage($language_id){
        $list = self::getAlbumByCache();
        foreach($list as &$item){
            if($item["language_id"] == $language_id  ){
                $item['follow_count'] = self::followCountDisplay($language_id,$item['follow_count']);
            }
        }
        return $list;
    }

    public static function getCacheAlbumByIdAndLanguage($id,$language_id){
        $list = self::getAlbumByCache();
        foreach($list as $item){
            if($item["id"] == $id ){
                $item['follow_count'] = self::followCountDisplay($language_id,$item['follow_count']);
                return $item;
            }
        }
        return null;
    }

    public static function followCountDisplay($follow_count){
        $follow_count = intval($follow_count);
        return $follow_count;
    }

    public static function query($page,$rows){
        $sql = ' where 1 = 1 ';
        $param = [];

        $count = Db::query('select count(1) as total  from album_info a '.$sql,$param);

        if(empty($page)==false){
            $limit = ($page-1)*$rows;
            $sql = $sql.' order by a.order_by  limit '.$limit.','.($rows);
        }

        $list = Db::query('select a.* from album_info a '.$sql,$param);

        $result = [];
        $result['total'] = intval($count[0]['total']/$rows)+1;
        $result['page'] = $page;
        $result['records'] = 13;
        $result['rows'] = $list;
        return $result;
    }

    //获得可用的专辑列表(非商城)
    public static function queryAll(){

        $list = Db::query('select a.*  from album_info a ');
        return $list;
    }

    //获得可用的专辑列表(非商城)
    public static function queryFollowAlbum(){
        $list = Db::query("select  a.*  from album_info a 
where a.id not in ( select name from article_tags where parent_id = ?)",[TagsInfo::$TAG_BOOK_AlBUM_ID]);
        return $list;
    }

    //获得可用的专辑列表(商城)
    public static function queryShopBookAll(){

        $list = Db::query('select b.* from article_tags a 
inner join album_info b on a.name = b.id 
where a.parent_id  = ? ',[TagsInfo::$TAG_BOOK_AlBUM_ID]);
        return $list;
    }

    public static function queryById($id){
        $list = Db::query(' select a.* from album_info a
            WHERE a.id = ?',[$id]);
        return $list;
    }

    public static function updateOrCreated($data){
        //数据格式的转化
        $app = array();
        $app['name'] = $data['name'];
        $app['order_by'] = $data['order_by'];
        $app['img_url'] = $data['img_url'];
        $app['is_published'] = $data['is_published'];
        $app['publish_dated'] = $data['publish_dated'];
        $app['author'] = $data['author'];
        $app['remark'] = $data['remark'];
        $app['updated_at'] = (new TimeHelper())->getCurrentTime();
        Cache::rm(self::CATCH_ALBUM_KEY);
        if(intval($data['id'])>0){
            $entity = new AlbumInfoModel();
            $entity->save($app,['id' => $data['id'] ]);
            return $data['id'];
        }
        else
        {
            $app['browse'] = 0;
            $app['follow_count'] = 0;
            $entity = new AlbumInfoModel($app);
            $entity->save();
            return $entity->getLastInsID();
        }
    }

    //获得专辑信息
    public static function getInfoById($id){
        return AlbumInfoModel::get(['id'=>$id]);;
    }

    //获得该专辑下的其他文章
    public static function getAlbumOtherArticleList($albumId,$articleId){
        $list = Db::query("select id,title from article_info where album_id = ? and id < ? and is_published=1 and publish_dated<now() order by id desc LIMIT 4",[$albumId,$articleId]);
        return $list;
    }


    public static function delete($id){
        $tags = AlbumInfoModel::get($id);
        $tags->delete();
        Cache::rm(self::CATCH_ALBUM_KEY);
    }

    //订阅专辑
    public static function followAlbum($openId,$albumId){

        if(UserInfo::isFollow($albumId,$openId)==true){
            return ;
        }

        $userInfo = UserInfo::getUserInfoByOpenId($openId);
        Db::execute(" insert into user_ablum_info(user_id,album_id) value(?,?) ",[$userInfo['id'],$albumId]);
        Db::execute(" update album_info set follow_count = follow_count+1 WHERE id = ? ",[$albumId]);
        UserInfo::removeCacheByAlbumOpenId($openId);
    }

    //删除专辑
    public static function userRemoveAblum($openId,$albumId){
        $userInfo = UserInfo::getUserInfoByOpenId($openId);
        Db::execute("delete from user_ablum_info where user_id=? and album_id=?",[$userInfo['id'],$albumId]);
        Db::execute("update album_info set follow_count = follow_count-1 WHERE id = ? ",[$albumId]);
        UserInfo::removeCacheByAlbumOpenId($openId);
    }
}









