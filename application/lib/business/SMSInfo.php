<?php
namespace app\lib\business;

use app\lib\model\SMSModel;
use think\Db;
use think\Cache;
use think\Config;
use app\lib\common\StringTool;
use app\lib\common\sms\SmsSingleSender;

class SMSInfo{

    public static function query($number,$page,$rows){
        $sql = ' ';
        $param = [];

        if(StringTool::hasValue($number)==true){
            $sql = ' where number like ? ';
            array_push($param,'%'.$number.'%');
        }

        if(StringTool::hasValue($page)==true){
            $limit = ($page-1)*$rows;
            $sql = $sql.' order by created_date  desc limit '.$limit.','.($rows);
        }

//        dump($sql);
//        echo '<br/>';
//
//        echo 'select a.*,b.code as language_family_name,c.`name` as language_name  from video_tags a
//                        LEFT JOIN language_family_code b on a.language_family_id = b.id
//                        LEFT JOIN language_iso_info c on a.language_id = c.id  '.$sql;
//        echo '<br/>';

        $count = Db::query('SELECT count(*) as total from Sms a '.$sql,$param);

        $list = Db::query('select a.*  from Sms a '.$sql,$param);

        $result = [];
        $result['total'] = intval($count);
        $result['page'] = $page;
        $result['records'] = 10;
        $result['rows'] = $list;
        return $result;
    }

    public static function updateOrCreated($data){
        //数据格式的转化
        $app = array();
        $app['number'] = $data['number'];
        $app['created_date'] = date("Y-m-d H:i:s",time());
        $app['content'] = $data['content'];
        $app['template_name'] = $data['template_name'];

        if(intval($data['id'])>0){
            $entity = new TagsModel();
            $entity->save($app,['id' => $data['id'] ]);
            return $data['id'];
        }
        else
        {
            $numbers=explode(';',$app['number']);
            $contents=explode(';',$app['content']);
            $info = sprintf('%s主内平安，我是天父花园服务号后台的同工%s，之前看到您%s找教会的信息，不知道您还记得吗？请问您现在有去到教会吗，还需要我们帮助吗？您的微信我加不上你，如果您方便的话可以加一下您的微信？我的微信号是：%s。',$contents[0],$contents[1],$contents[2],$contents[3]);
            foreach($numbers as $item){
                $result = self::getSms($item,$contents[0],$contents[1],$contents[2],$contents[3]);
                $app['number'] = $item;
                $app['content'] = $info;
                $entity = new SMSModel($app);
                $entity->save();
            }
//            dump($numbers);
//            $entity = new SMSModel($app);
//            $entity->save();
            return 1;
        }
    }

    public static function getSms($phoneNumber,$userName,$myName,$userInfo,$wechatNubmer){
        //http://localhost:1066/api/ChristianReaderUser/getSms
        // if($this->request->isPost()){

        // }
        $appid = Config::get('offline_appid');
        $appkey = Config::get('offline_appkey');
        $templId = 68507;
        try {
            $sender = new SmsSingleSender($appid, $appkey);
            $params = [$userName,$myName,$userInfo,'您的微信我加不上你，如果您方便的话可以加一下您的微信？我的微信号是：'.$wechatNubmer];
            dump($params);
            $result = $sender->sendWithParam("86", $phoneNumber, $templId, $params, "", "", "");
            $rsp = json_decode($result);
            dump($rsp);
        } catch(\Exception $e) {
            echo var_dump($e);
        }
        return 0;
    }

    public static function getInfoById($id){
        return SMSModel::get(['id'=>$id]);;
    }

    public static function delete($id){
        $tags = SMSModel::get($id);
        $tags->delete();
    }
}









