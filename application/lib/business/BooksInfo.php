<?php
/**
 * Created by PhpStorm.
 * User: james
 * Date: 2018/3/6
 * Time: 下午2:29
 */

namespace app\lib\business;


use think\Db;
use think\Config;
use think\Cache;

class BooksInfo
{
    const CATCH_BOOK_TAG='CATCH_BOOK_TAG';

    public static function getTags(){
        $tags =  Cache::get(self::CATCH_BOOK_TAG);
        if($tags){
            return $tags;
        }else{
            $tags = Db::query(" select a.id,a.name,count(*) as total from article_tags a
                inner join article_info b on a.id = b.tag_id
                where parent_id = ? GROUP BY a.id,a.name
                 ",[TagsInfo::$TAG_BOOK_ID]);
            Cache::set(self::CATCH_BOOK_TAG,$tags,3600*24);
            return $tags;
        }
    }


    //添加pdf书籍
    public static function addNewEpubBook($param) {
        
        try {
            $data = [
                'title' => $param['title'],
                'book_address' => $param['book_address'],
                'img_url' => $param['img_url'],
                'publish_dated' => date('Y-m-d H:i'),
                'tag_id' => $param['tag_id'],
                // 'is_pdf' => $param['is_pdf'],
                'is_published' => 0,
                'album_id' => 2275,
            ];
            $id = Db::name('article_info')->insertGetId($data);
            $article_content = [
                'article_id' => $id,
                'content' => $param['des']
            ];
            $msg = Db::name('article_content')-> insert($article_content);
            return $msg;

        }catch(\Exception $e) {
            echo var_dump($e);
        } 
        
    }

    public static function getBooksByTagId($id,$pageIndex){
        $books =  Cache::get('BOOKS_BY_TAG_ID'.$pageIndex);
//        $IMG_HOST = Config::get('IMG_HOST');
        $IMG_DOMAIN = Config::get('IMG_DOMAIN');
        if($books){
            return $books;
        }else{
            if($id) {
                $books = Db::table('article_info')
                    ->where('tag_id', $id)
                    ->page($pageIndex, 10)
                    ->order('publish_dated', 'desc')
                    ->select();
            }
            else
            {
                $books = Db::table('article_info')
                    ->page($pageIndex, 10)
                    ->order('publish_dated', 'desc')
                    ->select();
            }
            foreach ($books as &$item){
                $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
            }

//            Cache::set('BOOKS_BY_TAG_ID'.$pageIndex,$books,3600*24);
            return $books;

        }

    }

    public static function getTagsByShop(){
        $tags =  Cache::get('TAGS');
        if($tags){
            return $tags;
        }else{
            $tags = Db::query("select * from article_tags a
                                inner JOIN
                                (
                                select tag_id, count(*) total from article_info where album_id = ".AlbumInfo::$ALBUM_SHOP_ID." GROUP BY tag_id
                                ) as b on a.id = b.tag_id
                                where a.album_id = ".AlbumInfo::$ALBUM_SHOP_ID." order by a.order_by desc");
//            Cache::set('TAGS',$tags,3600*24);
            return $tags;
        }


    }

    public static function getProfileById($id,$openId){
        $msg =  Cache::get('BOOKS_INFO'.$id);
        if($msg){
            return $msg;
        }else{
            $imgDomain = Config::get('IMG_DOMAIN');
            $resualt = Db::view('article_info', 'id as book_id,title,author,tag_id,img_url,book_address')
                ->view('article_tags', 'name', 'article_tags.id=article_info.tag_id')
                ->view('article_content', 'content', 'article_content.article_id=article_info.id')
                ->where('article_info.id',  $id)
                ->find();
           // dump($resualt);
           // return;
            $resualt['img_url'] = $imgDomain.$resualt['img_url'];
            $resualt['book_address'] = $imgDomain.$resualt['book_address'];
            $tagId = $resualt['tag_id'];

            $suggestBookLists = Db::table('article_info')->where('tag_id',$tagId)
                ->order('browse','desc')
                ->limit('3')
                ->select();
            foreach($suggestBookLists as &$item){
                $item['img_url'] = $imgDomain.$item['img_url'];

            }
            $isInShelf = Db::query("select 1 from user_book_info a
INNER join user_info b on a.user_id = b.id
where a.type = 1 and a.article_book_id  = ? and b.user_token = ?",[$id,$openId]);
            if(count($isInShelf)>0){
                $msg['isInShelf'] = 1;
            }else{
                $msg['isInShelf'] = 0;
            }
            $msg['bookLists'] = $suggestBookLists;
            $msg['bookInfo'] = $resualt;
//            Cache::set('BOOKS_INFO'.$id,$msg,3600*24);
            return $msg;
        }

    }

    public static function addBookToMyShelf ($data){
        $resault = Db::table('user_book_info')->where('article_book_id',$data['article_book_id'])->where('type','1')->where('user_id',$data['user_id'])->find();
        if($resault){
            $msg = 0;
            return $msg;
        }else{
            $msg = Db::name('user_book_info')
                 ->data($data)
                 ->insert();
            return $msg;
        }


    }
    public static function removeBookFromMyShelf($data){
        //没有删除，返回数据0；删除返回删除条数
        $resualt = Db::table('user_book_info')
            ->where('article_book_id',$data['article_book_id'])
            ->where('type',1)
            ->where('user_id',$data['user_id'])
            ->delete();
        return $resualt;
    }
    public static function getBooksOnShelfByUserId($user_id,$pageIndex){
            $IMG_DOMAIN = Config::get('IMG_DOMAIN');
            $result = [];

            $param = [$user_id];
            $count = Db::query('select COUNT(*) as total from user_book_info a where a.type = 1 and a.user_id = ? ',$param);
            $rows = 50;
            $limit = ($pageIndex-1)*$rows;

            $result['list']  = Db::query('select b.title,b.id,b.book_address as address,b.img_url from user_book_info a
            INNER JOIN article_info b on a.article_book_id = b.id
            WHERE a.type = 1 and a.user_id = ? order by a.id limit '.$limit.','.($rows),$param);

            $result['host'] = $IMG_DOMAIN;
            $result['total'] = $count[0]['total'];
            return $result;
    }
    public static function getSearchResult($name,$pageIndex){
        $IMG_DOMAIN = Config::get('IMG_DOMAIN');
        $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
        $position = ($pageIndex-1)*10;
        $lists = Db::query("select a.*,b.content from article_info a
                                inner join article_content b on a.id = b.article_id
                                where a.title like ? and a.album_id = 2275 limit ?,10",['%'.$name.'%',$position]);
        foreach($lists as &$item){
            if($item['img_url']){
                $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
            }else{
                $radNum = rand(1,10);
                $item['img_url']=$Img_URL_DEFAULT.$radNum.'.jpg';
            }

        }
        $len = count($lists);
        if($len == 0){
            return $len;
        }else{
            return $lists;
        }

    }
    public static function suggestBookLists(){
        $lists = Cache::get('BOOK_LISTS');
        if($lists){
            return $lists;
        }else{
            $IMG_DOMAIN = Config::get('IMG_DOMAIN');
            $lists = Db::table('article_complex')->where('catetory_id',2)->select();
            foreach($lists as &$item){
                $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
            }
//            Cache::set('BOOK_LISTS',$lists,3600*24);
            return $lists;
        }

    }
    public static function bookListsInfo($id){
        $IMG_DOMAIN = Config::get('IMG_DOMAIN');
        Db::execute("update article_complex set browse = browse + 1 WHERE id = ? ",[$id]);
        $lists = Db::view('article_complex_content','article_id')
            ->view('article_info','title,img_url','article_info.id=article_complex_content.article_id')
            ->view('article_content','content','article_content.article_id=article_info.id')
            ->where('article_complex_id',$id)
            ->select();
        foreach($lists as &$item){
            $item['img_url'] = $IMG_DOMAIN.$item['img_url'];
        }
        return $lists;
    }

}
