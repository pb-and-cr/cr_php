<?php
namespace app\lib\business;

use app\lib\model\ChristianReaderUserModel;
use think\Config;
use think\Db;
use app\lib\common\Json;
use app\lib\common\Email;
use app\lib\common\StringTool;


class ChristianReaderUserInfo{


    //新用户注册，基督徒阅读2020-05-14，云端神学院的最新注册方式
    public static function newUserRegist($param) {
        // $msg = Db::excute("");
        $data = [
            'name' => $param['name'],
            'nickName' => $param['name'],
            'password' => StringTool::MD5Code($param['password']),
            'user_token' => ChristianReaderUserInfo::setToken($param['name'].$param['password']),
            'created_time' => date('Y-m-d H:i'),
            // 'role_id' => $param['role_id']
            'role_id' => 1
        ];
        $re = Db::name('user_info')->insert($data);
        return $re;
    }

	//注册新用户
    public static function createUserByEmail($data,$control){
        $newUser = new ChristianReaderUserModel();
		$data['user_token'] = ChristianReaderUserInfo::setToken($data['email']);
        $data['password'] = StringTool::MD5Code($data['password']);
        $data['created_time'] = date('Y-m-d H:i');
        $email = $data['email'];
        $time = date('Y-m-d H:i');
        $control->output([
            'name'  => $data['nickName'],
            'time' => $time,
        ]);
        $info = "注册成功，请登录";
        $newUser ->data($data)->save();
        return Json::ajaxData('',$info,1);
	}

	public static function createUserByPhone($data){
        $newUser = new ChristianReaderUserModel();
        $data['user_token'] = ChristianReaderUserInfo::setToken($data['mobile']);
        $data['password'] = StringTool::MD5Code($data['password']);
        $data['created_time'] = date('Y-m-d H:i');
        unset($data['verifyCode']);
        $newUser ->data($data)->save();
    }
	//生成token值
    public static function setToken($token){
        $str = md5(uniqid(md5($token)));//生成一个不会重复的字符串
        $str = sha1($str);//加密
        return $str;
    }
	//用户密码错误的次数
	public static function updatePassword_Attempts($userName){
        $attempts = new ChristianReaderUserModel();
		//$info = $attempts->where('name',$name)->find();
        $info = $attempts->where('mobile|email','=',$userName)
                         ->find();//获取用户所有相关信息
        if($info){
			$count_attempts = $info->getEntity();
		    $attempts::execute("update user_info set password_attempts=password_attempts+1 where id=?",[$count_attempts['id']]);
		    if($count_attempts['password_attempts']>=5){
	    		Db::name('user_info')->where('id',$count_attempts['id'])
                                                ->data(['error_login_time' => date('Y-m-d H:i:s')])
                                                ->update();
		    }
		}
	}
    //登陆之前判断用户是否有error_login_time
    public static function isErrorLogin($data){
        // $userLogin = new ChristianReaderUserModel();
        // $error_login_info = $userLogin->where('name','=',$data['userName'])->find();
        // if($error_login_info!=null){
        //     return ChristianReaderUserInfo::finalLogin($data);
        // }
        // else{
        //     dump('1');
        //     $info = "用户名或密码错误";
        //     return Json::ajaxData('',$info,0);
        // }
        return ChristianReaderUserInfo::finalLogin($data);
    }
    //各种验证后，用户的最终登陆
    public static function finalLogin($data){

        $userInfo = Db::table('user_info')
            ->whereOr('nickName','=',$data['userName'])
            ->whereOr('email','=',$data['userName'])
            ->whereOr('mobile','=',$data['userName'])
            ->where('password',$data['password'])
            ->find();

        // $message = Db::query("select * from user_info where name = ? ",[$data['userName']]);

        // $userLogin = new ChristianReaderUserModel();
        // $userInfo = $userLogin->where('name','=',$data['userName'])
        //         ->where('password',$data['password'])
        //         ->find();//获取用户所有相关信息

        // if(strpos($data['userName'],'@') !== false){
        //     $userInfo = $userLogin->where('name','=',$data['userName'])
        //         ->where('password',$data['password'])
        //         ->find();//获取用户所有相关信息

        //         dump($userInfo);
        // }
        // else{
        //     $userInfo = $userLogin->where('mobile','=',$data['userName'])
        //         ->where('password',$data['password'])
        //         ->find();//获取用户所有相关信息
        // }


        if($userInfo){
            // $userInfoData = $userInfo->getEntity();
            // if(StringTool::isNull($userInfoData['avatar'])==false){
            //     $userInfoData['avatar'] = Config::get('IMG_DOMAIN').$userInfoData['avatar'];
            // }
            $info = "恭喜,登陆成功";
            $userInfoData = self::transformUserInfo($userInfo);
            return Json::ajaxData($userInfoData,$info,1);
        }
        else{
            $info = "用户名或密码错误";
            return Json::ajaxData('',$info,0);
        }
    }


    //清空密码尝试次数以及error_login_time
    public static function reSetPswAttempts($userId){
        $userInfo = new ChristianReaderUserModel();
        $userInfo::execute("update user_info set error_login_time=null,password_attempts = 0 where id=?",[$userId]);
    }
	//用户更新资料
    public static function updateProfile($data){
        $userInfo = new ChristianReaderUserModel();
            $userAllData = $userInfo->where('user_token',$data['user_token'])->find();
            if(empty($userAllData)){
            	$info = '没有相关账号,修改无效';
            	return Json::ajaxData('',$info,0);
            }else{
                $userData = $userAllData->getEntity();
                $user_token = $userData['user_token'];
                $data['update_time'] = date('Y-m-d H:i');
                unset($data['time']);
                Db::name('user_info')->where('user_token', $user_token)->data($data)->update();
                $info = "恭喜您，修改成功";
        		return Json::ajaxData('',$info,1);
            }
    }
    //用户通过邮箱找回密码
    public static function getPassword($data,$control){
        $data['email'] = trim($data['email']);
        $userInfo = new ChristianReaderUserModel();
        //return Json::ajaxData($data['email'],'---111---',0);
        $userAllData = $userInfo->where('email',$data['email'])->find();
        if(empty($userAllData)){
            $info = '该邮箱尚未注册';
            return Json::ajaxData('',$info,0);
        }else{
            $userData = $userAllData->getEntity();
            $email = $userData['email'];
            //$user_token = $userData['user_token'];
            //$getPassTime = time();
            $time = date('Y-m-d H:i:s');
            $control->output([
                'name'  => $data['email'],
                'time' => $time,
                'token' => $userData['user_token'],
            ]);
            $content = $control->readTemplate(APP_PATH.request()->module().'/view/getPsw.html');
            $mail = new Email();
            $result = $mail->sendEmail($email,$content);
            if($result){
                $info = '系统已向您的邮箱发送一封重置密码的邮件，请查收';
                return Json::ajaxData('',$info,1);
            }else{
                $info = '重置密码邮件发送失败';
                return Json::ajaxData($result,$info,0);
            }

        }
    }
    //生成随机四位验证码
    public static function getRandStr($len){
        $chars = array(
            "0", "1", "2","3", "4", "5", "6", "7", "8", "9"
        );
        $charsLen = count($chars) - 1;
        shuffle($chars);//把数组中的元素按随机顺序重新排序
        $output = "";
        for ($i=0; $i<$len; $i++)
        {
            $output .= $chars[mt_rand(0, $charsLen)];//生成随机字符串
        }
        return $output;
    }
    //用户通过手机端设置新密码
    public static function setNewPasswordByPhone($data){
        $userInfo = new ChristianReaderUserModel();
        $userAllData = $userInfo->where('mobile',$data['mobile'])->find();
        if(empty($userAllData)){
            $info = '该手机尚未注册';
            return Json::ajaxData('',$info,0);
        }else{
            $userData = $userAllData->getEntity();
            $data['user_token'] = $userData['user_token'];
            unset($data['mobile']);
            return ChristianReaderUserInfo::updateProfile($data);
        }

    }

    public static function transformUserInfo($user){

        $entity = [];
        if($user){
            if($user['brief_personal_des']){
                $entity['profile'] = $user['brief_personal_des'];
            }
            else {
                $entity['profile'] = "这个家伙很不赖,什么也没有留下!";
            }
            $entity['openId'] = $user['user_token'];
            if ($user['head_icon']) {
                $entity['head_icon'] = Config::get('IMG_DOMAIN') . $user['head_icon'];
            } else {
                $entity['head_icon'] = Config::get('Img_URL_DEFAULT') . '2' . '.jpg';
            }
            $entity['id'] = $user['id'];
            $entity['nickName'] = $user['nickName'];
        }
        return $entity;
    }

    //获取用户信息
    public static function UserInfoData ($user_token){
        $imgDomain = Config::get('IMG_DOMAIN');
        $Img_URL_DEFAULT = Config::get('Img_URL_DEFAULT');
        $msg = Db::query("select * from user_info where user_token = ? and `password` <> ''",[$user_token]);
        if (count($msg)>0) {
            $msg = $msg[0];
            if ($msg['head_icon']) {
                $msg['head_icon'] = $imgDomain . $msg['head_icon'];
            } else {
                $msg['head_icon'] = $Img_URL_DEFAULT . '2' . '.jpg';
            }
            if($msg['brief_personal_des']){
                $msg['profile'] = $msg['brief_personal_des'];
            }
            else {
                $msg['profile'] = "主的好孩子, 什么也没有留下";
            }
            $msg['openId'] = $msg['user_token'];
            $followCounts = Db::table('user_ablum_info')->where('user_id', '=', $msg['id'])->count('user_id');

            $msg['followCounts'] = $followCounts;
            $loveCounts = Db::table('user_book_info')
                ->where('user_id', '=', $msg['id'])
                ->where('type', '=', 2)
                ->count('user_id');

            $bookCounts = Db::table('user_book_info')->where('user_id', '=', $msg['id'])->where('type','=',1)->count('user_id');
            $msg['bookCounts'] = $bookCounts;
            $msg['loveCounts'] = $loveCounts;
            unset($msg['user_token']);
            unset($msg['password']);
            // dump($msg);
            return Json::ajaxData($msg, '', 1);
        } else {
            $info = '没有查询到该用户';
            $msg = [];
            $msg['head_icon'] = $Img_URL_DEFAULT . '2' . '.jpg';
            $msg['id'] = -1;
            $msg['nickName'] = '主的好孩子, 没有任何信息';
            $msg['profile'] = "点击这里登录或注册";
            $msg['openId'] = '';
            $msg['bookCounts'] = 0;
            $msg['loveCounts'] = 0;
            $msg['followCounts'] = 0;
            return Json::ajaxData($msg, $info, 0);
        }

    }


}
