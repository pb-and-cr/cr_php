<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/12/1
 * Time: 上午10:02
 */

namespace app\lib\lang;


class Chinese
{
    const Language = array(
        'personal_browse'=>'次观看',
        'thousand_Display'=>'万次观看',
        'time_format'=>'H:i',
        'date_format'=>'m月d日',
        'follow_count_msg'=>'位订阅者',
    );
}
