<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/12/1
 * Time: 上午10:02
 */

namespace app\lib\lang;


class Arabic
{
    const Language = array(
        'personal_browse'=>'مشاهدته',
        'thousand_Display'=>'مليون مرة يراقب',
        'time_format'=>'H:i',
        'date_format'=>'d-m',
        'follow_count_msg'=>'اشتراك',
    );
}
