<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/12/1
 * Time: 上午10:02
 */

namespace app\lib\model;


class ApplyModel
{
    private $color;
    private $size;
    private $weight;

    public function __get($property_name)
    {
        if(isset($this->$property_name))
        {
            return ($this->$property_name);
        }
        else
        {
            return (NULL);
        }
    }

    public function __set($property_name,$value)
    {
        $this->$property_name=$value;
    }

}