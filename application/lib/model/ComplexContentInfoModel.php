<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/15
 * Time: 下午5:23
 */

namespace app\lib\model;

use think\Db;
use think\Model;


class ComplexContentInfoModel extends Model {
    protected $table = 'article_complex_content';
}
