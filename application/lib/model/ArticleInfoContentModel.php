<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/11/15
 * Time: 下午3:53
 */

namespace app\lib\model;

use think\Db;
use think\Model;


class ArticleInfoContentModel extends Model {

    protected $table = 'article_content';

    /**
     * 反转义HTML实体标签
     * @param $value
     * @return string
     */
    protected function setContentAttr($value){
        return htmlspecialchars_decode($value);
    }

    /**
     * 获取层级缩进列表数据
     * @return array
     */
    public function getLevelList() {
        $category_level = $this->order(['sort' => 'DESC', 'id' => 'ASC'])->select();
        return array2level($category_level);
    }

}