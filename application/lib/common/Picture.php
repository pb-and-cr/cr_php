<?php

namespace app\lib\common;

class Picture
{

    //图片裁剪
    public static function resizeImage($im,$maxwidth,$maxheight,$name,$filetype,$imageset)
    {
        if(stripos($im,'.png')!=false){
            $im = imagecreatefrompng($im);
        }else if(stripos($im,'.jpg')!=false){
            $im = ImageCreateFromJpeg($im);
        }else if(stripos($im,'.jpeg')!=false){
            $im = ImageCreateFromJpeg($im);
        }else if(stripos($im,'.gif')!=false){
            $im = imagecreatefromgif($im);
        }

        $pic_width = imagesx($im);
        $pic_height = imagesy($im);
        $resizewidth_tag = false;
        $resizeheight_tag = false;
        if(($maxwidth && $pic_width > $maxwidth) || ($maxheight && $pic_height > $maxheight))
        {
            if($maxwidth && $pic_width>$maxwidth)
            {
                $widthratio = $maxwidth/$pic_width;
                $resizewidth_tag = true;
            }

            if($maxheight && $pic_height>$maxheight)
            {
                $heightratio = $maxheight/$pic_height;
                $resizeheight_tag = true;
            }

            if($resizewidth_tag && $resizeheight_tag)
            {
                if($widthratio<$heightratio)
                    $ratio = $widthratio;
                else
                    $ratio = $heightratio;
            }

            if($resizewidth_tag && !$resizeheight_tag)
                $ratio = $widthratio;
            if($resizeheight_tag && !$resizewidth_tag)
                $ratio = $heightratio;

            $newwidth = $pic_width * $ratio;
            $newheight = $pic_height * $ratio;

            if(function_exists("imagecopyresampled"))
            {
                $newim = imagecreatetruecolor($newwidth,$newheight);
                imagecopyresampled($newim,$im,0,0,0,0,$newwidth,$newheight,$pic_width,$pic_height);
            }
            else
            {
                $newim = imagecreate($newwidth,$newheight);
                imagecopyresized($newim,$im,0,0,0,0,$newwidth,$newheight,$pic_width,$pic_height);
            }

            $name = $name.$filetype;
            imagejpeg($newim,$name);
            imagedestroy($newim);
        }
        else
        {
            $name = $name.$filetype;
            imagejpeg($im,$name);
        }
    }

    /**
     * 抓取远程图片
     *
     * @param string $url 远程图片路径
     * @param string $filename 本地存储文件名
     */
    public static function grabImage($url, $filename = '') {
        if($url == '') {
            return false; //如果 $url 为空则返回 false;
        }
        $ext_name = strrchr($filename, '.'); //获取图片的扩展名
        if($filename == '') {
            $filename = time().$ext_name; //以时间戳另起名
        }
        //开始捕获
        ob_start();
        readfile($url);
        $img_data = ob_get_contents();
        ob_end_clean();
        $size = strlen($img_data);
        $local_file = fopen($filename , 'a');
        fwrite($local_file, $img_data);
        fclose($local_file);
        return $filename;
    }

    /**
     * 抓取远程图片
     *
     * @param string $url 远程图片路径
     * @param string $filename 本地存储文件名
     */
    public static function grabImageTemp($url, $filename = '',$dir) {
        if($url == '') {
            return false; //如果 $url 为空则返回 false;
        }

        if (!file_exists($dir)) {
            mkdir($dir);
        }

//    $ext_name = strrchr($filename, '.'); //获取图片的扩展名
//    if($filename == '') {
//        $filename = time().$ext_name; //以时间戳另起名
//    }
        //开始捕获
        ob_start();
        readfile($url);
        $img_data = ob_get_contents();
        ob_end_clean();
        $size = strlen($img_data);
        $local_file = fopen($filename , 'a');
        fwrite($local_file, $img_data);
        fclose($local_file);
        return $filename;
    }

}

?>