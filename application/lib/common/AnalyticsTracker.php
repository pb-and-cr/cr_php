<?php
namespace app\lib\common;

use app\lib\model\TagsModel;
use think\Db;
use think\Cache;
use app\lib\common\StringTool;
use app\lib\business\LanguageInfo;
use app\lib\model\languageISOModel;


class AnalyticsTracker{

    const ROOTURL = "http://xinxiwang.me/visitor";
    const GMO_API_KEY = "F4oFIT7gEA3h8RgiDArnGFosqFc4oIXW8SpIMJ5XQwcZFMdjOFdln43nDlvtNHR";

    public static function getVisitorId(){
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, self::ROOTURL);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 1);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //执行命令
        $data = curl_exec($curl);
        //关闭URL请求
        curl_close($curl);
        //显示获得的数据
        $beginIndex = stripos($data,'"id":"')+6;
        $endIndex = stripos($data,'"}');
        $data = substr($data,$beginIndex,$endIndex-$beginIndex);
        return $data;
    }

    public static function postGospel(){

        $post_data = array (
            "key" => self::GMO_API_KEY
            ,"appName" => "Christian Reader"
            ,"visitorId" => self::getVisitorId()
            ,"eventType" => "gospel"
            ,"language" => "zh-cn"
            ,"deviceType" => "web"
            ,"timestamp" => time()
        );

        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, self::ROOTURL); // 要访问的地址
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $res = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        echo $res;
    }
}