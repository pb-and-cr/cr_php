<?php
/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 2017/12/11
 * Time: 下午3:54
 */

namespace app\lib\common;

use Exception;
use think\Config;
use think\exception\Handle;
use think\exception\HttpException;

class AppException extends Handle
{
    public function render(Exception $e)
    {
        if(Config::get('app_debug')) {
            // 参数验证错误
            if ($e instanceof ValidateException) {
                return json($e->getError(), 422);
            }

            // 请求异常
            if ($e instanceof HttpException && request()->isAjax()) {
                return response($e->getMessage(), $e->getStatusCode());
            }

            //TODO::开发者对异常的操作
            //可以在此交由系统处理
            return parent::render($e);
        }
        else
        {
            header("Location:/admin/index/findnot");
            exit;
        }
    }

}