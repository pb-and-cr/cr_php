<?php

namespace app\lib\common;

class StringTool
{
    public static function isNull($fields){
        if(is_int($fields)){
            return false;
        }

        if (!is_string($fields)) {
            return true;
        } //判断是否是字符串类型

        if (empty($fields)){
            return true;
        } //判断是否已定义字符串

        if ($fields=='') {
            return true;
        } //判断字符串是否为空

        return false;
    }

    public static function MD5Code($str){
        return md5($str.'987&^%dfd');
    }
    
    public static function hasValue($fields){

        if (is_string($fields)) {
            if ($fields=='') {
                return false;
            }
            if(strlen($fields)>0){
                return true;
            }
        } //判断是否是字符串类型
        else
        {
            return false;
        }
    }

}

?>