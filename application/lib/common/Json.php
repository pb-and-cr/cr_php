<?php

namespace app\lib\common;

class Json
{


    public static function ajaxData($data,$info='',$status=0) {
        $result  =  array();
        $result['status']  =  $status;
        $result['info'] =  $info;
        $result['data'] = $data;
        return $result;
    }


}

?>